/*!
 * \file ISocketInterface.h
 *
 * \author Administrator
 * \date 一月 2017
 *
 * 数据发送接收接口
 */
#pragma once
#include "live_event.h"
#include "boost/shared_ptr.hpp"
#include "boost/function.hpp"
#include "RUdpProtocol.pb.h"
using namespace ProtoNameSpace;

class ISocketInterface
{
public:
    virtual ~ISocketInterface(){}
    //@param player_type_:玩家类型， 0：数据下载玩家， 1:数据上传玩家
    virtual int     login(const char* ip_, const short port_, int player_type = 0) = 0;
    virtual int     logout() = 0;
    virtual bool    is_login() = 0;
    
    virtual int     send_video(NetPacket* pkt_) = 0;
    virtual int     send_audio(NetPacket* pkt_) = 0;

    //事件回调接口
    virtual void    set_event(ILiveEvent* event_) = 0;

    //设置音视频接收回调
    virtual void    set_video_cb(boost::function<void(boost::shared_ptr<NetPacket>)>) = 0;
    virtual void    set_audio_cb(boost::function<void(boost::shared_ptr<NetPacket>)>) = 0;

protected:
    long long       _session_flag;  //每次直播不一样的标识,用于记录在直播的前一部分媒体包中
};

extern ISocketInterface*    GetUdpSockInterface(ILiveEvent* event_, long long uid_, long long room_id_);
extern ISocketInterface*    GetHttpInterface(ILiveEvent* event_, long long uid_, long long room_id_);