#pragma once
#include "base_singleton.h"
#include "message_thread.h"
#include "boost/atomic.hpp"

//using namespace MEDIA_MESSAGE;

class /*__declspec(dllexport) */CGlobalConfig
{
public:
    CGlobalConfig(void);
    virtual ~CGlobalConfig(void);

#ifndef _MEDIA_PLAY_CLIENT
    //获取所有的视频采集设备，返回设备个数
    virtual int     enum_video_dev(__out char dev_[][256], __in const int& max_count_);
#endif // _MEDIA_PLAY_CLIENT
    //设置主播静态画面
    virtual void    set_anchor_img(const char* img_str_);

private:
#define             MAX_VIDEO_DEV_COUNT     10

public:
    boost::atomic_int32_t               _preview_pic_width;
    boost::atomic_int32_t               _preview_pic_height;
    boost::atomic_int32_t               _codec_pic_width;
    boost::atomic_int32_t               _codec_pic_height;
    boost::atomic_int32_t               _frame_rate;
    boost::atomic_int32_t               _video_bitrate;
    boost::atomic_int32_t               _audio_bitrate;
    boost::atomic_int32_t               _key_frame_interval;        //关键帧间隔（秒）
    boost::atomic_int32_t               _cur_video_show_num;        //标志当前使用哪个视频框中的视频
    boost::atomic_uint32_t              _local_uid;
    char                                _video_dev_list[MAX_VIDEO_DEV_COUNT][256];
    boost::atomic_int32_t               _video_dev_count;
    char                                _anchor_img[256];
    boost::atomic_bool                  _send_def_img_flag;         //是否发送主播自定义图片标志
    CMessageThread                      _msg_thr;
    boost::atomic_bool                  _benable_accompany;         //是否启用伴奏
    boost::atomic_bool                  _benable_mic;               //是否启用本地麦克风
    boost::atomic<float >               _play_volume_scale;         //播放音量级别，默认1.0,0.0为静音
    boost::atomic_bool                  _brecord_handle_init_flag;  //CRecordInterface是否初始化标识
};

#define CREATE_GLOBAL_CONFIG		CSingleton<CGlobalConfig>::instance
#define GLOBAL_CONFIG				CSingleton<CGlobalConfig>::instance
#define DESTROY_GLOBAL_CONFIG		CSingleton<CGlobalConfig>::destroy
