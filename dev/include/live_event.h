#pragma once

class ILiveEvent
{
public:
    virtual void on_login_resualt_cb(const int& ret) {};
    virtual void on_logout_resualt_cb(const int& ret){};
    virtual void on_offline_cb(){};

    virtual void on_video_preview_cb(const int video_show_num, const unsigned char* rgb24_data_,
        const int pic_width_, const int pic_height_) {};
    virtual void on_video_play_cb(const unsigned char* rgb24_data_,
        const int pic_width_, const int pic_height_) {};
    virtual void on_host_onmic_cb(const unsigned int& uid_) {};
    virtual void on_host_offmic_cb(const unsigned int& uid_) {};

    virtual void on_video_dev_lost_cb(const char* dev_name_, const int len_) {};
    virtual void on_audio_dev_lost_cb(const char* dev_name_, const int len_) {};
    virtual void on_default_audio_dev_change_cb(){};

    //播放消息回调
    virtual void on_buffering_cb() {};         //缓冲中
    virtual void on_begin_playing_cb() {};     //缓冲结束开始播放
};
