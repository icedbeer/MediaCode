/*
The MIT License (MIT)

Copyright (c) 2013-2014 winlin

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <WinSock2.h>
#include <srs_lib_simple_socket.hpp>

#include <srs_kernel_error.hpp>

#ifndef ST_UTIME_NO_TIMEOUT
    #define ST_UTIME_NO_TIMEOUT -1
#endif

//add by myself at 2015-6-3
#ifndef ETIME
#define ETIME           137
#endif
#ifndef ECONNRESET
#define ECONNRESET           137
#endif

SimpleSocketStream::SimpleSocketStream()
{
    fd = -1;
    send_timeout = recv_timeout = ST_UTIME_NO_TIMEOUT;
    recv_bytes = send_bytes = 0;
}

SimpleSocketStream::~SimpleSocketStream()
{
    if (fd != -1) {
        ::closesocket(fd);
        fd = -1;
    }
}

int SimpleSocketStream::create_socket()
{
    if((fd = ::socket(AF_INET, SOCK_STREAM, 0)) < 0){
        return -1;
    }
    return ERROR_SUCCESS;
}

int SimpleSocketStream::close_socket()
{
	if (0 < fd)
	{
		::closesocket(fd);
		fd = -1;
	}
	return 0;
}

int SimpleSocketStream::connect(const char* server_ip, int port)
{
    sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(server_ip);
    
    if(::connect(fd, (const struct sockaddr*)&addr, sizeof(sockaddr_in)) < 0){
        return -1;
    }

    return ERROR_SUCCESS;
}

int SimpleSocketStream::listen(const char* listen_ip, int port, int backlog)
{
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(listen_ip);

	int val = 100; 
	if(setsockopt(fd, SOL_SOCKET,SO_REUSEADDR,(char *)&val,sizeof(val)) == SOCKET_ERROR) 
	{
		return -1;
	}

	if (::bind(fd, (const struct sockaddr*)&addr, sizeof(sockaddr_in)) == SOCKET_ERROR) 
	{
		int err = errno;
		return -1;
	}
	if (::listen(fd, backlog) == -1) {
		return -1;
	}
	return 0;
}

int SimpleSocketStream::accept(SimpleSocketStream& new_stream)
{
	struct sockaddr_storage ss;
	memset(&ss, 0, sizeof (ss));
	int ss_len = sizeof (ss);
	int new_fd = ::accept(fd, (struct sockaddr *) &ss, &ss_len);
	if (new_fd == -1) {
		return -1;
	}
	assert(ss_len > 0);
	new_stream.fd = new_fd;
	return 0;
}

// ISrsBufferReader
int SimpleSocketStream::read(void* buf, size_t size, ssize_t* nread)
{
    int ret = ERROR_SUCCESS;
    
    *nread = ::recv(fd, (char*)buf, size, 0);
    
    // On success a non-negative integer indicating the number of bytes actually read is returned 
    // (a value of 0 means the network connection is closed or end of file is reached).
    if (*nread <= 0) {
        if (errno == ETIME) {
            return ERROR_SOCKET_TIMEOUT;
        }
        
        if (*nread == 0) {
            errno = ECONNRESET;
        }
        
        return ERROR_SOCKET_READ;
    }
    
    recv_bytes += *nread;
    
    return ret;
}

// ISrsProtocolReader
void SimpleSocketStream::set_recv_timeout(int64_t timeout_us)
{
    recv_timeout = timeout_us;
}

int64_t SimpleSocketStream::get_recv_timeout()
{
    return recv_timeout;
}

int64_t SimpleSocketStream::get_recv_bytes()
{
    return recv_bytes;
}

int SimpleSocketStream::get_recv_kbps()
{    
    return 0;
}

// ISrsProtocolWriter
void SimpleSocketStream::set_send_timeout(int64_t timeout_us)
{
    send_timeout = timeout_us;
}

int64_t SimpleSocketStream::get_send_timeout()
{
    return send_timeout;
}

int64_t SimpleSocketStream::get_send_bytes()
{
    return send_bytes;
}

int SimpleSocketStream::get_send_kbps()
{    
    return 0;
}

int SimpleSocketStream::writev(const iovec *iov, int iov_size, ssize_t* nwrite)
{
	int ret = ERROR_SUCCESS;
	for (int i = 0; i < iov_size; ++i)
	{
		ssize_t n;
		ret = this->write(iov[i].iov_base, iov[i].iov_len, &n);
		if (ret != ERROR_SUCCESS)
		{
			return ret;
		}
		*nwrite += n;
		send_bytes += n;
		
	}
	return ERROR_SUCCESS;
}

// ISrsProtocolReaderWriter
bool SimpleSocketStream::is_never_timeout(int64_t timeout_us)
{
    return timeout_us == (int64_t)ST_UTIME_NO_TIMEOUT;
}

int SimpleSocketStream::read_fully(const void* buf, size_t size, ssize_t* nread)
{
    int ret = ERROR_SUCCESS;
    
    size_t left = size;
    *nread = 0;
    
    while (left > 0) {
        char* this_buf = (char*)buf + *nread;
        ssize_t this_nread;
        
        if ((ret = this->read(this_buf, left, &this_nread)) != ERROR_SUCCESS) {
            return ret;
        }
        
        *nread += this_nread;
        left -= this_nread;
    }
    
    recv_bytes += *nread;
    
    return ret;
}

int SimpleSocketStream::write(const void* buf, size_t size, ssize_t* nwrite)
{
    int ret = ERROR_SUCCESS;
    
    *nwrite = ::send(fd, (char*)buf, size, 0);
    
    if (*nwrite <= 0) {
        if (errno == ETIME) {
            return ERROR_SOCKET_TIMEOUT;
        }
        
        return ERROR_SOCKET_WRITE;
    }
    
    send_bytes += *nwrite;
    
    return ret;
}

