#include "room_mgr.h"
#include "WaiJeiGameManager.h"
#include "boost_log.h"
#include "libevent_header.h"

#ifdef _DEBUG
#pragma comment(lib, "MemoryShareD.lib")
#else
#pragma comment(lib, "MemoryShare.lib")
#endif // _DEBUG

extern char   g_workingDir[512];

WaiJeiGameManager WaiJeiGameManager::s_WaiJeiInstance;

WaiJeiGameManager::WaiJeiGameManager()
:_pMemoryShare(0)
,_NotRecvGameHeartCount(0)
,_fHeartTestTime(0.0f)
,_MSNo(0)
,_GameId(0) 
,_room_mgr(0)
,_last_heart_tv(0)
{
    
}

WaiJeiGameManager::~WaiJeiGameManager()
{
	if(_pMemoryShare)
	{
		delete _pMemoryShare;
		_pMemoryShare=0;
	}

	std::list<DelayDeleteMemoryShare>::iterator it=_DelayDeleteMemoryShareList.begin();
	while(it!=_DelayDeleteMemoryShareList.end())
	{
		delete it->_pMemoryShare;
		it++;
	}
	_DelayDeleteMemoryShareList.clear();
    CloseMemoryShare();
}

void WaiJeiGameManager::Set_Room_Mgr(CRoomMgr* mgr_)
{
    _room_mgr = mgr_;
}

int WaiJeiGameManager::Read()
{
    if(!_pMemoryShare)
    {
        return 0;
    }

    return _pMemoryShare->Read();
}

void WaiJeiGameManager::Heart()
{ 
    if ((::GetTickCount() - _last_heart_tv) >= 2000 || ::GetTickCount() < _last_heart_tv)
    {
        WJS_InByteStream ins;
        SendData_VideoServer2HttpServer::Send_HttpServerHeart(ins);
        if(_pMemoryShare)
            _pMemoryShare->SendMsg(ins,ins.size());
        _last_heart_tv = ::GetTickCount();
    }
}

void WaiJeiGameManager::OnRecvMemoryShareMsg(void* data, size_t len)
{
	if(len==0)
		return;
	WJSByteSeq tt_seq;
	tt_seq.resize(len,true);
	memcpy(&(tt_seq[0]),data,len);

	try
	{
		if(_PC2CocosRecv.Parser(tt_seq,this,0))
		{
			return;
		}
	}
	catch (...)
	{
		return;		
	}
}

void WaiJeiGameManager::IReceiver_HttpServerWillClose(void* pExData)
{
    BOOST_WARNING << "IReceiver_HttpServerWillClose.";
    timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000;
    event_base_loopexit(_room_mgr->get_event_base(), &tv);
}

void WaiJeiGameManager::IReceiver_NotifyAnchorStatusToHttpServer(int result,unsigned int roomFlag,
                                                                 const AnchorInfoObjectList& AnchorInfoList,void* pExData)
{
    BOOST_DEBUG << "recv anchor status change event. room:" << roomFlag << 
        "; anchor size=" << AnchorInfoList.size() << "; resualt:" << result;
    if (0 != result)
    {
        return ;
    }
    if (NULL == _room_mgr)
    {
        return;
    }
    if (AnchorInfoList.size() == 0)
    {
        _room_mgr->set_room_host(roomFlag, 0, false);
        return;
    }
    for (int i(0); i < AnchorInfoList.size(); i++)
    {
        if (3 == AnchorInfoList.at(i).status) //此ID正在直播
        {
            _room_mgr->set_room_host(roomFlag, AnchorInfoList.at(i).anchorId, true);
            return ;
        }
    }
    //走到这里说明没有玩家在直播，则设置此房间无直播
    _room_mgr->set_room_host(roomFlag, 0, false);
}

void WaiJeiGameManager::WriteMemoryShareLog(const string& strLog)
{
}

bool WaiJeiGameManager::CreateShareMemory(const string& share_memory_name_)
{
    if (_pMemoryShare)
    {
        if(0 == _pMemoryShare->GetName().compare(share_memory_name_))
        {
            return true;
        }
        delete _pMemoryShare;
    }
    _pMemoryShare = new(std::nothrow) WJS_Memory_Share_Game(share_memory_name_, false, this);
    if (_pMemoryShare)
    {
        BOOST_DEBUG << "CreateShareMemory : " << share_memory_name_ << " success.";
        return true;
    }else
    {
        BOOST_DEBUG << "CreateShareMemory : " << share_memory_name_ << " failed.";
        return false;
    }
}

void  WaiJeiGameManager::CloseMemoryShare()
{
    if (_pMemoryShare)
    {
        delete _pMemoryShare;
        _pMemoryShare = NULL;
    }
}

