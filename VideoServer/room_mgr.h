#pragma once
#include <map>
#include <set>
#include "base_room.h"
#include "IShareMedia.h"

using namespace std;

class CRoomMgr : public IShareMedia
{
public:
    CRoomMgr(event_base* eb_);
    virtual ~CRoomMgr(void);

    //数据入口
    virtual int             process_msg(evbuffer *&ev_buf_, evhttp_request *&req_);
    virtual int             process_get_msg(evhttp_request *&req_);

    //定时清理僵尸链接
    virtual void            detect_dead_node();

    virtual void            quit_event();

    //设置某个房间主播上麦或者下麦
    //@param:bOnMic_ true:上麦;false:下麦.

    virtual void            set_room_host(const ROOM_ID& room_id_, const USER_ID& uid_, bool bOnMic_);

    //设置socket keepalive属性
    virtual void            set_sock_opt(const evutil_socket_t& sock_);
    //删除socket
    virtual void            del_sock(const evutil_socket_t& sock_);

    virtual event_base*     get_event_base(){return _event_base;}

    virtual void            add_shared_media_data(unsigned long long& room_id_, 
        unsigned char* data_, const int& len_);
    virtual void            add_shared_room(unsigned long long room_id_);
    virtual void            del_shared_room(unsigned long long room_id_);
    virtual void            reinit_room(unsigned long long room_id_);

protected:
    //添加node到对应的room中，如果不存在此room，则新建对应的room id
    virtual bool            add_node(const USER_ID &uid_, const ROOM_ID &room_id_,
        const evhttp_request *&req_, const uint8_t& user_type_);
    //从node对应的room中删除此node，如果room没有了任何node，则删除此room
    virtual bool            del_node(const USER_ID &user_id_, const ROOM_ID &room_id_, 
        const uint8_t& user_type_);

    //添加房间
    //@return 0:success; -1:failed; 1:already exist.
    virtual bool            add_room(const ROOM_ID &room_id_);
    virtual bool            del_room(const ROOM_ID &room_id_);
    //保存节点的数据请求
    virtual int             buffer_node_req(evhttp_request *&req_, ROOM_ID &room_id_, USER_ID &uid_);

    CBaseRoom*&             get_room(const ROOM_ID &room_id_);
    // delete all rooms
    void                    clear_room();

    //更新修改每个房间的缓冲区个数
    void                    update_buffer_count();

protected:
typedef boost::unordered_map<ROOM_ID, CBaseRoom *>              ROOM_MAP;
typedef boost::unordered_map<ROOM_ID, CBaseRoom *>::iterator    ROOM_ITERATOR;
typedef set<evutil_socket_t >                                   SOCKET_SET;
typedef set<evutil_socket_t >::iterator                         SOCKET_SET_ITERATOR;

    ROOM_MAP                _room_map;

    event_base              *_event_base;
    CBaseRoom*              _pTmp_room;
    evbuffer*               _tmp_evbuffer;
    USER_ID                 _tmp_uid;
    ROOM_ID                 _tmp_room_id;
    uint8_t                 _tmp_user_type;     //主播 or 普通玩家
    uint8_t                 _tmp_request_ret;
    uint8_t                 _type_buf[2];
    uint8_t                 _protocal_buf[64];
    uint8_t*                _buffer;
    uint8_t*                _pkt_buf;
    int32_t                 _ev_buf_len;
    const size_t            _base_protocal_len;
    NetPacket*              _pTmpNetPkt;
    int32_t                 _buffer_count;      //缓冲区数量,默认为5
    int32_t                 _cur_player_count;  //当前玩家数量
    int32_t                 _max_player_count;  //最大可挂载玩家数量
    SOCKET_SET              _sock_set;
    //socket tmp var
    BOOL                    _bKeepAlive;
    tcp_keepalive           _alive_in;
    tcp_keepalive           _alive_out;
    unsigned long           _ulBytesReturn;
    short                   _tmp_pkt_len;

    static uint8_t          _tmp_buf[8];
};
