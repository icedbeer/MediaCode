#pragma once
#include <windows.h>
#include <vector>
#include <string>

#include "memory/WJS_MemoryPoolManage.h"
#include "stream/WJS_IByteStream.h"
#include "WJS_Memory_File.h"


class WJS_Memory_Share
{
public:
	explicit WJS_Memory_Share(string name,bool bCreater);
	virtual ~WJS_Memory_Share();

	bool  Write(char* pData,int len);//写数据

	WJSByteSeq Read(); //读数据

	void            SetCloseProtocolData(const unsigned char* pData,int len);//设关闭协议的数据

protected:
	bool  m_bCreater;//是否是创建者
	HANDLE m_hMutex;

	WJS_Memory_File m_MemoryFile1; //创建者是1发2收，否则相反
	WJS_Memory_File m_MemoryFile2;

	vector<unsigned char>         m_CloseProtocolData;
	string  m_closename;
	HANDLE m_hCloseEvent; //关闭事件

};