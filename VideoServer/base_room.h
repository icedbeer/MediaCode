/************************************************************************/
/* 房间单位
   房间内一个节点接收到rudp数据，直接转发给房间内的其它的所有节点*/
/************************************************************************/
#pragma once

#include <set>
#include <list>
#include "libevent_header.h"
#include "EasyBuffer.h"
#include "base_timer_value.h"
#include "GlobalServer.h"

using namespace std;

class Node_Addr;
class Host_Node_Addr;

class CBaseRoom
{
public:
    CBaseRoom(const ROOM_ID &room_id_, const int32_t& buffer_num_);
    virtual ~CBaseRoom(void);

public:
    // 添加用户节点
    virtual bool            add_node(const USER_ID& user_id_, const uint8_t& user_type_);
    // 删除指定用户节点
    virtual bool            del_node(const USER_ID &user_id_, const uint8_t& user_type_);

    // 添加主播节点
    virtual bool            add_host_node(const USER_ID& user_id_);
    // 删除指定主播节点
    virtual bool            del_host_node(const USER_ID& user_id_);

    uint32_t                get_nodes_count();
    //virtual CUdpSession     *get_udp_session() { return _udp_session; };
    virtual ROOM_ID&        get_room_id() { return _local_room_id; };

    //数据总入口
    virtual int             on_recv_data(const USER_ID &uid_, uint8_t *&buffer_, int32_t& buf_len_,
                               evhttp_request *&req_, NetPacket& pkt_);

    //将浦东节点的数据请求添加到缓冲中
    virtual bool            insert_node_req(evhttp_request *&req_, USER_ID &uid_);

    // 检测掉线节点
    virtual void            detect_nodes_state();

    // 清除缓冲区中超时的数据包
    virtual void            clear_timeout_buffer();

    virtual void            set_host_id(const USER_ID& uid_, bool bOnMic_);

    //设置缓冲区个数
    virtual void            set_buffer_num(const int32_t& num_);
    //重置参数
    virtual void            reinit();

protected:
    //退出清除所有节点信息
    virtual void            clear_nodes();

    virtual Host_Node_Addr* get_host_node(const USER_ID &uid_);

    //处理协议包
    virtual int             process_protocal_msg(const USER_ID &uid_, EPROTO_NUM payload_type_, 
                                                    evhttp_request *&req_);
    //处理媒体数据包
    virtual int             process_media_msg(const USER_ID &uid_, NetPacket& pkt_, 
                                                    uint8_t *&buffer_, int32_t& buf_len_, evhttp_request *&req_);
    //转发数据包给所有的其它nodes
    virtual void            forward_packet();
    //通知房间内所有节点主播上下麦信息
    virtual void            forward_protocal(uint8_t protocal_);

    virtual void            send_host_msg(
        const boost::unordered_map<USER_ID, Node_Addr *>::iterator& itor_, const uint8_t& protocal_,
        const uint8_t *data_, short data_len_);
    //
    virtual void            OnGetProtocal(const USER_ID& uid_, evhttp_request* req_);

    virtual void            packet_protocal_evbuffer();

    //移动缓冲区
    virtual void            move_buffer(Host_Node_Addr*& host_);

protected:
    typedef boost::unordered_map<USER_ID, Node_Addr *>                 UMAP;
    typedef boost::unordered_map<USER_ID, Node_Addr *>::iterator       UMAP_ITOR;
    typedef boost::unordered_map<USER_ID, Host_Node_Addr *>            HOSTUMAP;
    typedef boost::unordered_map<USER_ID, Host_Node_Addr *>::iterator  HOSTUMAP_ITOR;
    
    UMAP                    _node_umap;
    Host_Node_Addr*         _host_node;
    ROOM_ID                 _local_room_id;         //自己的房间ID
    //uint8_t                 _type_buf[6];           //前面有4个字节的包头
    int32_t                 _media_buf_len;         //_media_buf有效字节的长度
    uint8_t                 _tmp_proto_buf[1500];
    const size_t            _base_protocal_len;
    USER_ID                 _cur_host_id;           //当前主播ID，如果为0，表示没有主播在麦
    evbuffer*               _tmp_evbuffer;
    evbuffer*               _response_evbuf;        //填充空的包头和包尾，回复客户端的Get请求
    int32_t                 _buffer_num;            //缓冲区个数

public:
    static int32_t          _cur_player_count;

};

class Node_Addr
{
public:
    Node_Addr()
    {
        _mem_block      = 0;
        _cur_block_size = 0;
        _get_req        = NULL;
        _user_id        = 0;
        _unpro_protocal = 0;
        _last_tv        = sev_get_epoch_time();
    }
    Node_Addr(const USER_ID &id_)
    {
        _mem_block      = 0;
        _cur_block_size = 0;
        _get_req        = NULL;
        _user_id        = id_;
        _unpro_protocal = 0;
        _last_tv        = sev_get_epoch_time();
    }
    ~Node_Addr()
    {
        //if (_get_req)
        //{
        //    evhttp_request_free(_get_req);
        //}
        _user_id  = 0;
        _last_tv  = 0;
    }
    uint8_t             _unpro_protocal;    //未处理的协议(host onmic, host off mic)
    uint64_t            _last_tv;           //记录上次通讯时间
    evhttp_request*     _get_req;           //get data request
    USER_ID             _user_id;
    int64_t             _mem_block;         //当前的数据块(client,server)
    size_t              _cur_block_size;    //only client
};

class Host_Node_Addr : public Node_Addr
{
public:
    Host_Node_Addr(const USER_ID &id_)
        :Node_Addr(id_)
        ,_easy_buffer_count(5)
    {
        _video_key_frame_count = 0;
        try{
            for (int i(0); i < _easy_buffer_count; i++)
            {
                _easy_buffer_list.push_back(new CEasyBuffer(1024*1024));
                if (i == (_easy_buffer_count - 2))
                {
                    _buffered = _easy_buffer_list.back();
                }
            }
            _buffering = _easy_buffer_list.back();
        }catch(std::bad_alloc&)
        {
            _buffered  = NULL;
            _buffering = NULL;
        }
    }
    ~Host_Node_Addr()
    {
        std::list<CEasyBuffer* >::iterator itor = _easy_buffer_list.begin();
        while (itor != _easy_buffer_list.end())
        {
            delete *itor;
            itor++;
        }
        _easy_buffer_list.clear();
    }
    int32_t                  _easy_buffer_count;
    CEasyBuffer*             _buffered;      //已缓冲的数据，以两个I帧之间的数据段为单位
    CEasyBuffer*             _buffering;     //正在缓冲的数据
    list<CEasyBuffer* >      _easy_buffer_list;
    int                      _video_key_frame_count; //视频关键帧计数
};

