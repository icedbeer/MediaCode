#ifdef WIN32
#include <stdio.h>
#include <windows.h>
#include <dbghelp.h>
#include "boost_log.h"

#pragma comment(lib, "Dbghelp.lib")

LONG WINAPI MyUnhandledFilter(struct _EXCEPTION_POINTERS *lpExceptionInfo)
{
    LONG ret = EXCEPTION_EXECUTE_HANDLER;

    TCHAR szFileName[64];
    //SYSTEMTIME st;
    //::GetLocalTime(&st);
    //wsprintf(szFileName, TEXT("%04d-%02d-%02d-%02d-%02d-%02d-%02d-%02d.dump"), st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds, rand()%100);
    wsprintf(szFileName, TEXT("HttpServer.dmp"));

    HANDLE hFile = ::CreateFile(szFileName, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
    if (hFile != INVALID_HANDLE_VALUE)
    {
        MINIDUMP_EXCEPTION_INFORMATION ExInfo;

        ExInfo.ThreadId = ::GetCurrentThreadId();
        ExInfo.ExceptionPointers = lpExceptionInfo;
        ExInfo.ClientPointers = false;

        // write the dump
        BOOST_ERROR << "create dump file.";

        BOOL bOK = MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), hFile, MiniDumpNormal, &ExInfo, NULL, NULL );

        if (bOK)
        {
            BOOST_ERROR"Create Dump File Success!";
        }
        else
        {
            BOOST_ERROR"MiniDumpWriteDump Failed: " <<GetLastError();
        }

        ::CloseHandle(hFile);
    }
    else
    {
        BOOST_ERROR<<"Create File failed. error:"<<GetLastError();
    }
    BoostLog::Uninit();
    return ret;
}
#endif // WIN32

