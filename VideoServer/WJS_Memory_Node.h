#pragma once
#include <windows.h>
#include <vector>
#include <string>

#include "memory/WJS_MemoryPoolManage.h"
//#include "WJS_BBSTree.h"
#include "stream/WJS_IByteStream.h"
//#include "WJS_NetLayer_Head.h"

using namespace std;



class WJS_Memory_Node
{
	friend class WJS_Memory_Node;
public:
	WJS_Memory_Node();
	WJS_Memory_Node(string name);
	/*virtual */~WJS_Memory_Node();
	WJS_Memory_Node(const WJS_Memory_Node& rNode)
	{
		memcpy(m_name2,rNode.m_name2,96);
		m_handle = rNode.m_handle;
		m_size = rNode.m_size;
	}
	WJS_Memory_Node& operator = (const WJS_Memory_Node& rNode)
	{
		if(this == &rNode)
			return *this;

		memcpy(m_name2,rNode.m_name2,96);
		m_handle = rNode.m_handle;
		m_size = rNode.m_size;
		return *this;
	}

	void  Free();

	bool  ReSize(int size);

	bool  Write(char* pData,int len);//写数据

	WJSByteSeq Read(); //读数据



protected:
	//string m_name;
    char   m_name2[96];
	HANDLE m_handle;
	int    m_size;
	bool   m_bSpecifyName;//是否指定名字

};

typedef vector<WJS_Memory_Node> WJS_Memory_Node_List;

