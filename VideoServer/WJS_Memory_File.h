#pragma once
#include <windows.h>
#include <vector>
#include <string>

#include "memory/WJS_MemoryPoolManage.h"
#include "stream/WJS_IByteStream.h"
#include "WJS_Memory_Node.h"

using namespace std;

class WJS_Memory_File
{
public:
	WJS_Memory_File(string name);
	~WJS_Memory_File();


	bool  Write(char* pData,int len);//写数据

	WJSByteSeq Read(); //读数据

	void Free() {m_bigMemoryNode.Free();}

protected:
	WJS_Memory_Node_List GetMemoryNodeList(); //取得此内存块上的所有小的内存节点

	void                 WriteMemoryNodeList(WJS_Memory_Node_List& NodeList,int n);
protected:

	WJS_Memory_Node  m_bigMemoryNode; //使用大一点的内存节点，如256KB

};