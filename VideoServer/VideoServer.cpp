// HttpServer.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <io.h>
#include "libevent_header.h"
#include "room_mgr.h"
#include "minidump.h"
#include "UdpServer.h"
#ifdef WJGM
#include "WaiJeiGameManager.h"
#endif // WJGM

#ifdef _DEBUG
#pragma comment(lib, "RUdpD.lib");
#else
#pragma comment(lib, "RUdp.lib");
#endif // _DEBUG

// 获得本机中处理器的数量
int GetNoOfProcessors()
{
    SYSTEM_INFO si;
    GetSystemInfo(&si);
    BOOST_INFO << "System cpu number : " << si.dwNumberOfProcessors;
    return si.dwNumberOfProcessors;
}

static void on_socket_close(struct evhttp_connection *con_, void *arg)
{
	BOOST_DEBUG << "on_socket_close";
    if (con_ && evhttp_connection_get_bufferevent(con_) && arg)
    {
        evutil_closesocket(bufferevent_getfd(evhttp_connection_get_bufferevent(con_)));
        //((CRoomMgr *)arg)->del_sock(bufferevent_getfd(evhttp_connection_get_bufferevent(con_)));
    }
}

static void dump_request_cb(struct evhttp_request *req, void *arg)
{
    if (NULL == req || NULL == arg)
    {
		BOOST_DEBUG << "dump_request_cb req or arg is null.";
        return;
    }
    bufferevent *bev = NULL;
    if (req->evcon && (bev = evhttp_connection_get_bufferevent(req->evcon)) != NULL && arg)
    {
        //((CRoomMgr *)arg)->set_sock_opt(bufferevent_getfd(evhttp_connection_get_bufferevent(req->evcon)));
        evhttp_connection_set_closecb(req->evcon, on_socket_close, /*&fd*/arg);
        bufferevent_enable(bev, EV_READ);
    }
    struct evbuffer *ev_buf = evhttp_request_get_input_buffer(req);
    if (NULL == ev_buf)
    {
		BOOST_DEBUG<<  "dump_request_cb recv data len is null";
        return;
    }
	switch (evhttp_request_get_command(req)) 
    {
	    case EVHTTP_REQ_PUT: 
		case EVHTTP_REQ_GET:
		case EVHTTP_REQ_POST:
            {
                if(((CRoomMgr *)arg)->process_msg(ev_buf, req) < 0)
                {
                    evhttp_send_reply(req, HTTP_ERROR, "ERROR", NULL);
                }
            }
            break;
	    default: 
            BOOST_DEBUG<<  "Received not PUT, unknow command." << " request for Headers:" << evhttp_request_get_uri(req);
            evhttp_send_error(req, HTTP_UNKNOWN_COMMAND, "unknown cmd");
            break;
	}
}

//僵尸链接清理定时器
void nodes_clear_cb(evutil_socket_t sock_, short event_, void *arg_)
{
    if (-1 != sock_ && NULL == arg_)
    {
        return ;
    }
    CRoomMgr *p = (CRoomMgr *)arg_;
    //僵尸链接清理定时器
    p->detect_dead_node();
}


void accept_error_cb(struct evconnlistener *lister, void *arg)
{
    BOOST_DEBUG<<  "accept_error_cb.";
}

BOOL CtrlHandler(DWORD fdwCtrlType)
{
    switch (fdwCtrlType)
    {
        // Handle the CTRL-C signal. 
    case CTRL_C_EVENT:
        //printf("Ctrl-C event\n\n");
        //Beep(750, 300);
        return(TRUE);
    default:
        return FALSE;
    }
}

#ifdef WJGM
//服务器之间通信回调
void server_trans_cb(evutil_socket_t sock_, short event_, void *arg_)
{
    if (-1 != sock_ && NULL == arg_)
    {
        return;
    }
    WaiJeiGameManager *p = (WaiJeiGameManager *)arg_;
    p->Heart();
    p->Read();
}
#endif // WJGM

int main(int argc, char **argv)
{
    // Verify that the version of the library that we linked against is
    // compatible with the version of the headers we compiled against.
    GOOGLE_PROTOBUF_VERIFY_VERSION;

	if (_access("hashttpserverlog", 0) != -1)
	{
		char file_buf[MAX_PATH] = {	0 };
		sprintf_s(file_buf, MAX_PATH, "httpserver_%lu", GetCurrentProcessId());
#ifdef _DEBUG
		BoostLog::Init_Debug("log", file_buf, debug); 
#else
		BoostLog::Init_Debug("log", file_buf, info); 
#endif
	}

    BOOST_INFO << "HttpServer start! arg num:"<< argc;
    for (int i(0); i < argc; i++)
    {
        BOOST_INFO << "argc "<< i+1 << " : " << argv[i];
    }  
    if (argc < 2) 
    {
        BOOST_ERROR << "HttpServer argc:"<< argc << " < 2 exit.";
        return 1;
    }
    //屏蔽ctrl + c
    if (FALSE == SetConsoleCtrlHandler((PHANDLER_ROUTINE)CtrlHandler, TRUE))
    {
        BOOST_ERROR << "SetConsoleCtrlHandler failed.";
    }

#ifndef WJGM
    //启动UDP
    CUdpServer udp_sev(GetNoOfProcessors() / 2);
    udp_sev.open_local_addr(6688);
    SetConsoleTitle("VideoServer_Http_Udp");
    add_extern_sharemedia_ptr(&udp_sev);
#endif // WJGM

	struct event_base           *base     = NULL;
	struct evhttp               *http     = NULL;
	struct evhttp_bound_socket  *handle   = NULL;
    struct evconnlistener       *listener = NULL;
    CRoomMgr                    *room_mgr = NULL;
    event                       *detect_timeout_ev = NULL;
    event                       *sev_timeout_ev = NULL;
#ifdef WJGM
    WaiJeiGameManager           *gm = NULL;
#endif // WJGM

#ifdef WJGM
	unsigned short port = atoi(argv[2]);
#else
    unsigned short port = atoi(argv[1]);
#endif // WJGM
    
#ifdef WIN32
	WSADATA WSAData;
	WSAStartup(0x101, &WSAData);
    ::SetUnhandledExceptionFilter(MyUnhandledFilter);
#else
	if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
    {
		return (1);
    }
#endif
    do
    {
        event_config *cfg = event_config_new();
        if (NULL == cfg)
        {
            BOOST_ERROR << "new event_config failed.";
            break;
        }
#ifdef WIN32
        evthread_use_windows_threads();
        event_config_set_num_cpus_hint(cfg, GetNoOfProcessors() / 2);
        event_config_set_flag(cfg, EVENT_BASE_FLAG_STARTUP_IOCP | EVENT_BASE_FLAG_EPOLL_USE_CHANGELIST);
#else
        evthread_use_pthreads();
#endif
        if ((base = event_base_new_with_config(cfg)) == NULL)
        {
            base = event_base_new();
        }
        event_config_free(cfg);

        if (!base)
        {
            BOOST_ERROR << "Couldn't create an event_base: exit.";
            break;
        }
		//new room manager
		room_mgr = new(std::nothrow) CRoomMgr(base);
		if (NULL == room_mgr)
		{
			BOOST_ERROR << "new room_mgr failed.";
			break;
		}
        add_extern_sharemedia_ptr(room_mgr);

        /* Create a new evhttp object to handle requests. */
        http = evhttp_new(base);
        if (!http)
        {
            BOOST_ERROR << "couldn't create evhttp. Exiting.";
            break;
        }

        /* The /dump URI will dump all requests to stdout and say 200 ok. */
        evhttp_set_cb(http, "/dump", dump_request_cb, (void *)room_mgr);

        /* Now we tell the evhttp what port to listen on */
        handle = evhttp_bind_socket_with_handle(http, "0.0.0.0", port);
        if (!handle)
        {
            BOOST_ERROR << "couldn't bind to port %d. Exiting." << port;
            break;
        }
        //evutil_make_socket_nonblocking(evhttp_bound_socket_get_fd(handle));
        evutil_make_listen_socket_reuseable(evhttp_bound_socket_get_fd(handle));
        // 设置服务超时时间，单位为秒
        evhttp_set_timeout(http, 86400); //1 days
        //  服务只接受 PUT 请求
        //evhttp_set_allowed_methods(http, EVHTTP_REQ_PUT | EVHTTP_REQ_GET | EVHTTP_REQ_POST);
		//evhttp_set_allowed_methods(http, EVHTTP_REQ_GET);
		//evhttp_set_allowed_methods(http, EVHTTP_REQ_POST);

        //僵尸节点清理永久定时器
        struct timeval tv;
        tv.tv_sec = 6;
        tv.tv_usec = 0;
        detect_timeout_ev = event_new(base, -1, EV_PERSIST, nodes_clear_cb, room_mgr);
        if (detect_timeout_ev)
        {
            event_add(detect_timeout_ev, &tv);
        } else
        {
            BOOST_ERROR << "new detect_timeout_ev failed.";
            break;
        }
#ifdef WJGM
        gm = new(std::nothrow) WaiJeiGameManager;
        if (!gm)
        {
            break;
        }
        gm->CreateShareMemory(argv[1]);
        gm->Set_Room_Mgr(room_mgr);

        struct timeval tv2;
        tv2.tv_sec = 0;
        tv2.tv_usec = 100000;
        sev_timeout_ev = event_new(base, -1, EV_PERSIST, server_trans_cb, gm);
        if (sev_timeout_ev)
        {
            event_add(sev_timeout_ev, &tv2);
        } else
        {
            BOOST_ERROR << "new sev_timeout_ev failed.";
            break;
        }
#endif // WJGM

        // Extract and display the address we're listening on.
        struct sockaddr_storage ss;
        evutil_socket_t fd;
        ev_socklen_t socklen = sizeof(ss);
        char addrbuf[128];
        void *inaddr;
        const char *addr;
        int got_port = -1;
        fd = evhttp_bound_socket_get_fd(handle);
        memset(&ss, 0, sizeof(ss));
        if (getsockname(fd, (struct sockaddr *)&ss, &socklen)) {
            perror("getsockname() failed");
        }
        if (ss.ss_family == AF_INET)
        {
            got_port = ntohs(((struct sockaddr_in*)&ss)->sin_port);
            inaddr = &((struct sockaddr_in*)&ss)->sin_addr;
        } else if (ss.ss_family == AF_INET6)
        {
            got_port = ntohs(((struct sockaddr_in6*)&ss)->sin6_port);
            inaddr = &((struct sockaddr_in6*)&ss)->sin6_addr;
        } else
        {
            BOOST_ERROR << "Weird address family " << ss.ss_family;
        }
        addr = evutil_inet_ntop(ss.ss_family, inaddr, addrbuf, sizeof(addrbuf));
        if (addr)
        {
            BOOST_DEBUG << "Listening on " << addr << ":" << got_port;
        } else {
            BOOST_ERROR << "evutil_inet_ntop failed";
        }

        event_base_dispatch(base);
    } while (false);

    BOOST_WARNING << "HttpServer delete.";
    if (room_mgr)
    {
        delete room_mgr;
    }
    if (detect_timeout_ev)
    {
        event_free(detect_timeout_ev);
    }
    if (sev_timeout_ev)
    {
        event_free(sev_timeout_ev);
    }
    if (http)
    {
        evhttp_free(http);
    }
    if (base)
    {
        event_base_free(base);
    }
    BOOST_WARNING << "HttpServer exit.";

	if (_access("hashttpserverlog", 0) != -1)
	{
		BoostLog::Uninit();
	}

    // Optional:  Delete all global objects allocated by libprotobuf.
    google::protobuf::ShutdownProtobufLibrary();

	return 0;
}
