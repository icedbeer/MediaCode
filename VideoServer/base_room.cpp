#include "base_room.h"
#include <climits>

int32_t CBaseRoom::_cur_player_count = 0;

CBaseRoom::CBaseRoom(const ROOM_ID &room_id_, const int32_t& buffer_num_)
:_local_room_id(room_id_)
,_host_node(NULL)
,_cur_host_id(0)
,_base_protocal_len(2 + sizeof(ROOM_ID) + sizeof(USER_ID)) //CBasePacket的长度
,_buffer_num(buffer_num_)
{
    _tmp_evbuffer = evbuffer_new();
    _response_evbuf = evbuffer_new();
}

CBaseRoom::~CBaseRoom(void)
{
    clear_nodes();
    if (_tmp_evbuffer)
    {
        evbuffer_free(_tmp_evbuffer);
    }
    if (_response_evbuf)
    {
        evbuffer_free(_response_evbuf);
    }
}

// 添加用户节点
bool CBaseRoom::add_node(const USER_ID& user_id_, const uint8_t& user_type_)
{
    if (EUSER_PLAYER == user_type_ && _node_umap.end() != _node_umap.find(user_id_))
    {
        //already exist.
        del_node(user_id_, user_type_);
    }
    if (EUSER_PLAYER == user_type_)
    {
		BOOST_DEBUG << "CBaseRoom::add_node  user id:" << user_id_;
		try{
            _node_umap.insert(std::pair<USER_ID, Node_Addr *>(user_id_, 
                new Node_Addr(user_id_)));
            _cur_player_count++;
        }catch(std::bad_alloc& e)
        {
            BOOST_INFO << "room : " << _local_room_id << " add node : " << user_id_ << " failed."
                << "error info:" << e.what();
            return false;
        }
        //BOOST_DEBUG << "room : " << _local_room_id << " has nodes : " << _node_umap.size();
    }else if (EUSER_ANCHOR == user_type_)
    {
        return add_host_node(user_id_);
    }else
    {
        BOOST_INFO << "CBaseRoom::add_node() id : " << user_id_ << ", user type not known. " << user_type_;
        return false;
    }
    return true;
}

// 删除制定用户节点
bool CBaseRoom::del_node(const USER_ID &user_id_, const uint8_t& user_type_)
{
    if (EUSER_ANCHOR == user_type_)
    {
        return del_host_node(user_id_);
    }
	BOOST_DEBUG << "CBaseRoom::del_node  user id:" << user_id_;
    UMAP_ITOR itor = _node_umap.find(user_id_);
    if (_node_umap.end() == itor)
    {
        return false;
    }
    if (itor->second)
    {
        delete itor->second;
    }
    _node_umap.erase(itor);
    _cur_player_count--;

    //BOOST_DEBUG << "room : " << _local_room_id << " del uid:" << user_id_ 
    //    << ", now has nodes : " << _node_umap.size();
    return true;
}

// 添加主播节点
bool CBaseRoom::add_host_node(const USER_ID& user_id_)
{
    BOOST_DEBUG << "add_host_node:" << user_id_ << " want to onmic.";
    //暂时注释掉，跟徐立斌的视频服务器合并了以后再打开
    //if (0 != _cur_host_id && _cur_host_id != user_id_)
    //{
    //    BOOST_INFO << "room " << _local_room_id << " add host node failed.current host is " << _cur_host_id;
    //    return false;
    //}

    if (_host_node)
    {
        BOOST_INFO << "user:" << _host_node->_user_id <<" already on mic, delete it.";
        delete _host_node;
        _host_node = NULL;
    }
    try{
		if (!_host_node)
		{
			_host_node = new Host_Node_Addr(user_id_);
		}
        set_buffer_num(_buffer_num);
    }catch(std::bad_alloc& e)
    {
        BOOST_INFO << "room : " << _local_room_id << " add host node : " << user_id_ << " failed."
            << "error info:" << e.what();
        return false;
    }
    _cur_host_id = user_id_;
    return true;
}
// 删除指定主播节点
bool CBaseRoom::del_host_node(const USER_ID &user_id_)
{
    BOOST_DEBUG << "deleted host user:" << user_id_ << ".";
    if (!_host_node)
    {
        BOOST_INFO << "room:" << _local_room_id <<" no host on mic.";
        return false;
    }
    if (_host_node->_user_id != user_id_)
    {
        BOOST_INFO << "room:" << _local_room_id <<" host node is:" << _host_node->_user_id
            << "; not " << user_id_;
        return false;
    }

    delete _host_node;
    _host_node = NULL;
    _cur_host_id = 0;
    return true;
}

//退出清除所有节点信息
void CBaseRoom::clear_nodes()
{
    //clear common nodes list
    UMAP_ITOR itor = _node_umap.begin();
    while (itor != _node_umap.end())
    {
        if (itor->second)
        {
            delete itor->second;
        }
        itor++;
    }
    _cur_player_count -= _node_umap.size();
    _node_umap.clear();
     //clear host nodes list
    if (_host_node)
    {
        delete _host_node;
        _host_node = NULL;
    }
}

// 将接收到的数据添加进RUdpNode缓冲中
int CBaseRoom::on_recv_data(const USER_ID &uid_, uint8_t *&buffer_, int32_t& buf_len_, 
                            evhttp_request *&req_, NetPacket& pkt_)
{
    switch(pkt_.proto_num())
    {
    case EPROTO_GET:
        process_protocal_msg(uid_, pkt_.proto_num(), req_);
        break;
    case EPROTO_LOGIN:
    case EPROTO_LOGOUT:
        break;
    case EPROTO_VIDEO:
    case EPROTO_AUDIO:
        process_media_msg(uid_, pkt_, buffer_, buf_len_, req_);
        break;
    default:
        {
            BOOST_DEBUG << "CBaseRoom::on_recv_data packet type:" << pkt_.proto_num() << " unknown.";
            return -1;
        }
    }
    return 0;
}

bool CBaseRoom::insert_node_req(evhttp_request *&req_, USER_ID &uid_)
{
    OnGetProtocal(uid_, req_);
    return true;
}

void CBaseRoom::OnGetProtocal(const USER_ID& uid_, evhttp_request* req_)
{
    UMAP_ITOR itor = _node_umap.find(uid_);
    if (_node_umap.end() == itor || NULL == itor->second)
    {
        return ;
    }
    itor->second->_get_req = req_;
    itor->second->_last_tv = sev_get_epoch_time();
    if (itor->second->_unpro_protocal != 0)
    {
        ALLOC_PROTOCAL_PACKET(pkt, (EPROTO_NUM)itor->second->_unpro_protocal, _local_room_id, _cur_host_id);
        if(pkt->SerializeToArray(_tmp_proto_buf, pkt->ByteSize()))
        {
            send_host_msg(itor, itor->second->_unpro_protocal, (uint8_t *)_tmp_proto_buf, (short)pkt->ByteSize());
        }
        itor->second->_unpro_protocal = 0;
        itor->second->_get_req = NULL;
        delete pkt;
    }
}

void CBaseRoom::packet_protocal_evbuffer()
{
    if (_response_evbuf)
    {
        evbuffer_drain(_response_evbuf, evbuffer_get_length(_response_evbuf));
    }
}

void CBaseRoom::move_buffer(Host_Node_Addr*& host_)
{
    if (!host_)
    {
        return;
    }
    //前面的清空，挪到后面
    CEasyBuffer* pTmp = host_->_easy_buffer_list.front();
    pTmp->reset();
    host_->_buffered = host_->_easy_buffer_list.back();
    host_->_easy_buffer_list.pop_front();
    host_->_easy_buffer_list.push_back(pTmp);
    host_->_buffering = pTmp;
}

void CBaseRoom::send_host_msg(const UMAP_ITOR& itor_, const uint8_t& protocal_,
                              const uint8_t *data_, short data_len_)
{
    data_len_ = CGlobalServer::small_2_big_short(data_len_);
    evbuffer_add(itor_->second->_get_req->output_buffer, &data_len_, sizeof(short));
    evbuffer_add(itor_->second->_get_req->output_buffer, data_, data_len_);

    evhttp_send_reply(itor_->second->_get_req, HTTP_HOST_ONMIC, "onmic", NULL);
}


//处理协议包
int CBaseRoom::process_protocal_msg(const USER_ID &uid_, EPROTO_NUM payload_type_,
                                     evhttp_request *&req_)
{
    if(EPROTO_GET == payload_type_)
    {
        OnGetProtocal(uid_, req_);
    }
	return 0;
}

//处理媒体数据包
int CBaseRoom::process_media_msg(const USER_ID &uid_, NetPacket& pkt_, 
                                  uint8_t *&buffer_, int32_t& buf_len_, evhttp_request *&req_)
{
    if (NULL == _host_node || NULL == buffer_)
    {
        BOOST_INFO << "CBaseRoom::process_media_msg video frame from user id:" 
            << uid_ << "  host node or buffer_ is null.";
        return -1;
    }
    if (uid_ != _host_node->_user_id)
    {
        BOOST_DEBUG << "CBaseRoom::process_media_msg video frame from user id:" 
            << uid_ << "!= host node id " << _host_node->_user_id ;
        return -1;
    }
    if (NULL == _host_node->_buffered || NULL == _host_node->_buffering)
    {
		BOOST_DEBUG << "_buffered or buffering is null";
        return -1;
    }
    if (EPROTO_VIDEO == pkt_.proto_num() && pkt_.has_video_pkt())
    {
        //判断首帧是否是关键帧（关键帧必须放到头部，由客户端来保证）
        //视频包的包头字节长度（到frame type）
        if (1 == pkt_.video_pkt().frame_type())
        {
            //BOOST_DEBUG << "recv video key frame. At room : " << _local_room_id;
            move_buffer(_host_node);
            if (_host_node->_video_key_frame_count >= INT_MAX)
            {
                _host_node->_video_key_frame_count = 2;
            }
            _host_node->_mem_block++;
            _host_node->_video_key_frame_count++;
        }
    }
    //add to buffer
    _host_node->_buffering->PushBack(buffer_, buf_len_);

    //转发
    forward_packet( );
	return 0;
}

//转发协议包和媒体数据包
void CBaseRoom::forward_packet()
{
	if (NULL == _host_node /*|| _host_node->_video_key_frame_count < 2*/)
	{
		//保证服务器缓冲两个关键帧再转发
		return ;
	}
	uint64_t cur_tv = sev_get_epoch_time();
	_host_node->_last_tv = cur_tv;
	UMAP_ITOR itor = _node_umap.begin();
	list<CEasyBuffer* >::iterator buff_list_itor;
	while (_node_umap.end() != itor)
	{
		if (NULL == itor->second) 
		{
			//exception:delete this invalid node.
			itor = _node_umap.erase(itor);
			_cur_player_count--;
			continue;
		}
		if (NULL == itor->second->_get_req) //dead node dectect
		{
			if (cur_tv < itor->second->_last_tv)//system time reset
			{
				itor->second->_last_tv = cur_tv;
			}else if (cur_tv - itor->second->_last_tv > NODE_DEAD_TIME * 1000) //node dead,delete it.
			{
// 				BOOST_DEBUG << "delete timeout node : " << itor->first 
// 				    << "; from room : " << this->_local_room_id << ".";
				delete itor->second;
				itor = _node_umap.erase(itor);
				_cur_player_count--;
				continue;
			}

			//next
			itor++;
			continue;
		}
		//DIY data and send.
		if (0 == itor->second->_mem_block && 0 == itor->second->_cur_block_size)
		{
			//node首次进入房间
// 			evbuffer_add(itor->second->_get_req->output_buffer, 
// 				_host_node->_buffered->GetBuffer(), _host_node->_buffered->GetUsedSize());
			evbuffer_add(itor->second->_get_req->output_buffer, 
				_host_node->_buffering->GetBuffer() + itor->second->_cur_block_size, 
				_host_node->_buffering->GetUsedSize() - itor->second->_cur_block_size);

			evhttp_send_reply(itor->second->_get_req, HTTP_HEATBEAT_CODE, "DATA", NULL);
			itor->second->_mem_block = _host_node->_mem_block;
			itor->second->_cur_block_size = _host_node->_buffering->GetUsedSize();
			itor->second->_get_req = NULL;

		}else if (itor->second->_mem_block == _host_node->_mem_block)
		{
			if (itor->second->_cur_block_size == _host_node->_buffering->GetUsedSize()) //no new data
			{
				; //next
			}else
			{
				evbuffer_add(itor->second->_get_req->output_buffer, 
					_host_node->_buffering->GetBuffer() + itor->second->_cur_block_size, 
					_host_node->_buffering->GetUsedSize() - itor->second->_cur_block_size);

				evhttp_send_reply(itor->second->_get_req, HTTP_HEATBEAT_CODE, "DATA", NULL);
				itor->second->_cur_block_size = _host_node->_buffering->GetUsedSize();
				itor->second->_get_req = NULL;
			}
		}else
		{
			if ( (itor->second->_mem_block + _host_node->_easy_buffer_count) >= _host_node->_mem_block) //缺失数据仍在缓冲中
			{
				buff_list_itor = _host_node->_easy_buffer_list.end();
				buff_list_itor--;
				while(itor->second->_mem_block != _host_node->_mem_block)
				{
					buff_list_itor--;
					itor->second->_mem_block++;
					if (buff_list_itor == _host_node->_easy_buffer_list.begin())
					{
						break;
					}
				}
				//从上次记录的位置开始
				evbuffer_add(itor->second->_get_req->output_buffer, 
					(*buff_list_itor)->GetBuffer() + itor->second->_cur_block_size,
					(*buff_list_itor)->GetUsedSize() - itor->second->_cur_block_size);
				buff_list_itor++;
			}else
			{
				//上次记录位置已经不存在了,只发送最后两个缓冲区的数据
				buff_list_itor = _host_node->_easy_buffer_list.end();
				buff_list_itor--;
				buff_list_itor--;
			}
			while(buff_list_itor != _host_node->_easy_buffer_list.end())
			{
				evbuffer_add(itor->second->_get_req->output_buffer, 
					(*buff_list_itor)->GetBuffer(), (*buff_list_itor)->GetUsedSize());
				buff_list_itor++;
			}
			evhttp_send_reply(itor->second->_get_req, HTTP_HEATBEAT_CODE, "DATA", NULL);
			itor->second->_mem_block = _host_node->_mem_block;
			itor->second->_cur_block_size = _host_node->_buffering->GetUsedSize();
			itor->second->_get_req = NULL;
		}
		itor->second->_last_tv = cur_tv; //update timestamp
		itor++;
	}
}

//目前只通知主播的开播和停播消息
void CBaseRoom::forward_protocal(uint8_t protocal_)
{
    ALLOC_PROTOCAL_PACKET(pkt, (EPROTO_NUM)protocal_, _local_room_id, _cur_host_id);
    if(!pkt->SerializeToArray(_tmp_proto_buf, pkt->ByteSize()))
    {
        delete pkt;
        return ;
    }
    UMAP_ITOR itor = _node_umap.begin();
    while (_node_umap.end() != itor)
    {
        if (!itor->second)
        {
            itor = _node_umap.erase(itor);
            _cur_player_count--;
            continue;
        }
        if (_host_node && itor->second->_get_req)
        {
            send_host_msg(itor, protocal_, _tmp_proto_buf, pkt->ByteSize());
            itor->second->_get_req = NULL;
        }else
        {
            itor->second->_unpro_protocal = protocal_;
        }
        //清除所有节点记录的发送位置，只有在主播上麦和下麦的时候清除
        itor->second->_mem_block = 0;
        itor->second->_cur_block_size = 0;
        itor++;
    }
    delete pkt;
}

//查找主播节点
Host_Node_Addr* CBaseRoom::get_host_node(const USER_ID &uid_)
{
    if (_host_node && _host_node->_user_id == uid_)
    {
        return _host_node;
    }else
    {
        return NULL;
    }
}

uint32_t CBaseRoom::get_nodes_count()
{
    return _node_umap.size() + _host_node?1:0;
}

// 检测掉线节点
void CBaseRoom::detect_nodes_state()
{
    uint64_t cur_tv = sev_get_epoch_time();
    UMAP_ITOR itor = _node_umap.begin();
    if (NULL == _host_node)   //host node is null,need hold all nodes's link
    {
        while(itor != _node_umap.end())
        {
            if (NULL == itor->second)
            {
                //异常
                itor = _node_umap.erase(itor);
                _cur_player_count--;
                continue;
            }
            //system time reset
            if (cur_tv < itor->second->_last_tv)
            {
                itor->second->_last_tv = cur_tv;
            }else if (cur_tv - itor->second->_last_tv > NODE_DEAD_TIME * 1000) //node dead
            {
//                 if (itor->second->_get_req)
//                 {
//                     try{
//                         evhttp_request_free(itor->second->_get_req);
//                     } catch (...)
//                     {
//                         BOOST_ERROR << "delete timeout node " << itor->first
//                             << " from room " << this->_local_room_id << ", exception occurred";
//                     }
//                 }
//                 BOOST_DEBUG << "delete timeout node " << itor->first 
//                     << " from room " << this->_local_room_id << ".";
                delete itor->second;
                itor = _node_umap.erase(itor);
                _cur_player_count--;
                continue;
            }else if (cur_tv - itor->second->_last_tv >= DETECT_LINK_TIME * 1000)
            {
                //定时给node回复空数据，保持链接
                if (itor->second->_get_req)
                {
                    packet_protocal_evbuffer();
                    evhttp_send_reply(itor->second->_get_req, HTTP_HEATBEAT_CODE_NULL, "", _response_evbuf);
                    itor->second->_get_req = NULL;
                }
            }
            itor++;
        }
    }else   //host node not null.
    {
        //首先通过host node的时间戳判断，如果在DETECT_LINK_TIME时间内，暂不检测其它所有节点的超时状态，
        //因为在forward_packet中转发数据时，已经检测过了，若果超过这个时间，则继续检测普通节点是否超时
        if (cur_tv < _host_node->_last_tv) //system time reset
        {
            _host_node->_last_tv = cur_tv;
        }else if (cur_tv - _host_node->_last_tv < DETECT_LINK_TIME * 1000)
        {
            //在forward_packet接口中顺道检测死节点，减少此处工作量
            return;
        }else if (cur_tv - _host_node->_last_tv > NODE_DEAD_TIME * 1000)
        {
//             BOOST_DEBUG << "delete timeout host node " << _host_node->_user_id 
//                 << " from room " << this->_local_room_id << ".";

            delete _host_node;
            _host_node = NULL;
        }
        uint64_t diff_tv = _host_node?(cur_tv - _host_node->_last_tv):(0);
        while(itor != _node_umap.end())
        {
            if (NULL == itor->second)
            {
                itor = _node_umap.erase(itor);
                _cur_player_count--;
                continue;
            }
            //system time reset
            if (cur_tv < itor->second->_last_tv)
            {
                itor->second->_last_tv = cur_tv;
            }else if (cur_tv - itor->second->_last_tv > diff_tv &&
                cur_tv - itor->second->_last_tv - diff_tv > NODE_DEAD_TIME * 1000) //dead node
            {
//                 if (itor->second->_get_req)
//                 {
//                     try
//                     {
//                         evhttp_request_free(itor->second->_get_req);
//                     }
//                     catch (...)
//                     {
//                         BOOST_ERROR << "delete timeout node " << itor->first 
//                             << " from room " << this->_local_room_id << ", exception occurred";
//                     }
//                 }
//                 BOOST_DEBUG << "delete timeout node " << itor->first 
//                     << " from room " << this->_local_room_id << ".";

                delete itor->second;
                itor = _node_umap.erase(itor);
                _cur_player_count--;
                continue;
            }else if (cur_tv - itor->second->_last_tv >= DETECT_LINK_TIME * 1000)
            {
                //定时给node回复空数据，保活链接
                if (itor->second->_get_req)
                {
                    packet_protocal_evbuffer();
                    evhttp_send_reply(itor->second->_get_req, HTTP_HEATBEAT_CODE_NULL, "", _response_evbuf);
                    itor->second->_get_req = NULL;
                }
            }
            itor++;
        }
    }
}

void CBaseRoom::clear_timeout_buffer()
{

}

void CBaseRoom::set_host_id(const USER_ID& uid_, bool bOnMic_)
{
    if (!bOnMic_) //下麦
    {
        //BOOST_DEBUG << "room:" << _local_room_id << ",  host:" << _cur_host_id << " off mic.";
        _cur_host_id = 0;
    }else //上麦
    {
        _cur_host_id = uid_;
        //BOOST_DEBUG << "room " << _local_room_id << "  host " << uid_ << " on mic.";
    }
}

void CBaseRoom::set_buffer_num(const int32_t& num_)
{
    if (num_ > 5 || num_ < 0)
    {
        return ;
    }
    _buffer_num = num_;
    if (_host_node)
    {
        _host_node->_easy_buffer_count = num_;
    }
}

void CBaseRoom::reinit()
{

}

