#pragma  once


////////////
//引入AB接口
///////////
//例：如有一个类C,原本有一个函数func1,现在需要添加新的函数func2,需要保证旧的能用
/////////添加友元是否不会影响导出的接口地址
class AInterface
{
public:
	virtual void AAddRef()=0;  //添加引用计数
	virtual void ARelease()=0; //释放
	virtual int  GetExVersion()=0; //得到扩展版本
};

//B谁创建，谁释放,外面只是获取，无需释放
class BInterface
{
public:
	virtual void BDestory()=0;
	virtual int  GetExVersion()=0; //得到扩展版本
};

