/*
* create by wangweijin
*/

//树型节点模板
#pragma once

#include  <windows.h>
#include <vector>
#include <string>

//#include "..\include\GameGuiItems.h"

using namespace std;

//struct stTreeNodeData
//{
//	int _TestData;//测试数据
//	//hgeItemBase* _pBase; //控件指针
//
//	stTreeNodeData()
//	{
//		_TestData=0;
//		//_pBase=0;
//	}
//
//	stTreeNodeData(const stTreeNodeData& info)
//	{
//		*this=info;
//	}
//
//	stTreeNodeData& operator= (const stTreeNodeData& info)
//	{
//		if(this==&info)
//			return *this;
//
//		_TestData=info._TestData;
//		//_pBase=info._pBase;
//		return *this;
//	}
//};

//树节点
template<class T>
class TreeNode
{
public:
	TreeNode(const string& name);
	~TreeNode();

	void AddOneChildNode(TreeNode* pChildNode);//增加一个儿子节点

	bool AddOneChildNode(TreeNode* pNode,TreeNode* pChildNode);//给一个指定节点增加儿子节点

	bool IsExistNode(TreeNode* pNode);//是否存在这样的节点

	bool DeleteOneChildNode(TreeNode* pChildNode);//删除一个儿子节点

	bool DeleteOneChildNode(TreeNode* pNode,TreeNode* pChildNode);//从指定节点上删除他的一个儿子节点

	void SetItemData(const T& data);//设数据

    void SetItemData(TreeNode* pNode,const T& data);//设指定节点数据

	T    GetItemData();//得到数据

	T&   GetItemData_Ref();//得到数据，引用版

	T    GetItemData(TreeNode* pNode);//得到指定节点的数据

	TreeNode*  FindNodeWithName(const string& name);//通过名称查找节点

	string GetName();//得到名字

	bool   IsZhanKai();//是否张开

	vector<TreeNode*> GetAllChildNode();//得到所有的儿子节点

	int    GetAllNodeNum();//得到所有的节点数，含子子孙孙

	void   GetSelfNodeNum(int &num);//得到自己的节点数

	void GetAllNode(int ParentDaiShu,vector<TreeNode*>& NodeVec);//得到所有节点,传父亲的代数

	void ModifyAllNodeDaiShu(int ParentDaiShu);//修订一次所有节点的代数

	void GetAllNeedRenderNode(int ParentDaiShu,vector<TreeNode*>& NodeVec);//得到所有需要渲染的节点

	bool   IsHasChild();//是否有孩子

	int    GetDaiShu();//得到代数

	void   SetIsZhanKai(bool bZhanKai);//设是否张开

	bool   IsHouDai(TreeNode* pNode);//判一个节点是否是自己的后代

	void   SetParentNode(TreeNode* pParentNode);//设父节点

	void   DestoryTree();//销毁树

	TreeNode* GetParentNode();//得到父节点

	void   ChangeNodeName(string strNodeName);//变更节点名,只在特殊需求是使用

	TreeNode* CopyNode(TreeNode* pNewParentNode); //复制节点

	TreeNode*   CopyAllNode(TreeNode* pNewParentNode);//递归复制所有节点

	void    ChangeName(string& newName);//改变名字

	bool    UpNodeOne(TreeNode* pNode,TreeNode** ppChangeNode);     //将节点上移一个，只在兄弟之间进行
	bool    UpNodeToFirst(TreeNode* pNode); //将节点上移到同级的第一个,只在兄弟间进行

	bool    DownNodeOne(TreeNode* pNode,TreeNode** ppChangeNode);   //将节点下移一个，只在兄弟之间进行,并得到实际交换的那个节点
	bool    DownNodeToEnd(TreeNode* pNode); //将节点下移到同级的最后一个,只在兄弟间进行

	bool    UpNodeHalf(TreeNode* pNode);                   //折半上移
	bool    DownNodeHalf(TreeNode* pNode);                 //折半下移

	TreeNode<T>* GetPreBrotherNode(TreeNode* pNode); //得到前一个兄弟节点
	TreeNode<T>* GetNextBrotherNode(TreeNode* pNode); //得到下一个兄弟节点

	void      JiangJiToPreBrotherChild(TreeNode* pNode); //降为前一个兄弟节点的小儿子
	void      ShengJiToParentBrother(TreeNode* pNode); //升级为其父的兄弟

protected:
	string _NodeName; //节点名
	bool   _bOwnerZhanKai; //节点是否展开,有子节点，这个属性才有用
	T      _ItemData;//数据
	int    _DaiShu;//代数，0为不清楚,根为第一代

	vector<TreeNode*> _ChildNodeVec;//存所有的孩子节点

	TreeNode*         _pParentNode; //父节点
};

template<class T>
TreeNode<T>::TreeNode(const string& name)
:_NodeName(name)
,_bOwnerZhanKai(false)
,_DaiShu(0)
,_pParentNode(0)
{

}

template<class T>
TreeNode<T>::~TreeNode()
{
	_ChildNodeVec.clear();

}

template<class T>
void TreeNode<T>::AddOneChildNode(TreeNode* pChildNode)//增加一个子节点
{
	pChildNode->SetParentNode(this);

	_ChildNodeVec.push_back(pChildNode);
}

template<class T>
bool TreeNode<T>::AddOneChildNode(TreeNode* pNode,TreeNode* pChildNode)//给一个指定节点增加儿子节点
{
	//先判定节点是否存在
	if(IsExistNode(pNode))
	{
		pNode->AddOneChildNode(pChildNode);	 
		return true;
	}

	return false;

}

//这是一个递归判断
template<class T>
bool TreeNode<T>::IsExistNode(TreeNode* pNode)
{
	if(pNode==this)
		return true;
	////先从儿子中找一遍
	//for(size_t i=0;i<_ChildNodeVec.size();i++)
	//{
	//	if(_ChildNodeVec[i]==pNode)
	//		return true;
	//}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->IsExistNode(pNode))	
			return true;
	}

	return false;
}

template<class T>
bool TreeNode<T>::DeleteOneChildNode(TreeNode* pChildNode)//删除一个儿子节点
{
	vector<TreeNode*>::iterator iter;
	for(iter=_ChildNodeVec.begin();iter!=_ChildNodeVec.end();iter++)
	{
		if((*iter)==pChildNode)
		{
			_ChildNodeVec.erase(iter);
			return true;
		}
	}

	return false;
}

template<class T>
bool TreeNode<T>::DeleteOneChildNode(TreeNode* pNode,TreeNode* pChildNode)//从指定节点上删除他的一个儿子节点
{
	if(IsExistNode(pNode))
	{
		return pNode->DeleteOneChildNode(pChildNode);
	}

	return false;
}

template<class T>
void TreeNode<T>::SetItemData(const T& data)//设数据
{
	_ItemData=data;

}

template<class T>
void TreeNode<T>::SetItemData(TreeNode* pNode,const T& data)//设指定节点数据
{
	if(IsExistNode(pNode))
	{
		pNode->SetItemData(data);
	}
}

template<class T>
T    TreeNode<T>::GetItemData()//得到数据
{
	return _ItemData;

}

template<class T>
T&   TreeNode<T>::GetItemData_Ref()//得到数据，引用版
{
	return _ItemData;
}

template<class T>
T    TreeNode<T>::GetItemData(TreeNode* pNode)//得到指定节点的数据
{
	if(IsExistNode(pNode))
	{
		return pNode->GetItemData();
	}
	T temp;
	return temp;
}

template<class T>
TreeNode<T>*  TreeNode<T>::FindNodeWithName(const string& name)//通过名称查找节点
{
	//确认是否是自己
	if(_NodeName.compare(name)==0)
	{
		return this;
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		TreeNode* p=0;
		p=_ChildNodeVec[i]->FindNodeWithName(name);

		if(p)
		{
			return p;
		}
	}

	return 0;
}

template<class T>
string TreeNode<T>::GetName()//得到名字
{
	return _NodeName;
}

template<class T>
bool   TreeNode<T>::IsZhanKai()//是否张开
{
   return _bOwnerZhanKai;
}

template<class T>
vector<TreeNode<T>*> TreeNode<T>::GetAllChildNode()//得到所有的儿子节点
{
	return _ChildNodeVec;
}

template<class T>
int    TreeNode<T>::GetAllNodeNum()
{
	int num=0;
	GetSelfNodeNum(num);
	return num;
}

template<class T>
void   TreeNode<T>::GetSelfNodeNum(int &num)
{
	num+=1;
	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		_ChildNodeVec[i]->GetSelfNodeNum(num);
	}
}

template<class T>
void TreeNode<T>::GetAllNode(int ParentDaiShu,vector<TreeNode<T>*>& NodeVec)//得到所有节点,传父亲的代数
{
	_DaiShu=ParentDaiShu+1;
	//自己加入进去
	NodeVec.push_back(this);

	//按儿子顺序排
	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		_ChildNodeVec[i]->GetAllNode(_DaiShu,NodeVec);
	}
}

template<class T>
void TreeNode<T>::ModifyAllNodeDaiShu(int ParentDaiShu)//修订一次所有节点的代数
{
	_DaiShu=ParentDaiShu+1;
	//按儿子顺序排
	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		_ChildNodeVec[i]->ModifyAllNodeDaiShu(_DaiShu);
	}
}

template<class T>
void TreeNode<T>::GetAllNeedRenderNode(int ParentDaiShu,vector<TreeNode<T>*>& NodeVec)//得到所有需要渲染的节点
{
	_DaiShu=ParentDaiShu+1;

	//自己加入进去
	NodeVec.push_back(this);

	//是展开的，才可见
	if(_bOwnerZhanKai)
	{
		for(size_t i=0;i<_ChildNodeVec.size();i++)
		{
			_ChildNodeVec[i]->GetAllNeedRenderNode(_DaiShu,NodeVec);
		}
	}
}

template<class T>
bool   TreeNode<T>::IsHasChild()
{
	if(_ChildNodeVec.size()>0)
		return true;

	return false;
}


template<class T>
int    TreeNode<T>::GetDaiShu()//得到代数
{
	return _DaiShu;
}

template<class T>
void   TreeNode<T>::SetIsZhanKai(bool bZhanKai)//设是否张开
{
	_bOwnerZhanKai=bZhanKai;

}

template<class T>
bool   TreeNode<T>::IsHouDai(TreeNode* pNode)//判一个节点是否是自己的后代
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->IsHouDai(pNode))
			return true;
	}

	return false;	
}

template<class T>
void   TreeNode<T>::SetParentNode(TreeNode* pParentNode)//设父节点
{
	_pParentNode=pParentNode;
}

template<class T>
void   TreeNode<T>::DestoryTree()//销毁树
{
	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		_ChildNodeVec[i]->DestoryTree();
	}

	delete this;
}

template<class T>
TreeNode<T>* TreeNode<T>::GetParentNode()//得到父节点
{
	return _pParentNode;
}

template<class T>
void   TreeNode<T>::ChangeNodeName(string strNodeName)//变更节点名,只在特殊需求是使用
{
	_NodeName=strNodeName;
}

template<class T>
TreeNode<T>* TreeNode<T>::CopyNode(TreeNode<T>* pNewParentNode) //复制节点
{
	TreeNode<T>* pNewNode=new TreeNode<T>(_NodeName);
	pNewNode->SetIsZhanKai(_bOwnerZhanKai);
	pNewNode->SetItemData(_ItemData);

	if(pNewParentNode)
	   pNewParentNode->AddOneChildNode(pNewNode);

	//int    _DaiShu;//代数，0为不清楚,根为第一代
	return pNewNode;
}

template<class T>
TreeNode<T>*   TreeNode<T>::CopyAllNode(TreeNode* pNewParentNode)//递归复制所有节点
{
	TreeNode<T>* pNewNode;
	pNewNode=CopyNode(pNewParentNode);

	//子节点
	vector<TreeNode*>::iterator iter;
	for(iter=_ChildNodeVec.begin();iter!=_ChildNodeVec.end();iter++)
	{
		//修改
		//(*iter)->CopyNode(pNewNode);	
		(*iter)->CopyAllNode(pNewNode);
	}
	return pNewNode;
}

template<class T>
void    TreeNode<T>::ChangeName(string& newName)//改变名字
{
	_NodeName=newName;

}

template<class T>
bool    TreeNode<T>::UpNodeOne(TreeNode* pNode,TreeNode** ppChangeNode)
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i] == pNode)
		{
			if(i!=0)
			{
				TreeNode* tt_pNode = _ChildNodeVec[i-1];
				_ChildNodeVec[i-1] = _ChildNodeVec[i];
				_ChildNodeVec[i] = tt_pNode;

				if(ppChangeNode)
				{
					(*ppChangeNode) = tt_pNode;
				}
			}
			return true;
		}
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->UpNodeOne(pNode,ppChangeNode))
			return true;
	}

	return false;
}

template<class T>
bool    TreeNode<T>::UpNodeToFirst(TreeNode* pNode)
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i] == pNode)
		{
			if(i!=0)
			{
				//前面全部后移一个
				for(int k=i-1;k>=0;k--)
				{
					_ChildNodeVec[k+1] = _ChildNodeVec[k];
				}
				//置为第一个
				_ChildNodeVec[0] = pNode;
			}
			return true;
		}
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->UpNodeToFirst(pNode))
			return true;
	}

	return false;
}

template<class T>
bool    TreeNode<T>::DownNodeOne(TreeNode* pNode,TreeNode** ppChangeNode)
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i] == pNode)
		{
			if((i+1)<_ChildNodeVec.size())
			{
				TreeNode* tt_pNode = _ChildNodeVec[i+1];
				_ChildNodeVec[i+1] = _ChildNodeVec[i];
				_ChildNodeVec[i] = tt_pNode;

				if(ppChangeNode)
				{
				    (*ppChangeNode) = tt_pNode;
				}
			}
			return true;
		}
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->DownNodeOne(pNode,ppChangeNode))
			return true;
	}

	return false;
}

template<class T>
bool    TreeNode<T>::DownNodeToEnd(TreeNode* pNode) //将节点下移到同级的最后一个,只在兄弟间进行
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i] == pNode)
		{
			//从此处开始前移一个
			for(int k=i+1;k<_ChildNodeVec.size();k++)
			{
				_ChildNodeVec[k-1] = _ChildNodeVec[k];

			}
			//置为最后
			_ChildNodeVec[_ChildNodeVec.size()-1] = pNode;

			return true;
		}
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->DownNodeToEnd(pNode))
			return true;
	}

	return false;
}

template<class T>
bool    TreeNode<T>::UpNodeHalf(TreeNode* pNode)                   //折半上移
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i] == pNode)
		{
			if(i!=0)
			{
				int tt_half = i/2;
				//前面全部后移一个
				for(int k=i-1;k>=tt_half;k--)
				{
					_ChildNodeVec[k+1] = _ChildNodeVec[k];
				}
				//置为第一个
				_ChildNodeVec[tt_half] = pNode;
			}
			return true;
		}
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->UpNodeToFirst(pNode))
			return true;
	}

	return false;
}

template<class T>
bool    TreeNode<T>::DownNodeHalf(TreeNode* pNode)                 //折半下移
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i] == pNode)
		{
			int tt_half = (_ChildNodeVec.size()-i)/2 + i;
			//从此处开始前移一个
			for(int k=i+1;(k<=tt_half) && (k<_ChildNodeVec.size());k++)
			{
				_ChildNodeVec[k-1] = _ChildNodeVec[k];

			}
			//置为最后
			if(tt_half>=_ChildNodeVec.size())
			{
				_ChildNodeVec[_ChildNodeVec.size()-1] = pNode;

			}
			else
			{
				_ChildNodeVec[tt_half] = pNode;
			}
			

			return true;
		}
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->DownNodeToEnd(pNode))
			return true;
	}

	return false;
}

template<class T>
TreeNode<T>* TreeNode<T>::GetPreBrotherNode(TreeNode* pNode) //得到前一个兄弟节点
{
	//如根,如果传入空
	if(this == pNode || (!pNode))
		return NULL;

	//不存在父
	if(!pNode->_pParentNode)
		return NULL;

	TreeNode* tt_pParent = pNode->_pParentNode;

	for(size_t i=0;i<tt_pParent->_ChildNodeVec.size();i++)
	{
		if(tt_pParent->_ChildNodeVec[i] == pNode)
		{
			//无前项
			if(i==0)
				return NULL;

			return tt_pParent->_ChildNodeVec[i-1]; 
		}

	}


	return NULL;
}

template<class T>
TreeNode<T>* TreeNode<T>::GetNextBrotherNode(TreeNode* pNode) //得到下一个兄弟节点
{
	//如根,如果传入空
	if(this == pNode || (!pNode))
		return NULL;

	//不存在父
	if(!pNode->_pParentNode)
		return NULL;

	TreeNode* tt_pParent = pNode->_pParentNode;

	for(size_t i=0;i<tt_pParent->_ChildNodeVec.size();i++)
	{
		if(tt_pParent->_ChildNodeVec[i] == pNode)
		{
			//无后项
			if(i==tt_pParent->_ChildNodeVec.size()-1)
				return NULL;

			return tt_pParent->_ChildNodeVec[i+1]; 
		}

	}


	return NULL;
}

template<class T>
void      TreeNode<T>::JiangJiToPreBrotherChild(TreeNode* pNode)
{
	//如根,如果传入空
	if(this == pNode || (!pNode))
		return;

	//不存在父
	if(!pNode->_pParentNode)
		return;

	TreeNode* tt_pParent = pNode->_pParentNode;

	for(size_t i=0;i<tt_pParent->_ChildNodeVec.size();i++)
	{
		if(tt_pParent->_ChildNodeVec[i] == pNode)
		{
			//无前项
			if(i==0)
				return;

			TreeNode* tt_pPreBrother = tt_pParent->_ChildNodeVec[i-1];
			tt_pParent->_ChildNodeVec.erase(tt_pParent->_ChildNodeVec.begin() + i);

			pNode->_pParentNode = NULL;
			tt_pPreBrother->AddOneChildNode(pNode);
		}

	}
}

template<class T>
void      TreeNode<T>::ShengJiToParentBrother(TreeNode* pNode)
{
	//如根,如果传入空
	if(this == pNode || (!pNode))
		return;

	//不存在父
	if(!pNode->_pParentNode)
		return;

	TreeNode* tt_pParent = pNode->_pParentNode;
	TreeNode* tt_pParentParent = tt_pParent->_pParentNode;
	if(tt_pParentParent)
	{
		for(int i=0;i<tt_pParent->_ChildNodeVec.size();i++)
		{
			if(tt_pParent->_ChildNodeVec[i] == pNode)
			{
				tt_pParent->_ChildNodeVec.erase(tt_pParent->_ChildNodeVec.begin() + i);
				pNode->_pParentNode = NULL;

				tt_pParentParent->AddOneChildNode(pNode);

				break;
			}
		}
	}

}

////树节点管理
//class TreeNodeManage
//{
//public:
//	TreeNodeManage();
//	~TreeNodeManage();
//
//protected:
//	TreeNode* _pRootNode;//根节点
//};



