/*
* create by wwjin 
*/

#pragma once
#include "memory/WJS_MemoryPoolManage.h"
#include <memory.h>

//#ifdef __PRINTLOG__
//#include <stdio.h>
//#endif

#define MAXARRAYNUM 16

//数组与链表的结合体
template <typename T>
class WJS_ArrayList
{
public:
	WJS_ArrayList();
	~WJS_ArrayList();

	void SetArrayNum(int ArrayNum);

	//主要是如果原来的大小满足需求，不适放只改变使用大小
	void SetArrayNumV2(int ArrayNum);

	void Zero();//清零

	int  size() {return _ArrayNum;}

	int  capcity() {return _capcity;}

protected:
	T _Array[MAXARRAYNUM];
	T* _pBegin;
	bool _bUseLocal;
	int _ArrayNum;
	int _capcity;//容量
public:
	T& operator [] (int index)
	{
//		if(index>=_ArrayNum)
//		{
//#ifdef __PRINTLOG__
//			printf("flow WJS_ArrayList %d,%d\n",index,_ArrayNum);
//#endif 
//
//		}
		if(_bUseLocal)
			return _Array[index];
		else
		{
			return *(_pBegin+index);
		}
	}

public:
	WJS_NEW_FROM_MEMORYPOOL
	WJS_DEL_FROM_MEMORYPOOL
};

template <typename T>
WJS_ArrayList<T>::WJS_ArrayList()
:_pBegin(0)
,_bUseLocal(true)
,_ArrayNum(MAXARRAYNUM)
,_capcity(0)
{

}

template <typename T>
WJS_ArrayList<T>::~WJS_ArrayList()
{
	if(_pBegin)
	{
		delete [] _pBegin;
		_pBegin=0;
	}

}

template <typename T>
void WJS_ArrayList<T>::SetArrayNum(int ArrayNum)
{
	if(_ArrayNum==ArrayNum)
		return;
	_ArrayNum=ArrayNum;

	if(ArrayNum>MAXARRAYNUM)
	{	
		_bUseLocal=false;
		if(_pBegin)
		{
			delete [] _pBegin;
		}
		_pBegin=new T[ArrayNum];
		_capcity=ArrayNum;

	}
	else
	{
		_capcity=0;
		_bUseLocal=true;
		if(_pBegin)
		{
			delete []_pBegin;
			_pBegin=0;
		}
	}
}

template <typename T>
void WJS_ArrayList<T>::SetArrayNumV2(int ArrayNum)
{
	if(_ArrayNum==ArrayNum)
		return;
	_ArrayNum=ArrayNum;

	if(ArrayNum>MAXARRAYNUM)
	{
		_bUseLocal=false;

		if(_capcity>=ArrayNum)
		{
			//如果不是大很多不用重新分配
			if(_capcity>ArrayNum+ArrayNum)
			{
				//容量过多，重新分配
				if(_pBegin)
				{
					delete [] _pBegin;
				}
				_pBegin=new T[ArrayNum];
				_capcity=ArrayNum;
			}

		}
		else
		{
			//容量不足，重新分配,并多分配一点
			if(_pBegin)
			{
				delete [] _pBegin;
			}
			_pBegin=new T[ArrayNum+MAXARRAYNUM];
			_capcity=ArrayNum+MAXARRAYNUM;
		}


	}
	else
	{
		_capcity=0;
		_bUseLocal=true;
		if(_pBegin)
		{
			delete []_pBegin;
			_pBegin=0;
		}
	}
}

template <typename T>
void WJS_ArrayList<T>::Zero()//清零
{
	if(_bUseLocal)
	{
		memset(_Array,0,sizeof(T)*_ArrayNum);
	}
	else
	{
		memset(_pBegin,0,sizeof(T)*_ArrayNum);
	}
}