/*
* create by wangweijin
*/

#pragma once
#include "memory/WJS_MemoryPoolManage.h"
//#include "WJS_CallBack.h"
#include <memory.h> 
#define LOCALNUM 16

class WJS_String
{
public:
	WJS_String(char* pChars=0);
	~WJS_String();

	WJS_String& operator= (WJS_String& Rstring)
	{
		if(this==&Rstring)
			return *this;
		m_size=Rstring.size();
		if(m_size<LOCALNUM)
		{
			//使用本地
			m_bUseLocal=true;
			memcpy(m_LocalCh,Rstring.c_str(),(m_size+1)*sizeof(char));
		}
		else
		{
			m_bUseLocal=false;
			if(m_ExtenSize>=m_size)
			{
				memcpy(m_pExtenChars,Rstring.c_str(),(m_size+1)*sizeof(char));
			}
			else
			{
				if(m_pExtenChars)
				{
					delete [] m_pExtenChars;
				}

				m_pExtenChars=new char[m_size+1];
				m_ExtenSize=m_size;
				memcpy(m_pExtenChars,Rstring.c_str(),(m_size+1)*sizeof(char));
			}

		}
		return *this;
	}

	const char* c_str();

	int   size()
	{
		return m_size;
	}

	char& operator [] (const int& index)
	{
		if(m_bUseLocal)
			return m_LocalCh[index];
		else
			return *(m_pExtenChars+index);
	}
	WJS_String& operator += (WJS_String& strR)
	{
		append(strR.c_str());
		return *this;
	}

	void append(const char* pChars);




	inline int  strlen(const char* p)
	{
		int count=0;

		while(*p!='\0')
		{
			count++;
			p++;
		}
		return count;

	}

public:
	//WJS_CallBack0<WJS_String> m_pCall0;

protected:
	bool append_Local(const char* pChars,int size);
	void append_Extern(const char* pChars,int size);


protected:
	char  m_LocalCh[LOCALNUM];//本地的字符数组

	char* m_pExtenChars; //扩展的指针地址
	int   m_ExtenSize;   //扩展分配的大小

	bool  m_bUseLocal; //是使用的本地还是扩展
	int   m_size;      //大小

public:
	WJS_NEW_FROM_MEMORYPOOL
	WJS_DEL_FROM_MEMORYPOOL
};