/*
*  create by wangweijin
*/

#pragma once
#include "../VideoServer/common/memory/WJS_MemoryPoolManage.h"
#include <memory.h>
//#ifdef __PRINTLOG__
//#include <stdio.h>
//#endif


//数组版vector

template <typename T>
class WJS_ArrayVector
{
public:
	WJS_ArrayVector(int initSize=0,bool bCanMemcpy=true);
	~WJS_ArrayVector();

	//批注，只能在第一次构造时使用，否则将会泄露，因为第一次构造时参数还未赋值，所以不能进行释放
	//操作，但非第一次又应执行，所以只能在第一次构造时使用复制构造
	WJS_ArrayVector(const WJS_ArrayVector& Array)
	{
		m_capcity=Array.m_capcity;
		m_pBeginData=0;
		m_bCanMemcpy = Array.m_bCanMemcpy;

		if(m_capcity>0)
		{
			m_pBeginData=new T[m_capcity];
		}

		m_useSize=Array.m_useSize;
		if(m_useSize>0)
		{
			if (m_bCanMemcpy)
			{
				memcpy(m_pBeginData, Array.m_pBeginData, m_useSize * sizeof(T));
			}
			else
			{
				for (int i = 0; i < m_useSize; i++)
				{
					*(m_pBeginData + i) = *(Array.m_pBeginData + i);
				}
			}
			
		}	
	}

	WJS_ArrayVector& operator = (const WJS_ArrayVector& Array)
	{
		if(this==&Array)
			return *this;

		m_capcity=Array.m_capcity;
		if(m_pBeginData)
		{
			delete [] m_pBeginData;
		}
		m_pBeginData=0;

		if(m_capcity>0)
		{
			m_pBeginData=new T[m_capcity];
		}

		m_useSize=Array.m_useSize;
		if(m_useSize>0)
		{
			if (m_bCanMemcpy)
			{
				memcpy(m_pBeginData, Array.m_pBeginData, m_useSize * sizeof(T));
			}
			else
			{
				for (int i = 0; i < m_useSize; i++)
				{
					*(m_pBeginData + i) = *(Array.m_pBeginData + i);
				}
			}
		}

		

		return *this;
	}

public:
	void push_back(const T& Data);//压入
	T    pop_front();//从前面弹出
	T    pop_back();//从后面弹出
	void push_back_adv(const T* Data,int size);//压入一组这样的数组,字节流使用

	void remove_begin(unsigned int num);//从前面移除多少个

	void remove_back(); //从后面移除

	void remove_index(int index); //根据序号移除

	void remove_value(const T& Data); //根据值移除

	//公将使用大小清为0
	void OnlySetUseSize0()
	{
		m_useSize=0;
	}

public:
	bool empty() {return m_useSize==0;}
	const int  size() const {return m_useSize;} 
	int  capacity() const {return m_capcity;}
	void clear();
	void resize(int newSize,bool bSetUseSizeSame/*=false*/);//重设大小
	void resizeV2(int newSize);//有足够的空间，将不再重新分配,除非这个空间是现在大小的四倍,也就是太多了

	////////////增加一个读一个int值，且并不将数据移除
	int  readintonly() const;

	void setforeigndata(T* pBeginData,int usesize,int usecapacity,bool bclearold=true)
	{
		if(bclearold)
		   clear();
		m_pBeginData = pBeginData;
		m_useSize = usesize;
		m_capcity = usecapacity;
	}

	void SetCanMemcpy(bool bCanMemcpy)
	{
		m_bCanMemcpy = bCanMemcpy;

	}

public:
    T&   operator [] (const int& index)
	{
//#ifdef __PRINTLOG__
//		if(index>=m_useSize)
//		   printf("flow WJS_ArrayVector %d,%d\n",index,m_useSize);
//#endif 

		return m_pBeginData[index];
	}
	const T& operator [] (const int& index) const
	{
		return m_pBeginData[index];
	}

	operator T*()
	{
		return m_pBeginData;
	}

	operator const T*()
	{
		return m_pBeginData;
	}
	operator T*() const 
	{
		return m_pBeginData;
	}

	operator const T*() const
	{
		return m_pBeginData;
	}


protected:
	bool ReMalloc(int newSize,bool bCopy);//重新分配,后一个是是否拷贝,解决压入数据时的空间不足问题

protected:
	T*  m_pBeginData;//起始指针
	int m_useSize;//使用大小
	int m_capcity;//分配大小
	bool m_bCanMemcpy; //是否可以memcpycopy
public:
	WJS_NEW_FROM_MEMORYPOOL
	WJS_DEL_FROM_MEMORYPOOL
};



template <typename T>
WJS_ArrayVector<T>::WJS_ArrayVector(int initSize,bool bCanMemcpy)
:m_useSize(0)
,m_pBeginData(0)
,m_capcity(0)
,m_bCanMemcpy(bCanMemcpy)
{
	if(initSize>0)
	{
		m_pBeginData=new T[initSize];
		if(m_pBeginData)
		{
		  m_capcity=initSize;
		}
	}
}

template <typename T>
WJS_ArrayVector<T>::~WJS_ArrayVector()
{
   clear();
}

template <typename T>
int  WJS_ArrayVector<T>::readintonly() const
{
	if(m_pBeginData && m_useSize>=4)
	{
		int tt_value = 0;
		memcpy(&tt_value,m_pBeginData,sizeof(int));

		return tt_value;
	}

	return 0;
}

template <typename T>
void WJS_ArrayVector<T>::clear()
{
	if(m_pBeginData)
	{
		delete [] m_pBeginData;
		m_pBeginData=0;
		m_useSize=0;
		m_capcity=0;
	}
}

template <typename T>
void WJS_ArrayVector<T>::resize(int newSize,bool bSetUseSizeSame)//重设大小
{
	if(newSize>0)
	  ReMalloc(newSize,false);

	if(bSetUseSizeSame)
	   m_useSize=newSize;
	else
	   m_useSize=0;

}

template <typename T>
void WJS_ArrayVector<T>::resizeV2(int newSize)
{
	if(newSize<=0)
		return;

	//使用空间过小或都不足，重新分配
	if(newSize*4<m_capcity || m_capcity<newSize)
	{
		resize(newSize,true);
	}
	else
	{
		m_useSize=newSize;
	}
}

template <typename T>
bool WJS_ArrayVector<T>::ReMalloc(int newSize,bool bCopy)//重新分配,后一个是是否拷贝
{
	T* pBegin=new T[newSize];
	if(!pBegin)
		return false;

	if(m_pBeginData)
	{
		if(bCopy)
		{
			
			if (m_bCanMemcpy)
			{
				memcpy(pBegin, m_pBeginData, m_useSize * sizeof(T));
			}
			else
			{
				for (int i = 0; i < m_useSize; i++)
				{
					*(pBegin + i) = *(m_pBeginData + i);
				}
			}
		}

		delete [] m_pBeginData;
	}


	m_pBeginData=pBegin;
	m_capcity=newSize;
	return true;
}

template <typename T>
void WJS_ArrayVector<T>::push_back(const T& Data)//压入
{
	if(m_capcity==0)
	{
		if(!ReMalloc(2,false))
			return;
	}
	//没有空间,重新分配
	else if(m_useSize>=m_capcity)
	{
		if(!ReMalloc(m_useSize*2,true))
			return;
	}

	m_pBeginData[m_useSize]=Data;
	m_useSize++;
}

template <typename T>
T    WJS_ArrayVector<T>::pop_front()
{
	T t_data;
	if(m_useSize>0)
	{
		t_data=m_pBeginData[0];

		if (m_bCanMemcpy)
		{
			memcpy(m_pBeginData, m_pBeginData + 1, (m_useSize - 1) * sizeof(T));
		}
		else
		{
			for (int i = 0; i < m_useSize -1; i++)
			{
				*(m_pBeginData + i) = *(m_pBeginData + (i+1));
			}
		}

		
		m_useSize--;
	}

	return t_data;
}

template <typename T>
void WJS_ArrayVector<T>::remove_begin(unsigned int num)
{
	if(num>m_useSize || (num<=0))
		return;
	if(m_useSize!=num)
	{
		if (m_bCanMemcpy)
		{
			memcpy(m_pBeginData, m_pBeginData + num, (m_useSize - num) * sizeof(T));
		}
		else
		{
			for (int i = 0; i < m_useSize - num; i++)
			{
				*(m_pBeginData + i) = *(m_pBeginData + (i + num));
			}
		}

		
	}
	m_useSize-=num;
}

template <typename T>
void WJS_ArrayVector<T>::remove_back()
{
	if(m_useSize>0)
	{
		m_useSize--;
	}
}

template <typename T>
T    WJS_ArrayVector<T>::pop_back()//从后面弹出
{
	T t_data;
	if(m_useSize>0)
	{
		t_data=m_pBeginData[m_useSize-1];
		m_useSize--;
	}

	return t_data;
}


template <typename T>
void WJS_ArrayVector<T>::push_back_adv(const T* Data,int size)
{
	if(m_capcity==0)
	{
		if(!ReMalloc(size,false))
			return;
	}
	////没有空间,重新分配
	else if(m_useSize+size>=m_capcity)
	{
		if(!ReMalloc(m_useSize*2+size,true))
			return;
	}

	for(int i=0;i<size;i++)
	{
		m_pBeginData[m_useSize++]=*Data;
		Data++;
	}
}



template <typename T>
void WJS_ArrayVector<T>::remove_index(int index) //根据序号移除
{
	if(index>=0 && index<m_useSize)
	{
		if(index==0)
		{
			remove_begin(1);
		}
		else if(index==m_useSize-1)
		{
			remove_back();
		}
		else
		{
			if (m_bCanMemcpy)
			{
				memcpy(&m_pBeginData[index], &m_pBeginData[index + 1], sizeof(T)*(m_useSize - 1 - index));
			}
			else
			{
				for (int i = index; i < m_useSize - index; i++)
				{
					*(m_pBeginData + i) = *(m_pBeginData + (i + 1));
				}
			}

			
			m_useSize--;
		}
	}
}

template <typename T>
void WJS_ArrayVector<T>::remove_value(const T& Data) //根据值移除
{
	if(m_useSize>0)
	{
		for(int i=0;i<m_useSize;i++)
		{
			if((*(m_pBeginData+i)) == Data)
			{
				remove_index(i);
				break;
			}
		}
	}
}