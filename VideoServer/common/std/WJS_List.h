/*
* create by wwjin 
*/

#pragma once
#include "memory/WJS_MemoryPoolManage.h"
#include <memory.h>

//单向结点
template <typename T>
class WJS_List_SNode
{
public:
	WJS_List_SNode(){m_pNext=0;}
	~WJS_List_SNode(){}

public:
	T   m_data;
	WJS_List_SNode* m_pNext;
public:
	WJS_NEW_FROM_MEMORYPOOL
	WJS_DEL_FROM_MEMORYPOOL
};

template <typename T>
class WJS_List
{
public:
	WJS_List();
	~WJS_List();

	void push_begin(const T& data);

	void push_end(const T& data);

	void pop_begin();

	void pop_end();

	int  size() {return m_num;}

	bool empty() {return m_num==0;}

	bool remove(T& data);

	bool remove(WJS_List_SNode<T>* pNode);

	void clear();

	WJS_List_SNode<T>* begin(){m_pSearch=m_pBegin;return m_pSearch;}

	WJS_List_SNode<T>* end(){return 0;}

	WJS_List_SNode<T>* next();

	WJS_List_SNode<T>* search(T& Data);

	void insert(T& Data);

	//小于0为小于,0为相等，大于0为大于
	typedef int (*ListCompare) (T* Data1,T* Data2);

	void SetCompareFunc(ListCompare pCompareFunc){m_pCompareFunc=pCompareFunc;}

	WJS_List_SNode<T>* GetEnd() {return m_pEnd;}

	void MoveToOtherList(WJS_List<T>& OtherList);
protected:
	WJS_List_SNode<T>* m_pBegin; //起始
	WJS_List_SNode<T>* m_pEnd;
	int               m_num;  //数量

	WJS_List_SNode<T>* m_pSearch;


	ListCompare    m_pCompareFunc;

public:
	WJS_NEW_FROM_MEMORYPOOL
	WJS_DEL_FROM_MEMORYPOOL
};

template <typename T>
WJS_List<T>::WJS_List()
:m_pBegin(0)
,m_pEnd(0)
,m_num(0)
,m_pCompareFunc(0)
,m_pSearch(0)
{

}

template <typename T>
WJS_List<T>::~WJS_List()
{
	clear();

}

template <typename T>
void WJS_List<T>::push_begin(const T& data)
{
	WJS_List_SNode<T>* node=new WJS_List_SNode<T>();
	if(node)
	{
		node->m_data=data;

		node->m_pNext=m_pBegin;
		if(!m_pBegin)
		{
			m_pEnd=node;
		}
		m_pBegin=node;
		m_num++;
	}
}

template <typename T>
void WJS_List<T>::push_end(const T& data)
{
	WJS_List_SNode<T>* node=new WJS_List_SNode<T>();
	if(node)
	{
		node->m_data=data;

		if(!m_pBegin)
		{
			m_pBegin=node;
			m_pEnd=node;
		}
		else
		{
			m_pEnd->m_pNext=node;
			m_pEnd=node;
			//WJS_List_SNode<T>* pp=m_pBegin;
			//while(pp->m_pNext)
			//{
			//	pp=pp->m_pNext;
			//}
			//pp->m_pNext=node;
		}
		m_num++;
	}

}

template <typename T>
void WJS_List<T>::pop_begin()
{
	if(m_pBegin)
	{
		WJS_List_SNode<T>* pp=m_pBegin;
		m_pBegin=m_pBegin->m_pNext;
		delete pp;
		m_num--;

		if(!m_pBegin)
		{
			m_pBegin=NULL;
			m_pEnd=NULL;
		}
		else if(!m_pBegin->m_pNext)
		{
			m_pEnd=m_pBegin;
		}
	}
}

template <typename T>
void WJS_List<T>::pop_end()
{
	if(!m_pBegin)
		return;
	WJS_List_SNode<T>* pp=m_pBegin;
	WJS_List_SNode<T>* pre=0;
	while(pp->m_pNext)
	{
		pre=pp;
		pp=pp->m_pNext;
	}

	if(pre)
	{
		pre->m_pNext=0;
		m_pEnd=pre;
	}
	else
	{
		m_pBegin=0;
		m_pEnd=0;
	}
	delete pp;
	m_num--;

}

template <typename T>
bool WJS_List<T>::remove(T& data)
{
	if(m_pBegin)
	{
		WJS_List_SNode<T>* pp=m_pBegin;
		WJS_List_SNode<T>* plast=0;

		while(pp)
		{
			if(m_pCompareFunc)
			{
				if(m_pCompareFunc(&(pp->m_data),&data)==0)
				{
					if(plast)
					{
						plast->m_pNext=pp->m_pNext;		
					}
					else
					{
						m_pBegin=pp->m_pNext;
					}
					if(m_pEnd==pp)
					{
						m_pEnd=plast;
					}
					if(m_pSearch==pp)
					{
						m_pSearch=m_pSearch->m_pNext;
					}
					delete pp;
					m_num--;
					return true;
				}
			}
			else
			{
				if(pp->m_data==data)
				{
					if(plast)
					{
						plast->m_pNext=pp->m_pNext;
					}
					else
					{
						m_pBegin=pp->m_pNext;
					}
					if(m_pEnd==pp)
					{
						m_pEnd=plast;
					}
					if(m_pSearch==pp)
					{
						m_pSearch=m_pSearch->m_pNext;
					}
					delete pp;
					m_num--;
					return true;
				}
			}
			plast=pp;
			pp=pp->m_pNext;
		}

	}

	return false;

}

template <typename T>
bool WJS_List<T>::remove(WJS_List_SNode<T>* pNode)
{
	if(m_pBegin)
	{
		WJS_List_SNode<T>* pp=m_pBegin;
		WJS_List_SNode<T>* plast=0;

		while(pp)
		{
			if(pp==pNode)
			{
				if(plast)
				{
					plast->m_pNext=pp->m_pNext;
				}
				else
				{
						m_pBegin=pp->m_pNext;
				}
				if(m_pEnd==pp)
				{
					m_pEnd=plast;
				}

				if(m_pSearch==pp)
				{
					m_pSearch=m_pSearch->m_pNext;
				}
				delete pp;
				m_num--;
				return true;
			}

			plast=pp;
			pp=pp->m_pNext;
		}

	}

	return false;
}

template <typename T>
void WJS_List<T>::clear()
{
	if(m_pBegin)
	{
		WJS_List_SNode<T>* pp;
		while(m_pBegin)
		{
			pp=m_pBegin;
			m_pBegin=m_pBegin->m_pNext;
			delete pp;
		}

		
	}

	m_pBegin=0;
	m_pEnd=0;
	m_num=0;

}

//template <typename T>
//WJS_List_SNode<T>* WJS_List<T>::begin()
//{
//	m_pSearch=m_pBegin;
//	return m_pSearch;
//}

//template <typename T>
//WJS_List_SNode<T>* WJS_List<T>::end()
//{
//	return 0;
//}

template <typename T>
WJS_List_SNode<T>* WJS_List<T>::next()
{
	if(m_pSearch)
	{
		m_pSearch=m_pSearch->m_pNext;
	}
	return m_pSearch;
}

template <typename T>
WJS_List_SNode<T>* WJS_List<T>::search(T& Data)
{
	if(m_pBegin)
	{
		WJS_List_SNode<T>* pp=m_pBegin;
		while(pp)
		{
			if(m_pCompareFunc)
			{
				if(m_pCompareFunc(&(pp->m_data),&Data)==0)
				{
					return pp;
				}
			}
			else
			{
				if(pp->m_data==Data)
				{
					return pp;
				}
			}
			pp=pp->m_pNext;
		}
	}

	return 0;
}

template <typename T>
void WJS_List<T>::insert(T& Data)
{
	if(m_pBegin)
	{
		WJS_List_SNode<T>* node=new WJS_List_SNode<T>();
		if(node)
		{
			node->m_data=Data;

			WJS_List_SNode<T>* pp=m_pBegin;
			WJS_List_SNode<T>* ppre=0;

			while(pp)
			{
				if(m_pCompareFunc)
				{
					if(m_pCompareFunc(&Data,&(pp->m_data))<0)
					{
						m_num++;
						node->m_pNext=pp;
						if(ppre)
							ppre->m_pNext=node;
						else
							m_pBegin=node;

						if(!node->m_pNext)
							m_pEnd=node;
						return;
					}
					else
					{
						//需插入的比较大，且没有后续了,直接插到最后
						if(!pp->m_pNext)
						{
							m_num++;
							pp->m_pNext=node;
							m_pEnd=node;
							break;
						}
					}
				}
				else 
				{
					if(Data<pp->m_data)
					{
						m_num++;
						node->m_pNext=pp;
						if(ppre)
							ppre->m_pNext=node;
						else
							m_pBegin=node;
						if(!node->m_pNext)
							m_pEnd=node;
						return;
					}
					else
					{
						//需插入的比较大，且没有后续了,直接插到最后
						if(!pp->m_pNext)
						{
							m_num++;
							pp->m_pNext=node;
							m_pEnd=node;
							break;
						}
					}

				}

				ppre=pp;
				pp=pp->m_pNext;
			}	
		}
	}
	else
	{
		push_begin(Data);
	}

}

template <typename T>
void WJS_List<T>::MoveToOtherList(WJS_List<T>& OtherList)
{
	//没有数据
	if(!m_pBegin)
		return ;

	if(OtherList.m_pBegin)
	{
		OtherList.m_pEnd->m_pNext=m_pBegin;
		OtherList.m_pEnd=m_pEnd;
		OtherList.m_num+=m_num;
	}
	else
	{
		OtherList.m_pBegin=m_pBegin;
		OtherList.m_pEnd=m_pEnd;
		OtherList.m_num=m_num;
	}

	m_pBegin=0; //起始
	m_pEnd=0;
	m_num=0;  //数量
	m_pSearch=0;
}