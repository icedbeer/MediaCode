/*
* create by wangweijin
*/
#pragma once
#include "memory/WJS_MemoryPoolManage.h"
#include <memory.h>


//堆，可用来排序,下标为0未使用，要切记
//第i个节点的子女为2i和2i+1,第i个节点的双亲为i/2
//0< <=size
template <typename T>
class WJS_Heap
{
public:
	WJS_Heap(int initsize=0);
	~WJS_Heap();

	//小于0为1小于2,0为相等，大于0为大于
	typedef int (*HeapCompare) (T* Data1,T* Data2);
public:
	void clear();
	bool empty() {return m_useSize==0;}
	int  size()  {return m_useSize;}
	int  capcity() {return m_capcity;}

public:
	void resetcapcity(int size);//重设容量,容量不足时重新分配，足时就不做任何处理
	void resize(int size,bool bSetUseSizeSame);//重设大小，并设定使用大小是否为现在大小，否则为0
	void resizeV2(int newSize);//有足够的空间，将不再重新分配,除非这个空间是现在大小的四倍也就是太多了
public:
	//插入
	void insert(const T& data);

	//提取最大元素
	T* PickMax();

	//删除最大元素,也就是首元素，并保证还是堆
	void pop();

	//生成堆
	void make_heap(T* pSrcData,int len);

	//生成堆cf版
	void make_heap_cf(T* pSrcData,int len,HeapCompare pCompareFunc);
	
	//内部建堆
	void make_heap_iner(int len);

	//内部建堆,使用比较函数的版本
	void make_heap_iner_cf(int len);

	//排序
	void sort_iner();//必须是调用make_heap后才能调

	void sort(T* pSrcData,int len);

	void sort_cf(T* pSrcData,int len,HeapCompare pCompareFunc);

	//void backPar();//备份参数
	//void resumePar();//恢复参数

public:    
	T&   operator [] (const int& index)
	{
	   return m_pBeginData[index];
	}
	
protected:
	bool ReMalloc(int newSize,bool bCopy);//重新分配,后一个是是否拷贝

	void percolateDown(int node);//下调
	void percolateDown_cf(int node);//下调cf版

	void percolateUp(int node);//上调

protected:
	T*  m_pBeginData;//起始指针
	int m_useSize;//使用大小
	int m_capcity;//容量
	int t_count;

	HeapCompare m_pCompare;
};

template <typename T>
WJS_Heap<T>::WJS_Heap(int initsize)
:m_useSize(0)
,m_pBeginData(0)
,m_capcity(0)
,m_pCompare(0)
{
	if(initsize>0)
	{
		m_pBeginData=new T[initsize+1];
		if(m_pBeginData)
		{
			m_capcity=initsize;
		}
	}
}

template <typename T>
WJS_Heap<T>::~WJS_Heap()
{
	clear();
}

template <typename T>
void WJS_Heap<T>::clear()
{
	if(m_pBeginData)
	{
		delete [] m_pBeginData;
		m_pBeginData=0;
	}
	m_useSize=0;
	m_capcity=0;
}

template <typename T>
void WJS_Heap<T>::resetcapcity(int size)
{
	if(size>m_capcity)
	{
		ReMalloc(size+1,false);
	}
}

template <typename T>
bool WJS_Heap<T>::ReMalloc(int newSize,bool bCopy)
{
	T* pBegin=new T[newSize];
	if(!pBegin)
		return false;

	if(m_pBeginData)
	{
		if(bCopy)
		{
			memcpy(pBegin,m_pBeginData,m_useSize*sizeof(T));
		}

		delete [] m_pBeginData;
	}

	m_pBeginData=pBegin;
	m_capcity=newSize;
	return true;
}

template <typename T>
void WJS_Heap<T>::resize(int size,bool bSetUseSizeSame)
{
	if(size>0)
		ReMalloc(size+1,false);

	if(bSetUseSizeSame)
		m_useSize=newSize;
	else
		m_useSize=0;
}
template <typename T>
void WJS_Heap<T>::resizeV2(int newSize)
{
	if(newSize<=0)
		return;

	//使用空间过小或都不足，重新分配
	if((newSize+1)*4<m_capcity || m_capcity<newSize+1)
	{
		resize(newSize+1,true);
	}
	else
	{
		m_useSize=newSize;
	}
}

template <typename T>
T* WJS_Heap<T>::PickMax()
{
	if(m_useSize==0)
		return 0;
	return m_pBeginData+1;
}

template <typename T>
void WJS_Heap<T>::pop()
{
	if(m_useSize==0)
		return;
	//只有一个元素
	if(m_useSize==1)
	{
		m_useSize=0;
		return;
	}
	//替换为未位的,成为近似堆
	m_pBeginData[1]=m_pBeginData[m_useSize];
	m_useSize--;

	//节点下调
	percolateDown(1);

}

template <typename T>
void WJS_Heap<T>::insert(const T& data)
{
	m_useSize++;

	if(m_useSize>=m_capcity)
		ReMalloc(m_useSize+m_useSize,true);
	//插到最后
	m_pBeginData[m_useSize]=data;

	//只有一个元素
	if(m_useSize==1)
		return;
	percolateUp(m_useSize);
	

}

template <typename T>
void WJS_Heap<T>::percolateDown(int node)
{
	int leftchild=node*2;
	int rightchild=node*2+1;

	if(leftchild >m_useSize )
	{
		//没有下调的空间，下调结束
		return;

	}
	if(rightchild > m_useSize)
	{
		if(m_pBeginData[leftchild]>m_pBeginData[node])
		{
			//下调到左节点，下调结束
			T temp=m_pBeginData[node];
			m_pBeginData[node]=m_pBeginData[leftchild];
			m_pBeginData[leftchild]=temp;

		}

		return;
	}

	int bignode;
	//如果大于子节点中最大的，则下调结束，否则调换
	if(m_pBeginData[rightchild]>m_pBeginData[leftchild])
	{
		bignode=rightchild;
	}
	else
	{
		bignode=leftchild;
	}

	if(m_pBeginData[node]<m_pBeginData[bignode])
	{
		T temp=m_pBeginData[node];
		m_pBeginData[node]=m_pBeginData[bignode];
		m_pBeginData[bignode]=temp;

		percolateDown(bignode);
	}

}

template <typename T>
void WJS_Heap<T>::percolateDown_cf(int node)
{
	t_count++;
	int leftchild=node*2;
	int rightchild=node*2+1;

	if(leftchild >m_useSize )
	{
		//没有下调的空间，下调结束
		return;

	}
	if(rightchild > m_useSize)
	{
	    if(m_pCompare(&m_pBeginData[leftchild],&m_pBeginData[node])>0)
		//if(m_pCompare(m_pBeginData+leftchild,m_pBeginData+node)>0)
		{
			//下调到左节点，下调结束
			T temp=m_pBeginData[node];
			m_pBeginData[node]=m_pBeginData[leftchild];
			m_pBeginData[leftchild]=temp;
		}

		return;
	}

	int bignode;
	//如果大于子节点中最大的，则下调结束，否则调换
	if(m_pCompare(&m_pBeginData[rightchild],&m_pBeginData[leftchild])>0)
	//if(m_pCompare(m_pBeginData+rightchild,m_pBeginData+leftchild)>0)
	{
		bignode=rightchild;
	}
	else
	{
		bignode=leftchild;	
	}

	if(m_pCompare(&m_pBeginData[node],&m_pBeginData[bignode])<0)
	//if(m_pCompare(m_pBeginData+node,m_pBeginData+bignode)<0)
	{
		T temp=m_pBeginData[node];
		m_pBeginData[node]=m_pBeginData[bignode];
		m_pBeginData[bignode]=temp;
		percolateDown_cf(bignode);
	}
}

template <typename T>
void WJS_Heap<T>::percolateUp(int node)
{
	int parentnode=node>>1;

	//没有父节点
	if(node<1)
		return;

	//大于父节点则调换
	if(m_pBeginData[node]>m_pBeginData[parentnode])
	{
		T temp=m_pBeginData[node];
		m_pBeginData[node]=m_pBeginData[parentnode];
		m_pBeginData[parentnode]=temp;
		percolateUp(parentnode);
	}
	//结束

}

template <typename T>
void WJS_Heap<T>::make_heap(T* pSrcData,int len)
{
	resetcapcity(len);
	m_useSize=len;
	memcpy(&m_pBeginData[1],pSrcData,len*sizeof(T));

	int r=m_useSize/2;

	for(int i=r;i>=1;i--)
	{
		percolateDown(i);
	}
}

template <typename T>
void WJS_Heap<T>::make_heap_cf(T* pSrcData,int len,HeapCompare pCompareFunc)
{
	m_pCompare=pCompareFunc;
	resetcapcity(len);
	m_useSize=len;
	memcpy(&m_pBeginData[1],pSrcData,len*sizeof(T));

	int r=m_useSize/2;

	for(int i=r;i>=1;i--)
	{
		percolateDown_cf(i);
	}
}

template <typename T>
void WJS_Heap<T>::make_heap_iner(int len)
{
	int r=len/2;

	for(int i=r;i>=1;i--)
	{
		percolateDown(i);
	}
}

template <typename T>
void WJS_Heap<T>::make_heap_iner_cf(int len)
{
	int r=len/2;

	for(int i=r;i>=1;i--)
	{
		percolateDown_cf(i);
	}
}

template <typename T>
void WJS_Heap<T>::sort_iner()
{
	int bakuseSize=m_useSize;
	T temp;

	for(int i=m_useSize;i>=2;i--)
	{
		temp=m_pBeginData[1];
		m_pBeginData[1]=m_pBeginData[m_useSize];
		m_pBeginData[m_useSize]=temp;
		m_useSize--;
		make_heap_iner(m_useSize);
	}
	m_useSize=bakuseSize;
}

template <typename T>
void WJS_Heap<T>::sort(T* pSrcData,int len)
{
	make_heap(pSrcData,len);
	int bakuseSize=m_useSize;
	T temp;

	for(int i=m_useSize;i>=2;i--)
	{
		temp=m_pBeginData[1];
		m_pBeginData[1]=m_pBeginData[m_useSize];
		m_pBeginData[m_useSize]=temp;
		m_useSize--;
		make_heap_iner(m_useSize);
	}
	m_useSize=bakuseSize;
}

template <typename T>
void WJS_Heap<T>::sort_cf(T* pSrcData,int len,HeapCompare pCompareFunc)
{
	t_count=0;
	make_heap_cf(pSrcData,len,pCompareFunc);
	int bakuseSize=m_useSize;
	T temp;
	int r;

	for(int i=m_useSize;i>=2;i--)
	{
		temp=m_pBeginData[1];
		m_pBeginData[1]=m_pBeginData[m_useSize];
		m_pBeginData[m_useSize]=temp;
		m_useSize--;
		//make_heap_iner_cf(m_useSize);
		r=m_useSize>>1;

		for(int j=r;j>=1;j--)
		{
			percolateDown_cf(j);
		}
	}
 	m_useSize=bakuseSize;
}

