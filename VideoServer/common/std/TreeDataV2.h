/*
* create by wangweijin
*/

//树型节点模板
#pragma once

#include  <windows.h>
#include <vector>
#include <string>

using namespace std;

//数据改用指针，需提供Copy接口

//树节点
template<class T>
class TreeNodeV2
{
public:
	TreeNodeV2(const string& name);
	~TreeNodeV2();

	void AddOneChildNode(TreeNodeV2* pChildNode);//增加一个儿子节点

	bool AddOneChildNode(TreeNodeV2* pNode,TreeNodeV2* pChildNode);//给一个指定节点增加儿子节点

	bool IsExistNode(TreeNodeV2* pNode);//是否存在这样的节点

	bool DeleteOneChildNode(TreeNodeV2* pChildNode);//删除一个儿子节点

	bool DeleteOneChildNode(TreeNodeV2* pNode,TreeNodeV2* pChildNode);//从指定节点上删除他的一个儿子节点

	void SetItemData(T* data);//设数据

	void SetItemData(TreeNodeV2* pNode,T* data);//设指定节点数据

	T*    GetItemData();//得到数据

	T*    GetItemData(TreeNodeV2* pNode);//得到指定节点的数据

	TreeNodeV2*  FindNodeWithName(const string& name);//通过名称查找节点

	string GetName();//得到名字

	bool   IsZhanKai();//是否张开

	vector<TreeNodeV2*> GetAllChildNode();//得到所有的儿子节点

	int    GetAllNodeNum();//得到所有的节点数，含子子孙孙

	void   GetSelfNodeNum(int &num);//得到自己的节点数

	void GetAllNode(int ParentDaiShu,vector<TreeNodeV2*>& NodeVec);//得到所有节点,传父亲的代数

	void ModifyAllNodeDaiShu(int ParentDaiShu);//修订一次所有节点的代数

	void GetAllNeedRenderNode(int ParentDaiShu,vector<TreeNodeV2*>& NodeVec);//得到所有需要渲染的节点

	bool   IsHasChild();//是否有孩子

	int    GetDaiShu();//得到代数

	void   SetIsZhanKai(bool bZhanKai);//设是否张开

	bool   IsHouDai(TreeNodeV2* pNode);//判一个节点是否是自己的后代

	void   SetParentNode(TreeNodeV2* pParentNode);//设父节点

	void   DestoryTree();//销毁树

	TreeNodeV2* GetParentNode();//得到父节点

	void   ChangeNodeName(string strNodeName);//变更节点名,只在特殊需求是使用

	TreeNodeV2* CopyNode(TreeNodeV2* pNewParentNode); //复制节点

	TreeNodeV2*   CopyAllNode(TreeNodeV2* pNewParentNode);//递归复制所有节点

	void    ChangeName(string& newName);//改变名字

	bool    UpNodeOne(TreeNodeV2* pNode,TreeNodeV2** ppChangeNode);     //将节点上移一个，只在兄弟之间进行
	bool    UpNodeToFirst(TreeNodeV2* pNode); //将节点上移到同级的第一个,只在兄弟间进行

	bool    DownNodeOne(TreeNodeV2* pNode,TreeNodeV2** ppChangeNode);   //将节点下移一个，只在兄弟之间进行
	bool    DownNodeToEnd(TreeNodeV2* pNode); //将节点下移到同级的最后一个,只在兄弟间进行

	bool    UpNodeHalf(TreeNodeV2* pNode);                   //折半上移
	bool    DownNodeHalf(TreeNodeV2* pNode);                 //折半下移

	TreeNodeV2<T>* GetPreBrotherNode(TreeNodeV2* pNode); //得到前一个兄弟节点
	TreeNodeV2<T>* GetNextBrotherNode(TreeNodeV2* pNode); //得到下一个兄弟节点
	void      JiangJiToPreBrotherChild(TreeNodeV2* pNode); //降为前一个兄弟节点的小儿子
	void      ShengJiToParentBrother(TreeNodeV2* pNode); //升级为其父的兄弟

protected:
	string _NodeName; //节点名
	bool   _bOwnerZhanKai; //节点是否展开,有子节点，这个属性才有用
	T*      _ItemData;//数据
	int    _DaiShu;//代数，0为不清楚,根为第一代

	vector<TreeNodeV2*> _ChildNodeVec;//存所有的孩子节点

	TreeNodeV2*         _pParentNode; //父节点
};

template<class T>
TreeNodeV2<T>::TreeNodeV2(const string& name)
:_NodeName(name)
,_bOwnerZhanKai(false)
,_DaiShu(0)
,_pParentNode(0)
,_ItemData(0)
{

}

template<class T>
TreeNodeV2<T>::~TreeNodeV2()
{
	if(_ItemData)
	{
		delete _ItemData;
		_ItemData=0;
	}
	
	//for(int i=0;i<_ChildNodeVec.size();i++)
	//{
	//	delete _ChildNodeVec[i];
	//}
	_ChildNodeVec.clear();
}

template<class T>
void TreeNodeV2<T>::AddOneChildNode(TreeNodeV2* pChildNode)//增加一个子节点
{
	pChildNode->SetParentNode(this);

	_ChildNodeVec.push_back(pChildNode);
}

template<class T>
bool TreeNodeV2<T>::AddOneChildNode(TreeNodeV2* pNode,TreeNodeV2* pChildNode)//给一个指定节点增加儿子节点
{
	//先判定节点是否存在
	if(IsExistNode(pNode))
	{
		pNode->AddOneChildNode(pChildNode);	 
		return true;
	}

	return false;

}

//这是一个递归判断
template<class T>
bool TreeNodeV2<T>::IsExistNode(TreeNodeV2* pNode)
{
	if(pNode==this)
		return true;
	////先从儿子中找一遍
	//for(size_t i=0;i<_ChildNodeVec.size();i++)
	//{
	//	if(_ChildNodeVec[i]==pNode)
	//		return true;
	//}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->IsExistNode(pNode))	
			return true;
	}

	return false;
}

template<class T>
bool TreeNodeV2<T>::DeleteOneChildNode(TreeNodeV2* pChildNode)//删除一个儿子节点
{
	vector<TreeNodeV2*>::iterator iter;
	for(iter=_ChildNodeVec.begin();iter!=_ChildNodeVec.end();iter++)
	{
		if((*iter)==pChildNode)
		{
			_ChildNodeVec.erase(iter);
			return true;
		}
	}

	return false;
}

template<class T>
bool TreeNodeV2<T>::DeleteOneChildNode(TreeNodeV2* pNode,TreeNodeV2* pChildNode)//从指定节点上删除他的一个儿子节点
{
	if(IsExistNode(pNode))
	{
		return pNode->DeleteOneChildNode(pChildNode);
	}

	return false;
}

template<class T>
void TreeNodeV2<T>::SetItemData(T* data)//设数据
{
	_ItemData=data;
}

template<class T>
void TreeNodeV2<T>::SetItemData(TreeNodeV2* pNode,T* data)//设指定节点数据
{
	if(IsExistNode(pNode))
	{
		pNode->SetItemData(data);
	}
}

template<class T>
T*    TreeNodeV2<T>::GetItemData()//得到数据
{
	return _ItemData;

}

template<class T>
T*    TreeNodeV2<T>::GetItemData(TreeNodeV2* pNode)//得到指定节点的数据
{
	if(IsExistNode(pNode))
	{
		return pNode->GetItemData();
	}
	return NULL;
}

template<class T>
TreeNodeV2<T>*  TreeNodeV2<T>::FindNodeWithName(const string& name)//通过名称查找节点
{
	//确认是否是自己
	if(_NodeName.compare(name)==0)
	{
		return this;
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		TreeNodeV2* p=0;
		p=_ChildNodeVec[i]->FindNodeWithName(name);

		if(p)
		{
			return p;
		}
	}

	return 0;
}

template<class T>
string TreeNodeV2<T>::GetName()//得到名字
{
	return _NodeName;
}

template<class T>
bool   TreeNodeV2<T>::IsZhanKai()//是否张开
{
	return _bOwnerZhanKai;
}

template<class T>
vector<TreeNodeV2<T>*> TreeNodeV2<T>::GetAllChildNode()//得到所有的儿子节点
{
	return _ChildNodeVec;
}

template<class T>
int    TreeNodeV2<T>::GetAllNodeNum()
{
	int num=0;
	GetSelfNodeNum(num);
	return num;
}

template<class T>
void   TreeNodeV2<T>::GetSelfNodeNum(int &num)
{
	num+=1;
	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		_ChildNodeVec[i]->GetSelfNodeNum(num);
	}
}

template<class T>
void TreeNodeV2<T>::GetAllNode(int ParentDaiShu,vector<TreeNodeV2<T>*>& NodeVec)//得到所有节点,传父亲的代数
{
	_DaiShu=ParentDaiShu+1;
	//自己加入进去
	NodeVec.push_back(this);

	//按儿子顺序排
	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		_ChildNodeVec[i]->GetAllNode(_DaiShu,NodeVec);
	}
}

template<class T>
void TreeNodeV2<T>::ModifyAllNodeDaiShu(int ParentDaiShu)//修订一次所有节点的代数
{
	_DaiShu=ParentDaiShu+1;
	//按儿子顺序排
	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		_ChildNodeVec[i]->ModifyAllNodeDaiShu(_DaiShu);
	}
}

template<class T>
void TreeNodeV2<T>::GetAllNeedRenderNode(int ParentDaiShu,vector<TreeNodeV2<T>*>& NodeVec)//得到所有需要渲染的节点
{
	_DaiShu=ParentDaiShu+1;

	//自己加入进去
	NodeVec.push_back(this);

	//是展开的，才可见
	if(_bOwnerZhanKai)
	{
		for(size_t i=0;i<_ChildNodeVec.size();i++)
		{
			_ChildNodeVec[i]->GetAllNeedRenderNode(_DaiShu,NodeVec);
		}
	}
}

template<class T>
bool   TreeNodeV2<T>::IsHasChild()
{
	if(_ChildNodeVec.size()>0)
		return true;

	return false;
}


template<class T>
int    TreeNodeV2<T>::GetDaiShu()//得到代数
{
	return _DaiShu;
}

template<class T>
void   TreeNodeV2<T>::SetIsZhanKai(bool bZhanKai)//设是否张开
{
	_bOwnerZhanKai=bZhanKai;

}

template<class T>
bool   TreeNodeV2<T>::IsHouDai(TreeNodeV2* pNode)//判一个节点是否是自己的后代
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->IsHouDai(pNode))
			return true;
	}

	return false;	
}

template<class T>
void   TreeNodeV2<T>::SetParentNode(TreeNodeV2* pParentNode)//设父节点
{
	_pParentNode=pParentNode;
}

template<class T>
void   TreeNodeV2<T>::DestoryTree()//销毁树
{
	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		_ChildNodeVec[i]->DestoryTree();
	}

	delete this;
}

template<class T>
TreeNodeV2<T>* TreeNodeV2<T>::GetParentNode()//得到父节点
{
	return _pParentNode;
}

template<class T>
void   TreeNodeV2<T>::ChangeNodeName(string strNodeName)//变更节点名,只在特殊需求是使用
{
	_NodeName=strNodeName;
}

template<class T>
TreeNodeV2<T>* TreeNodeV2<T>::CopyNode(TreeNodeV2<T>* pNewParentNode) //复制节点
{
	TreeNodeV2<T>* pNewNode=new TreeNodeV2<T>(_NodeName);
	pNewNode->SetIsZhanKai(_bOwnerZhanKai);
	pNewNode->SetItemData(_ItemData->Copy());

	if(pNewParentNode)
		pNewParentNode->AddOneChildNode(pNewNode);

	//int    _DaiShu;//代数，0为不清楚,根为第一代
	return pNewNode;
}

template<class T>
TreeNodeV2<T>*   TreeNodeV2<T>::CopyAllNode(TreeNodeV2* pNewParentNode)//递归复制所有节点
{
	TreeNodeV2<T>* pNewNode;
	pNewNode=CopyNode(pNewParentNode);

	//子节点
	vector<TreeNodeV2*>::iterator iter;
	for(iter=_ChildNodeVec.begin();iter!=_ChildNodeVec.end();iter++)
	{
		//修改
		//(*iter)->CopyNode(pNewNode);	
		(*iter)->CopyAllNode(pNewNode);
	}
	return pNewNode;
}

template<class T>
void    TreeNodeV2<T>::ChangeName(string& newName)//改变名字
{
	_NodeName=newName;

}
template<class T>
bool    TreeNodeV2<T>::UpNodeOne(TreeNodeV2* pNode,TreeNodeV2** ppChangeNode)
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i] == pNode)
		{
			if(i!=0)
			{
				TreeNodeV2* tt_pNode = _ChildNodeVec[i-1];
				_ChildNodeVec[i-1] = _ChildNodeVec[i];
				_ChildNodeVec[i] = tt_pNode;

				if(ppChangeNode)
				{
					(*ppChangeNode) = tt_pNode;
				}
			}
			return true;
		}
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->UpNodeOne(pNode,ppChangeNode))
			return true;
	}

	return false;
}

template<class T>
bool    TreeNodeV2<T>::UpNodeToFirst(TreeNodeV2* pNode)
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i] == pNode)
		{
			if(i!=0)
			{
				//前面全部后移一个
				for(int k=i-1;k>=0;k--)
				{
					_ChildNodeVec[k+1] = _ChildNodeVec[k];
				}
				//置为第一个
				_ChildNodeVec[0] = pNode;
			}
			return true;
		}
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->UpNodeToFirst(pNode))
			return true;
	}

	return false;
}

template<class T>
bool    TreeNodeV2<T>::DownNodeOne(TreeNodeV2* pNode,TreeNodeV2** ppChangeNode)
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i] == pNode)
		{
			if((i+1)<_ChildNodeVec.size())
			{
				TreeNodeV2* tt_pNode = _ChildNodeVec[i+1];
				_ChildNodeVec[i+1] = _ChildNodeVec[i];
				_ChildNodeVec[i] = tt_pNode;

				if(ppChangeNode)
				{
					(*ppChangeNode)=tt_pNode;
				}
			}
			return true;
		}
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->DownNodeOne(pNode,ppChangeNode))
			return true;
	}

	return false;
}

template<class T>
bool    TreeNodeV2<T>::DownNodeToEnd(TreeNodeV2* pNode) //将节点下移到同级的最后一个,只在兄弟间进行
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i] == pNode)
		{
			//从此处开始前移一个
			for(int k=i+1;k<_ChildNodeVec.size();k++)
			{
				_ChildNodeVec[k-1] = _ChildNodeVec[k];

			}
			//置为最后
			_ChildNodeVec[_ChildNodeVec.size()-1] = pNode;

			return true;
		}
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->DownNodeToEnd(pNode))
			return true;
	}

	return false;
}

template<class T>
bool    TreeNodeV2<T>::UpNodeHalf(TreeNodeV2* pNode)                   //折半上移
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i] == pNode)
		{
			if(i!=0)
			{
				int tt_half = i/2;
				//前面全部后移一个
				for(int k=i-1;k>=tt_half;k--)
				{
					_ChildNodeVec[k+1] = _ChildNodeVec[k];
				}
				//置为第一个
				_ChildNodeVec[tt_half] = pNode;
			}
			return true;
		}
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->UpNodeToFirst(pNode))
			return true;
	}

	return false;
}

template<class T>
bool    TreeNodeV2<T>::DownNodeHalf(TreeNodeV2* pNode)                 //折半下移
{
	if(pNode==this)
		return true;

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i] == pNode)
		{
			int tt_half = (_ChildNodeVec.size()-i)/2 + i;
			//从此处开始前移一个
			for(int k=i+1;(k<=tt_half) && (k<_ChildNodeVec.size());k++)
			{
				_ChildNodeVec[k-1] = _ChildNodeVec[k];

			}
			//置为最后
			if(tt_half>=_ChildNodeVec.size())
			{
				_ChildNodeVec[_ChildNodeVec.size()-1] = pNode;

			}
			else
			{
				_ChildNodeVec[tt_half] = pNode;
			}


			return true;
		}
	}

	for(size_t i=0;i<_ChildNodeVec.size();i++)
	{
		if(_ChildNodeVec[i]->DownNodeToEnd(pNode))
			return true;
	}

	return false;
}
template<class T>
TreeNodeV2<T>* TreeNodeV2<T>::GetPreBrotherNode(TreeNodeV2* pNode) //得到前一个兄弟节点
{
	//如根,如果传入空
	if(this == pNode || (!pNode))
		return NULL;

	//不存在父
	if(!pNode->_pParentNode)
		return NULL;

	TreeNodeV2* tt_pParent = pNode->_pParentNode;

	for(size_t i=0;i<tt_pParent->_ChildNodeVec.size();i++)
	{
		if(tt_pParent->_ChildNodeVec[i] == pNode)
		{
			//无前项
			if(i==0)
				return NULL;

			return tt_pParent->_ChildNodeVec[i-1]; 
		}

	}


	return NULL;
}

template<class T>
TreeNodeV2<T>* TreeNodeV2<T>::GetNextBrotherNode(TreeNodeV2* pNode) //得到下一个兄弟节点
{
	//如根,如果传入空
	if(this == pNode || (!pNode))
		return NULL;

	//不存在父
	if(!pNode->_pParentNode)
		return NULL;

	TreeNodeV2* tt_pParent = pNode->_pParentNode;

	for(size_t i=0;i<tt_pParent->_ChildNodeVec.size();i++)
	{
		if(tt_pParent->_ChildNodeVec[i] == pNode)
		{
			//无后项
			if(i==tt_pParent->_ChildNodeVec.size()-1)
				return NULL;

			return tt_pParent->_ChildNodeVec[i+1]; 
		}

	}


	return NULL;
}

template<class T>
void      TreeNodeV2<T>::JiangJiToPreBrotherChild(TreeNodeV2* pNode)
{
	//如根,如果传入空
	if(this == pNode || (!pNode))
		return;

	//不存在父
	if(!pNode->_pParentNode)
		return;

	TreeNodeV2* tt_pParent = pNode->_pParentNode;

	for(size_t i=0;i<tt_pParent->_ChildNodeVec.size();i++)
	{
		if(tt_pParent->_ChildNodeVec[i] == pNode)
		{
			//无前项
			if(i==0)
				return;

			TreeNodeV2* tt_pPreBrother = tt_pParent->_ChildNodeVec[i-1];
			tt_pParent->_ChildNodeVec.erase(tt_pParent->_ChildNodeVec.begin() + i);

			pNode->_pParentNode = NULL;
			tt_pPreBrother->AddOneChildNode(pNode);
		}

	}
}

template<class T>
void      TreeNodeV2<T>::ShengJiToParentBrother(TreeNodeV2* pNode)
{
	//如根,如果传入空
	if(this == pNode || (!pNode))
		return;

	//不存在父
	if(!pNode->_pParentNode)
		return;

	TreeNodeV2* tt_pParent = pNode->_pParentNode;
	TreeNodeV2* tt_pParentParent = tt_pParent->_pParentNode;
	if(tt_pParentParent)
	{
		for(int i=0;i<tt_pParent->_ChildNodeVec.size();i++)
		{
			if(tt_pParent->_ChildNodeVec[i] == pNode)
			{
				tt_pParent->_ChildNodeVec.erase(tt_pParent->_ChildNodeVec.begin() + i);
				pNode->_pParentNode = NULL;

				tt_pParentParent->AddOneChildNode(pNode);

				break;
			}
		}
	}

}



////树节点管理
//class TreeNodeManage
//{
//public:
//	TreeNodeManage();
//	~TreeNodeManage();
//
//protected:
//	TreeNode* _pRootNode;//根节点
//};