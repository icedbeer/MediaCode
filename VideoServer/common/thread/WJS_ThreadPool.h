/*
* create by wangweijin
*/
#pragma once

#include <vector>

#include "Lock/WJS_Lock.h"
#include "Lock/WJS_AutoLock.h"
#include "Log/Logger.h"

using namespace std;
//#include "std/WJS_ArrayVector.h"

struct stEntryPar
{
	void* _pEx_Self;   //自带的
	void* _pEx_Out;   //外部传入的

	bool _bNoticeExit; //是否通知退出

	HANDLE _handle;   //线程句柄

	stEntryPar()
	{
		_pEx_Self=0;
		_pEx_Out=0;
		_bNoticeExit=false;
		_handle=0;
	}
};

struct stThreadHandle
{
	HANDLE       _handle;   //线程句柄
	stEntryPar*  _pEntryPar;  //入口参数

	stThreadHandle()
	{
		_handle=0;
		_pEntryPar=0;
	}
};

class WJS_IThread
{
public:
	virtual void* ThreadReclaimProcess(HANDLE handle)=0;//线程回收时的处理

};

typedef DWORD (WINAPI * _THREADFUNC)(LPVOID lpParam);
typedef void*  (* _THREADCALLBACK)(HANDLE handle,void* pUserData);

//线程池
class WJS_ThreadPool
{
public:
	WJS_ThreadPool(FileLogger* pFLog=0);
	~WJS_ThreadPool();

//	void SetThreadEntryFunc(DWORD   (WINAPI * pThreadFunc)(LPVOID lpParam)); //设线程的入口函数
	void SetThreadEntryFunc(_THREADFUNC pThreadFunc); //设线程的入口函数

	void SetThreadInterfance(WJS_IThread* pIThread); //线程处理的接口

	void CreatePool(int num);  //创建池子

	int  GetFreeThreadNum(); //得到空闭线程数

	int  GetUsingThreadNum();//得到正在使用的线程数量

	bool StartOneThread(void* pParam); //开始一个线程

	bool ThreadReclaim(HANDLE handle); //线程回收

	void SetThreadCallbackFunc(_THREADCALLBACK CallbackFunc,void* pUserData); //设线程的回调函数

protected:
	void NotifyAllThreadExit(); //通知所有线程退出

protected:
	//DWORD(WINAPI  * _pThreadFunc)(LPVOID lpParam); //线程的入口函数指针
	_THREADFUNC  _pThreadFunc;

	vector<stThreadHandle*>  _ThreadUsedHandleList; //在用线程句柄列表

	vector<stThreadHandle*>  _ThreadFreeHandleList; //线程空闲的句柄列表

	WJS_IThread*  _pIThread; //线程接口

	WJS_Lock      _ThreadLock;

	FileLogger*   _pFLog;//日记指针

	BOOL          _bEnterDestroy;//是否进入析构了

	_THREADCALLBACK _pThreadCallback;
	void*           _pUserData;
};
