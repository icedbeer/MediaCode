//#include "stdafx.h"
#include "WJS_ThreadPool.h"


WJS_ThreadPool::WJS_ThreadPool(FileLogger* pFLog)
:_pThreadFunc(0)
,_pIThread(0)
,_pFLog(pFLog)
,_bEnterDestroy(FALSE)
,_pThreadCallback(NULL)
,_pUserData(NULL)
{

}

WJS_ThreadPool::~WJS_ThreadPool()
{
	_bEnterDestroy=TRUE;
	if(_pFLog)
		_pFLog->Log(eLogTypeNone,"WJS_ThreadPool 析构 --1");
	NotifyAllThreadExit();

	if(_pFLog)
		_pFLog->Log(eLogTypeNone,"WJS_ThreadPool 析构 --2");
}

void WJS_ThreadPool::SetThreadEntryFunc(_THREADFUNC pThreadFunc)
{
  _pThreadFunc=pThreadFunc;
}
void WJS_ThreadPool::SetThreadInterfance(WJS_IThread* pIThread)
{
	_pIThread=pIThread;

}

void WJS_ThreadPool::CreatePool(int num)
{
	WJS_AutoLock tt_AutoLock(&_ThreadLock);
	for(int i=0;i<num;i++)
	{
		stEntryPar* pEntryPar=new stEntryPar();
		pEntryPar->_pEx_Self=this;

		//创建线程
        HANDLE tt_handle=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)(_pThreadFunc),(void*)pEntryPar,CREATE_SUSPENDED,0);

		//DWORD tt=10;
		if(tt_handle!=0)
		{
			pEntryPar->_handle=tt_handle;

			stThreadHandle* tt_pThreadHandle=new stThreadHandle();
			tt_pThreadHandle->_handle=tt_handle;
			tt_pThreadHandle->_pEntryPar=pEntryPar;
			_ThreadFreeHandleList.push_back(tt_pThreadHandle);

			//tt=SuspendThread(tt_handle);  //挂起线程	
		}	
		else
		{
			delete pEntryPar;
		}
	}	
}

int  WJS_ThreadPool::GetFreeThreadNum()
{
	return _ThreadFreeHandleList.size();
}

int  WJS_ThreadPool::GetUsingThreadNum()
{
	return _ThreadUsedHandleList.size();

}

bool WJS_ThreadPool::StartOneThread(void* pParam)
{
	WJS_AutoLock tt_AutoLock(&_ThreadLock);

	bool tt_bresult=false;

	vector<stThreadHandle*> tt_tempList;
	while(TRUE)
	{
		if(_ThreadFreeHandleList.size()==0)
		{
			break;
		}

		stThreadHandle* tt_pThreadHandle=_ThreadFreeHandleList[_ThreadFreeHandleList.size()-1];
		_ThreadFreeHandleList.pop_back();
		tt_pThreadHandle->_pEntryPar->_pEx_Out=pParam;

		int tt_tret=ResumeThread(tt_pThreadHandle->_handle);
		if(tt_tret == 0)
		{
			//未挂起需要等待挂起后再执行
			tt_pThreadHandle->_pEntryPar->_pEx_Out = 0;
			tt_tempList.push_back(tt_pThreadHandle);
			if(_pFLog)
				_pFLog->Logf(eLogTypeNone,"线程=%d未被挂起",(int)tt_pThreadHandle->_handle);
		}
		else if(tt_tret==1)
		{
			tt_bresult=true;
			_ThreadUsedHandleList.push_back(tt_pThreadHandle);
			break;
		}	
		else if(tt_tret>1)
		{
			while(ResumeThread(tt_pThreadHandle->_handle)>1);
			tt_bresult=true;
			_ThreadUsedHandleList.push_back(tt_pThreadHandle);
			if(_pFLog)
				_pFLog->Logf(eLogTypeNone,"线程=%d多次挂起",(int)tt_pThreadHandle->_handle);
			break;
		}
		else
		{
		   //此线程就会被丢弃
			if(_pFLog)
				_pFLog->Logf(eLogTypeNone,"线程=%d被丢弃1",(int)tt_pThreadHandle->_handle);

			break;

		}
	}

	if(tt_tempList.size()>0)
	{
		for(int i=0;i<(int)tt_tempList.size();i++)
		{
			_ThreadFreeHandleList.push_back(tt_tempList[i]);
		}
	}

	return tt_bresult;
}

void WJS_ThreadPool::NotifyAllThreadExit()
{
	if(_pFLog)
		_pFLog->Log(eLogTypeNone,"开始退出标识");
	if(_bEnterDestroy)
	{
		WJS_AutoLock tt_AutoLock(&_ThreadLock);
		//通知所有在使用的线程退出
		for(int i=0;i<_ThreadUsedHandleList.size();i++)
		{
			_ThreadUsedHandleList[i]->_pEntryPar->_bNoticeExit=true;
		}
		//先通知所有空闲线程退出
		for(int i=0;i<_ThreadFreeHandleList.size();i++)
		{
			_ThreadFreeHandleList[i]->_pEntryPar->_bNoticeExit=true;
		}
	}

	if(_pFLog)
		_pFLog->Log(eLogTypeNone,"开始退出在空闲线程");

	//先通知所有空闲线程退出
	for(int i=0;i<_ThreadFreeHandleList.size();i++)
	{
		_ThreadFreeHandleList[i]->_pEntryPar->_bNoticeExit=true;
		int tt_tret;
		int tt_tcount=0;
		do 
		{
			tt_tret=ResumeThread(_ThreadFreeHandleList[i]->_handle);
		} while (tt_tret>1 && (tt_tcount++<20));
	}

	while(_ThreadFreeHandleList.size()>0)
	{
		_ThreadFreeHandleList[0]->_pEntryPar->_bNoticeExit=true;
		DWORD tt_ret = WaitForSingleObject(_ThreadFreeHandleList[0]->_handle,10000);
		if(tt_ret == WAIT_TIMEOUT)
		{
			int tt_tret;
			int tt_tcount=0;
			do 
			{
				tt_tret=ResumeThread(_ThreadFreeHandleList[0]->_handle);
			} while (tt_tret>1 && (tt_tcount++<20));
			continue;
		}
		delete _ThreadFreeHandleList[0]->_pEntryPar;
		delete _ThreadFreeHandleList[0];
		_ThreadFreeHandleList.erase(_ThreadFreeHandleList.begin());
	}


	if(_pFLog)
		_pFLog->Log(eLogTypeNone,"开始退出在用线程");
	while(_ThreadUsedHandleList.size()>0)
	{
		_ThreadUsedHandleList[0]->_pEntryPar->_bNoticeExit=true;
		DWORD tt_ret=WaitForSingleObject(_ThreadUsedHandleList[0]->_handle,10000);
		if(tt_ret == WAIT_TIMEOUT)
		{
			int tt_tret;
			int tt_tcount=0;
			do 
			{
				tt_tret=ResumeThread(_ThreadUsedHandleList[0]->_handle);
			} while (tt_tret>1 && (tt_tcount++<20));
			continue;
		}

		delete _ThreadUsedHandleList[0]->_pEntryPar;
		delete _ThreadUsedHandleList[0];
		_ThreadUsedHandleList.erase(_ThreadUsedHandleList.begin());
	}




}

bool  WJS_ThreadPool::ThreadReclaim(HANDLE handle)
{
	//WJS_AutoLock tt_AutoLock(&_ThreadLock);
	if(_bEnterDestroy)
	{
		if(_pFLog)
			_pFLog->Log(eLogTypeNone,"进入析构不再回收线程");
		//进入析构将不再回收
		return true;
	}

	_ThreadLock.Enter();

	stThreadHandle* tt_pThreadHandle=0;
	int index=0;
	for(int i=0;i<_ThreadUsedHandleList.size();i++)
	{
		if(_ThreadUsedHandleList[i]->_handle==handle)
		{
			tt_pThreadHandle=_ThreadUsedHandleList[i];
			index=i;
			break;
		}
	}

	if(tt_pThreadHandle==0)
	{
		_ThreadLock.Leave();
		//此线程就会被丢弃
		if(_pFLog)
			_pFLog->Logf(eLogTypeNone,"线程%d回收已用中无",(int)handle);
		return false;
	}


	if(_pIThread!=0)
	{
		void* pData=_pIThread->ThreadReclaimProcess(tt_pThreadHandle->_handle);
		if(pData!=0)
		{
			tt_pThreadHandle->_pEntryPar->_pEx_Out=pData;
			_ThreadLock.Leave();
			return false;
		}
	}
	else if(_pThreadCallback)
	{
		void* pData=_pThreadCallback(tt_pThreadHandle->_handle,_pUserData);
		if(pData!=0)
		{
			tt_pThreadHandle->_pEntryPar->_pEx_Out=pData;
			_ThreadLock.Leave();
			return false;
		}
	}
	//////////////////回收
	tt_pThreadHandle->_pEntryPar->_pEx_Out=0;

	_ThreadUsedHandleList.erase(_ThreadUsedHandleList.begin()+index);
	_ThreadFreeHandleList.push_back(tt_pThreadHandle);

	_ThreadLock.Leave();
	//可以由外面挂起线程
	SuspendThread(tt_pThreadHandle->_handle);

	return true;
}

void WJS_ThreadPool::SetThreadCallbackFunc(_THREADCALLBACK CallbackFunc,void* pUserData)
{
	_pThreadCallback = CallbackFunc;
	_pUserData = pUserData;

}