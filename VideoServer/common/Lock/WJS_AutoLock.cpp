#include "WJS_AutoLock.h"

WJS_AutoLock::WJS_AutoLock(WJS_Lock* pLock)
:_pLock(pLock)
{
	_pLock->Enter();

}

WJS_AutoLock::~WJS_AutoLock()
{
	if(_pLock)
	{
	  _pLock->Leave();
	}
}

