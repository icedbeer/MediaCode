/*
* create by wangweijin
*/
#pragma once

#include "WJS_Lock.h"

class WJS_AutoLock
{
public:
	WJS_AutoLock(WJS_Lock* pLock);
	WJS_AutoLock()
		:_pLock(0){}
	~WJS_AutoLock();

	void PreEnter(WJS_Lock* pLock) {_pLock=pLock;_pLock->Enter();}
	void PreLeave(){_pLock=0;}
protected:
	WJS_Lock* _pLock;

protected:
	//WJS_AutoLock();
};