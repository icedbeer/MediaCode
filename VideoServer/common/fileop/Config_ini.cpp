#include "Config_ini.h"
#include <stdio.h>

FileConfig_ini::FileConfig_ini(const string& strPathName,string NoteMark,bool bSectionHasTypeName)
{
	_strFileName=strPathName;
	_strNoteMark=NoteMark;
	_pSectionListBegin=0;
	_pSectionListCurNode=0;
	_bSectionHasTypeName=bSectionHasTypeName;
	_bSaveKeyValueList=false;
}

FileConfig_ini::FileConfig_ini()
{
	_bSectionHasTypeName=false;
	_strNoteMark="#";
	_pSectionListBegin=0;
	_pSectionListCurNode=0;
	_bSaveKeyValueList=false;
}

FileConfig_ini::~FileConfig_ini()
{
	Free();

}

void FileConfig_ini::SetFileName(const string& strPathName,string NoteMark,bool bSectionHasTypeName)//设文件名
{
	_strFileName=strPathName;
	_strNoteMark=NoteMark;
	_bSectionHasTypeName=bSectionHasTypeName;
}

bool      FileConfig_ini::Parser()//进行解析
{
	Free();
	FILE *fp;

	//二进制方式打开
	fp = fopen(_strFileName.c_str(), "rb");
	if(!fp)
	   return false;

	//得到文件大小
	fseek(fp,0,SEEK_END);
	long fileLen = ftell(fp);
	fseek(fp,0,SEEK_SET);

	//读出所有数据
	char* pBuf=new char[fileLen+1];
	fread(pBuf,fileLen,1,fp);
	pBuf[fileLen]='\0';

	fclose(fp);

	char* pOld=pBuf;

	bool bEnd=false;
	while(!bEnd)
	{
		string strLine;
		int readNum=0;
		strLine=GetLine(pBuf,readNum,bEnd);

		pBuf+=readNum;

		if(strLine.length()>0)
		{
			strLine=RemoveLineNote(strLine);

			strLine=RemoveSpaceAndN(strLine);
		}

		if(strLine.length()>0)
		{
			ParserLine(strLine);
		}
	}


	delete [] pOld;
	return true;
}

void     FileConfig_ini::Free()
{
	stSection* pNode;

	while(_pSectionListBegin)
	{
		pNode=_pSectionListBegin;
		_pSectionListBegin=_pSectionListBegin->_pNext;

		pNode->_KeyAndValueMap.clear();
		pNode->_KeyValueList.clear();
		delete pNode;
	}

	_pSectionListBegin=0;
	_pSectionListCurNode=0;
}

string    FileConfig_ini::RemoveLineNote(string& strLine)
{
	int index=strLine.find(_strNoteMark.c_str());

	if(index!=-1)
	{
		return strLine.substr(0,index);
	}

	return strLine;
}

string    FileConfig_ini::RemoveSpaceAndN(string& strLine)//移除空格和换行
{
	if(strLine.empty())
		return strLine;
	const char* pChLine=strLine.c_str();
	size_t len=strLine.length();

	char* pCh=new char[len+1];

	int count=0;

	while((*pChLine)!='\0')
	{
		if((*pChLine)!=' ' &&  (*pChLine)!='\n' &&  (*pChLine)!='\r' && (*pChLine)!='\t')
		{
			pCh[count]=*pChLine;
			count++;
		}
		pChLine++;
	}

	pCh[count]='\0';

	string strTemp;
	strTemp.append(pCh);
	delete [] pCh;

	return strTemp;
}

string    FileConfig_ini::GetLine(char* pChar,int& readNum,bool& bReadAllEnd)//取到一行
{
	//bool bFoundEnd=false;
	readNum=0;
	bReadAllEnd=false;
	string strLine;
	char ch[2];

	while((*pChar)!='\0'/* && (!bFoundEnd)*/)
	{
		if((*pChar)!='\n' && (*pChar)!='\r')
		{
			ch[0]=*pChar;
			ch[1]='\0';

			strLine.append(ch);
			pChar++;
			readNum++;
		}
		else
		{
			//bFoundEnd=true;
			pChar++;
			readNum++;
			break;
		}
	}

	if((*pChar)=='\0')
	{
		bReadAllEnd=true;
	}

	return strLine;
}

void  FileConfig_ini::ParserLine(string strLine)
{
	//先进行段的判别
	string strSeciton;
	string strSectionType;
	if(IsSection(strLine.c_str(),(int)strLine.length(),strSeciton,strSectionType))
	{
		stSection* pNode=new stSection();
		pNode->_SectionName=strSeciton;
		pNode->_SectionTypeName=strSectionType;

		if(!_pSectionListCurNode)
		{
			_pSectionListBegin=pNode;
			_pSectionListCurNode=pNode;
		}
		else
		{
			_pSectionListCurNode->_pNext=pNode;
			_pSectionListCurNode=_pSectionListCurNode->_pNext;
		}

		return;
	}

	string strKey;
	string strValue;
	if(IsKeyAndValue(strLine.c_str(),(int)strLine.length(),strKey,strValue))
	{
		if(_pSectionListCurNode)
		{
			_pSectionListCurNode->_KeyAndValueMap.insert(make_pair(strKey,strValue));
			if(_bSaveKeyValueList)
			{
				stKeyValueIni tt_keyvalue;
				tt_keyvalue._strKey = strKey;
				tt_keyvalue._strValue = strValue;
				_pSectionListCurNode->_KeyValueList.push_back(tt_keyvalue);
			}


		}
		return;
	}
}

bool      FileConfig_ini::IsSection(const char* pChLine,int charLen,string& strSection,string& strSectionType)//是否是段
{
	//查找[]
	bool bFoundLeft=false;
	bool bFoundRight=false;
	int  LeftPos=0;
	int  RightPos=0;
	int  count=0;

	int len=charLen;

	const char* p=pChLine;

	while((*p)!='\0')
	{
		if((*p)=='[')
		{
			//找到两个
			if(bFoundLeft)
				return false;
			else
			{
				LeftPos=count;
				bFoundLeft=true;
			}
		}
		else if((*p)==']')
		{
			//找到两个
			if(bFoundRight)
				return false;
			else
			{
				RightPos=count;
				bFoundRight=true;
				break;
			}
		}
		else if((*p)=='=')
		{
			//出现了等号的情况下，还没有找到左[
			if(!bFoundLeft)
				return false;
		}

		p++;
		count++;
	}

	if(bFoundLeft && bFoundRight)
	{
		if(!_bSectionHasTypeName)
		{
			//要去掉两边的[]
			char *t_p=new char[RightPos-LeftPos];

			const char* pCh=pChLine;
			pCh+=LeftPos+1;
			memcpy(t_p,pCh,RightPos-LeftPos-1);
			t_p[RightPos-LeftPos-1]='\0';
			strSection.append(t_p);

			delete [] t_p;

			if(strSection.empty())
				return false;

			return true;
		}
		else
		{
			//要去掉两边的[]
			char *t_p=new char[RightPos-LeftPos];

			const char* pCh=pChLine;
			pCh+=LeftPos+1;
			memcpy(t_p,pCh,RightPos-LeftPos-1);
			t_p[RightPos-LeftPos-1]='\0';

			string tt_key;
			string tt_value;
			if(IsKeyAndValue(t_p,RightPos-LeftPos-1,tt_key,tt_value))
			{
				strSectionType=tt_key;
				strSection=tt_value;
				delete [] t_p;
				return true;
			}
			else
			{
				delete [] t_p;
				return false;
			}

		}

	}

	return false;
}

bool      FileConfig_ini::IsKeyAndValue(const char* pChLine,int charLen,string& strKey,string& strValue)//是否是键和值
{
	//查找=,进行左右分隔
	int dengPos=0;

	int len=charLen;

	const char* p=pChLine;

	while((*p)!='\0')
	{
		if((*p)=='=')
		{
			break;
		}

		p++;
		dengPos++;
	}

	if(dengPos>0 && dengPos<len)
	{
		p=pChLine;
		char* pChLeft=new char[dengPos+1];
		memcpy(pChLeft,p,dengPos);
		pChLeft[dengPos]='\0';
		strKey.append(pChLeft);

		delete [] pChLeft;

		char* pChRight=new char[len-dengPos];
		memcpy(pChRight,(p+(dengPos+1)),len-dengPos-1);
		pChRight[len-dengPos-1]='\0';
		strValue.append(pChRight);

		delete [] pChRight;

		return true;
	}


	return false;
}





stSection* FileConfig_ini::FindeSectionNode(const string& strSection)//查找段节点
{

	stSection* pNode=_pSectionListBegin;

	while(pNode)
	{
		if(pNode->_SectionName.compare(strSection.c_str())==0)
			return pNode;

		pNode=pNode->_pNext;
	}

	return 0;
}


bool FileConfig_ini::GetBool( const string&  strSection, const string& Key, bool& bValue )
{
	stSection* pNode=FindeSectionNode(strSection);
	if(!pNode)
		return false;

	KeyAndValueMap::iterator iter;
	iter=pNode->_KeyAndValueMap.find(Key);

	//找到
	if(iter!=pNode->_KeyAndValueMap.end())
	{
		//0值false,其他值true
		if(atoi(iter->second.c_str())==0)
			 bValue=false;
		else
			 bValue=true;

		return true;
	}

	return false;
}

bool FileConfig_ini::GetInt( const string&  strSection, const string& Key, int& iValue )
{
	stSection* pNode=FindeSectionNode(strSection);
	if(!pNode)
		return false;

	KeyAndValueMap::iterator iter;
	iter=pNode->_KeyAndValueMap.find(Key);

	//找到
	if(iter!=pNode->_KeyAndValueMap.end())
	{
		iValue=atoi(iter->second.c_str());
		return true;
	}

	return false;
}

bool FileConfig_ini::GetShort(const string& strSection,const string& Key,short& sValue)
{
	stSection* pNode=FindeSectionNode(strSection);
	if(!pNode)
		return false;

	KeyAndValueMap::iterator iter;
	iter=pNode->_KeyAndValueMap.find(Key);

	//找到
	if(iter!=pNode->_KeyAndValueMap.end())
	{
		sValue=(short)(atoi(iter->second.c_str()));
		return true;
	}

	return false;
}

bool FileConfig_ini::GetLong( const string&  strSection, const string& Key, long& iValue )
{
	stSection* pNode=FindeSectionNode(strSection);
	if(!pNode)
		return false;

	KeyAndValueMap::iterator iter;
	iter=pNode->_KeyAndValueMap.find(Key);

	//找到
	if(iter!=pNode->_KeyAndValueMap.end())
	{
		iValue=atol(iter->second.c_str());
		return true;
	}

	return false;
}

bool FileConfig_ini::GetFloat( const string&  strSection, const string& Key, float& fValue)
{
	stSection* pNode=FindeSectionNode(strSection);
	if(!pNode)
		return false;

	KeyAndValueMap::iterator iter;
	iter=pNode->_KeyAndValueMap.find(Key);

	//找到
	if(iter!=pNode->_KeyAndValueMap.end())
	{
		fValue=(float)atof(iter->second.c_str());
		return true;
	}

	return false;
}

bool FileConfig_ini::GetString( const string&  strSection, const string& Key, string& StrValue)
{
	stSection* pNode=FindeSectionNode(strSection);
	if(!pNode)
		return false;

	KeyAndValueMap::iterator iter;
	iter=pNode->_KeyAndValueMap.find(Key);

	//找到
	if(iter!=pNode->_KeyAndValueMap.end())
	{
		StrValue=iter->second;
		return true;
	}

	return false;
}


bool FileConfig_ini::GetBool_section( stSection*  pSection, const string& Key, bool& bValue )
{

	KeyAndValueMap::iterator iter;
	iter=pSection->_KeyAndValueMap.find(Key);

	//找到
	if(iter!=pSection->_KeyAndValueMap.end())
	{
		if(atoi(iter->second.c_str())==1)
			bValue=true;
		else
			bValue=false;
		return true;
	}

	return false;
}

bool FileConfig_ini::GetInt_section(stSection*  pSection, const string& Key, int& iValue )
{

	KeyAndValueMap::iterator iter;
	iter=pSection->_KeyAndValueMap.find(Key);

	//找到
	if(iter!=pSection->_KeyAndValueMap.end())
	{
		iValue=atoi(iter->second.c_str());
		return true;
	}

	return false;
}
bool GetShort_section(stSection*  pSection, const string& Key, short& sValue)
{
	KeyAndValueMap::iterator iter;
	iter=pSection->_KeyAndValueMap.find(Key);

	//找到
	if(iter!=pSection->_KeyAndValueMap.end())
	{
		sValue=(short)(atoi(iter->second.c_str()));
		return true;
	}

	return false;
}

bool FileConfig_ini::GetFloat_section(stSection*  pSection, const string& Key, float& fValue)
{

	KeyAndValueMap::iterator iter;
	iter=pSection->_KeyAndValueMap.find(Key);

	//找到
	if(iter!=pSection->_KeyAndValueMap.end())
	{
		fValue=atof(iter->second.c_str());
		return true;
	}

	return false;
}

bool FileConfig_ini::GetString_section(stSection*  pSection, const string& Key, string& StrValue)
{
	KeyAndValueMap::iterator iter;
	iter=pSection->_KeyAndValueMap.find(Key);

	//找到
	if(iter!=pSection->_KeyAndValueMap.end())
	{
		StrValue=iter->second;
		return true;
	}

	return false;
}

bool FileConfig_ini::GetLong_section( stSection*  pSection, const string& Key, long& iValue )
{
	KeyAndValueMap::iterator iter;
	iter=pSection->_KeyAndValueMap.find(Key);

	//找到
	if(iter!=pSection->_KeyAndValueMap.end())
	{
		iValue=atol(iter->second.c_str());
		return true;
	}

	return false;

}

void   FileConfig_ini::SetSaveKeyValueList(bool bSaveKeyValueList)
{
	_bSaveKeyValueList = bSaveKeyValueList;

}