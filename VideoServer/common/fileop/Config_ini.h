/*
* create by wangweijin
*/
#pragma once
#include <string>
#include <map>
#include <vector>


using namespace std;

typedef map<string,string> KeyAndValueMap; //键和值的map

struct stKeyValueIni
{
	string _strKey;
	string _strValue;
};


//段结构，用链表，内含链和其值的map
struct stSection
{
	string _SectionName; //段名

	string _SectionTypeName; //新增段类型名

	KeyAndValueMap _KeyAndValueMap;

	vector<stKeyValueIni> _KeyValueList; //键值列表

	stSection* _pNext;

	stSection()
	{
		_pNext=0;
	}
};
class IFileConfig_ini
{
public:
	virtual bool GetBool( const string&  strSection, const string& Key, bool& bValue )=0; //用int值表示的0false,其他值true
	virtual bool GetInt( const string&  strSection, const string& Key, int& iValue )=0;
	virtual bool GetShort(const string& strSection,const string& Key,short& sValue)=0;
	virtual bool GetLong( const string&  strSection, const string& Key, long& iValue )=0;
	virtual bool GetFloat( const string&  strSection, const string& Key, float& fValue)=0;
	virtual bool GetString( const string&  strSection, const string& Key, string& StrValue)=0;
	virtual stSection*    GetBeginSection() = 0;

};

// ini的配置文件的操作类
class  FileConfig_ini : public IFileConfig_ini
{
public:
	    FileConfig_ini();
		FileConfig_ini(const string& strPathName,string NoteMark="#",bool bSectionHasTypeName=false);
		virtual ~FileConfig_ini();

		void SetFileName(const string& strPathName,string NoteMark="#",bool bSectionHasTypeName=false);//设文件名

		virtual bool GetBool( const string&  strSection, const string& Key, bool& bValue ); //用int值表示的0false,其他值true
		virtual bool GetInt( const string&  strSection, const string& Key, int& iValue );
		virtual bool GetShort(const string& strSection,const string& Key,short& sValue);
		virtual bool GetLong( const string&  strSection, const string& Key, long& iValue );
		virtual bool GetFloat( const string&  strSection, const string& Key, float& fValue);
		virtual bool GetString( const string&  strSection, const string& Key, string& StrValue);

		bool GetBool_section( stSection*  pSection, const string& Key, bool& bValue );
		bool GetShort_section(stSection*  pSection, const string& Key, short& sValue);
		bool GetInt_section( stSection*  pSection, const string& Key, int& iValue );
		bool GetLong_section( stSection*  pSection, const string& Key, long& iValue );
		bool GetFloat_section( stSection*  pSection, const string& Key, float& fValue);
		bool GetString_section( stSection*  pSection, const string& Key, string& StrValue);

		bool      Parser();//进行解析

protected:
		 string    RemoveLineNote(string& strLine);//移去行里的注释

		 string    RemoveSpaceAndN(string& strLine);//移除空格和换行,和制表符

		 string    GetLine(char* pChar,int& readNum,bool& bReadAllEnd);//取到一行字符串，需返回实读的字符数和是否读完所有的标志

		 void	   ParserLine(string strLine);//解析行

		 bool      IsSection(const char* pChLine,int charLen,string& strSection,string& strSectionType);//是否是段

		 bool      IsKeyAndValue(const char* pChLine,int charLen,string& strKey,string& strValue);//是否是键和值
		
		

		 void      Free();

public:
	virtual stSection*    GetBeginSection(){return _pSectionListBegin;}//得到首段

	stSection* FindeSectionNode(const string& strSection);//查找段节点 ,转成公有

public:
	void   SetSaveKeyValueList(bool bSaveKeyValueList);
	   
protected:
		string		    _strFileName;//文件名
		string          _strNoteMark;//注释符号,#


		stSection*      _pSectionListBegin;//段列表开始
		stSection*      _pSectionListCurNode; //段列表当前节点

		//section段是否有类型名，有类型名为[key=value],key视为类型名，value为名
		bool            _bSectionHasTypeName; 

		bool            _bSaveKeyValueList;   //是否保存键值列表
};


