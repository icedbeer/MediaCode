/*
*  create by wangweijin
*/

#pragma once
#include "memory/WJS_MemoryPoolManage.h"
#include "callback/WJS_CallBack.h"
#include <windows.h>
#include <Mmsystem.h>


//#define WJSMAXTIMERNUM_1024 1024
//#define WJSMAXTIMERNUM_64 64
//#define WJSMAXTIMERNUM_256 256

enum
{
	eTS_NOTUSE=0,
	eTS_USE,
	eTS_WAITKILL
};

//id,时间，挂接数据
typedef void (*callback_timerover_v2)(int,void*);
class WJS_Timer
{
public:
	WJS_Timer();
	~WJS_Timer();

	inline bool Updata(float dt)//更新
	{
		m_LeftTime-=dt;
		if(m_LeftTime<=0)
			return true;

		return false;
	}
	inline void reinit()//重新计时
	{
		m_LeftTime=m_initTime;
	}
public:
	float m_LeftTime;//剩余时间
	float m_initTime;//初始认定值
	int   m_id;//id
	int   m_State;//等待删除的
	bool  m_bLoop; //是否循环计时

	void* m_pData;//挂接的数据

	WJS_Timer* m_pPre;
	WJS_Timer* m_pNext;

	//回调
public:
	callback_timerover_v2  m_callbackFuncV2;

public:
	WJS_NEW_FROM_MEMORYPOOL
	WJS_DEL_FROM_MEMORYPOOL	
};

//////////将0取消,改为最低1
//一组定时器
template<size_t N>
class WJS_TimerGroup
{
public:
	WJS_TimerGroup();
	~WJS_TimerGroup();

	bool Updata(float dt);
	bool Updata();//自己更新

	int  CreateTimer(float totaltime,callback_timerover_v2 callbackFunc,void* pData,bool bLoop);

	void KillTimer(int id);

	void Reset(); //重置，用于游戏里面，因为重新分配，这段时间一直没有调用过这里的updata,所以需要重置

	float GetTimerLeftTime(int timerid); //得到某个定时器的剩余多少时间到时

protected:
	void InsertToNotUseList(WJS_Timer* pTimer);//插入未用列表
	void InsertToUseList(WJS_Timer* pTimer);//插入使用列表
	void KilledTimer(WJS_Timer* pTimer);//从使用中清除

protected:
	WJS_Timer  m_TimerArray[N];

	WJS_Timer* m_UsedTimerBegin;
	WJS_Timer* m_NotUsedTimerBegin;//末用的

	DWORD      m_LastTime;//上一次时间


public:
	WJS_NEW_FROM_MEMORYPOOL
	WJS_DEL_FROM_MEMORYPOOL
};



//////////////////////////////
template<size_t N>
WJS_TimerGroup<N>::WJS_TimerGroup()
:m_UsedTimerBegin(0)
,m_NotUsedTimerBegin(0)
//,m_CanUseTimerIndex(0)
,m_LastTime(0)
{
	m_LastTime=timeGetTime();
	for(int i=0;i<N;i++)
	{
		m_TimerArray[i].m_id=i+1;
	}

	//并创建初始末用列表
	m_NotUsedTimerBegin=&m_TimerArray[0];
	m_NotUsedTimerBegin->m_pPre=0;

	WJS_Timer* ppre=0;
	WJS_Timer* pcur=0;
	WJS_Timer* pnext=0;
	for(int i=1;i<N;i++)
	{
		pcur=&m_TimerArray[i];
		ppre=&m_TimerArray[i-1];
		if(i==N-1)
		{
			pnext=0;
		}
		else
		{
			pnext=&m_TimerArray[i+1];
		}	
		ppre->m_pNext=pcur;
		pcur->m_pPre=ppre;
		pcur->m_pNext=pnext;

		if(pnext)
		{
			pnext->m_pPre=pcur;
			pnext->m_pNext=0;
		}
	}
}

template<size_t N>
WJS_TimerGroup<N>::~WJS_TimerGroup()
{

}

template<size_t N>
void WJS_TimerGroup<N>::InsertToNotUseList(WJS_Timer* pTimer)
{
	if(pTimer->m_State!=eTS_NOTUSE)
	{
		pTimer->m_State=eTS_NOTUSE;
		if(m_NotUsedTimerBegin)
		{
			pTimer->m_pPre=0;
			pTimer->m_pNext=m_NotUsedTimerBegin;
			m_NotUsedTimerBegin->m_pPre=pTimer;
			m_NotUsedTimerBegin=pTimer;
		}
		else
		{
			pTimer->m_pPre=0;
			pTimer->m_pNext=0;
			m_NotUsedTimerBegin=pTimer;
		}
	}


}

template<size_t N>
void WJS_TimerGroup<N>::InsertToUseList(WJS_Timer* pTimer)
{
	if(pTimer->m_State!=eTS_USE)
	{
		pTimer->m_State=eTS_USE;
		if(m_UsedTimerBegin)
		{
			pTimer->m_pPre=0;
			pTimer->m_pNext=m_UsedTimerBegin;
			m_UsedTimerBegin->m_pPre=pTimer;
			m_UsedTimerBegin=pTimer;
		}
		else
		{
			pTimer->m_pPre=0;
			pTimer->m_pNext=0;
			m_UsedTimerBegin=pTimer;
		}
	}

}

template<size_t N>
void WJS_TimerGroup<N>::KilledTimer(WJS_Timer* pTimer)
{
	WJS_Timer* pPre=pTimer->m_pPre;
	WJS_Timer* pNext=pTimer->m_pNext;

	if(pPre)
	{
		pPre->m_pNext=pNext;
	}
	else
	{
		m_UsedTimerBegin=pNext;
	}

	if(pNext)
	{
		pNext->m_pPre=pPre;
	}

	InsertToNotUseList(pTimer);
}

template<size_t N>
bool WJS_TimerGroup<N>::Updata(float dt)
{
	WJS_Timer* p=m_UsedTimerBegin;
	bool bHasTimerOver=false;
	WJS_Timer* t_p=0;
	//更新定时器
	while(p)
	{
		if(p->m_State==eTS_WAITKILL)
		{
			t_p=p;
			p=p->m_pNext;
			KilledTimer(t_p);
		}
		else if(p->Updata(dt))
		{	
			bHasTimerOver=true;
			//if(p->m_pPre)
			//{
			//	p->m_pPre->m_pNext=p->m_pNext;
			//}

			//if(p->m_pNext)
			//{
			//	p->m_pNext->m_pPre=p->m_pPre;
			//}

			//调用回调
			if(p->m_callbackFuncV2)
			{
				p->m_callbackFuncV2(p->m_id,p->m_pData);
			}

			//循环计时
			if(p->m_bLoop)
			{
				p->reinit();
				p=p->m_pNext;
			}
			else
			{
				//删除
				t_p=p;
				p=p->m_pNext;
				KilledTimer(t_p);
			}

		}
		else
		{
			p=p->m_pNext;
		}
	}

	//有定时器到时了
	if(bHasTimerOver)
		return TRUE;

	return FALSE;
}

template<size_t N>
bool WJS_TimerGroup<N>::Updata()//自己更新
{
	DWORD t_curtime=timeGetTime();
	if(t_curtime<m_LastTime)
	{
		m_LastTime=t_curtime;
		return true;
	}
	else
	{
		if((t_curtime - m_LastTime)>=2)
		{
			float dt=(t_curtime-m_LastTime)/1000.0f;
			m_LastTime=t_curtime;
			return Updata(dt);
		}
		else
			return true;
	}
}


template<size_t N>
int WJS_TimerGroup<N>::CreateTimer(float totaltime,callback_timerover_v2 callbackFunc,void* pData,bool bLoop)
{
	//没用可用的定时器了
	if(m_NotUsedTimerBegin==0)
		return -1;

	WJS_Timer* p=m_NotUsedTimerBegin;
	m_NotUsedTimerBegin=m_NotUsedTimerBegin->m_pNext;
	if(m_NotUsedTimerBegin)
	{
		m_NotUsedTimerBegin->m_pPre = NULL;
	}
	p->m_pNext = NULL;

	int id=p->m_id;
	p->m_LeftTime=totaltime;
	p->m_initTime=totaltime;
	p->m_callbackFuncV2=callbackFunc;
	p->m_pData=pData;
	p->m_bLoop=bLoop;

	InsertToUseList(p);

	return id;

}

template<size_t N>
void WJS_TimerGroup<N>::KillTimer(int id)
{
	if(id<=0 || id>N)
		return;

	if(m_TimerArray[id-1].m_State==eTS_USE)
	{
		m_TimerArray[id-1].m_State=eTS_WAITKILL;
	}
}

template<size_t N>
void WJS_TimerGroup<N>::Reset()
{
	m_LastTime = timeGetTime();

}

template<size_t N>
float WJS_TimerGroup<N>::GetTimerLeftTime(int timerid)
{
	if(timerid<=0 || timerid>N)
		return 0.0f;

	if(m_TimerArray[timerid-1].m_State==eTS_USE)
	{
		return m_TimerArray[timerid-1].m_LeftTime;
	}
	return 0.0f;
}


//一组定时器
class WJS_TimerGroupV2
{
public:
	WJS_TimerGroupV2(int maxTimerNum=32);
	virtual ~WJS_TimerGroupV2();

	bool Updata(float dt);
	bool Updata();//自己更新

	int  CreateTimer(float totaltime,callback_timerover_v2 callbackFunc,void* pData,bool bLoop);

	void KillTimer(int id);

	void Reset(); //重置，用于游戏里面，因为重新分配，这段时间一直没有调用过这里的updata,所以需要重置

	float GetTimerLeftTime(int timerid); //得到某个定时器的剩余多少时间到时

protected:
	void InsertToNotUseList(WJS_Timer* pTimer);//插入未用列表
	void InsertToUseList(WJS_Timer* pTimer);//插入使用列表
	void KilledTimer(WJS_Timer* pTimer);//从使用中清除

protected:
	int        m_maxTimerNum; //最大的定时器数量
	WJS_Timer** m_pInitTimerArray; //初始的定时器数组

	WJS_Timer* m_UsedTimerBegin;
	WJS_Timer* m_NotUsedTimerBegin;//末用的

	DWORD      m_LastTime;//上一次时间


public:
	WJS_NEW_FROM_MEMORYPOOL
	WJS_DEL_FROM_MEMORYPOOL
};





//##include   "WJS_TimerGroup.cpp " 