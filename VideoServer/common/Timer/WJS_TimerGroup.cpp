
#include "WJS_TimerGroup.h"



WJS_Timer::WJS_Timer()
:m_LeftTime(0)
,m_pPre(0)
,m_pNext(0)
,m_id(0)
,m_pData(0)
,m_callbackFuncV2(0)
,m_State(eTS_NOTUSE)
,m_bLoop(false)
,m_initTime(0)
{

}

WJS_Timer::~WJS_Timer()
{

}


////////////////////////////////


//////////////////////////////
WJS_TimerGroupV2::WJS_TimerGroupV2(int maxTimerNum)
:m_UsedTimerBegin(0)
,m_NotUsedTimerBegin(0)
,m_LastTime(0)
,m_maxTimerNum(maxTimerNum)
{
	m_LastTime=timeGetTime();
	m_pInitTimerArray=new WJS_Timer*[maxTimerNum];
	for(int i=0;i<m_maxTimerNum;i++)
	{
		m_pInitTimerArray[i] = new WJS_Timer();
		m_pInitTimerArray[i]->m_id = i+1; 
	}


	//并创建初始末用列表
	m_NotUsedTimerBegin=m_pInitTimerArray[0];
	m_NotUsedTimerBegin->m_pPre=0;

	WJS_Timer* ppre=0;
	WJS_Timer* pcur=0;
	WJS_Timer* pnext=0;
	for(int i=1;i<m_maxTimerNum;i++)
	{
		pcur=m_pInitTimerArray[i];
		ppre=m_pInitTimerArray[i-1];
		if(i==m_maxTimerNum-1)
		{
			pnext=0;
		}
		else
		{
			pnext=m_pInitTimerArray[i+1];
		}	
		ppre->m_pNext=pcur;
		pcur->m_pPre=ppre;
		pcur->m_pNext=pnext;

		if(pnext)
		{
			pnext->m_pPre=pcur;
			pnext->m_pNext=0;
		}
	}
}

WJS_TimerGroupV2::~WJS_TimerGroupV2()
{
	for(int i=0;i<m_maxTimerNum;i++)
	{
		delete m_pInitTimerArray[i];
	}
	delete [] m_pInitTimerArray;
}

void WJS_TimerGroupV2::InsertToNotUseList(WJS_Timer* pTimer)
{
	if(pTimer->m_State!=eTS_NOTUSE)
	{
		pTimer->m_State=eTS_NOTUSE;
		if(m_NotUsedTimerBegin)
		{
			pTimer->m_pPre=0;
			pTimer->m_pNext=m_NotUsedTimerBegin;
			m_NotUsedTimerBegin->m_pPre=pTimer;
			m_NotUsedTimerBegin=pTimer;
		}
		else
		{
			pTimer->m_pPre=0;
			pTimer->m_pNext=0;
			m_NotUsedTimerBegin=pTimer;
		}
	}


}

void WJS_TimerGroupV2::InsertToUseList(WJS_Timer* pTimer)
{
	if(pTimer->m_State!=eTS_USE)
	{
		pTimer->m_State=eTS_USE;
		if(m_UsedTimerBegin)
		{
			pTimer->m_pPre=0;
			pTimer->m_pNext=m_UsedTimerBegin;
			m_UsedTimerBegin->m_pPre=pTimer;
			m_UsedTimerBegin=pTimer;
		}
		else
		{
			pTimer->m_pPre=0;
			pTimer->m_pNext=0;
			m_UsedTimerBegin=pTimer;
		}
	}
}

void WJS_TimerGroupV2::KilledTimer(WJS_Timer* pTimer)
{
	WJS_Timer* pPre=pTimer->m_pPre;
	WJS_Timer* pNext=pTimer->m_pNext;

	if(pPre)
	{
		pPre->m_pNext=pNext;
	}
	else
	{
		m_UsedTimerBegin=pNext;
	}

	if(pNext)
	{
		pNext->m_pPre=pPre;
	}

	InsertToNotUseList(pTimer);
}

bool WJS_TimerGroupV2::Updata(float dt)
{
	WJS_Timer* p=m_UsedTimerBegin;
	bool bHasTimerOver=false;
	WJS_Timer* t_p=0;
	//更新定时器
	while(p)
	{
		if(p->m_State==eTS_WAITKILL)
		{
			t_p=p;
			p=p->m_pNext;
			KilledTimer(t_p);
		}
		else if(p->Updata(dt))
		{	
			bHasTimerOver=true;
			//if(p->m_pPre)
			//{
			//	p->m_pPre->m_pNext=p->m_pNext;
			//}

			//if(p->m_pNext)
			//{
			//	p->m_pNext->m_pPre=p->m_pPre;
			//}

			//调用回调
			if(p->m_callbackFuncV2)
			{
				p->m_callbackFuncV2(p->m_id,p->m_pData);
			}

			//循环计时
			if(p->m_bLoop)
			{
				p->reinit();
				p=p->m_pNext;
			}
			else
			{
				//删除
				t_p=p;
				p=p->m_pNext;
				KilledTimer(t_p);
			}

		}
		else
		{
			p=p->m_pNext;
		}
	}

	//有定时器到时了
	if(bHasTimerOver)
		return TRUE;

	return FALSE;
}

bool WJS_TimerGroupV2::Updata()//自己更新
{
	DWORD t_curtime=timeGetTime();
	if(t_curtime<m_LastTime)
	{
		m_LastTime=t_curtime;
		return true;
	}
	else
	{
		if(t_curtime - m_LastTime >=2)
		{
			float dt=(t_curtime-m_LastTime)/1000.0f;
			m_LastTime=t_curtime;
			return Updata(dt);
		}

		return true;

	}

}



int WJS_TimerGroupV2::CreateTimer(float totaltime,callback_timerover_v2 callbackFunc,void* pData,bool bLoop)
{
	//没用可用的定时器了
	if(m_NotUsedTimerBegin==0)
		return -1;

	WJS_Timer* p=m_NotUsedTimerBegin;
	m_NotUsedTimerBegin=m_NotUsedTimerBegin->m_pNext;
	if(m_NotUsedTimerBegin)
	{
		m_NotUsedTimerBegin->m_pPre = NULL;
	}
	
	p->m_pNext = NULL;

	int id=p->m_id;
	p->m_LeftTime=totaltime;
	p->m_initTime=totaltime;
	p->m_callbackFuncV2=callbackFunc;
	p->m_pData=pData;
	p->m_bLoop=bLoop;

	InsertToUseList(p);

	return id;

}


void WJS_TimerGroupV2::KillTimer(int id)
{
	if(id<=0 || id>m_maxTimerNum)
		return;

	if(m_pInitTimerArray[id-1]->m_State==eTS_USE)
	{
		m_pInitTimerArray[id-1]->m_State=eTS_WAITKILL;
	}
}


void WJS_TimerGroupV2::Reset() //重置，用于游戏里面，因为重新分配，这段时间一直没有调用过这里的updata,所以需要重置
{
	m_LastTime = timeGetTime();

}

float WJS_TimerGroupV2::GetTimerLeftTime(int timerid) //得到某个定时器的剩余多少时间到时
{
	if(timerid<=0 || timerid>m_maxTimerNum)
		return 0.0f;

	if(m_pInitTimerArray[timerid-1]->m_State==eTS_USE)
	{
		return m_pInitTimerArray[timerid-1]->m_LeftTime;
	}
	return 0.0f;

}
