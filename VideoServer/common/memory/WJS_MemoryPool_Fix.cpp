#include "WJS_MemoryPool_Fix.h"
#include <malloc.h>


WJS_MemoryPool_FIX::WJS_MemoryPool_FIX()
:_pUsedMemory(0)
{
	_pInitBlock._startAddr=(WJS_CHAR*)WJS_MALLOC(WJS_MPF_SIZE);
	_pInitBlock._length=WJS_MPF_SIZE;
	_pInitBlock.computerEndAddr();

	WJS_CHAR* startAddr=_pInitBlock._startAddr;

	//8byte
	for(int i=0;i<WJS_MPF_8NUM;i++)
	{
		_MemoryBlock8[i]._startAddr=startAddr+i*8;	
	}
	for(int i=0;i<WJS_MPF_8NUM-1;i++)
	{
		_MemoryBlock8[i]._pNext=&_MemoryBlock8[i+1];
	}

	_GroupMemoryBlock[0]._startAddr=startAddr;
	_GroupMemoryBlock[0]._length=WJS_MPF_8NUM*8;
	_GroupMemoryBlock[0].computerEndAddr();
	_GroupMemoryBlock[0]._pBeginFreeblock=_MemoryBlock8;

	startAddr=_GroupMemoryBlock[0]._endAddr+1;

	//
	for(int i=0;i<WJS_MPF_16NUM;i++)
	{
		_MemoryBlock16[i]._startAddr=startAddr+i*16;
	}
	for(int i=0;i<WJS_MPF_16NUM-1;i++)
	{
		_MemoryBlock16[i]._pNext=&_MemoryBlock16[i+1];
	}
	_GroupMemoryBlock[1]._startAddr=startAddr;
	_GroupMemoryBlock[1]._length=WJS_MPF_16NUM*16;
	_GroupMemoryBlock[1].computerEndAddr();
	_GroupMemoryBlock[1]._pBeginFreeblock=_MemoryBlock16;
	startAddr=_GroupMemoryBlock[1]._endAddr+1;

	//
	for(int i=0;i<WJS_MPF_32NUM;i++)
	{
		_MemoryBlock32[i]._startAddr=startAddr+i*32;
	}
	for(int i=0;i<WJS_MPF_32NUM-1;i++)
	{
		_MemoryBlock32[i]._pNext=&_MemoryBlock32[i+1];
	}
	_GroupMemoryBlock[2]._startAddr=startAddr;
	_GroupMemoryBlock[2]._length=WJS_MPF_32NUM*32;
	_GroupMemoryBlock[2].computerEndAddr();
	_GroupMemoryBlock[2]._pBeginFreeblock=_MemoryBlock32;
	startAddr=_GroupMemoryBlock[2]._endAddr+1;

	//
	for(int i=0;i<WJS_MPF_64NUM;i++)
	{
		_MemoryBlock64[i]._startAddr=startAddr+i*64;
	}
	for(int i=0;i<WJS_MPF_64NUM-1;i++)
	{
		_MemoryBlock64[i]._pNext=&_MemoryBlock64[i+1];
	}
	_GroupMemoryBlock[3]._startAddr=startAddr;
	_GroupMemoryBlock[3]._length=WJS_MPF_64NUM*64;
	_GroupMemoryBlock[3].computerEndAddr();
	_GroupMemoryBlock[3]._pBeginFreeblock=_MemoryBlock64;
	startAddr=_GroupMemoryBlock[3]._endAddr+1;

	//
	for(int i=0;i<WJS_MPF_128NUM;i++)
	{
		_MemoryBlock128[i]._startAddr=startAddr+i*128;
	}
	for(int i=0;i<WJS_MPF_128NUM-1;i++)
	{
		_MemoryBlock128[i]._pNext=&_MemoryBlock128[i+1];
	}
	_GroupMemoryBlock[4]._startAddr=startAddr;
	_GroupMemoryBlock[4]._length=WJS_MPF_128NUM*128;
	_GroupMemoryBlock[4].computerEndAddr();
	_GroupMemoryBlock[4]._pBeginFreeblock=_MemoryBlock128;
	startAddr=_GroupMemoryBlock[4]._endAddr+1;

}

WJS_MemoryPool_FIX::~WJS_MemoryPool_FIX()
{
	if(_pInitBlock._startAddr)
	{
		WJS_FREE(_pInitBlock._startAddr);
	}
	_pUsedMemory=0;
}

//void* WJS_MemoryPool_FIX::Malloc_8()
//{
//	if(_GroupMemoryBlock[0].bHasSpace())
//	{
//		_pUsedMemory+=8;
//		stFixedMemoryBlock* p=_GroupMemoryBlock[0]._pBeginFreeblock;
//		p->_bUsed=true;
//		_GroupMemoryBlock[0]._pBeginFreeblock=_GroupMemoryBlock[0]._pBeginFreeblock->_pNext;
//		return p->_startAddr;
//	}
//	return 0;
//}
//void* WJS_MemoryPool_FIX::Malloc_16()
//{
//	if(_GroupMemoryBlock[1].bHasSpace())
//	{
//		_pUsedMemory+=16;
//		stFixedMemoryBlock* p=_GroupMemoryBlock[1]._pBeginFreeblock;
//		p->_bUsed=true;
//		_GroupMemoryBlock[1]._pBeginFreeblock=_GroupMemoryBlock[1]._pBeginFreeblock->_pNext;
//		return p->_startAddr;
//	}
//	return 0;
//}
//void* WJS_MemoryPool_FIX::Malloc_32()
//{
//	if(_GroupMemoryBlock[2].bHasSpace())
//	{
//		_pUsedMemory+=32;
//		stFixedMemoryBlock* p=_GroupMemoryBlock[2]._pBeginFreeblock;
//		p->_bUsed=true;
//		_GroupMemoryBlock[2]._pBeginFreeblock=_GroupMemoryBlock[2]._pBeginFreeblock->_pNext;
//		return p->_startAddr;
//	}
//	return 0;	
//}
//void* WJS_MemoryPool_FIX::Malloc_64()
//{
//	if(_GroupMemoryBlock[3].bHasSpace())
//	{
//		_pUsedMemory+=64;
//		stFixedMemoryBlock* p=_GroupMemoryBlock[3]._pBeginFreeblock;
//		p->_bUsed=true;
//		_GroupMemoryBlock[3]._pBeginFreeblock=_GroupMemoryBlock[3]._pBeginFreeblock->_pNext;
//		return p->_startAddr;
//	}
//	return 0;	
//}
//void* WJS_MemoryPool_FIX::Malloc_128()
//{
//	if(_GroupMemoryBlock[4].bHasSpace())
//	{
//		_pUsedMemory+=128;
//		stFixedMemoryBlock* p=_GroupMemoryBlock[4]._pBeginFreeblock;
//		p->_bUsed=true;
//		_GroupMemoryBlock[4]._pBeginFreeblock=_GroupMemoryBlock[4]._pBeginFreeblock->_pNext;
//		return p->_startAddr;
//	}
//	return 0;	
//}


void* WJS_MemoryPool_FIX::Malloc(int size)
{
	if(size>128)
		return 0;

	void* p=0;
	if(size<=8)
	{
		p=Malloc_8();
		if(p)
			return p;
	}

	if(size<=16)
	{
		p=Malloc_16();
		if(p)
			return p;
	}

	if(size<=32)
	{
		p=Malloc_32();
		if(p)
			return p;
	}

	if(size<=64)
	{
		p=Malloc_64();
		if(p)
			return p;
	}

	if(size<=128)
	{
		p=Malloc_128();
		if(p)
			return p;
	}

	return 0;
}


int   WJS_MemoryPool_FIX::GetFreeMemory()
{
	return (_pInitBlock._length-_pUsedMemory);
}

int   WJS_MemoryPool_FIX::GetUseMemory()
{
	return _pUsedMemory;
}

bool  WJS_MemoryPool_FIX::Free(void* p)
{
	if(!p)
		return false;

	if(!bAdrSpace(p))
		return false;

	//
	if(_GroupMemoryBlock[0].bAdrSpace(p))
	{
		//求偏移量
		//int index=((WJS_CHAR*)p-_GroupMemoryBlock[0]._startAddr)/8;
		int index=((WJS_CHAR*)p-_GroupMemoryBlock[0]._startAddr)>>3;

		if(_MemoryBlock8[index]._bUsed)
		{
			_pUsedMemory-=8;
			_MemoryBlock8[index]._bUsed=false;
			_MemoryBlock8[index]._pNext=_GroupMemoryBlock[0]._pBeginFreeblock;
			_GroupMemoryBlock[0]._pBeginFreeblock=&_MemoryBlock8[index];
			return true;
		}
		else
			return false;
	}
	//
	if(_GroupMemoryBlock[1].bAdrSpace(p))
	{
		//求偏移量
		//int index=((WJS_CHAR*)p-_GroupMemoryBlock[1]._startAddr)/16;
		int index=((WJS_CHAR*)p-_GroupMemoryBlock[1]._startAddr)>>4;
		if(_MemoryBlock16[index]._bUsed)
		{
			_pUsedMemory-=16;
			_MemoryBlock16[index]._bUsed=false;
			_MemoryBlock16[index]._pNext=_GroupMemoryBlock[1]._pBeginFreeblock;
			_GroupMemoryBlock[1]._pBeginFreeblock=&_MemoryBlock16[index];
			return true;
		}
		else
			return false;
	}
	//
	if(_GroupMemoryBlock[2].bAdrSpace(p))
	{
		//求偏移量
		//int index=((WJS_CHAR*)p-_GroupMemoryBlock[2]._startAddr)/32;
		int index=((WJS_CHAR*)p-_GroupMemoryBlock[2]._startAddr)>>5;
		if(_MemoryBlock32[index]._bUsed)
		{
			_pUsedMemory-=32;
			_MemoryBlock32[index]._bUsed=false;
			_MemoryBlock32[index]._pNext=_GroupMemoryBlock[2]._pBeginFreeblock;
			_GroupMemoryBlock[2]._pBeginFreeblock=&_MemoryBlock32[index];
			return true;
		}
		else
			return false;
	}
	//
	if(_GroupMemoryBlock[3].bAdrSpace(p))
	{
		//求偏移量
		//int index=((WJS_CHAR*)p-_GroupMemoryBlock[3]._startAddr)/64;
		int index=((WJS_CHAR*)p-_GroupMemoryBlock[3]._startAddr)>>6;
		if(_MemoryBlock64[index]._bUsed)
		{
			_pUsedMemory-=64;
			_MemoryBlock64[index]._bUsed=false;
			_MemoryBlock64[index]._pNext=_GroupMemoryBlock[3]._pBeginFreeblock;
			_GroupMemoryBlock[3]._pBeginFreeblock=&_MemoryBlock64[index];
			return true;
		}
		else
			return false;
	}
	//
	if(_GroupMemoryBlock[4].bAdrSpace(p))
	{
		//求偏移量
		//int index=((WJS_CHAR*)p-_GroupMemoryBlock[4]._startAddr)/128;
		int index=((WJS_CHAR*)p-_GroupMemoryBlock[4]._startAddr)>>7;
		if(_MemoryBlock128[index]._bUsed)
		{
			_pUsedMemory-=128;
			_MemoryBlock128[index]._bUsed=false;
			_MemoryBlock128[index]._pNext=_GroupMemoryBlock[4]._pBeginFreeblock;
			_GroupMemoryBlock[4]._pBeginFreeblock=&_MemoryBlock128[index];
			return true;
		}
		else
			return false;
	}
	return false;
}


