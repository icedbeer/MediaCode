/*
* create by wangweijin
*/
#pragma once
#include "WJS_MemoryPool.h"

//有固定粒数块的内存池


//1024K,更改配置
#define WJS_MPF_SIZE 1048576  //1024*1024
#define WJS_MPF_8NUM 2048   //2*1024
#define WJS_MPF_16NUM 7168  //7*1024
#define WJS_MPF_32NUM 8192   //8*1024
#define WJS_MPF_64NUM 4096   //4*1024
#define WJS_MPF_128NUM 3072   //3*1024



//固定池的最大字节数,现在为128字节
#define WJS_FIX_MAXBYTES 128

//固定大小内存块
struct stFixedMemoryBlock
{
	WJS_CHAR*  _startAddr;//起始地址
	bool       _bUsed;    //是否使用

	stFixedMemoryBlock* _pNext;

	stFixedMemoryBlock()
	{
		_startAddr=0;
		_bUsed=false;
		_pNext=0;
	}
};


//一组内存块的信息
struct stGroupFixedMemoryBlock
{
	//是一个闭合区间,[]
	WJS_CHAR*  _startAddr;//起始地址
	WJS_CHAR*  _endAddr;  //结束地址
	int        _length;   //内存块长度

	stFixedMemoryBlock* _pBeginFreeblock; //首块内存块

	stGroupFixedMemoryBlock()
	{
		_startAddr=0;
		_endAddr=0;
		_length=0;
		_pBeginFreeblock=0;
	}

	inline bool bHasSpace()
	{
		if(_pBeginFreeblock)
			return true;
		return false;
	}

	void computerEndAddr()
	{
		_endAddr=_startAddr+(_length-1);
	}

	inline bool  bAdrSpace(void* p)//是否是这个地址空间
	{
		if(p>=_startAddr && p<=_endAddr)
			return true;

		return false;
	}

};

class WJS_MemoryPool_FIX
{
public:
	WJS_MemoryPool_FIX();
	~WJS_MemoryPool_FIX();

public:
	void* Malloc(int size);//分配

	bool  Free(void* p);   //释放

	int   GetTotalMemory(){ return _pInitBlock._length;} //得到总内存

	int   GetFreeMemory();  //得到空闲内存数

	int   GetUseMemory();   //得到使用的内存数

	
	inline bool  bAdrSpace(void* p)//是否是这个地址空间
	{
		if(p>=_pInitBlock._startAddr && p<=_pInitBlock._endAddr)
			return true;
		return false;
	}


protected:
	//void* Malloc_8();  //分配
	//void* Malloc_16();
	//void* Malloc_32();
	//void* Malloc_64();
	//void* Malloc_128();
	inline void* WJS_MemoryPool_FIX::Malloc_8()
	{
		if(_GroupMemoryBlock[0].bHasSpace())
		{
			_pUsedMemory+=8;
			stFixedMemoryBlock* p=_GroupMemoryBlock[0]._pBeginFreeblock;
			p->_bUsed=true;
			_GroupMemoryBlock[0]._pBeginFreeblock=_GroupMemoryBlock[0]._pBeginFreeblock->_pNext;
			return p->_startAddr;
		}
		return 0;
	}
	inline void* WJS_MemoryPool_FIX::Malloc_16()
	{
		if(_GroupMemoryBlock[1].bHasSpace())
		{
			_pUsedMemory+=16;
			stFixedMemoryBlock* p=_GroupMemoryBlock[1]._pBeginFreeblock;
			p->_bUsed=true;
			_GroupMemoryBlock[1]._pBeginFreeblock=_GroupMemoryBlock[1]._pBeginFreeblock->_pNext;
			return p->_startAddr;
		}
		return 0;
	}
	inline void* WJS_MemoryPool_FIX::Malloc_32()
	{
		if(_GroupMemoryBlock[2].bHasSpace())
		{
			_pUsedMemory+=32;
			stFixedMemoryBlock* p=_GroupMemoryBlock[2]._pBeginFreeblock;
			p->_bUsed=true;
			_GroupMemoryBlock[2]._pBeginFreeblock=_GroupMemoryBlock[2]._pBeginFreeblock->_pNext;
			return p->_startAddr;
		}
		return 0;	
	}
	inline void* WJS_MemoryPool_FIX::Malloc_64()
	{
		if(_GroupMemoryBlock[3].bHasSpace())
		{
			_pUsedMemory+=64;
			stFixedMemoryBlock* p=_GroupMemoryBlock[3]._pBeginFreeblock;
			p->_bUsed=true;
			_GroupMemoryBlock[3]._pBeginFreeblock=_GroupMemoryBlock[3]._pBeginFreeblock->_pNext;
			return p->_startAddr;
		}
		return 0;	
	}
	inline void* WJS_MemoryPool_FIX::Malloc_128()
	{
		if(_GroupMemoryBlock[4].bHasSpace())
		{
			_pUsedMemory+=128;
			stFixedMemoryBlock* p=_GroupMemoryBlock[4]._pBeginFreeblock;
			p->_bUsed=true;
			_GroupMemoryBlock[4]._pBeginFreeblock=_GroupMemoryBlock[4]._pBeginFreeblock->_pNext;
			return p->_startAddr;
		}
		return 0;	
	}


protected:
	stMemoryBlock           _pInitBlock;//初始内存分配
	int                     _pUsedMemory;//已使用内存

	stGroupFixedMemoryBlock  _GroupMemoryBlock[5];
	stFixedMemoryBlock      _MemoryBlock8[WJS_MPF_8NUM];
	stFixedMemoryBlock      _MemoryBlock16[WJS_MPF_16NUM];
	stFixedMemoryBlock      _MemoryBlock32[WJS_MPF_32NUM];
	stFixedMemoryBlock      _MemoryBlock64[WJS_MPF_64NUM];
	stFixedMemoryBlock      _MemoryBlock128[WJS_MPF_128NUM];

private:
	WJS_MemoryPool_FIX& operator = (const WJS_MemoryPool_FIX& pool);
};