#include "memory/WJS_MemoryPoolManage.h"


#define MEMORYPOOLNUM 2

#ifdef __WJS_USEMEMORYPOOL__
WJS_MemoryPoolManage g_PoolManage[MEMORYPOOLNUM];
#endif

#ifdef __MEMLOG__
int WJS_MemoryPoolManage::_FileLogIndex=0;
#endif

WJS_MemoryPoolManage::WJS_MemoryPoolManage(int initSize)
:_pBeginMemoryPoolNormal(0)
,_pBeginMemoryPoolLittle(0)
,_iPoolNum(0)
,_OnlyUseForLittleMemoryNum(0)
{
#ifdef __MEMLOG__
	string logPathName="MemLog";
	char ch[256];
	sprintf(ch,"MemLog%d_",_FileLogIndex++);
	string preFileName=ch;
	_FileLog.CreateLog(logPathName,preFileName,true);
	_FileLog.Log(eLogTypeNone,"WJS_MemoryPoolManage()");
#endif
	
	AddMemoryPool(initSize,true);
}

WJS_MemoryPoolManage::~WJS_MemoryPoolManage()
{
	m_PoolTree.Free();

   WJS_MemoryPool* p=_pBeginMemoryPoolNormal;
   while(p)
   {
	   _pBeginMemoryPoolNormal=_pBeginMemoryPoolNormal->_pNext;
#ifdef __MEMLOG__
	   _FileLog.Logf(eLogTypeNone,"WJS_MemoryPoolManage 析构 池子=%d,起始地址=%d,大小=%d",(int)p,p->GetFullBlock()._startAddr,p->GetFullBlock()._length);
#endif
	   delete p;
	   p=_pBeginMemoryPoolNormal;
   }

   p=_pBeginMemoryPoolLittle;
   while(p)
   {
	   _pBeginMemoryPoolLittle=_pBeginMemoryPoolLittle->_pNext;
#ifdef __MEMLOG__
	   _FileLog.Logf(eLogTypeNone,"WJS_MemoryPoolManage 析构 池子=%d,起始地址=%d,大小=%d",(int)p,p->GetFullBlock()._startAddr,p->GetFullBlock()._length);
#endif
	   delete p;
	   p=_pBeginMemoryPoolLittle;
   }
#ifdef __MEMLOG__
   _FileLog.Log(eLogTypeNone,"~WJS_MemoryPoolManage()");
#endif
}

int   WJS_MemoryPoolManage::GetTotalMemory()
{
	WJS_AutoLock autolock(&_MFLock);
	int total=0;
	WJS_MemoryPool* p=_pBeginMemoryPoolNormal;
	while(p)
	{
		total+=p->GetTotalMemory();
		p=p->_pNext;
	}

	p=_pBeginMemoryPoolLittle;
	while(p)
	{
		total+=p->GetTotalMemory();
		p=p->_pNext;
	}

	
	return total+_FixedMemoryPool.GetTotalMemory();
}

int   WJS_MemoryPoolManage::GetFreeMemory()
{
	WJS_AutoLock autolock(&_MFLock);
	int totalFree=0;
	WJS_MemoryPool* p=_pBeginMemoryPoolNormal;
	while(p)
	{
		totalFree+=p->GetFreeMemory();
		p=p->_pNext;
	}

	p=_pBeginMemoryPoolLittle;
	while(p)
	{
		totalFree+=p->GetFreeMemory();
		p=p->_pNext;
	}
	return totalFree+_FixedMemoryPool.GetFreeMemory();
}

int   WJS_MemoryPoolManage::GetUseMemory()
{
	WJS_AutoLock autolock(&_MFLock);
	int totalUse=0;
	WJS_MemoryPool* p=_pBeginMemoryPoolNormal;
	while(p)
	{
		totalUse+=p->GetUseMemory();
		p=p->_pNext;
	}

	p=_pBeginMemoryPoolLittle;
	while(p)
	{
		totalUse+=p->GetUseMemory();
		p=p->_pNext;
	}
	return totalUse+_FixedMemoryPool.GetUseMemory();
}

int   WJS_MemoryPoolManage::GetNotFixedPoolNum()
{
	WJS_AutoLock autolock(&_MFLock);

	int count=0;
	WJS_MemoryPool* p=_pBeginMemoryPoolNormal;
	while(p)
	{
		count++;
		p=p->_pNext;
	}
	p=_pBeginMemoryPoolLittle;
	while(p)
	{
		count++;
		p=p->_pNext;
	}
	return count;
}

bool WJS_MemoryPoolManage::AddMemoryPool(int size,bool bOnlyUseForLittleMemory)//增加池子
{
	int t_size=size;
	//每个池子，最小1M
	if(size<MINMEMORYBLOCK_4M)
	{
		//if(_iPoolNum<2)
		//  t_size=MINMEMORYBLOCK_1M;
		//else 
		if(_iPoolNum<2)
		  t_size=MINMEMORYBLOCK_4M;
		else if(_iPoolNum<4)
		  t_size=MINMEMORYBLOCK_4M;
		else if(_iPoolNum<6)
			t_size=MINMEMORYBLOCK_8M;
		else
		    t_size=MINMEMORYBLOCK_16M;
		  
	}
	else
	{
		//是否是4个字节对齐,也就是都是4字节的部数
#ifdef USEMEMORYALIGH
		 t_size=(size+3) & (~3);
#else
		 t_size=size;
#endif
		 if(_iPoolNum>=8)
		 {
			 if(t_size<MINMEMORYBLOCK_16M)
				  t_size=MINMEMORYBLOCK_16M;
		 }
		 else if(_iPoolNum>=6)
		 {
			 if(t_size<MINMEMORYBLOCK_8M)
				 t_size=MINMEMORYBLOCK_8M;
		 }
		 else if(_iPoolNum>=4)
		 {
			 if(t_size<MINMEMORYBLOCK_4M)
				 t_size=MINMEMORYBLOCK_4M;
		 }

	}

#ifdef __MEMLOG__
	WJS_MemoryPool* p=new WJS_MemoryPool(&_FileLog,t_size,bOnlyUseForLittleMemory);
#else
	WJS_MemoryPool* p=new WJS_MemoryPool(NULL,t_size,bOnlyUseForLittleMemory);
#endif
	if(p)
	{
		if(p->GetFullBlock()._startAddr==0)
		{
#ifdef __MEMLOG__
			_FileLog.Logf(eLogTypeNone,"AddMemoryPool失败 池子=%d,大小=%d",(int)p,t_size);
#endif
			delete p;
			return false;
		}

		_iPoolNum++;
		if(bOnlyUseForLittleMemory)
		{
			_OnlyUseForLittleMemoryNum++;

			p->_pNext = _pBeginMemoryPoolLittle;
			_pBeginMemoryPoolLittle = p;
		}
		else
		{
			p->_pNext=_pBeginMemoryPoolNormal;
			_pBeginMemoryPoolNormal=p;
		}
		
		//使用起始地址作为key
		m_PoolTree.Insert(p->GetFullBlock()._startAddr,p);
#ifdef __MEMLOG__
		_FileLog.Logf(eLogTypeNone,"AddMemoryPool 池子=%d,大小=%d,当前池数=%d",(int)p,t_size,_iPoolNum);
#endif
		return true;
	}
	else
	{
		return false;
	}
}

void* WJS_MemoryPoolManage::Malloc(int size)
{
	//<=0或者超过256M,判定为异常分配
	if(size<=0 || size>268435456)
	{
#ifdef __MEMLOG__
		_FileLog.Logf(eLogTypeNone,"请求分配内存异常=%d",size);
#endif
		return 0;
	}
	//if(size == 260 || size==268 /*|| size==48 || size==1280*/)
	//{
	//	int a;
	//	a=10;
	//}
	WJS_AutoLock autolock(&_MFLock);

	//如果符合固定字节长度的,先在固定池中分配
	if(size<=WJS_FIX_MAXBYTES)
	{
		void* t_p=_FixedMemoryPool.Malloc(size);
		if(t_p)
		{
//#ifdef __MEMLOG__
//			_FileLog.Logf(eLogTypeNone,"请求分配内存%x+%d",(int)((int*)((char*)t_p)),size);
//#endif
			return t_p;
		}
	}

	//////
	//动态中增加8个字节
	///////
	size += 8;
#ifdef USEMEMORYALIGH
	size=(size+3) & (~3);
#endif

	if(size<=512)
	{
		WJS_MemoryPool* p=_pBeginMemoryPoolLittle; //先从小的找
		void* pAdr=0;
		while(p)
		{
			pAdr=p->Malloc(size);
			if(pAdr)
			{
//#ifdef __MEMLOG__
//				_FileLog.Logf(eLogTypeNone,"请求分配内存%x+%d",(int)((int*)((char*)pAdr)),size);
//#endif
				return pAdr;
			}
			p=p->_pNext;
		}
	}

	//由动态池列表中分配
	WJS_MemoryPool* p=_pBeginMemoryPoolNormal;
	void* pAdr=0;
	while(p)
	{
		pAdr=p->Malloc(size);
		if(pAdr)
		{
//#ifdef __MEMLOG__
//			_FileLog.Logf(eLogTypeNone,"请求分配内存%x+%d",(int)((int*)((char*)pAdr)),size);
//#endif
			return pAdr;
		}
		p=p->_pNext;
	}

	//最多分配6个
	if(size<=512 && _OnlyUseForLittleMemoryNum<6)
	{
		//新增一个池子,进行分配
		if(AddMemoryPool(size,true))
		{
			if(_pBeginMemoryPoolLittle)
			{
				void* pv=_pBeginMemoryPoolLittle->Malloc(size);
				if(pv)
				{
//#ifdef __MEMLOG__
//					_FileLog.Logf(eLogTypeNone,"请求分配内存%x+%d",(int)((int*)((char*)pv)),size);
//#endif
				   return pv;
				}
			}
		}
	}
	else
	{
		if(AddMemoryPool(size,false))
		{
			if(_pBeginMemoryPoolNormal)
			{
				void* pv=_pBeginMemoryPoolNormal->Malloc(size);
				if(pv)
				{
//#ifdef __MEMLOG__
//					_FileLog.Logf(eLogTypeNone,"请求分配内存%x+%d",(int)((int*)((char*)pv)),size);
//#endif
				  return pv;
				}
			}
		}
	}
#ifdef __MEMLOG__
	_FileLog.Logf(eLogTypeNone,"请求分配内存失败=%d",size);
#endif
	return 0;
}

bool  WJS_MemoryPoolManage::Free(void* p)
{
	WJS_AutoLock autolock(&_MFLock);

	//确认此内存所属池子
	if(_FixedMemoryPool.bAdrSpace(p))
	{
		bool bret=_FixedMemoryPool.Free(p);
//#ifdef __MEMLOG__
//		_FileLog.Logf(eLogTypeNone,"释放固定内存%x,结果=%d",(int)((int*)(p)),(int)bret);
//#endif
		return bret;
	}

	//查找
	WJS_MemoryPool* pm=FoundPoolWithP(p);
	if(pm)
	{
			if(pm->Free(p))
			{
//#ifdef __MEMLOG__
//				_FileLog.Logf(eLogTypeNone,"释放动态内存%x",(int)((int*)(p)));
//#endif
				if(_iPoolNum>=12 && (pm->GetUseMemory()==0))
				{
				   //检测是否可释放池子
					if(pm->CheckCanFreePool())
					{
						DeleteOneMemoryPool(pm);
					}
				}
				return true;
			}
			else
			{
//#ifdef __MEMLOG__
//				_FileLog.Logf(eLogTypeNone,"释放动态内存%x,失败",(int)((int*)(p)));
//#endif
				return false;
			}
	}
//#ifdef __MEMLOG__
//	_FileLog.Logf(eLogTypeNone,"释放未知内存%x",(int)((int*)(p)));
//#endif
	return false;
}

bool WJS_MemoryPoolManage::DeleteOneMemoryPool(WJS_MemoryPool* pPool)
{
	WJS_MemoryPool* pPre=0;
	WJS_MemoryPool* pm=0;

	if(pPool->GetOnlyUseForLittleMemory())
	{
		pPre=0;
		pm=_pBeginMemoryPoolLittle;
		while(pm)
		{
			if(pm==pPool)
			{
				if(pPre)
				{
					pPre->_pNext=pm->_pNext;

					m_PoolTree.DeleteKey(pm->GetFullBlock()._startAddr);
#ifdef __MEMLOG__
					_FileLog.Logf(eLogTypeNone,"DeleteOneMemoryPool 小池子=%d,起始地址=%d,大小=%d,当前池数=%d",(int)pm,(int)pm->GetFullBlock()._startAddr,pm->GetFullBlock()._length,_iPoolNum);
#endif
					delete pm;
				}
				else
				{
					_pBeginMemoryPoolLittle=_pBeginMemoryPoolLittle->_pNext;
					m_PoolTree.DeleteKey(pm->GetFullBlock()._startAddr);
#ifdef __MEMLOG__
					_FileLog.Logf(eLogTypeNone,"DeleteOneMemoryPool 小池子=%d,起始地址=%d,大小=%d,当前池数=%d",(int)pm,(int)pm->GetFullBlock()._startAddr,pm->GetFullBlock()._length,_iPoolNum);
#endif
					delete pm;
				}
				_iPoolNum--;
				return true;
			}

			pPre=pm;
			pm=pm->_pNext;
		}
	}


	pPre=0;
	pm=_pBeginMemoryPoolNormal;
	while(pm)
	{
		if(pm==pPool)
		{
			if(pPre)
			{
				pPre->_pNext=pm->_pNext;

				m_PoolTree.DeleteKey(pm->GetFullBlock()._startAddr);
#ifdef __MEMLOG__
				_FileLog.Logf(eLogTypeNone,"DeleteOneMemoryPool 大池子=%d,起始地址=%d,大小=%d,当前池数=%d",(int)pm,(int)pm->GetFullBlock()._startAddr,pm->GetFullBlock()._length,_iPoolNum);
#endif
				delete pm;
			}
			else
			{
				_pBeginMemoryPoolNormal=_pBeginMemoryPoolNormal->_pNext;
				m_PoolTree.DeleteKey(pm->GetFullBlock()._startAddr);
#ifdef __MEMLOG__
				_FileLog.Logf(eLogTypeNone,"DeleteOneMemoryPool 大池子=%d,起始地址=%d,大小=%d,当前池数=%d",(int)pm,(int)pm->GetFullBlock()._startAddr,pm->GetFullBlock()._length,_iPoolNum);
#endif
				delete pm;
			}
			_iPoolNum--;
			return true;
		}

		pPre=pm;
		pm=pm->_pNext;
	}



	return false;
}

WJS_MemoryPool* WJS_MemoryPoolManage::FoundPoolWithP(void* p)
{
	WJS_BBSTree_NM_Node<WJS_CHAR*,WJS_MemoryPool*>* pNode=m_PoolTree.m_RootNode;

	while(pNode)
	{
		if(p < pNode->m_key)
		{
			pNode=pNode->m_pLeftChild;
		}
		else
		{
			if(pNode->m_data->bAdrSpace(p))
				return pNode->m_data;

			else
				pNode=pNode->m_pRightChild;
		}
	}

	return 0;

}


#ifdef __WJS_USEMEMORYPOOL__
WJS_MemoryPoolManage* GetMemoryPoolManage()
{
	return &g_PoolManage[0];
}
WJS_MemoryPoolManage* GetMemoryPoolManage(int index)
{
	return &g_PoolManage[index];
}
#endif





