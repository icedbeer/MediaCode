/*
* create by wangweijin
*/
#pragma once

#include "common/Log/Logger.h"

//定义内存按四字节对齐
#define USEMEMORYALIGH

#define WJS_MALLOC malloc
#define WJS_FREE   free
#define WJS_CHAR   char


class IGetMemoryBlock
{
public:
	virtual void* GetBlock()=0;
};

//内存块 ,双向
struct stMemoryBlock
{
  WJS_CHAR*  _startAddr;//起始地址
  WJS_CHAR*  _endAddr;  //结束地址
  int        _length;   //长度

  stMemoryBlock* _pPre;
  stMemoryBlock* _pNext;

  stMemoryBlock()
  {
	  _pPre=0;
	  _pNext=0;
  }

  //void ReInit()
  //{
	 // _pPre=0;
	 // _pNext=0;
  //}

  //闭合[],计算
  inline void computerEndAddr()
  {
	  _endAddr=_startAddr+(_length-1);
  }

  //从一块内存中,分割一块内存出来
  inline stMemoryBlock* SplitOneBlock(int size,stMemoryBlock** pFreeBlockBegin,IGetMemoryBlock* pIGetMemoryBlock)
  {
	  if(_length>size)
	  {
		  stMemoryBlock* pBlock=(stMemoryBlock*)(pIGetMemoryBlock->GetBlock());//new stMemoryBlock();
		  pBlock->_length=size;
		  pBlock->_startAddr=_startAddr;
		  pBlock->computerEndAddr();

		  _length-=size;
		  _startAddr=pBlock->_endAddr+1;
		  computerEndAddr();

		  return pBlock;
	  }
	  else if(_length==size)
	  {
		  if(_pPre)
		  {
			  _pPre->_pNext=_pNext;
		  }
		  else
		  {
			  *pFreeBlockBegin=_pNext;
		  }

		  if(_pNext)
		  {
			_pNext->_pPre=_pPre;
		  }
		  //此处不用将前后节点置空，因为后面的处理函数做了这个事情

		  return this;
	  }
	  return 0;
  }

  //比较容量,大于等于时返回true,排序时使用
  inline bool CompareCapcityBig(stMemoryBlock* pBlock)
  {
	  return _length>=pBlock->_length;
  }

   //比较容量,小于等于时返回true,排序时使用
  inline bool CompareCapcityLittle(stMemoryBlock* pBlock)
  {
	  return _length<=pBlock->_length;
  }

  //空间是否够的判定
  inline bool CapcityEnough(int size)
  {
	  return _length>=size;
  }

  //是否可以合并
  inline bool bCanUnit(stMemoryBlock* pBlock)
  {
	  if(_endAddr+1==pBlock->_startAddr)
		  return true;

	  return false;
  }

  //地址是否在其后
  inline bool bAfterAddr(stMemoryBlock* pBlock)
  {
	  if(_startAddr>pBlock->_startAddr)
		  return true;
	  return false;
  }

};

//桶数,按1M计，大约每个64KB,
//分桶的方法 一种是按大小，可用于未使用列表
//一种是按地址用于已使用列表
//测试结果，由16-32-64,随着桶数的提高，插入时间要应增长，16-32查找时间变小，后面基本不变，删除时间变少
#define USEBLOCKBUCKET  512
#define UNUSEBLOCKBUCKET 512




class WJS_MemoryPool : public IGetMemoryBlock
{
public:
	WJS_MemoryPool(FileLogger* pLogger,int size=512,bool bOnlyUseForLittleMemory=false);
	~WJS_MemoryPool();

public:
	void* Malloc(int size);//分配

	bool  Free(void* p);  //释放

	int   GetTotalMemory(){return _pInitBlock._length;} //得到总内存

	inline int   GetFreeMemory(){return _pInitBlock._length-_UsedMemory;};  //得到空闲内存

	inline int   GetUseMemory(){return _UsedMemory;}   //得到使用内存


	//bool  bAdrSpace(void* p);
	inline bool  bAdrSpace(void* p)//是否是这个地址空间
	{
		if(p>=_pInitBlock._startAddr && p<=_pInitBlock._endAddr)
			return true;		
		return false;
	}

	inline bool  CheckCanFreePool()//查测是否可以释放这个池子
	{
		if(_UsedMemory!=0 || _pBucketUseBlockBeginNew || _bOnlyUseForLittleMemory)
			return false;

		for(int i=0;i<4;i++)
		{
			if(_pCashFreeBlock[i])
				return false;
		}

		for(int i=0;i<3;i++)
		{
			if(_pFixedCashFreeBlock[i])
				return false;
		}

		//for(int i=0;i<USEBLOCKBUCKET;i++)
		//{
		//	if(_pBucketUseBlockBegin[i])
		//		return false;
		//}
		return true;
	}

	stMemoryBlock& GetFullBlock(){return _pInitBlock;} //得到完整的内存块

	virtual void* GetBlock() {return GetBlockFromCash();}

	bool   GetOnlyUseForLittleMemory() {return _bOnlyUseForLittleMemory;} //得到是否仅为小内存
	void   SetOnlyUseForLittleMemory(bool bOnlyUseForLittleMemory){_bOnlyUseForLittleMemory = bOnlyUseForLittleMemory;}

protected:

	inline stMemoryBlock* FindAppropriateBlock(int size,int& bucket)//查找合适的内存块
	{
		//增加最后一个桶不分配小于512大小的
		bool tt_bCanUseEndBucket=false;
		if(size>=768)
			tt_bCanUseEndBucket=true;

		if(_LastFoundBlockBucket1>=0 && (_LastFoundBlockBucket1<UNUSEBLOCKBUCKET))
		{
			if(_pBucketFreeBlockBegin[_LastFoundBlockBucket1] && (_LogBucketFreeBlockMaxLength[_LastFoundBlockBucket1]>=size))
			{
				//最后一个桶
				if(_LastFoundBlockBucket1 != UNUSEBLOCKBUCKET-1 || tt_bCanUseEndBucket)
				{
					stMemoryBlock* pBlock=_pBucketFreeBlockBegin[_LastFoundBlockBucket1];

					while(pBlock)
					{
						if(pBlock->CapcityEnough(size))
						{
							bucket=_LastFoundBlockBucket1;
							return pBlock;
						}
						pBlock=pBlock->_pNext;
					}
				}

			}
		}

		if(_LastFoundBlockBucket2>=0 && _LastFoundBlockBucket2<UNUSEBLOCKBUCKET)
		{
			if(_pBucketFreeBlockBegin[_LastFoundBlockBucket2] && (_LogBucketFreeBlockMaxLength[_LastFoundBlockBucket2]>=size))
			{
				if(_LastFoundBlockBucket2 != UNUSEBLOCKBUCKET-1 || tt_bCanUseEndBucket)
				{
					stMemoryBlock* pBlock=_pBucketFreeBlockBegin[_LastFoundBlockBucket2];

					while(pBlock)
					{
						if(pBlock->CapcityEnough(size))
						{
							bucket=_LastFoundBlockBucket2;
							return pBlock;
						}
						pBlock=pBlock->_pNext;
					}
				}
			}
		}
		//分成奇偶
		for(int i=0;i<UNUSEBLOCKBUCKET;i+=2)
		{
			if(_pBucketFreeBlockBegin[i] && (_LogBucketFreeBlockMaxLength[i]>=size) && (i!=_LastFoundBlockBucket1) && (i!=_LastFoundBlockBucket2))
			{
				stMemoryBlock* pBlock=_pBucketFreeBlockBegin[i];

				while(pBlock)
				{
					if(pBlock->CapcityEnough(size))
					{
						bucket=i;
						if(_LastFoundBlockBucket1!=i)
						{
							_LastFoundBlockBucket1 = i;
							_LastFoundBlockBucket2 = _LastFoundBlockBucket1;
						}
						
						return pBlock;
					}
					pBlock=pBlock->_pNext;
				}
			}
		}

		for(int i=1;i<UNUSEBLOCKBUCKET-1;i+=2)
		{
			if(_pBucketFreeBlockBegin[i] && (_LogBucketFreeBlockMaxLength[i]>=size) && (i!=_LastFoundBlockBucket1) && (i!=_LastFoundBlockBucket2))
			{
				stMemoryBlock* pBlock=_pBucketFreeBlockBegin[i];

				while(pBlock)
				{
					if(pBlock->CapcityEnough(size))
					{
						bucket=i;
						if(_LastFoundBlockBucket1!=i)
						{
							_LastFoundBlockBucket1 = i;
							_LastFoundBlockBucket2 = _LastFoundBlockBucket1;
						}
						return pBlock;
					}
					pBlock=pBlock->_pNext;
				}
			}
		}

		int i=UNUSEBLOCKBUCKET-1;
		if(_pBucketFreeBlockBegin[i] && tt_bCanUseEndBucket && (_LogBucketFreeBlockMaxLength[i]>=size) && (i!=_LastFoundBlockBucket1) && (i!=_LastFoundBlockBucket2))
		{
			stMemoryBlock* pBlock=_pBucketFreeBlockBegin[i];

			while(pBlock)
			{
				if(pBlock->CapcityEnough(size))
				{
					bucket=i;
					if(_LastFoundBlockBucket1!=i)
					{
						_LastFoundBlockBucket1 = i;
						_LastFoundBlockBucket2 = _LastFoundBlockBucket1;
					}
					return pBlock;
				}
				pBlock=pBlock->_pNext;
			}
		}

		return 0;
	}

	void  Push_front_UseBlcok(stMemoryBlock* pBlock);//压到使用块中

	void  Push_front_Freeblock(stMemoryBlock* pBlock);//压到空闲

	void  Push_insert_Freeblock(stMemoryBlock* pBlock);//插到空闲中

	//inline stMemoryBlock* FindUseBlock(void* p,int& bucket)//查找合适的内存块
	//{
	//	//先要获取其所在的桶
	//	bucket = ComputerInBlucketUse((WJS_CHAR*)p);
	//	if(bucket>=0 && (bucket<USEBLOCKBUCKET))
	//	{
	//		if(_pBucketUseBlockBegin[bucket])
	//		{
	//			stMemoryBlock* pBlock=_pBucketUseBlockBegin[bucket];

	//			while(pBlock)
	//			{
	//				if(pBlock->_startAddr==(WJS_CHAR*)p)
	//					return pBlock;
	//				pBlock=pBlock->_pNext;
	//			}
	//		}
	//	}
	//	return 0;
	//};

	//inline int  ComputerInBlucketUse(WJS_CHAR* pAdr)//计算属哪一个桶
	//{
	//	int len = pAdr - _pInitBlock._startAddr;
	//    if(len>_pInitBlock._length)
	//		return USEBLOCKBUCKET; //直接用这个值，表明不在有效桶内
	//	int tt = len>>_BucketUseBitNum;
	//	if(tt >= USEBLOCKBUCKET)
	//	     tt=USEBLOCKBUCKET-1;
	//	return tt;	
	//}

	inline int  ComputerInBlucketUnUse(WJS_CHAR* pAdr)//计算属哪一个桶
	{
		int len = pAdr - _pInitBlock._startAddr;
		if(len>_pInitBlock._length)
			return UNUSEBLOCKBUCKET; //直接用这个值，表明不在有效桶内
		int tt = len >>_BucketUnUseBitNum;
		if(tt >= UNUSEBLOCKBUCKET)
			tt=UNUSEBLOCKBUCKET-1;
		return tt;	
	}

	void  InitPreAllocBlock(int num);//初始化预分配块
	stMemoryBlock* GetBlockFromCash();//从缓存中获取块
	inline void  PutBlockToCash(stMemoryBlock* pBlock)//将块放到缓存
	{
		if(_CashBlockNum>=10240)
		{
			_FreeBlockNum++;
			delete pBlock;
			return;
		}
		_CashBlockNum++;
		pBlock->_pNext = _PreAllocBlockBegin;
		if(_PreAllocBlockBegin)
			_PreAllocBlockBegin->_pPre = pBlock;

		pBlock->_pPre=0;

		_PreAllocBlockBegin=pBlock;
	}

	void  ComputerBucketInfo(int needspace,int& bucketsize,int& bucketbit); //计算桶信息

	void  ComputerBucketMaxFreeBlockLength(int bucket); //计算桶的最大块长度

protected:
	stMemoryBlock  _pInitBlock;//初始内存分配

	int   _UsedMemory;//使用的内存计数

	////////////////使用的不再用桶
	stMemoryBlock* _pBucketUseBlockBeginNew;
	//stMemoryBlock* _pBucketUseBlockBegin[USEBLOCKBUCKET];
	// //WJS_CHAR*     _pBucketUse[USEBLOCKBUCKET]; //代表了多个地址    
	// int           _BucketUseSizeV2; //每个桶的区间大小,进行修订将除法改为移位，那么则要求每一桶的大小为2^n
	// int           _BucketUseBitNum; //使用位数

	 stMemoryBlock* _pBucketFreeBlockBegin[UNUSEBLOCKBUCKET]; //未使用
	// WJS_CHAR*     _pBucketUnUse[UNUSEBLOCKBUCKET]; //代表了多个地址    
	 int            _BucketUnUseSizeV2; //未用区的每个桶大小
	  int           _BucketUnUseBitNum; //使用位数

	  int           _LogBucketFreeBlockMaxLength[UNUSEBLOCKBUCKET];  //记录某个桶的最大可用块的长度
	 stMemoryBlock* _pLastFreeBlockLog[UNUSEBLOCKBUCKET]; //最近的回收记录点


	 //预分配一些
	 stMemoryBlock* _PreAllocBlockBegin; 
	 int            _CashBlockNum; //缓存的块数

private:
	WJS_MemoryPool& operator = (const WJS_MemoryPool& pool);

public:
	WJS_MemoryPool* _pNext;

	int             _AllocBlockNum;
	int             _FreeBlockNum;
	int             _FreeFailedNum;

	int             _LastFoundBlockBucket1; //上一次找到块的桶
	int             _LastFoundBlockBucket2;

	//为加快分配速度，提供四种区间缓存,分别是128,256,512,1K,
	stMemoryBlock*  _pCashFreeBlock[4];
	int             _CashFreeBlockNum[4];
	int             _MaxCashFreeBlockNum[4];
	int             _CashFreeBlockSpace[4];

	///////////固定4K,16K,32K,72字节
	stMemoryBlock*  _pFixedCashFreeBlock[3];
	int             _FixedCashFreeBlockNum[3];
	int             _FixedMaxCashFreeBlockNum[3];
	int             _FixedcCashFreeBlockSpace[3];

	bool            _bOnlyUseForLittleMemory; //仅为小内存使用,最大分配512大小

	
	FileLogger*     _pLogger;





};

