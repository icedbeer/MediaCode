#include "WJS_MemoryPool.h"
#include <malloc.h>
#include <math.h>
#include <memory.h>

WJS_MemoryPool::WJS_MemoryPool(FileLogger* pLogger,int size,bool bOnlyUseForLittleMemory)
:_pNext(0)
,_UsedMemory(0)
,_PreAllocBlockBegin(0)
,_CashBlockNum(0)
,_AllocBlockNum(0)
,_FreeBlockNum(0)
,_FreeFailedNum(0)
,_LastFoundBlockBucket1(-1)
,_LastFoundBlockBucket2(-1)
,_bOnlyUseForLittleMemory(bOnlyUseForLittleMemory)
,_pLogger(pLogger)
,_pBucketUseBlockBeginNew(0)
{
	//4KB
	if(size<1048576)
		size=1048576;
		//return;
#ifdef USEMEMORYALIGH
	size=(size+3) & (~3);
#endif

	for(int i=0;i<UNUSEBLOCKBUCKET;i++)
	{
		_pBucketFreeBlockBegin[i]=0;
		_LogBucketFreeBlockMaxLength[i]=0;
		_pLastFreeBlockLog[i]=0;
	}


	for(int i=0;i<4;i++)
	{
		_pCashFreeBlock[i] = 0;
		_CashFreeBlockNum[i]=0;
	}

	_CashFreeBlockSpace[0]=128;
	_CashFreeBlockSpace[1]=256;
	_CashFreeBlockSpace[2]=512;
	_CashFreeBlockSpace[3]=1024;

	if(_bOnlyUseForLittleMemory)
	{
		_MaxCashFreeBlockNum[0]=16384;//128
		_MaxCashFreeBlockNum[1]=2048;//256
		_MaxCashFreeBlockNum[2]=1024;//512
		_MaxCashFreeBlockNum[3]=256;//1K
	}
	else
	{
		_MaxCashFreeBlockNum[0]=6144;//128
		_MaxCashFreeBlockNum[1]=512;//256
		_MaxCashFreeBlockNum[2]=256;//512
		_MaxCashFreeBlockNum[3]=256;//1K
	}


	for(int i=0;i<3;i++)
	{
		_pFixedCashFreeBlock[i]=0;
		_FixedCashFreeBlockNum[i]=0;
	}
	_FixedcCashFreeBlockSpace[0]=4108;
	_FixedcCashFreeBlockSpace[1]=16396;
	_FixedcCashFreeBlockSpace[2]=32780;

	_FixedMaxCashFreeBlockNum[0]=128;//4K
	_FixedMaxCashFreeBlockNum[1]=64; //16K
	_FixedMaxCashFreeBlockNum[2]=32; //32K

	//16M,这样每个桶只有32KB
	//现在对于16M的进行修改，只有一半的是用桶的

	_pInitBlock._startAddr=(WJS_CHAR*)WJS_MALLOC(size);
	_pInitBlock._length=size;
	_pInitBlock.computerEndAddr();

	if(size>=16777216)
	{
		ComputerBucketInfo(8338608 / UNUSEBLOCKBUCKET,_BucketUnUseSizeV2,_BucketUnUseBitNum);
	}
	else
	{
		ComputerBucketInfo(size / UNUSEBLOCKBUCKET,_BucketUnUseSizeV2,_BucketUnUseBitNum);
	}

	if(_pInitBlock._startAddr!=0)
	{
		InitPreAllocBlock(100);

		_pBucketFreeBlockBegin[0]=GetBlockFromCash();//new stMemoryBlock();
		_pBucketFreeBlockBegin[0]->_startAddr=_pInitBlock._startAddr;
		_pBucketFreeBlockBegin[0]->_length=_pInitBlock._length;
		_pBucketFreeBlockBegin[0]->computerEndAddr();
		_LogBucketFreeBlockMaxLength[0] = _pInitBlock._length;
	}

	if(_pLogger)
	{
		_pLogger->Logf(eLogTypeNone,"分配初始地址:%d,长度:%d",(int)_pInitBlock._startAddr,_pInitBlock._length);
	}

}

WJS_MemoryPool::~WJS_MemoryPool()
{
	if(_pInitBlock._startAddr)
	{
		WJS_FREE(_pInitBlock._startAddr);
	}

	for(int i=0;i<UNUSEBLOCKBUCKET;i++)
	{
		_pLastFreeBlockLog[i]=0;
	}

	stMemoryBlock* pBlock1=0;
	stMemoryBlock* pBlock2=0;
	for(int i=0;i<UNUSEBLOCKBUCKET;i++)
	{
		pBlock1 =_pBucketFreeBlockBegin[i];
		pBlock2=0;

		if(pBlock1!=0)
			pBlock2=pBlock1->_pNext;
		while(pBlock1!=0)
		{
			delete pBlock1;

			pBlock1=pBlock2;
			if(pBlock1!=0)
				pBlock2=pBlock1->_pNext;
			else
				pBlock2=0;
			_FreeBlockNum++;

		}
		_pBucketFreeBlockBegin[i]=0;
	}


	int tt_NotSafeFreeBlockCount = 0;
	pBlock1=_pBucketUseBlockBeginNew;
	pBlock2=0;
	if(pBlock1!=0)
		pBlock2=pBlock1->_pNext;
	while(pBlock1!=0)
	{
		if(_pLogger)
		{
			_pLogger->Logf(eLogTypeNone,"地址=%d,大小=%d",pBlock1->_startAddr,pBlock1->_length);
		}
		delete pBlock1;

		pBlock1=pBlock2;
		if(pBlock1!=0)
			pBlock2=pBlock1->_pNext;
		else
			pBlock2=0;
		_FreeBlockNum++;

		tt_NotSafeFreeBlockCount++;
	}
	_pBucketUseBlockBeginNew=0;

	//for(int i=0;i<USEBLOCKBUCKET;i++)
	//{
	//	pBlock1=_pBucketUseBlockBegin[i];
	//	pBlock2=0;
	//	if(pBlock1!=0)
	//		pBlock2=pBlock1->_pNext;
	//	while(pBlock1!=0)
	//	{
	//		delete pBlock1;

	//		pBlock1=pBlock2;
	//		if(pBlock1!=0)
	//			pBlock2=pBlock1->_pNext;
	//		else
	//			pBlock2=0;
	//		_FreeBlockNum++;
	//	}
	//	_pBucketUseBlockBegin[i]=0;
	//}

	if(_PreAllocBlockBegin)
	{
		pBlock1=_PreAllocBlockBegin;
		pBlock2=0;
		if(pBlock1!=0)
			pBlock2=pBlock1->_pNext;
		while(pBlock1!=0)
		{
			delete pBlock1;

			pBlock1=pBlock2;
			if(pBlock1!=0)
				pBlock2=pBlock1->_pNext;
			else
				pBlock2=0;
			_FreeBlockNum++;
		}
		_PreAllocBlockBegin=0;
	}

	//if(_FreeBlockNum!=_AllocBlockNum)
	//{
	//	int a;
	//	a=10;
	//}

	for(int i=0;i<4;i++)
	{
		pBlock1=_pCashFreeBlock[i];
		pBlock2=0;
		if(pBlock1!=0)
			pBlock2=pBlock1->_pNext;
		while(pBlock1!=0)
		{
			delete pBlock1;

			pBlock1=pBlock2;
			if(pBlock1!=0)
				pBlock2=pBlock1->_pNext;
			else
				pBlock2=0;
			_FreeBlockNum++;
		}
		_pCashFreeBlock[i]=0;
		_CashFreeBlockNum[i]=0;
	}

	for(int i=0;i<3;i++)
	{
		pBlock1=_pFixedCashFreeBlock[i];
		pBlock2=0;
		if(pBlock1!=0)
			pBlock2=pBlock1->_pNext;
		while(pBlock1!=0)
		{
			delete pBlock1;

			pBlock1=pBlock2;
			if(pBlock1!=0)
				pBlock2=pBlock1->_pNext;
			else
				pBlock2=0;
			_FreeBlockNum++;
		}
		_pFixedCashFreeBlock[i]=0;
		_FixedCashFreeBlockNum[i]=0;
	}

	if(tt_NotSafeFreeBlockCount>0 && _pLogger)
	{
		_pLogger->Logf(eLogTypeNone,"未安全释放内存块数:%d",tt_NotSafeFreeBlockCount);
	}

}

void* WJS_MemoryPool::Malloc(int size)
{
//#ifdef USEMEMORYALIGH
//	size=(size+3) & (~3);
//#endif
	//if(size == 2876)
	//{
	//	int a;
	//	a=10;
	//}

	//仅为小内存使用
	if(_bOnlyUseForLittleMemory && size>512)
		return 0;

	//////////先从缓存中找
	if(size<=1024)
	{
		for(int i=0;i<4;i++)
		{
			if(size<=_CashFreeBlockSpace[i])
			{
				if(_CashFreeBlockNum[i]>0 && _pCashFreeBlock[i])
				{
					stMemoryBlock* pBlock2 =  _pCashFreeBlock[i];
					_pCashFreeBlock[i] = _pCashFreeBlock[i]->_pNext;
					if(_pCashFreeBlock[i])
						_pCashFreeBlock[i]->_pPre = 0;
					pBlock2->_pNext = 0;
					_CashFreeBlockNum[i]--;
					_UsedMemory+=size;
					//前8个字节用来填充
					int tt_head[2];
					tt_head[0] = (int)pBlock2;
					tt_head[1] = tt_head[0]+48076; //BBCC
					memcpy(pBlock2->_startAddr,tt_head,sizeof(int)*2);
					Push_front_UseBlcok(pBlock2);
					return (void*)(pBlock2->_startAddr+8);

				}
			}
		}
	}
	else
	{
		for(int i=0;i<3;i++)
		{
			if(size==_FixedcCashFreeBlockSpace[i])
			{
				if(_FixedCashFreeBlockNum[i]>0 && _pFixedCashFreeBlock[i])
				{
					stMemoryBlock* pBlock2 =  _pFixedCashFreeBlock[i];
					_pFixedCashFreeBlock[i] = _pFixedCashFreeBlock[i]->_pNext;
					if(_pFixedCashFreeBlock[i])
						_pFixedCashFreeBlock[i]->_pPre = 0;
					pBlock2->_pNext = 0;
					_FixedCashFreeBlockNum[i]--;
					_UsedMemory+=size;
					//前8个字节用来填充
					int tt_head[2];
					tt_head[0] = (int)pBlock2;
					tt_head[1] = tt_head[0]+48076; //BBCC
					memcpy(pBlock2->_startAddr,tt_head,sizeof(int)*2);
					Push_front_UseBlcok(pBlock2);
					return (void*)(pBlock2->_startAddr+8);
				}
				break;
			}
		}
	}

	int bucket=0;
	stMemoryBlock* pBlock=FindAppropriateBlock(size,bucket);
	if(pBlock)
	{
		//找到的这一块是记录的那一块
		if(pBlock == _pLastFreeBlockLog[bucket])
		{
			_pLastFreeBlockLog[bucket]=0;
		}

		bool bNeedReComputer=false;
		if(/*bucket!=0 && */(pBlock->_length >=_LogBucketFreeBlockMaxLength[bucket]))
		{
			bNeedReComputer = true;
		}

		//
		stMemoryBlock* pSplitBlock=0;
		pSplitBlock=pBlock->SplitOneBlock(size,&_pBucketFreeBlockBegin[bucket],this);
		if(pSplitBlock)
		{
			_UsedMemory+=size;

			//前8个字节用来填充
			int tt_head[2];
			tt_head[0] = (int)pSplitBlock;
			tt_head[1] = tt_head[0]+48076; //BBCC
			memcpy(pSplitBlock->_startAddr,tt_head,sizeof(int)*2);
		   Push_front_UseBlcok(pSplitBlock);

		   if(bNeedReComputer)
		     ComputerBucketMaxFreeBlockLength(bucket);

		   return (void*)(pSplitBlock->_startAddr+8);
		}
	}

	return 0;
}


void  WJS_MemoryPool::Push_front_UseBlcok(stMemoryBlock* pBlock)
{
	//if(pBlock == _pBucketUseBlockBeginNew)
	//{
	//	int a;
	//	a=10;
	//}
	pBlock->_pPre=0;
	pBlock->_pNext=_pBucketUseBlockBeginNew;
	if(_pBucketUseBlockBeginNew)
		_pBucketUseBlockBeginNew->_pPre=pBlock;
	_pBucketUseBlockBeginNew=pBlock;


	//int bucket = ComputerInBlucketUse(pBlock->_startAddr);
	//if(bucket>=0 && (bucket<USEBLOCKBUCKET))
	//{
	//	pBlock->_pPre=0;
	//	pBlock->_pNext=_pBucketUseBlockBegin[bucket];
	//	if(_pBucketUseBlockBegin[bucket])
	//		_pBucketUseBlockBegin[bucket]->_pPre=pBlock;
	//	_pBucketUseBlockBegin[bucket]=pBlock;
	//}
	//else
	//{
	//	int a;
	//	a=10;
	//}
}

void  WJS_MemoryPool::Push_front_Freeblock(stMemoryBlock* pBlock)
{
	//if(pBlock->_length == 2876)
	//{
	//	int a;
	//	a=10;
	//}
	int bucket = ComputerInBlucketUnUse(pBlock->_startAddr);
	if(bucket>=0 && (bucket<UNUSEBLOCKBUCKET))
	{
		//if(pBlock == _pBucketFreeBlockBegin[bucket])
		//{
		//	int a;
		//	a=10;
		//}
		pBlock->_pPre=0;
		pBlock->_pNext=_pBucketFreeBlockBegin[bucket];
		if(_pBucketFreeBlockBegin[bucket])
			_pBucketFreeBlockBegin[bucket]->_pPre=pBlock;
		_pBucketFreeBlockBegin[bucket]=pBlock;

		if(/*bucket!=0 && */(pBlock->_length>_LogBucketFreeBlockMaxLength[bucket]))
		{
			_LogBucketFreeBlockMaxLength[bucket] = pBlock->_length;
		}
	}
	//else
	//{
	//	int a;
	//	a=10;
	//}

}

void  WJS_MemoryPool::Push_insert_Freeblock(stMemoryBlock* pBlock)
{
	//if(pBlock->_length == 2876)
	//{
	//	int a;
	//	a=10;
	//}
	int bucket = ComputerInBlucketUnUse(pBlock->_startAddr);
	if(bucket>=0 && (bucket<UNUSEBLOCKBUCKET))
	{
		if(_pBucketFreeBlockBegin[bucket])
		{
			if(/*bucket!=0 && */(pBlock->_length>_LogBucketFreeBlockMaxLength[bucket]))
			{
				_LogBucketFreeBlockMaxLength[bucket] = pBlock->_length;
			}

			//修改为直接插入，合并改到别的地方，统一做，或者定时做

			//从开始
			stMemoryBlock* pBlock_search = _pBucketFreeBlockBegin[bucket];
			stMemoryBlock* pBlock_Pre=0;
			//有记录,并且在记录之后，则从记录开始，无记录则还是从头开始
			if(_pLastFreeBlockLog[bucket]!=0)
			{
				if(pBlock->bAfterAddr(_pLastFreeBlockLog[bucket]))
				{
					pBlock_Pre = _pLastFreeBlockLog[bucket]; 
					pBlock_search = _pLastFreeBlockLog[bucket]->_pNext;	
				}
			}

			while(pBlock_search)
			{
				if(pBlock->bAfterAddr(pBlock_search))
				{
					pBlock_Pre=pBlock_search;
					pBlock_search=pBlock_search->_pNext;
				}
				else
				{

					//先看是否能合并
					if(pBlock->bCanUnit(pBlock_search))
					{
						pBlock_search->_startAddr=pBlock->_startAddr;
						pBlock_search->_length+=pBlock->_length;
						PutBlockToCash(pBlock);
						//delete pBlock;

						_pLastFreeBlockLog[bucket] = pBlock_search;

						if(/*bucket!=0 && */(pBlock_search->_length>_LogBucketFreeBlockMaxLength[bucket]))
						{
							_LogBucketFreeBlockMaxLength[bucket] = pBlock_search->_length;
						}

						if(pBlock_search->_pPre)
						{
							//与前一个是否能合并
							if(pBlock_search->_pPre->bCanUnit(pBlock_search))
							{
								stMemoryBlock* pPreBlock=pBlock_search->_pPre;
								pBlock_search->_startAddr=pPreBlock->_startAddr;
								pBlock_search->_length+=pPreBlock->_length;

								pBlock_search->_pPre=pPreBlock->_pPre;
								if(pPreBlock->_pPre)
								{
									pPreBlock->_pPre->_pNext=pBlock_search;
								}
								else
								{
									_pBucketFreeBlockBegin[bucket]=pBlock_search;
								}


								PutBlockToCash(pPreBlock);
								//delete pPreBlock;

								if(/*bucket!=0 && */(pBlock_search->_length>_LogBucketFreeBlockMaxLength[bucket]))
								{
									_LogBucketFreeBlockMaxLength[bucket] = pBlock_search->_length;
								}
							}
						}
					}
					else
					{
						//进入插入
						if(pBlock_search==_pBucketFreeBlockBegin[bucket])
						{
							Push_front_Freeblock(pBlock);
							//可以不用改变记录
							//_pLastFreeBlockLog[bucket] = 0;
						}
						else
						{
							pBlock->_pNext=pBlock_search;
							pBlock->_pPre=pBlock_search->_pPre;
							pBlock_search->_pPre->_pNext=pBlock;
							pBlock_search->_pPre=pBlock;

							_pLastFreeBlockLog[bucket] = pBlock;
						}

						if(pBlock->_pPre)
						{
							//与前一个是否能合并
							if(pBlock->_pPre->bCanUnit(pBlock))
							{
								stMemoryBlock* pPreBlock=pBlock->_pPre;
								pBlock->_startAddr=pPreBlock->_startAddr;
								pBlock->_length+=pPreBlock->_length;

								pBlock->_pPre=pPreBlock->_pPre;
								if(pPreBlock->_pPre)
								{
									pPreBlock->_pPre->_pNext=pBlock;
								}
								else
								{
									_pBucketFreeBlockBegin[bucket]=pBlock;

								}
								PutBlockToCash(pPreBlock);
								//delete pPreBlock;

								if(/*bucket!=0 && */(pBlock->_length>_LogBucketFreeBlockMaxLength[bucket]))
								{
									_LogBucketFreeBlockMaxLength[bucket] = pBlock->_length;
								}
							}
						}
					}

					//if(_pLastFreeBlockLog[bucket] && _pLastFreeBlockLog[bucket] == _pLastFreeBlockLog[bucket]->_pNext)
					//{
					//	int a;
					//	a=10;
					//}
					return;

				}		
			}

			//这里少加了情况,造成了汇漏,现在加上
			if(pBlock_Pre->bCanUnit(pBlock))
			{
				pBlock_Pre->_endAddr=pBlock->_endAddr;
				pBlock_Pre->_length+=pBlock->_length;
				PutBlockToCash(pBlock);
				//delete pBlock;
				_pLastFreeBlockLog[bucket] = pBlock_Pre;

				if(/*bucket!=0 && */(pBlock_Pre->_length>_LogBucketFreeBlockMaxLength[bucket]))
				{
					_LogBucketFreeBlockMaxLength[bucket] = pBlock_Pre->_length;
				}
			}
			else
			{
				pBlock->_pNext=pBlock_Pre->_pNext;
				pBlock_Pre->_pNext=pBlock;
				pBlock->_pPre=pBlock_Pre;
				if(pBlock->_pNext)
					pBlock->_pNext->_pPre=pBlock;

				_pLastFreeBlockLog[bucket] = pBlock;
			}

			//if(_pLastFreeBlockLog[bucket] && _pLastFreeBlockLog[bucket] == _pLastFreeBlockLog[bucket]->_pNext)
			//{
			//	int a;
			//	a=10;
			//}
		}
		else
		{
			//
			_pBucketFreeBlockBegin[bucket]=pBlock;
			_pBucketFreeBlockBegin[bucket]->_pPre=0;
			_pBucketFreeBlockBegin[bucket]->_pNext=0;
		}
	}


	//else
	//{
	//	int a;
	//	a=10;
	//}
}

//int   WJS_MemoryPool::GetFreeMemory()
//{
//	return _pInitBlock._length-_UsedMemory;
//}

//int   WJS_MemoryPool::GetUseMemory()
//{
//	return _UsedMemory;
//}

bool  WJS_MemoryPool::Free(void* p)
{
	if(!p)
		return false;

	if(!bAdrSpace(p))
		return false;

	//向前编编移8个字节
	char* pPre = ((char*)p) - 8;
	int tt_head[2];
	memcpy(tt_head,pPre,sizeof(int)*2);

	if(tt_head[0] + 48076!=tt_head[1])
	{
		_FreeFailedNum++;
		return false;
	}

	stMemoryBlock* pBlock = (stMemoryBlock *)((int *)tt_head[0]);

	//需要将前8字节置空
	tt_head[0] = 0;
	tt_head[1] = 0; 
	memcpy(pBlock->_startAddr,tt_head,sizeof(int)*2);
	//int bucket = ComputerInBlucketUse(pBlock->_startAddr);
	//if(bucket<0 || bucket>=USEBLOCKBUCKET)
	//{
	//	_FreeFailedNum++;
	//	return false;
	//}

	//int bucket=0;
	//stMemoryBlock* pBlock=FindUseBlock(p,bucket);
	//if(!pBlock)
	//{
	//	_FreeFailedNum++;
	//	return false;
	//}

	_UsedMemory-=pBlock->_length;

	if(pBlock==_pBucketUseBlockBeginNew)
	{
		_pBucketUseBlockBeginNew=_pBucketUseBlockBeginNew->_pNext;
		if(_pBucketUseBlockBeginNew)
			_pBucketUseBlockBeginNew->_pPre=0;
	}
	else
	{
		if(pBlock->_pPre)
		{
			pBlock->_pPre->_pNext=pBlock->_pNext;
		}

		if(pBlock->_pNext)
		{
			pBlock->_pNext->_pPre=pBlock->_pPre;
		}
	}


	//if(pBlock==_pBucketUseBlockBegin[bucket])
	//{
	//	_pBucketUseBlockBegin[bucket]=_pBucketUseBlockBegin[bucket]->_pNext;
	//	if(_pBucketUseBlockBegin[bucket])
	//	  _pBucketUseBlockBegin[bucket]->_pPre=0;
	//}
	//else
	//{
	//	if(pBlock->_pPre)
	//	{
	//		pBlock->_pPre->_pNext=pBlock->_pNext;
	//	}

	//	if(pBlock->_pNext)
	//	{
	//		pBlock->_pNext->_pPre=pBlock->_pPre;
	//	}
	//}

	bool tt_bNeedPush=true;
	if(pBlock->_length>=128 && (pBlock->_length<=1536))
	{
		for(int i=3;i>=0;i--)
		{
			if(pBlock->_length>=_CashFreeBlockSpace[i])
			{
				if(_CashFreeBlockNum[i]<_MaxCashFreeBlockNum[i])
				{
					pBlock->_pNext = _pCashFreeBlock[i];
					pBlock->_pPre = 0;
					_pCashFreeBlock[i] = pBlock;
					_CashFreeBlockNum[i]++;
					tt_bNeedPush=false;
				}
				else
				{
					Push_insert_Freeblock(pBlock);
					tt_bNeedPush=false;
				}
				break;
			}
		}//for
	}
	else
	{
		for(int i=0;i<3;i++)
		{
			if(pBlock->_length == _FixedcCashFreeBlockSpace[i])
			{
				if(_FixedCashFreeBlockNum[i]<_FixedMaxCashFreeBlockNum[i])
				{
					pBlock->_pNext = _pFixedCashFreeBlock[i];
					pBlock->_pPre = 0;
					_pFixedCashFreeBlock[i] = pBlock;
					_FixedCashFreeBlockNum[i]++;
					tt_bNeedPush=false;
				}
				else
				{
					Push_insert_Freeblock(pBlock);
					tt_bNeedPush = false;
				}
				break;
			}
		}
	}

	if(tt_bNeedPush)
	   Push_insert_Freeblock(pBlock);

	return true;
}

//bool  WJS_MemoryPool::bAdrSpace(void* p)//是否是这个地址空间
//{
//	if(p>=_pInitBlock._startAddr && p<=_pInitBlock._endAddr)
//		return true;
//
//	return false;
//}


//stMemoryBlock* WJS_MemoryPool::FindUseBlock(void* p,int& bucket)
//{
//	//先要获取其所在的桶
//	bucket = ComputerInBlucketUse((WJS_CHAR*)p);
//	if(bucket>=0 && (bucket<USEBLOCKBUCKET))
//	{
//		if(_pBucketUseBlockBegin[bucket])
//		{
//			stMemoryBlock* pBlock=_pBucketUseBlockBegin[bucket];
//
//			while(pBlock)
//			{
//				if(pBlock->_startAddr==(WJS_CHAR*)p)
//					return pBlock;
//				pBlock=pBlock->_pNext;
//			}
//		}
//	}
//	//else
//	//{
//	//	int a;
//	//	a=10;
//	//}
//
//	return 0;
//}

//bool  WJS_MemoryPool::CheckCanFreePool()//查测是否可以释放这个池子
//{
//	if(_UsedMemory!=0 || _pBucketUseBlockBeginNew || _bOnlyUseForLittleMemory)
//		return false;
//
//	for(int i=0;i<4;i++)
//	{
//		if(_pCashFreeBlock[i])
//			return false;
//	}
//
//	//for(int i=0;i<USEBLOCKBUCKET;i++)
//	//{
//	//	if(_pBucketUseBlockBegin[i])
//	//		return false;
//	//}
//	return true;
//}

void  WJS_MemoryPool::InitPreAllocBlock(int num)
{
	for(int i=0;i<num;i++)
	{	
		stMemoryBlock* pBlock = new stMemoryBlock();
		if(pBlock)
		{
			_AllocBlockNum++;
			_CashBlockNum++;
			pBlock->_pNext = _PreAllocBlockBegin;
			if(_PreAllocBlockBegin)
			  _PreAllocBlockBegin->_pPre = pBlock;
			_PreAllocBlockBegin=pBlock;
		}	
	}

}

stMemoryBlock* WJS_MemoryPool::GetBlockFromCash()
{
	if(_PreAllocBlockBegin)
	{
		_CashBlockNum--;
		stMemoryBlock* pBlock = _PreAllocBlockBegin;
		_PreAllocBlockBegin = _PreAllocBlockBegin->_pNext;
		if(_PreAllocBlockBegin)
		  _PreAllocBlockBegin->_pPre=0;

		pBlock->_pNext=0;
		
		return pBlock;
	}
	else
	{
		InitPreAllocBlock(10);
		if(_PreAllocBlockBegin)
		{
			_CashBlockNum--;
			stMemoryBlock* pBlock = _PreAllocBlockBegin;
			_PreAllocBlockBegin = _PreAllocBlockBegin->_pNext;
			if(_PreAllocBlockBegin)
			  _PreAllocBlockBegin->_pPre=0;

			pBlock->_pNext=0;
			return pBlock;
		}
		else
		{
			_AllocBlockNum++;
			return new stMemoryBlock();
		}
	}

}

//void  WJS_MemoryPool::PutBlockToCash(stMemoryBlock* pBlock)
//{
//	if(_CashBlockNum>=4096)
//	{
//		_FreeBlockNum++;
//		delete pBlock;
//	    return;
//	}
//	_CashBlockNum++;
//	pBlock->_pNext = _PreAllocBlockBegin;
//	if(_PreAllocBlockBegin)
//		_PreAllocBlockBegin->_pPre = pBlock;
//
//	pBlock->_pPre=0;
//
//	_PreAllocBlockBegin=pBlock;
//}

void  WJS_MemoryPool::ComputerBucketInfo(int needspace,int& bucketsize,int& bucketbit)
{
	static int s_space[11]={0};
	if(s_space[0]==0)
	{
		//改到最小8KB
		//2^13 - 2^23
		for(int i=13;i<24;i++)
		{
			s_space[i-13] = _Pow_int(2,i);
		}
	}

	for(int i=10;i>=0;i--)
	{
		if(needspace>=s_space[i])
		{
			bucketsize = s_space[i];
			bucketbit = i+13;
			return;
		}
	}


	bucketsize = s_space[0];
	bucketbit = 13;
	
}

void  WJS_MemoryPool::ComputerBucketMaxFreeBlockLength(int bucket)
{
	if(bucket>=0 && bucket<UNUSEBLOCKBUCKET)
	{
		_LogBucketFreeBlockMaxLength[bucket] = 0;
		if(_pBucketFreeBlockBegin[bucket])
		{
			stMemoryBlock* pBlock=_pBucketFreeBlockBegin[bucket];

			while(pBlock)
			{
				if(pBlock->_length>_LogBucketFreeBlockMaxLength[bucket])
				{
					_LogBucketFreeBlockMaxLength[bucket] = pBlock->_length;
				}
				pBlock=pBlock->_pNext;
			}
		}
	}

}