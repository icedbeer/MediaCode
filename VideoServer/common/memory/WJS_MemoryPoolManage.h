/*
*  create by wangweijin
*/

#pragma once

#include "../VideoServer/common/memory/WJS_MemoryPool.h"
#include "../VideoServer/common/memory/WJS_MemoryPool_Fix.h"
#include "../VideoServer/common/std/WJS_BBSTree_NM.h"
#include "../VideoServer/common/Lock/WJS_Lock.h"
#include "../VideoServer/common/Lock/WJS_AutoLock.h"

#ifdef __MEMLOG__
#include "Log/Logger.h"
#endif

//-----------为了减少内存块数,根据内存块数决定分配内存的大小
//1M,定义最小的初分配内存块
#define MINMEMORYBLOCK_1M 1048576

//2M,定义内存块
#define MINMEMORYBLOCK_2M 2097152

//4M,定义内存块
#define MINMEMORYBLOCK_4M 4194304

//8M,定义内存块
#define MINMEMORYBLOCK_8M 8338608

//16M
#define MINMEMORYBLOCK_16M 16777216



//这是一个内存管理类
class WJS_MemoryPoolManage
{
public:
	WJS_MemoryPoolManage(int initSize=MINMEMORYBLOCK_2M);
	~WJS_MemoryPoolManage();

public:
	void* Malloc(int size);//分配内存

	bool  Free(void* p);    //释放内存


	/////////////////
	int   GetTotalMemory(); //得到总内存数

	int   GetFreeMemory();  //得到空闲内存数

	int   GetUseMemory();   //得到使用的内存数

	int   GetPoolNum(){return _iPoolNum;} //得到里面的池子数

	int   GetNotFixedPoolNum();//得到动态的池子数

protected:
	bool AddMemoryPool(int size,bool bOnlyUseForLittleMemory);//增加一个大小为size的池子
	bool DeleteOneMemoryPool(WJS_MemoryPool* pPool); //删除一个池子

	WJS_MemoryPool* FoundPoolWithP(void* p); //根据地址查找其所在的池子


private:
	WJS_MemoryPool* _pBeginMemoryPoolNormal;  //动态池子的首指针
	WJS_MemoryPool* _pBeginMemoryPoolLittle;  //动态池子，小内存专用部分

	int             _iPoolNum;          //池子数

	WJS_MemoryPool_FIX _FixedMemoryPool; //固定池子

	WJS_BBSTree_NM<WJS_CHAR*,WJS_MemoryPool*> m_PoolTree; //管理池子的map数

	WJS_Lock         _MFLock;//分配和释放锁

	int              _OnlyUseForLittleMemoryNum; //仅用于小内存的分配池子数

#ifdef __MEMLOG__
	FileLogger       _FileLog;
	static int       _FileLogIndex;
#endif

	/////////////////////////禁止复制构造
protected:
	WJS_MemoryPoolManage& operator = (const WJS_MemoryPoolManage& ManageR);
	WJS_MemoryPoolManage(const WJS_MemoryPoolManage& ManageR);
};

///////////////////////////////////
#ifndef __WJS_MEMORYPOOLMANAGE__
#define __WJS_MEMORYPOOLMANAGE__

    #define __WJS_USEMEMORYPOOL__  //定义是否使用内存池技术

	#ifdef  __WJS_USEMEMORYPOOL__

		WJS_MemoryPoolManage* GetMemoryPoolManage();
		WJS_MemoryPoolManage* GetMemoryPoolManage(int index);


		//重载new delete
		#define WJS_NEW_FROM_MEMORYPOOL void * operator new (size_t size) \
		{\
			return GetMemoryPoolManage()->Malloc((int)size);\
		}\
			void * operator new [] (size_t size) \
		{\
			return GetMemoryPoolManage()->Malloc((int)size);\
		}

		#define WJS_DEL_FROM_MEMORYPOOL void  operator delete (void *p) \
		{\
			GetMemoryPoolManage()->Free(p);\
		}\
			void  operator delete [] (void *p) \
		{\
			GetMemoryPoolManage()->Free(p);\
		}

		#define WJS_NEW_FROM_MEMORYPOOL_1 void * operator new (size_t size) \
		{\
			return GetMemoryPoolManage(1)->Malloc((int)size);\
		}\
			void * operator new [] (size_t size) \
		{\
			return GetMemoryPoolManage(1)->Malloc((int)size);\
		}

		#define WJS_DEL_FROM_MEMORYPOOL_1 void  operator delete (void *p) \
		{\
			GetMemoryPoolManage(1)->Free(p);\
		}\
			void  operator delete [] (void *p) \
		{\
			GetMemoryPoolManage(1)->Free(p);\
		}



		#define WJS_NEW(T)  (T*)(GetMemoryPoolManage()->Malloc(sizeof(T)));
		#define WJS_NEWS(T,count) (T*)GetMemoryPoolManage()->Malloc(sizeof(T)*(count));
		#define WJS_DEL(p)   GetMemoryPoolManage()->Free((void*)p);
		#define WJS_DELS(p)   GetMemoryPoolManage()->Free((void*)p);

		//#define WJS_NEW_1(T) new T;
		//#define WJS_NEWS_1(T,count) new T[count];
		//#define WJS_DEL_1(p) delete p;
		//#define WJS_DELS_1(p) delete [] p;

		#define WJS_NEW_1(T)  (T*)(GetMemoryPoolManage(1)->Malloc(sizeof(T)));
		#define WJS_NEWS_1(T,count) (T*)GetMemoryPoolManage(1)->Malloc(sizeof(T)*(count));
		#define WJS_DEL_1(p)   GetMemoryPoolManage(1)->Free((void*)p);
		#define WJS_DELS_1(p)   GetMemoryPoolManage(1)->Free((void*)p);

	#else
		//////////////在未使用内存池时,使得这些宏无意或被替代
		#define WJS_NEW_FROM_MEMORYPOOL  
		#define WJS_DEL_FROM_MEMORYPOOL  
		#define WJS_NEW_FROM_MEMORYPOOL_1
		#define WJS_DEL_FROM_MEMORYPOOL_1

		#define WJS_NEW(T) new T;
		#define WJS_NEWS(T,count) new T[count];
		#define WJS_DEL(p) delete p;
		#define WJS_DELS(p) delete [] p;

		#define WJS_NEW_1(T) new T;
		#define WJS_NEWS_1(T,count) new T[count];
		#define WJS_DEL_1(p) delete p;
		#define WJS_DELS_1(p) delete [] p;

	#endif

#endif
