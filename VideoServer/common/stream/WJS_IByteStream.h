/*
* create by wwjin
*/

#pragma once
#include "../VideoServer/common/std/WJS_ArrayVector.h"
#include "../VideoServer/common/std/WJS_String.h"
#include "../VideoServer/common/memory/WJS_MemoryPoolManage.h"
#include <string>
#include <assert.h>

using namespace std;

#define WJS_Byte unsigned char
#define WJS_DWORD unsigned long
#ifndef DWORD
typedef unsigned long       DWORD;
#endif
typedef WJS_ArrayVector<WJS_Byte> WJSByteSeq;

struct _guid
{
    WJS_Byte bytes[16];
};

struct wjs_char32
{
	char c[32];
	wjs_char32()
	{
		c[0]=0;
	}

	operator const char*()
	{
		return c;
	}
};
struct wjs_char64
{
	char c[64];
	wjs_char64()
	{
		c[0]=0;
	}
	operator const char*()
	{
		return c;
	}
};
struct wjs_char128
{
	char c[128];
	wjs_char128()
	{
		c[0]=0;
	}
	operator const char*()
	{
		return c;
	}
};
struct wjs_char256
{
	char c[256];
	wjs_char256()
	{
		c[0]=0;
	}
	operator const char*()
	{
		return c;
	}
};
struct wjs_char512
{
	char c[512];
	wjs_char512()
	{
		c[0]=0;
	}
	operator const char*()
	{
		return c;
	}
};
struct wjs_char1024
{
	char c[1024];
	wjs_char1024()
	{
		c[0]=0;
	}
	operator const char*()
	{
		return c;
	}
};


////字节流
//class WJS_IByteStream
//{
//public:
//	WJSByteSeq GetByteSeq()
//	{
//		return m_ByteSeq;
//	}
//
//	//引用版
//	WJSByteSeq& GetByteSeq_R()
//	{
//		return m_ByteSeq;
//	}
//
//	unsigned int GetSize()
//	{
//		return m_size;
//	}
//
//protected:
//	WJSByteSeq m_ByteSeq;
//	unsigned int m_size;
//};

//输入流
class WJS_InByteStream : public WJSByteSeq
{
public:
	//默认64字节
	WJS_InByteStream(unsigned int size=64)
	{
		resize(size,false);
		//m_ByteSeq.resize(size,false);
		//m_size=size;
	}

	//提供写入方法
	inline void write(void* buf, unsigned int size)
	{
		WJS_Byte* b = (WJS_Byte*)buf;
		push_back_adv(b,size);
	}

	operator unsigned char *()
	{
		return (unsigned char*)m_pBeginData;
	}

	operator const unsigned char*()
	{
		return (const unsigned char*)m_pBeginData;
	}
	operator unsigned char*() const 
	{
		return (unsigned char*)m_pBeginData;
	}

	operator const unsigned char*() const
	{
		return (const unsigned char*)m_pBeginData;
	}
};

	//
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, int v)
	{
		is.write((void*)&v, sizeof(v));
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, unsigned int v)
	{
		is.write((void*)&v, sizeof(v));
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, __int64 v)
	{
		is.write((void*)&v, sizeof(v));
		return is;
	}
	
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, short v)
	{
		is.write((void*)&v, sizeof(v));
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, unsigned short v)
	{
		is.write((void*)&v, sizeof(v));
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, bool v)
	{
		is.write((void*)&v, sizeof(v));
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, float v)
	{
		is.write((void*)&v, sizeof(v));
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, double v)
	{
		is.write((void*)&v, sizeof(v));
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, long v)
	{
		is.write((void*)&v, sizeof(v));
		return is;
	}

	inline WJS_InByteStream& operator << (WJS_InByteStream& is, WJS_DWORD v)
	{
		is.write((void*)&v, sizeof(v));
		return is;
	}

	inline WJS_InByteStream& operator << (WJS_InByteStream& is, char v)
	{
		is.write((void*)&v, sizeof(v));
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, WJS_Byte v)
	{
		is.write((void*)&v, sizeof(v));
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, _guid v)
	{
		is.write((void*)v.bytes, 16);
		return is;
	}

	inline WJS_InByteStream& operator << (WJS_InByteStream& is, const wjs_char32& v)
	{
		int size = (int)strlen(v.c);
		is.write((void*)&size,sizeof(int));
		if(size>0)
		{
			is.write((void*)v.c,size);
		}
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, const wjs_char64& v)
	{
		int size = (int)strlen(v.c);
		is.write((void*)&size,sizeof(int));
		if(size>0)
		{
			is.write((void*)v.c,size);
		}
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, const wjs_char128& v)
	{
		int size = (int)strlen(v.c);
		is.write((void*)&size,sizeof(int));
		if(size>0)
		{
			is.write((void*)v.c,size);
		}
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, const wjs_char256& v)
	{
		int size = (int)strlen(v.c);
		is.write((void*)&size,sizeof(int));
		if(size>0)
		{
			is.write((void*)v.c,size);
		}
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, const wjs_char512& v)
	{
		int size = (int)strlen(v.c);
		is.write((void*)&size,sizeof(int));
		if(size>0)
		{
			is.write((void*)v.c,size);
		}
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is, const wjs_char1024& v)
	{
		int size = (int)strlen(v.c);
		is.write((void*)&size,sizeof(int));
		if(size>0)
		{
			is.write((void*)v.c,size);
		}
		return is;
	}

	inline WJS_InByteStream& operator << (WJS_InByteStream& is,  WJS_String& v)
	{
		int size=(int)v.size();
		is.write((void*)&size,sizeof(int));
		if(size>0)
		{
		   is.write((void*)v.c_str(), size);
		}
		return is;
	}
	inline WJS_InByteStream& operator << (WJS_InByteStream& is,  const string& v)
	{
		int size=(int)v.size();
		is.write((void*)&size,sizeof(int));
		if(size>0)
		{
		  is.write((void*)v.c_str(), size);
		}

		return is;
	}

	inline WJS_InByteStream& operator << (WJS_InByteStream& is,  const char* v)
	{
		int size=strlen(v);
		is.write((void*)&size,sizeof(int));
		if(size>0)
		{
			is.write((void*)v, size);
		}

		return is;
	}



//输出流
class WJS_OutByteStream : public WJSByteSeq
{
public:
	//默认64字节,增加是否直接转移
	WJS_OutByteStream(const WJSByteSeq& ByteSeq)
	{
		m_capcity=ByteSeq.capacity();
		m_pBeginData=0;

		if(m_capcity>0)
		{
			m_pBeginData=new WJS_Byte[m_capcity];
		}

		m_useSize=ByteSeq.size();
		if(m_useSize>0)
		{
			memcpy(m_pBeginData,&(ByteSeq[0]),m_useSize*sizeof(WJS_Byte));
		}	

		m_pointer=0;
	}
	WJS_OutByteStream(BYTE *p,int nLen)
	{
		m_capcity = nLen + 4;
		m_pBeginData=0;

		if(m_capcity>0)
		{
			m_pBeginData=new WJS_Byte[m_capcity];
		}
		m_useSize = nLen;
		if(m_useSize>0)
		{
			memcpy(m_pBeginData,p,m_useSize*sizeof(WJS_Byte));
		}	

		m_pointer=0;
	}


	//提供写入方法
	inline bool read(void* buf, unsigned int size)
	{
		if(m_pointer+size > (unsigned int)m_useSize)
		{
			//assert(0);
			throw("overflow");
			//溢出
			return false;
		}
		//void* b = (void*)(&m_ByteSeq[m_pointer]);
		memcpy(buf,m_pBeginData+m_pointer,size);
		m_pointer+=size;
		return true;
	}



	//提供偏移
	inline bool CurOffset(unsigned int size)
	{
		if(m_pointer+size > (unsigned int)m_useSize)
		{
			//assert(0);
			throw("overflow");
			//溢出
			return false;
		}
		//void* b = (void*)(&m_ByteSeq[m_pointer]);
		m_pointer+=size;
		return true;
	}
	inline  int GetCurOffsetPos() {return m_pointer;}
protected:
	unsigned int m_pointer;
};


	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& os, int& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& os, unsigned int& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& os, __int64& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& os, short& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& os, unsigned short& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& os, bool& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& os, WJS_Byte& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& os, char& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& os, float& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& os, double& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& os, long& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& os, WJS_DWORD& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}

	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& is,  WJS_String& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));
		if(size<0 || size>10485760)
		{
			throw("strOverFlow");
			return is;
		}

		char* pch=WJS_NEWS(char,size+1);
		if(pch)
		{
			is.read((void*)pch,size);
			pch[size]='\0';
			v.append(pch);
			WJS_DELS(pch);
		}

		return is;
	}

	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& is,  string& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));
		if(size<0 || size>10485760)
		{
			return is;
		}

		if(size==0)
		{
			v="";
		}
		else
		{
			if(size<256)
			{
				char tt_ch[256];
				is.read((void*)tt_ch,size);
				tt_ch[size]='\0';
				v=tt_ch;
			}
			else
			{
				char* pch=WJS_NEWS(char,size+1);
				if(pch)
				{
					is.read((void*)pch,size);
					pch[size]='\0';
					//v.append(pch);
					v=pch;
					WJS_DELS(pch);
				}
			}

		}


		return is;
	}

	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& is,  wjs_char32& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));

		if(size==0)
		{
			v.c[0]=0;
		}
		else if(size>=32)
		{
			v.c[0]=0;
			throw("C32OverFlow");
		}
		else
		{
			is.read((void*)v.c,size);
			v.c[size]='\0';
		}
		return is;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& is,  wjs_char64& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));

		if(size==0)
		{
			v.c[0]=0;
		}
		else if(size>=64)
		{
			v.c[0]=0;
			throw("C64OverFlow");

		}
		else
		{
			is.read((void*)v.c,size);
			v.c[size]='\0';
		}
		return is;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& is,  wjs_char128& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));

		if(size==0)
		{
			v.c[0]=0;
		}
		else if(size>=128)
		{
			v.c[0]=0;
			throw("C128OverFlow");
		}
		else
		{
			is.read((void*)v.c,size);
			v.c[size]='\0';
		}
		return is;
	}

	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& is,  wjs_char256& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));

		if(size==0)
		{
			v.c[0]=0;
		}
		else if(size>=256)
		{
			v.c[0]=0;
			throw("C256OverFlow");
		}
		else
		{
			is.read((void*)v.c,size);
			v.c[size]='\0';
		}
		return is;
	}

	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& is,  wjs_char512& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));

		if(size==0)
		{
			v.c[0]=0;
		}
		else if(size>=512)
		{
			v.c[0]=0;
			throw("C512OverFlow");
		}
		else
		{
			is.read((void*)v.c,size);
			v.c[size]='\0';
		}
		return is;
	}

	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& is,  wjs_char1024& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));

		if(size==0)
		{
			v.c[0]=0;
		}
		else if(size>=1024)
		{
			v.c[0]=0;
			throw("C1024OverFlow");
		}
		else
		{
			is.read((void*)v.c,size);
			v.c[size]='\0';
		}
		return is;
	}

	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& is, _guid& v)
	{
		is.read((void*)v.bytes,16);
		return is;
	}

	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& is,  char* v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));
		if(size<0 || size>10485760)
		{
			throw("COverFlow");
			return is;
		}
		if(size==0)
		{
			v[0]='\0';
		}
		else
		{
			char* pch=WJS_NEWS(char,size+1);
			if(pch)
			{
				is.read((void*)pch,size);
				pch[size]='\0';
				memcpy(v,pch,size);
				v[size+1]='\0';
				WJS_DELS(pch);
			}
		}
		return is;
	}


	////////////第二版，直接借用内存
	//输出流
	class WJS_OutByteStream2
	{
	public:
		//默认64字节,增加是否直接转移
		WJS_OutByteStream2(const WJSByteSeq& ByteSeq)
		{
			m_pBeginData = (unsigned char*)ByteSeq;
			m_useSize = ByteSeq.size();
			m_pointer=0;
		}

		//提供写入方法
		inline bool read(void* buf, unsigned int size)
		{
			if(m_pointer+size > (unsigned int)m_useSize)
			{
				throw("overflow");
				//溢出
				return false;
			}
			memcpy(buf,m_pBeginData+m_pointer,size);
			m_pointer+=size;
			return true;
		}

		//提供偏移
		inline bool CurOffset(unsigned int size)
		{
			if(m_pointer+size > (unsigned int)m_useSize)
			{
				//assert(0);
				throw("overflow");
				//溢出
				return false;
			}
			m_pointer+=size;
			return true;
		}
		inline  int GetCurOffsetPos() {return m_pointer;}
	protected:
		unsigned char*  m_pBeginData;//起始指针
		int m_useSize;//使用大小
		unsigned int m_pointer;
	};
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& os, int& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& os, unsigned int& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& os, __int64& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& os, short& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& os, unsigned short& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& os, bool& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& os, WJS_Byte& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& os, char& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& os, float& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& os, double& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& os, long& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& os, WJS_DWORD& v)
	{
		os.read((void*)&v, sizeof(v));
		return os;
	}

	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& is,  WJS_String& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));
		if(size<0 || size>10485760)
		{
			throw("strOverFlow");
			return is;
		}

		char* pch=WJS_NEWS(char,size+1);
		if(pch)
		{
			is.read((void*)pch,size);
			pch[size]='\0';
			v.append(pch);
			WJS_DELS(pch);
		}

		return is;
	}

	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& is,  string& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));
		if(size<0 || size>10485760)
		{
			return is;
		}

		if(size==0)
		{
			v="";
		}
		else
		{
			if(size<256)
			{
				char tt_ch[256];
				is.read((void*)tt_ch,size);
				tt_ch[size]='\0';
				v=tt_ch;
			}
			else
			{
				char* pch=WJS_NEWS(char,size+1);
				if(pch)
				{
					is.read((void*)pch,size);
					pch[size]='\0';
					//v.append(pch);
					v=pch;
					WJS_DELS(pch);
				}
			}

		}


		return is;
	}

	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& is,  wjs_char32& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));

		if(size==0)
		{
			v.c[0]=0;
		}
		else if(size>=32)
		{
			v.c[0]=0;
			throw("C32OverFlow");
		}
		else
		{
			is.read((void*)v.c,size);
			v.c[size]='\0';
		}
		return is;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& is,  wjs_char64& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));

		if(size==0)
		{
			v.c[0]=0;
		}
		else if(size>=64)
		{
			v.c[0]=0;
			throw("C64OverFlow");

		}
		else
		{
			is.read((void*)v.c,size);
			v.c[size]='\0';
		}
		return is;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& is,  wjs_char128& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));

		if(size==0)
		{
			v.c[0]=0;
		}
		else if(size>=128)
		{
			v.c[0]=0;
			throw("C128OverFlow");
		}
		else
		{
			is.read((void*)v.c,size);
			v.c[size]='\0';
		}
		return is;
	}

	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& is,  wjs_char256& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));

		if(size==0)
		{
			v.c[0]=0;
		}
		else if(size>=256)
		{
			v.c[0]=0;
			throw("C256OverFlow");
		}
		else
		{
			is.read((void*)v.c,size);
			v.c[size]='\0';
		}
		return is;
	}

	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& is,  wjs_char512& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));

		if(size==0)
		{
			v.c[0]=0;
		}
		else if(size>=512)
		{
			v.c[0]=0;
			throw("C512OverFlow");
		}
		else
		{
			is.read((void*)v.c,size);
			v.c[size]='\0';
		}
		return is;
	}

	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& is,  wjs_char1024& v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));

		if(size==0)
		{
			v.c[0]=0;
		}
		else if(size>=1024)
		{
			v.c[0]=0;
			throw("C1024OverFlow");
		}
		else
		{
			is.read((void*)v.c,size);
			v.c[size]='\0';
		}
		return is;
	}

	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& is, _guid& v)
	{
		is.read((void*)v.bytes,16);
		return is;
	}

	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& is,  char* v)
	{
		int size=0;
		is.read((void*)&size,sizeof(int));
		if(size<0 || size>10485760)
		{
			throw("COverFlow");
			return is;
		}
		if(size==0)
		{
			v[0]='\0';
		}
		else
		{
			char* pch=WJS_NEWS(char,size+1);
			if(pch)
			{
				is.read((void*)pch,size);
				pch[size]='\0';
				memcpy(v,pch,size);
				v[size+1]='\0';
				WJS_DELS(pch);
			}
		}
		return is;
	}
