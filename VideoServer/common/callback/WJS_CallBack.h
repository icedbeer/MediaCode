/*
* create by wangweijin
*/

#pragma once

template<typename obj_type>
class WJS_CallBack0
{
public:
	WJS_CallBack0()
	{
		m_pobject=0;
		m_pmemfun=0;
	}

	WJS_CallBack0(obj_type* pobject, void (obj_type::*pmemfun)())
	{
		m_pobject=pobject;
		m_pmemfun=pmemfun;
	}

	void CreateCallBack(obj_type* pobject, void (obj_type::*pmemfun)())
	{
		m_pobject=pobject;
		m_pmemfun=pmemfun;
	}

	void DisconectCallBack()
	{
		m_pobject=0;
		m_pmemfun=0;
	}

	void ExcuteCallBack()
	{
		if(m_pobject && m_pmemfun)
		{
			(m_pobject->*m_pmemfun)();
		}
	}

public:
	obj_type* GetObj()
	{
		return m_pobject;
	}
	typedef void (obj_type::* pFun) ();
	pFun GetFunc()
	{
		return m_pmemfun;
	}
	WJS_CallBack0& operator= (const WJS_CallBack0& Ref)
	{
		if(this==&Ref)
			return *this;
		m_pobject=Ref.GetObj();
		m_pmemfun=Ref;

		return *this;
	}

private:
	WJS_CallBack0(const WJS_CallBack0& Ref);

private:
	obj_type* m_pobject; //对象
	void (obj_type::* m_pmemfun)();//函数指针
};



template<typename obj_type,typename arg1_type>
class WJS_CallBack1
{
public:
	WJS_CallBack1()
	{
		m_pobject=0;
		m_pmemfun=0;
	}

	WJS_CallBack1(obj_type* pobject, void (obj_type::*pmemfun)(arg1_type))
	{
		m_pobject=pobject;
		m_pmemfun=pmemfun;
	}

	void CreateCallBack(obj_type* pobject, void (obj_type::*pmemfun)(arg1_type))
	{
		m_pobject=pobject;
		m_pmemfun=pmemfun;
	}
	void DisconectCallBack()
	{
		m_pobject=0;
		m_pmemfun=0;
	}

	void ExcuteCallBack(arg1_type arg1)
	{
		if(m_pobject && m_pmemfun)
		{
			(m_pobject->*m_pmemfun)(arg1);
		}
	}
public:
	obj_type* GetObj()
	{
		return m_pobject;
	}
	typedef void (obj_type::* pFun) (arg1_type);
	pFun GetFunc()
	{
		return m_pmemfun;
	}
	WJS_CallBack1& operator= (const WJS_CallBack1& Ref)
	{
		if(this==&Ref)
			return *this;
		m_pobject=Ref.GetObj();
		m_pmemfun=Ref;

		return *this;
	}
private:
	obj_type* m_pobject; //对象
	void (obj_type::* m_pmemfun)(arg1_type);//函数指针
};

//
template<typename obj_type,typename arg1_type,typename arg2_type>
class WJS_CallBack2
{
public:
	WJS_CallBack2()
	{
		m_pobject=0;
		m_pmemfun=0;
	}

	WJS_CallBack2(obj_type* pobject, void (obj_type::*pmemfun)(arg1_type,arg2_type))
	{
		m_pobject=pobject;
		m_pmemfun=pmemfun;
	}

	void CreateCallBack(obj_type* pobject, void (obj_type::*pmemfun)(arg1_type,arg2_type))
	{
		m_pobject=pobject;
		m_pmemfun=pmemfun;
	}
	void DisconectCallBack()
	{
		m_pobject=0;
		m_pmemfun=0;
	}

	void ExcuteCallBack(arg1_type arg1,arg2_type arg2)
	{
		if(m_pobject && m_pmemfun)
		{
			(m_pobject->*m_pmemfun)(arg1,arg2);
		}
	}
public:
	obj_type* GetObj()
	{
		return m_pobject;
	}
	typedef void (obj_type::* pFun) (arg1_type,arg2_type);
	pFun GetFunc()
	{
		return m_pmemfun;
	}
	WJS_CallBack2& operator= (const WJS_CallBack2& Ref)
	{
		if(this==&Ref)
			return *this;
		m_pobject=Ref.GetObj();
		m_pmemfun=Ref;

		return *this;
	}
private:
	obj_type* m_pobject; //对象
	void (obj_type::* m_pmemfun)(arg1_type,arg2_type);//函数指针
};

template<typename obj_type,typename arg1_type,typename arg2_type, typename arg3_type>
class WJS_CallBack3
{
public:
	WJS_CallBack3()
	{
		m_pobject=0;
		m_pmemfun=0;
	}

	WJS_CallBack3(obj_type* pobject, void (obj_type::*pmemfun)(arg1_type,arg2_type,arg3_type))
	{
		m_pobject=pobject;
		m_pmemfun=pmemfun;
	}

	void CreateCallBack(obj_type* pobject, void (obj_type::*pmemfun)(arg1_type,arg2_type,arg3_type))
	{
		m_pobject=pobject;
		m_pmemfun=pmemfun;
	}
	void DisconectCallBack()
	{
		m_pobject=0;
		m_pmemfun=0;
	}

	void ExcuteCallBack(arg1_type arg1,arg2_type arg2,arg3_type arg3)
	{
		if(m_pobject && m_pmemfun)
		{
			(m_pobject->*m_pmemfun)(arg1,arg2,arg3);
		}
	}
public:
	obj_type* GetObj()
	{
		return m_pobject;
	}
	typedef void (obj_type::* pFun) (arg1_type,arg2_type,arg3_type);
	pFun GetFunc()
	{
		return m_pmemfun;
	}
	WJS_CallBack3& operator= (const WJS_CallBack3& Ref)
	{
		if(this==&Ref)
			return *this;
		m_pobject=Ref.GetObj();
		m_pmemfun=Ref;

		return *this;
	}
private:
	obj_type* m_pobject; //对象
	void (obj_type::* m_pmemfun)(arg1_type,arg2_type,arg3_type);//函数指针
};

template<typename obj_type,typename arg1_type,typename arg2_type, typename arg3_type,typename arg4_type>
class WJS_CallBack4
{
public:
	WJS_CallBack4()
	{
		m_pobject=0;
		m_pmemfun=0;
	}

	WJS_CallBack4(obj_type* pobject, void (obj_type::*pmemfun)(arg1_type,arg2_type,arg3_type,arg4_type))
	{
		m_pobject=pobject;
		m_pmemfun=pmemfun;
	}

	void CreateCallBack(obj_type* pobject, void (obj_type::*pmemfun)(arg1_type,arg2_type,arg3_type,arg4_type))
	{
		m_pobject=pobject;
		m_pmemfun=pmemfun;
	}
	void DisconectCallBack()
	{
		m_pobject=0;
		m_pmemfun=0;
	}

	void ExcuteCallBack(arg1_type arg1,arg2_type arg2,arg3_type arg3,arg4_type arg4)
	{
		if(m_pobject && m_pmemfun)
		{
			(m_pobject->*m_pmemfun)(arg1,arg2,arg3,arg4);
		}
	}
public:
	obj_type* GetObj()
	{
		return m_pobject;
	}
	typedef void (obj_type::* pFun) (arg1_type,arg2_type,arg3_type,arg4_type arg4);
	pFun GetFunc()
	{
		return m_pmemfun;
	}
	WJS_CallBack4& operator= (const WJS_CallBack4& Ref)
	{
		if(this==&Ref)
			return *this;
		m_pobject=Ref.GetObj();
		m_pmemfun=Ref;

		return *this;
	}
private:
	obj_type* m_pobject; //对象
	void (obj_type::* m_pmemfun)(arg1_type,arg2_type,arg3_type,arg4_type);//函数指针
};

template<typename obj_type,typename arg1_type,typename arg2_type, typename arg3_type,typename arg4_type,typename arg5_type>
class WJS_CallBack5
{
public:
	WJS_CallBack5()
	{
		m_pobject=0;
		m_pmemfun=0;
	}

	WJS_CallBack5(obj_type* pobject, void (obj_type::*pmemfun)(arg1_type,arg2_type,arg3_type,arg4_type,arg5_type))
	{
		m_pobject=pobject;
		m_pmemfun=pmemfun;
	}

	void CreateCallBack(obj_type* pobject, void (obj_type::*pmemfun)(arg1_type,arg2_type,arg3_type,arg4_type,arg5_type))
	{
		m_pobject=pobject;
		m_pmemfun=pmemfun;
	}
	void DisconectCallBack()
	{
		m_pobject=0;
		m_pmemfun=0;
	}

	void ExcuteCallBack(arg1_type arg1,arg2_type arg2,arg3_type arg3,arg4_type arg4,arg5_type arg5)
	{
		if(m_pobject && m_pmemfun)
		{
			(m_pobject->*m_pmemfun)(arg1,arg2,arg3,arg4,arg5);
		}
	}
public:
	obj_type* GetObj()
	{
		return m_pobject;
	}
	typedef void (obj_type::* pFun) (arg1_type,arg2_type,arg3_type,arg4_type arg4,arg5_type arg5);
	pFun GetFunc()
	{
		return m_pmemfun;
	}
	WJS_CallBack5& operator= (const WJS_CallBack5& Ref)
	{
		if(this==&Ref)
			return *this;
		m_pobject=Ref.GetObj();
		m_pmemfun=Ref;

		return *this;
	}
private:
	obj_type* m_pobject; //对象
	void (obj_type::* m_pmemfun)(arg1_type,arg2_type,arg3_type,arg4_type,arg5_type);//函数指针
};