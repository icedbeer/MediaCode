//#include "stdafx.h"
#include "CryptMethod.h"


CryptMethod::CryptMethod()
{
	_strMiYao="ABCDEFG#NMLKJIH#OPQRST#ZYXWVU@#$%ABCDEFG#NMLKJIH#OPQRST#ZYXWVU"; //长64
	_strOtherMiYao="GFEDCBA@HIJKLMN@TSRQPO@UVWXYZ%$#@"; //32

	_MiYaoLen = _strMiYao.length();
	_OtherMiYaoLen = _strOtherMiYao.length();
}

CryptMethod::~CryptMethod()
{

}


bool CryptMethod::EnCryptData(const void* InData,size_t len,int method,string& strOut)
{
	switch(method)
	{
	case 0:
		{
			strOut.resize(len);
			memcpy(&strOut[0], InData, len);
			return true;
		}
	case 1:
		return EncryptString1(InData,len,strOut);
	case 2:
		return EncryptString2(InData,len,strOut);
	case 3:
		return EncryptString3(InData,len,strOut);
	case 4:
		return EncryptString4(InData,len,strOut);
	case 5:
		return EncryptString5(InData,len,strOut);
	case 6:
		return EncryptString6(InData,len,strOut);
	default:
		break;
	}

	return false;
}

bool CryptMethod::DeCryptData(const void* InData,size_t len,int method,string& strOut)
{
	switch(method)
	{
	case 0:
		{
			strOut.resize(len);
			memcpy(&strOut[0], InData, len);
			return true;
		}
	case 1:
		return DecryptString1(InData,len,strOut);
	case 2:
		return DecryptString2(InData,len,strOut);
	case 3:
		return DecryptString3(InData,len,strOut);
    case 4:
		return DecryptString4(InData,len,strOut);
    case 5:
		return DecryptString5(InData,len,strOut);
	case 6:
		return DecryptString6(InData,len,strOut);
	default:
		break;
	}
	return false;
}


bool CryptMethod::EnCryptData_InOut(void* InData,size_t len,int method)
{
	switch(method)
	{
	case 0:
		return true;
	case 1:
		return EncryptString1_InOut(InData,len);
	case 2:
		return EncryptString2_InOut(InData,len);
	case 3:
		return EncryptString3_InOut(InData,len);
	case 4:
		return EncryptString4_InOut(InData,len);
	case 5:
		return EncryptString5_InOut(InData,len);
	case 6:
		return EncryptString6_InOut(InData,len);
	default:
		break;
	}

	return false;
}
bool CryptMethod::DeCryptData_InOut(void* InData,size_t len,int method)
{
	switch(method)
	{
	case 0:
		return true;
	case 1:
		return DecryptString1_InOut(InData,len);
	case 2:
		return DecryptString2_InOut(InData,len);
	case 3:
		return DecryptString3_InOut(InData,len);
	case 4:
		return DecryptString4_InOut(InData,len);
	case 5:
		return DecryptString5_InOut(InData,len);	
	case 6:
		return DecryptString6_InOut(InData,len);
	default:
		break;
	}
	return false;
}

////////////////////////////////
bool CryptMethod::EncryptString1(const void* InData,size_t len,string& strOut)
{
	strOut.resize(len);
	memcpy(&strOut[0], InData, len);

	size_t tt_testlen=len;
	if(tt_testlen>_MiYaoLen)
		tt_testlen=_MiYaoLen;

	//只加密前面一部分的字节
	for(size_t i=0;i<tt_testlen;i++)
	{
		strOut[i]= strOut[i] ^ (_strMiYao[i]);
	}

	return true;
}
bool CryptMethod::DecryptString1(const void* InData,size_t len,string& strOut)
{
	strOut.resize(len);
	memcpy(&strOut[0], InData, len);

	size_t tt_testlen=len;
	if(tt_testlen>_MiYaoLen)
		tt_testlen=_MiYaoLen;

	//只解密前面一部分的字节
	for(size_t i=0;i<tt_testlen;i++)
	{
		strOut[i]= strOut[i] ^ (_strMiYao[i]);
	}

	return true;
}

bool CryptMethod::EncryptString2(const void* InData,size_t len,string& strOut)
{
	strOut.resize(len);
	memcpy(&strOut[0], InData, len);

	size_t tt_testlen=len;
	if(tt_testlen>(_MiYaoLen+_MiYaoLen))
		tt_testlen=_MiYaoLen + _MiYaoLen;

	int count=0;
	//只加密前面一部分的字节
	for(size_t i=0;i<tt_testlen;)
	{
		strOut[i]= strOut[i] ^ (_strMiYao[count++]);
		i+=2;
	}

	return true;
}
bool CryptMethod::DecryptString2(const void* InData,size_t len,string& strOut)
{
	strOut.resize(len);
	memcpy(&strOut[0], InData, len);

	size_t tt_testlen=len;
	if(tt_testlen>(_MiYaoLen+_MiYaoLen))
		tt_testlen=_MiYaoLen + _MiYaoLen;

	int count=0;
	//只解密前面一部分的字节
	for(size_t i=0;i<tt_testlen;)
	{
		strOut[i]= strOut[i] ^ (_strMiYao[count++]);
		i+=2;
	}
	return true;
}

bool CryptMethod::EncryptString3(const void* InData,size_t len,string& strOut)
{
	strOut.resize(len);
	memcpy(&strOut[0], InData, len);

	size_t tt_testlen=len;
	if(tt_testlen>(_MiYaoLen+_MiYaoLen))
		tt_testlen=_MiYaoLen + _MiYaoLen;

	int count=0;
	//只加密前面一部分的字节
	for(size_t i=1;i<tt_testlen;)
	{
		strOut[i]= strOut[i] ^ (_strMiYao[count++]);
		i+=2;
	}

	return true;
}
bool CryptMethod::DecryptString3(const void* InData,size_t len,string& strOut)
{
	strOut.resize(len);
	memcpy(&strOut[0], InData, len);

	size_t tt_testlen=len;
	if(tt_testlen>(_MiYaoLen+_MiYaoLen))
		tt_testlen=_MiYaoLen + _MiYaoLen;

	int count=0;
	//只解密前面一部分的字节
	for(size_t i=1;i<tt_testlen;)
	{
		strOut[i]= strOut[i] ^ (_strMiYao[count++]);
		i+=2;
	}
	return true;
}

bool CryptMethod::EncryptString4(const void* InData,size_t len,string& strOut)  //按顺序奇偶用不同的密钥加密
{
	strOut.resize(len);
	memcpy(&strOut[0], InData, len);

	///////////////////奇数先用_strMiYao加密
	size_t tt_testlen=len;
	if(tt_testlen>(_MiYaoLen+_MiYaoLen))
		tt_testlen=_MiYaoLen + _MiYaoLen;

	int count=0;
	//只解密前面一部分的字节
	for(size_t i=0;i<tt_testlen;)
	{
		strOut[i]= strOut[i] ^ (_strMiYao[count++]);
		i+=2;
	}

	/////////////////偶数用_strOtherMiYao加密
	tt_testlen=len;
	if(tt_testlen>(_OtherMiYaoLen+_OtherMiYaoLen))
		tt_testlen=_OtherMiYaoLen + _OtherMiYaoLen;

	count=0;
    for(size_t i=1;i<tt_testlen;)
	{
		strOut[i]= strOut[i] ^ (_strOtherMiYao[count++]);
		i+=2;
	}

	return true;
}
bool CryptMethod::DecryptString4(const void* InData,size_t len,string& strOut)
{
	strOut.resize(len);
	memcpy(&strOut[0], InData, len);

	///////////////////奇数先用_strMiYao加密
	size_t tt_testlen=len;
	if(tt_testlen>(_MiYaoLen+_MiYaoLen))
		tt_testlen=_MiYaoLen + _MiYaoLen;

	int count=0;
	//只解密前面一部分的字节
	for(size_t i=0;i<tt_testlen;)
	{
		strOut[i]= strOut[i] ^ (_strMiYao[count++]);
		i+=2;
	}

	/////////////////偶数用_strOtherMiYao加密
	tt_testlen=len;
	if(tt_testlen>(_OtherMiYaoLen+_OtherMiYaoLen))
		tt_testlen=_OtherMiYaoLen + _OtherMiYaoLen;

	count=0;
	for(size_t i=1;i<tt_testlen;)
	{
		strOut[i]= strOut[i] ^ (_strOtherMiYao[count++]);
		i+=2;
	}

	return true;
}

bool CryptMethod::EncryptString5(const void* InData,size_t len,string& strOut)
{
	strOut.resize(len);
	memcpy(&strOut[0], InData, len);

	size_t tt_testlen=len;
	if(tt_testlen>_MiYaoLen)
		tt_testlen=_MiYaoLen;

	//只加密前面一部分的字节
	for(size_t i=0;i<tt_testlen;i++)
	{
		strOut[i]= strOut[i] ^ (_strMiYao[i]);
	}

	/////////////
	tt_testlen=len;
	if(tt_testlen>_OtherMiYaoLen)
		tt_testlen=_OtherMiYaoLen;
	for(size_t i=0;i<tt_testlen;i++)
	{
		strOut[i]= strOut[i] ^ (_strOtherMiYao[i]);
	}

	return true;
}
bool CryptMethod::DecryptString5(const void* InData,size_t len,string& strOut)
{
	strOut.resize(len);
	memcpy(&strOut[0], InData, len);

	size_t tt_testlen=len;
	if(tt_testlen>_OtherMiYaoLen)
		tt_testlen=_OtherMiYaoLen;

	//只解密前面一部分的字节
	for(size_t i=0;i<tt_testlen;i++)
	{
		strOut[i]= strOut[i] ^ (_strOtherMiYao[i]);
	}

	/////////////
	tt_testlen=len;
	if(tt_testlen>_MiYaoLen)
		tt_testlen=_MiYaoLen;
	for(size_t i=0;i<tt_testlen;i++)
	{
		strOut[i]= strOut[i] ^ (_strMiYao[i]);
	}
	return true;
}


///////////////////
bool CryptMethod::EncryptString6(const void* InData,size_t len,string& strOut)
{
	strOut.resize(len);
	memcpy(&strOut[0], InData, len);
	for(size_t i=0;i<strOut.size();i++)
	{
		if(i!=0)
		{
			strOut[i]=strOut[i] ^ strOut[i-1];
		}
	}
	return true;
}
bool CryptMethod::DecryptString6(const void* InData,size_t len,string& strOut)
{
	strOut.resize(len);
	memcpy(&strOut[0], InData, len);
	for(size_t i=strOut.size()-1;i>=1;i--)
	{
		strOut[i]=strOut[i] ^ strOut[i-1];
	}
	return true;
}

/////////////////
bool CryptMethod::EncryptString1_InOut(void* InData,size_t len)  //按顺序只加密前一部分
{
	unsigned char* pInData = (unsigned char*)InData;
	size_t tt_testlen=len;
	if(tt_testlen>_MiYaoLen)
		tt_testlen=_MiYaoLen;

	//只加密前面一部分的字节
	for(size_t i=0;i<tt_testlen;i++)
	{
		pInData[i]= pInData[i] ^ (_strMiYao[i]);
	}
	return true;
}
bool CryptMethod::DecryptString1_InOut(void* InData,size_t len)
{
	unsigned char* pInData = (unsigned char*)InData;
	size_t tt_testlen=len;
	if(tt_testlen>_MiYaoLen)
		tt_testlen=_MiYaoLen;

	//只解密前面一部分的字节
	for(size_t i=0;i<tt_testlen;i++)
	{
		pInData[i]= pInData[i] ^ (_strMiYao[i]);
	}

	return true;

}

bool CryptMethod::EncryptString2_InOut(void* InData,size_t len)  //按顺序只加密前一部分的奇数
{
	unsigned char* pInData = (unsigned char*)InData;
	size_t tt_testlen=len;
	if(tt_testlen>(_MiYaoLen+_MiYaoLen))
		tt_testlen=_MiYaoLen + _MiYaoLen;

	int count=0;
	//只加密前面一部分的字节
	for(size_t i=0;i<tt_testlen;)
	{
		pInData[i]= pInData[i] ^ (_strMiYao[count++]);
		i+=2;
	}

	return true;
}
bool CryptMethod::DecryptString2_InOut(void* InData,size_t len)
{
	unsigned char* pInData = (unsigned char*)InData;

	size_t tt_testlen=len;
	if(tt_testlen>(_MiYaoLen+_MiYaoLen))
		tt_testlen=_MiYaoLen + _MiYaoLen;

	int count=0;
	//只解密前面一部分的字节
	for(size_t i=0;i<tt_testlen;)
	{
		pInData[i]= pInData[i] ^ (_strMiYao[count++]);
		i+=2;
	}
	return true;
}

bool CryptMethod::EncryptString3_InOut(void* InData,size_t len)  //按顺序只加密前一部分的偶数
{
	unsigned char* pInData = (unsigned char*)InData;

	size_t tt_testlen=len;
	if(tt_testlen>(_MiYaoLen+_MiYaoLen))
		tt_testlen=_MiYaoLen + _MiYaoLen;

	int count=0;
	//只加密前面一部分的字节
	for(size_t i=1;i<tt_testlen;)
	{
		pInData[i]= pInData[i] ^ (_strMiYao[count++]);
		i+=2;
	}

	return true;
}
bool CryptMethod::DecryptString3_InOut(void* InData,size_t len)
{
	unsigned char* pInData = (unsigned char*)InData;

	size_t tt_testlen=len;
	if(tt_testlen>(_MiYaoLen+_MiYaoLen))
		tt_testlen=_MiYaoLen + _MiYaoLen;

	int count=0;
	//只加密前面一部分的字节
	for(size_t i=1;i<tt_testlen;)
	{
		pInData[i]= pInData[i] ^ (_strMiYao[count++]);
		i+=2;
	}

	return true;
}

bool CryptMethod::EncryptString4_InOut(void* InData,size_t len)  //按顺序奇偶用不同的密钥加密
{
	unsigned char* pInData = (unsigned char*)InData;

	///////////////////奇数先用_strMiYao加密
	size_t tt_testlen=len;
	if(tt_testlen>(_MiYaoLen+_MiYaoLen))
		tt_testlen=_MiYaoLen + _MiYaoLen;

	int count=0;
	//只解密前面一部分的字节
	for(size_t i=0;i<tt_testlen;)
	{
		pInData[i]= pInData[i] ^ (_strMiYao[count++]);
		i+=2;
	}

	/////////////////偶数用_strOtherMiYao加密
	tt_testlen=len;
	if(tt_testlen>(_OtherMiYaoLen+_OtherMiYaoLen))
		tt_testlen=_OtherMiYaoLen + _OtherMiYaoLen;

	count=0;
	for(size_t i=1;i<tt_testlen;)
	{
		pInData[i]= pInData[i] ^ (_strOtherMiYao[count++]);
		i+=2;
	}

	return true;
}
bool CryptMethod::DecryptString4_InOut(void* InData,size_t len)
{
	unsigned char* pInData = (unsigned char*)InData;

	///////////////////奇数先用_strMiYao加密
	size_t tt_testlen=len;
	if(tt_testlen>(_MiYaoLen+_MiYaoLen))
		tt_testlen=_MiYaoLen + _MiYaoLen;

	int count=0;
	//只解密前面一部分的字节
	for(size_t i=0;i<tt_testlen;)
	{
		pInData[i]= pInData[i] ^ (_strMiYao[count++]);
		i+=2;
	}

	/////////////////偶数用_strOtherMiYao加密
	tt_testlen=len;
	if(tt_testlen>(_OtherMiYaoLen+_OtherMiYaoLen))
		tt_testlen=_OtherMiYaoLen + _OtherMiYaoLen;

	count=0;
	for(size_t i=1;i<tt_testlen;)
	{
		pInData[i]= pInData[i] ^ (_strOtherMiYao[count++]);
		i+=2;
	}

	return true;
}

bool CryptMethod::EncryptString5_InOut(void* InData,size_t len)  //用两个密钥按顺序加
{
	unsigned char* pInData = (unsigned char*)InData;

	size_t tt_testlen=len;
	if(tt_testlen>_MiYaoLen)
		tt_testlen=_MiYaoLen;

	//只加密前面一部分的字节
	for(size_t i=0;i<tt_testlen;i++)
	{
		pInData[i]= pInData[i] ^ (_strMiYao[i]);
	}

	/////////////
	tt_testlen=len;
	if(tt_testlen>_OtherMiYaoLen)
		tt_testlen=_OtherMiYaoLen;
	for(size_t i=0;i<tt_testlen;i++)
	{
		pInData[i]= pInData[i] ^ (_strOtherMiYao[i]);
	}

	return true;
}
bool CryptMethod::DecryptString5_InOut(void* InData,size_t len)
{
	unsigned char* pInData = (unsigned char*)InData;

	size_t tt_testlen=len;
	if(tt_testlen>_OtherMiYaoLen)
		tt_testlen=_OtherMiYaoLen;

	//只解密前面一部分的字节
	for(size_t i=0;i<tt_testlen;i++)
	{
		pInData[i]= pInData[i] ^ (_strOtherMiYao[i]);
	}

	/////////////
	tt_testlen=len;
	if(tt_testlen>_MiYaoLen)
		tt_testlen=_MiYaoLen;
	for(size_t i=0;i<tt_testlen;i++)
	{
		pInData[i]= pInData[i] ^ (_strMiYao[i]);
	}
	return true;
}

bool CryptMethod::EncryptString6_InOut(void* InData,size_t len)
{
	unsigned char* pInData = (unsigned char*)InData;
	for(size_t i=0;i<len;i++)
	{
		if(i!=0)
		{
			pInData[i]=pInData[i] ^ pInData[i-1];
		}
	}
	return true;
}
bool CryptMethod::DecryptString6_InOut(void* InData,size_t len)
{
	unsigned char* pInData = (unsigned char*)InData;
	for(size_t i=len-1;i>=1;i--)
	{
		pInData[i]=pInData[i] ^ pInData[i-1];
	}
	return true;
}