/*
* create by wangweijin
*/
#pragma once
#include "CryptHead.h"
#include <string>
using namespace std;

//加解密的方法
//考虑效率只做简单加密,也就是异或法
class CryptMethod : public ICrypt
{
public:
	CryptMethod();
	virtual ~CryptMethod();
public:
	virtual bool EnCryptData(const void* InData,size_t len,int method,string& strOut); //加密数据
	virtual bool DeCryptData(const void* InData,size_t len,int method,string& strOut); //解密数据

	virtual bool EnCryptData_InOut(void* InData,size_t len,int method);//加密
	virtual bool DeCryptData_InOut(void* InData,size_t len,int method);//解密

protected:
	bool EncryptString1(const void* InData,size_t len,string& strOut);  //按顺序只加密前一部分
	bool DecryptString1(const void* InData,size_t len,string& strOut);

	bool EncryptString2(const void* InData,size_t len,string& strOut);  //按顺序只加密前一部分的奇数
	bool DecryptString2(const void* InData,size_t len,string& strOut);

	bool EncryptString3(const void* InData,size_t len,string& strOut);  //按顺序只加密前一部分的偶数
	bool DecryptString3(const void* InData,size_t len,string& strOut);

    bool EncryptString4(const void* InData,size_t len,string& strOut);  //按顺序奇偶用不同的密钥加密
	bool DecryptString4(const void* InData,size_t len,string& strOut);

    bool EncryptString5(const void* InData,size_t len,string& strOut);  //用两个密钥按顺序加
	bool DecryptString5(const void* InData,size_t len,string& strOut);

	//有首字母递归向后异或
	bool EncryptString6(const void* InData,size_t len,string& strOut); 
	bool DecryptString6(const void* InData,size_t len,string& strOut);

protected:
	bool EncryptString1_InOut(void* InData,size_t len);  //按顺序只加密前一部分
	bool DecryptString1_InOut(void* InData,size_t len);

	bool EncryptString2_InOut(void* InData,size_t len);  //按顺序只加密前一部分的奇数
	bool DecryptString2_InOut(void* InData,size_t len);

	bool EncryptString3_InOut(void* InData,size_t len);  //按顺序只加密前一部分的偶数
	bool DecryptString3_InOut(void* InData,size_t len);

	bool EncryptString4_InOut(void* InData,size_t len);  //按顺序奇偶用不同的密钥加密
	bool DecryptString4_InOut(void* InData,size_t len);

	bool EncryptString5_InOut(void* InData,size_t len);  //用两个密钥按顺序加
	bool DecryptString5_InOut(void* InData,size_t len);

	//有首字母递归向后异或
	bool EncryptString6_InOut(void* InData,size_t len); 
	bool DecryptString6_InOut(void* InData,size_t len);

protected:
	string  _strMiYao; //密钥
	string  _strOtherMiYao;//另一个密钥
	size_t  _MiYaoLen; //密钥长
	size_t  _OtherMiYaoLen; //另一个密钥长
};