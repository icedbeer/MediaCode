/*
* create by wangweijin
*/
#pragma once
#include <string>
using namespace std;

class ICrypt
{
public:
	//加密,目前0不加密，1-6为不同的加密方法
	virtual bool EnCryptData(const void* InData,size_t len,int method,string& strOut)=0; //加密数据
	virtual bool DeCryptData(const void* InData,size_t len,int method,string& strOut) = 0 ;  //解密数据

	//第二种直接在传入数据上加密
	virtual bool EnCryptData_InOut(void* InData,size_t len,int method) = 0;//加密
	virtual bool DeCryptData_InOut(void* InData,size_t len,int method) = 0 ;//解密
};