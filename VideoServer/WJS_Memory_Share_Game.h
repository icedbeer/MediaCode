#pragma once
#include <windows.h>
#include <vector>
#include <string>

#include "stream/WJS_IByteStream.h"
#include "WJS_Memory_Share.h"
#include "WJS_MemoryShare_Head.h"

using namespace std;

class MEMORYSHARE_API IMemoryShareHandler
{
public:
	IMemoryShareHandler(){}
	virtual ~IMemoryShareHandler(){}

	virtual void OnRecvMemoryShareMsg(void* data, size_t len)=0;
	virtual void WriteMemoryShareLog(const string& strLog)=0;
};

class MEMORYSHARE_API WJS_Memory_Share_Game
{
public:
	WJS_Memory_Share_Game(string name,bool bCreater,IMemoryShareHandler* pHandler);
	virtual ~WJS_Memory_Share_Game();

	void SetHandler(IMemoryShareHandler* pHandler) {m_pHandler=pHandler;}

	bool			SendMsg(void* pData, int len);
	int 			Read();
	void            SetCloseProtocolData(const unsigned char* pData,int len);//设关闭协议的数据

	string          GetName(); //得到名字
protected:
	IMemoryShareHandler* m_pHandler;
	WJS_Memory_Share*    m_pMemoryShare;
	string               m_name;
	
};