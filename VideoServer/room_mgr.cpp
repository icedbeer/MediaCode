#include "room_mgr.h"
#include "GlobalServer.h"

uint8_t CRoomMgr::_tmp_buf[8] = {0};

// 短整型大小端互换  
#define BigLittleSwap16(A)  ((((uint16_t)(A) & 0xff00) >> 8) | \
                            (((uint16_t)(A) & 0x00ff) << 8)) 
//长整型大小端互换  
#define BigLittleSwap32(A)  ((((uint32_t)(A) & 0xff000000) >> 24) |\
                            (((uint32_t)(A) & 0x00ff0000) >> 8) | \
                            (((uint32_t)(A) & 0x0000ff00) << 8) | \
                            (((uint32_t)(A) & 0x000000ff) << 24))


CRoomMgr::CRoomMgr(event_base* eb_)
: _base_protocal_len(3)
,_event_base(eb_)
,_buffer(0)
,_buffer_count(2)
,_max_player_count(-1)
{
    _tmp_evbuffer = NULL;
    _tmp_evbuffer = evbuffer_new();
    _buffer = new uint8_t[51200];
    _pkt_buf = new uint8_t[51200];
    _pTmpNetPkt = new NetPacket;

    //socket keepalive tmp var
    _bKeepAlive = TRUE;
    memset(&_alive_in, 0, sizeof(_alive_in));
    memset(&_alive_in, 0, sizeof(_alive_out));
    _alive_in.keepalivetime = 5000; // 开始首次KeepAlive探测前的TCP空闭时间
    _alive_in.keepaliveinterval = 5000; // 两次KeepAlive探测间的时间间隔
    _alive_in.onoff = TRUE;

    _ulBytesReturn = 0;
}

CRoomMgr::~CRoomMgr(void)
{
    clear_room();
    if (_tmp_evbuffer)
    {
        evbuffer_free(_tmp_evbuffer);
        _tmp_evbuffer = NULL;
    }
    if (_buffer)
    {
        delete []_buffer;
    }
    if (_pkt_buf)
    {
        delete []_pkt_buf;
    }
    if (_pTmpNetPkt)
    {
        delete _pTmpNetPkt;
    }
}

//添加node到对应的room中，如果不存在此room，则新建对应的room id
bool CRoomMgr::add_node(const USER_ID &uid_, const ROOM_ID &room_id_, 
                       const evhttp_request *&req_, const uint8_t& user_type_)
{
    BOOST_DEBUG << "CRoomMgr::add_node room id:" << room_id_ << "; user id:" << uid_;
    BOOST_DEBUG << "user id:" << uid_ << " login  room:" << room_id_ << " success.";
    if (_max_player_count > 0 && CBaseRoom::_cur_player_count >= _max_player_count)
    {
        return false;
    }
    if (_room_map.end() == _room_map.find(room_id_) && add_room(room_id_))
    {
        return get_room(room_id_)->add_node(uid_, user_type_);
    }else
    {
        _pTmp_room = get_room(room_id_);
        if (_pTmp_room)
        {
            return _pTmp_room->add_node(uid_, user_type_);
        }
    }
    return false;
}

//从node对应的room中删除此node，如果room没有了任何node，则删除此room
bool CRoomMgr::del_node(const USER_ID &user_id_, const ROOM_ID &room_id_, 
                        const uint8_t& user_type_)
{
    BOOST_DEBUG << "CRoomMgr::del_node id:" << user_id_;
    CBaseRoom *pRoom = get_room(room_id_);
    if (NULL != pRoom)
    {
        BOOST_DEBUG << "user id:" << user_id_ << " logout  room:" << pRoom->get_room_id();
        pRoom->del_node(user_id_, user_type_);
        if (0 == pRoom->get_nodes_count())
        {
            del_room(room_id_);
        }
        return true;
    }
    return false;
}

//添加房间
//@return 0:success; -1:failed; 1:already exist.
bool CRoomMgr::add_room(const ROOM_ID &room_id_)
{
    BOOST_DEBUG << "CRoomMgr::add_room :" << room_id_;
    if(_room_map.end() != _room_map.find(room_id_))
    {
        return false;
    }else
    {
        try{
            _room_map[room_id_] = new CBaseRoom(room_id_, _buffer_count);
        }catch(std::bad_alloc& e)
        {
            BOOST_INFO << "new CBaseRoom failed. room id:" << room_id_
                << "error info:" << e.what();
            return false;
        }
        return true;
    }
}

bool CRoomMgr::del_room(const ROOM_ID &room_id_)
{
    BOOST_DEBUG << "CRoomMgr::del_room :" << room_id_;
    if (_room_map.end() == _room_map.find(room_id_))
    {
        return false;
    }else
    {
        ROOM_ITERATOR itor = _room_map.find(room_id_);
        delete itor->second;
        _room_map.erase(itor);
        return true;
    }
}

CBaseRoom*& CRoomMgr::get_room(const ROOM_ID &room_id_)
{
    _pTmp_room = NULL;
    if (_room_map.end() == _room_map.find(room_id_))
    {
        return _pTmp_room;
    }
    return _room_map.find(room_id_)->second;
}

// delete all rooms
void CRoomMgr::clear_room()
{
    BOOST_INFO << "CRoomMgr::clear_room room count:" << _room_map.size();

    ROOM_ITERATOR itor = _room_map.begin();
    while(_room_map.end() != itor)
    {
        delete itor->second;
        itor->second = NULL;
        itor++;
    }
    _room_map.clear();
}

void CRoomMgr::update_buffer_count()
{
    ROOM_ITERATOR itor = _room_map.begin();
    while (itor != _room_map.end())
    {
        if (itor->second)
        {
            itor->second->set_buffer_num(_buffer_count);
        }
        itor++;
    }
}

int CRoomMgr::process_msg(evbuffer *&ev_buf_, evhttp_request *&req_)
{
    _ev_buf_len = evbuffer_get_length(ev_buf_);
    if (_ev_buf_len > 51200)
    {
        BOOST_WARNING<<  "recv data len to big:" << _ev_buf_len;
        return -1;
    }
    if ((_ev_buf_len = evbuffer_copyout(const_cast<evbuffer *>(ev_buf_), _buffer, _ev_buf_len)) <= 2) 
    {
		BOOST_DEBUG<<  "dump_request_cb recv data len:" << evbuffer_get_length(ev_buf_) << "too small!";
        //只够包头包尾的长度?
        return -1;
    }
    //解析第一个包(如果有多个的话)
    try
    {
        memcpy(&_tmp_pkt_len, _buffer, sizeof(short));
        _tmp_pkt_len = CGlobalServer::small_2_big_short(_tmp_pkt_len);
        if (_ev_buf_len- 2 < _tmp_pkt_len)
        {
            return -1;
        }
        if(!_pTmpNetPkt->ParseFromArray(_buffer + 2, _tmp_pkt_len))
        {
            BOOST_INFO << "process_msg parse_data failed. len:"<<_ev_buf_len;
            return -1;
        }
    }catch(google::protobuf::FatalException& e)
    {
        BOOST_WARNING << "process_msg parse_data failed. exception msg:"<<e.what();
        return -1;
    }catch (...)
    {
        return -1;
    }
    _tmp_room_id = _pTmpNetPkt->room_id();
    _tmp_uid = _pTmpNetPkt->user_id();

    //protocol packet
    switch (_pTmpNetPkt->proto_num())
    {
    case EPROTO_GET:
        return process_get_msg(req_);
    case EPROTO_LOGIN:
        //在下面处理，判断是否可以登入
        break;
    case EPROTO_LOGOUT:
        {
            _tmp_user_type = _pTmpNetPkt->proto_pkt().user_type();
            evhttp_send_reply(req_, HTTP_PROTOCAL_CODE_ME, "OK", ev_buf_);
        }
    	break;
    case EPROTO_BUFFER_NUM:
        {
//             evbuffer_drain(_tmp_evbuffer, evbuffer_get_length(_tmp_evbuffer));
//             evbuffer_add(_tmp_evbuffer, PROTOCOL_HEADER, 4);
//             if (_pTmpNetPkt->pkt->has_proto_pkt() && _pTmpNetPkt->pkt->proto_pkt().has_value())
//             {
//                 if(_pTmpNetPkt->pkt->proto_pkt().value() > 5 || 
//                     _pTmpNetPkt->pkt->proto_pkt().value() < 0)
//                 {
//                     //不在正常范围内，不操作
//                     BOOST_WARNING << "set buffer num error, vaule is excption! vul:" << 
//                         _pTmpNetPkt->pkt->proto_pkt().value();
//                     _pTmpNetPkt->pkt->mutable_proto_pkt()->set_request_ret(-1);
//                 }else
//                 {
//                     BOOST_WARNING << "set buffer num :" << _pTmpNetPkt->pkt->proto_pkt().value();
//                     _buffer_count = _pTmpNetPkt->pkt->proto_pkt().value();
//                     _pTmpNetPkt->pkt->mutable_proto_pkt()->set_request_ret(0);
// 
//                     update_buffer_count();
//                 }
//             }else
//             {
//                 _pTmpNetPkt->pkt->mutable_proto_pkt()->set_request_ret(-1);
//                 BOOST_WARNING << "set buffer num failed.";
//             }
//             string str = _pTmpNetPkt->pack_2_string();
//             evbuffer_add(_tmp_evbuffer, str.data(), str.length());
//             evbuffer_add(_tmp_evbuffer, PROTOCOL_TAIL, 4);
//             evhttp_send_reply(req_, HTTP_PROTOCAL_CODE_ME, "OK", _tmp_evbuffer);
        }
        break;
    case EPROTO_PLAYER_NUM:
        {
//             CBasePacket *new_pkt = new CBasePacket(*_pTmpNetPkt->pkt);
//             evbuffer_drain(_tmp_evbuffer, evbuffer_get_length(_tmp_evbuffer));
//             evbuffer_add(_tmp_evbuffer, PROTOCOL_HEADER, 4);
//             if (_pTmpNetPkt->pkt->has_proto_pkt() && _pTmpNetPkt->pkt->proto_pkt().has_request_ret())
//             {
//                 new_pkt->mutable_proto_pkt()->set_value(CBaseRoom::_cur_player_count);
//                 new_pkt->mutable_proto_pkt()->set_request_ret(0);
//                 BOOST_WARNING << "Current player num is : " << CBaseRoom::_cur_player_count;
//             }else
//             {
//                 new_pkt->mutable_proto_pkt()->set_request_ret(-1);
//                 BOOST_WARNING << "Get player num failed.";
//             }
//             NetPacket tmp_pkt(PROTOCOL_PLAYER_NUM, new_pkt);
//             string str = tmp_pkt.pack_2_string();
//             evbuffer_add(_tmp_evbuffer, str.data(), str.length());
//             evbuffer_add(_tmp_evbuffer, PROTOCOL_TAIL, 4);
//             evhttp_send_reply(req_, HTTP_PROTOCAL_CODE_ME, "OK", _tmp_evbuffer);
        }
        break;
    case EPROTO_MAX_PLAYER_COUNT:
        {
//             evbuffer_drain(_tmp_evbuffer, evbuffer_get_length(_tmp_evbuffer));
//             evbuffer_add(_tmp_evbuffer, PROTOCOL_HEADER, 4);
//             if (_pTmpNetPkt->pkt->has_proto_pkt() && _pTmpNetPkt->pkt->proto_pkt().has_value())
//             {
//                 _max_player_count = _pTmpNetPkt->pkt->proto_pkt().value();
//                 _pTmpNetPkt->pkt->mutable_proto_pkt()->set_request_ret(0);
//                 BOOST_WARNING << "set max player num : " << _max_player_count;
//             }else
//             {
//                 _pTmpNetPkt->pkt->mutable_proto_pkt()->set_request_ret(-1);
//                 BOOST_WARNING << "set max player num failed.";
//             }
//             string str = _pTmpNetPkt->pack_2_string();
//             evbuffer_add(_tmp_evbuffer, str.data(), str.length());
//             evbuffer_add(_tmp_evbuffer, PROTOCOL_TAIL, 4);
//             evhttp_send_reply(req_, HTTP_PROTOCAL_CODE_ME, "OK", _tmp_evbuffer);
        }
        break;
    case EPROTO_VIDEO:
    case EPROTO_AUDIO:
        {
            // response the host node
            evbuffer_drain(_tmp_evbuffer, evbuffer_get_length(_tmp_evbuffer));
            evhttp_send_reply(req_, HTTP_MEDIA_CODE_ME, "OK", _tmp_evbuffer);
            //将音视频数据共享到RUDP模块
            share_media_data(this, _tmp_room_id, _buffer, _ev_buf_len);
        }
        break;
    default:
        break;
    }
    if (EPROTO_LOGIN == _pTmpNetPkt->proto_num())
    {
        _tmp_user_type = _pTmpNetPkt->proto_pkt().user_type();
        evbuffer_drain(_tmp_evbuffer, evbuffer_get_length(_tmp_evbuffer));
        _pTmpNetPkt->mutable_proto_pkt()->mutable_vul_list()->Clear();

        if (add_node(_tmp_uid, _tmp_room_id, (const evhttp_request *&)req_, _tmp_user_type))
        {
            _pTmpNetPkt->mutable_proto_pkt()->add_vul_list(0);
            //主播上线，重置下RUDP模块房间内的相关参数
            if (EUSER_ANCHOR == _tmp_user_type)
            {
                reinit_shared_room(this, _tmp_room_id);
            }
        }else
        {
            //登入失败，可能人数太多
            _pTmpNetPkt->mutable_proto_pkt()->add_vul_list(-1);
        }
        short len = _pTmpNetPkt->ByteSize();
        len = CGlobalServer::small_2_big_short(len);
        evbuffer_add(_tmp_evbuffer, &len, sizeof(short));
        if (_pTmpNetPkt->SerializeToArray(_pkt_buf, 51200))
        {
            evbuffer_add(_tmp_evbuffer, _pkt_buf, _pTmpNetPkt->ByteSize());
            evhttp_send_reply(req_, HTTP_PROTOCAL_CODE_ME, "OK", _tmp_evbuffer);
        }else
        {
            return -1;
        }
    }else if (EPROTO_LOGOUT == _pTmpNetPkt->proto_num())
    {
        del_node(_tmp_uid, _tmp_room_id, _tmp_user_type);
    }

    //分发给房间去处理
    ROOM_ITERATOR itor = _room_map.find(_tmp_room_id);
    if (_room_map.end() != itor && itor->second)
    {
        return itor->second->on_recv_data(_tmp_uid, _buffer, _ev_buf_len, req_, *_pTmpNetPkt);
    }else
	{
		BOOST_DEBUG << "not find room :"  << _tmp_room_id;
	}
    return -1;
}

int CRoomMgr::process_get_msg(evhttp_request *&req_)
{
	//BOOST_DEBUG<<  "process_get_msg recv get cmd from:" << _tmp_uid;
    return buffer_node_req(req_, _tmp_room_id, _tmp_uid);
}

int CRoomMgr::buffer_node_req(evhttp_request *&req_, ROOM_ID &room_id_, USER_ID &uid_)
{
    if (NULL == req_)
    {
        return -1;
    }
    //分给房间去处理
    ROOM_ITERATOR itor = _room_map.find(room_id_);
    if (_room_map.end() != itor && itor->second)
    {
        return itor->second->insert_node_req(req_, uid_);
    }else
    {
        return -1;
    }
}

void CRoomMgr::detect_dead_node()
{
    ROOM_ITERATOR itor = _room_map.begin();
    while (itor != _room_map.end())
    {
        if (itor->second)
        {
            itor->second->detect_nodes_state();
            if (0 == itor->second->get_nodes_count())
            {
                delete itor->second;
                itor = _room_map.erase(itor);
                continue;
            }
        }
        itor++;
    }
}

void CRoomMgr::quit_event()
{
    BOOST_WARNING << "CRoomMgr::quit_event()";
    if (_event_base)
    {
        event_base_loopbreak(_event_base);
    }
}

//设置某个房间主播上麦或者下麦
//@param:bOnMic_ true:上麦;false:下麦.
void CRoomMgr::set_room_host(const ROOM_ID& room_id_, const USER_ID& uid_, bool bOnMic_)
{
    BOOST_INFO << "set_room_host() room:" << room_id_ <<"; uid_:" << uid_ << "; " << bOnMic_?"on mic.":"off mic.";
    ROOM_ITERATOR itor = _room_map.find(room_id_);
    if (_room_map.end() != itor && itor->second)
    {
        itor->second->set_host_id(uid_, bOnMic_);
    }else
    {
        BOOST_WARNING << "set_room_host() failed. room not exist.";
    }
}

void CRoomMgr::set_sock_opt(const evutil_socket_t& sock_)
{
    if (sock_ == INVALID_SOCKET)
    {
        return;
    }
    SOCKET_SET_ITERATOR itor = _sock_set.find(sock_);
    if (itor == _sock_set.end()) //新的socket
    {
        // 开启KeepAlive
        _bKeepAlive = TRUE;
        if (::setsockopt(sock_, SOL_SOCKET, SO_KEEPALIVE, (char*)&_bKeepAlive, sizeof(_bKeepAlive)) != SOCKET_ERROR)
        {
            // 设置KeepAlive参数
            _alive_in.keepalivetime = 10000; // 开始首次KeepAlive探测前的TCP空闭时间
            _alive_in.keepaliveinterval = 30000; // 两次KeepAlive探测间的时间间隔
            _alive_in.onoff = TRUE;
            memset(&_alive_out, 0, sizeof(_alive_out));
            _ulBytesReturn = 0;
            if (SOCKET_ERROR == WSAIoctl(sock_, SIO_KEEPALIVE_VALS, &_alive_in, sizeof(_alive_in),
                &_alive_out, sizeof(_alive_out), &_ulBytesReturn, NULL, NULL))
            {
            }
        }
        _sock_set.insert(_sock_set.end(), sock_);
    }
}

void CRoomMgr::del_sock(const evutil_socket_t& sock_)
{
    if (sock_ == INVALID_SOCKET)
    {
        return;
    }
    evutil_closesocket(sock_);
    SOCKET_SET_ITERATOR itor = _sock_set.find(sock_);
    if (itor != _sock_set.end())
    {
        _sock_set.erase(itor);
    }
}

void CRoomMgr::add_shared_media_data(unsigned long long& room_id_, 
    unsigned char* data_, const int& len_)
{
    if (!data_ || len_ <= 0)
    {
        return;
    }
}

void CRoomMgr::add_shared_room(unsigned long long room_id_)
{
    add_room(room_id_);
}

void CRoomMgr::del_shared_room(unsigned long long room_id_)
{
    del_room(room_id_);
}

void CRoomMgr::reinit_room(unsigned long long room_id_)
{
    CBaseRoom* tmp = get_room(room_id_);
    if (tmp)
    {
        tmp->reinit();
    }
}

