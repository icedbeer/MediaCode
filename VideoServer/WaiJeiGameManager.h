#pragma  once

#include <list>
#include "WJS_Memory_Share_Game.h"
#include "VideoServer2HttpServerProtocol_client.h"

using namespace std;
using namespace VideoServer2HttpServer;

//心跳，为了便列调试，加大
#ifdef _DEBUG
#define PCSHEARTTIMEOUTCOUNT 3000
#else
#define PCSHEARTTIMEOUTCOUNT 10
#endif

class CRoomMgr;

struct DelayDeleteMemoryShare // add by xu
{
	unsigned int _dwStartTickCount; //起始时间
	WJS_Memory_Share_Game *_pMemoryShare;
};

class WaiJeiGameManager : public IReceiver_VideoServer2HttpServer, public IMemoryShareHandler
{
public:
    WaiJeiGameManager();
    ~WaiJeiGameManager();

    void        Set_Room_Mgr(CRoomMgr* mgr_);

    virtual int Read();
    //send heartbeat packet to admin 
    virtual void Heart();

    virtual bool CreateShareMemory(const string& share_memory_name_);
    virtual void CloseMemoryShare();                            //关闭共享内存
    
    /////////////////MemoryShareHandler
public:
	virtual void OnRecvMemoryShareMsg(void* data, size_t len);
	virtual void WriteMemoryShareLog(const string& strLog);

    /////////////IReceiver_VideoServer2HttpServer
    virtual void IReceiver_HttpServerWillClose(void* pExData);
    //通知HttpServer服务器主播状态
    //result  0成功，1失败，-1操作失败
    //roomFlag  房间标识
    //AnchorInfoList  主播信息列表
    virtual  void IReceiver_NotifyAnchorStatusToHttpServer(int result,unsigned int roomFlag,const AnchorInfoObjectList& AnchorInfoList,void* pExData);


public:
	static WaiJeiGameManager s_WaiJeiInstance;

protected:
	WJS_Memory_Share_Game*            _pMemoryShare; //共享内存接口 
	std::list<DelayDeleteMemoryShare> _DelayDeleteMemoryShareList;
	int                               _NotRecvGameHeartCount;       //未收到心跳计数
	float                             _fHeartTestTime;              //心跳测试时间
	int                               _MSNo;//为了使得创建的共享内存即使相同游戏前后也是不同名字的
	int                               _GameId;
    CRoomMgr*                         _room_mgr;

	ReceiveCallback_VideoServer2HttpServer     _PC2CocosRecv;
    unsigned long long                _last_heart_tv;
};
