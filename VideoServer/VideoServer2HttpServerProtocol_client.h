//MD5=7b2b1bd7cd4455d0cca4bcbb4e159e9eBDM
//tool vertion:1.1 createtime:2010-02-27
//author:wwjin
//protocolgen:[2016-06-17 10:33:21]
//////////////copyright///////////
#pragma once
#include <string>
#include <vector>
#include "stream/WJS_IByteStream.h"
#include "std/WJS_ArrayVector.h" 
using namespace std;
namespace VideoServer2HttpServer
{
	namespace METHOD
	{
		const int ID_VideoServer2HttpServerRequest_HttpServerHeart=1073217281;
		const int ID_VideoServer2HttpServerCallback_HttpServerWillClose=536870657;
		const int ID_VideoServer2HttpServerCallback_NotifyAnchorStatusToHttpServer=536870658;
	}
	const int VideoServer2HttpServer_No=1244;
	#pragma pack(push)
	#pragma pack(1)
	// 主播信息数据 
	struct AnchorInfoObject
	{
		// 主播session
		unsigned int sessionId;
		// 主播状态
		int status;
		// 主播名称 
		string anchorName;
		// 主播ID 
		int anchorId;
	};
#if defined(_WINDOWS) || defined(WIN32)
	#pragma  pack(pop)
#endif
	inline WJS_InByteStream& operator << (WJS_InByteStream& ins,const AnchorInfoObject& v)
	{
		short tt_len=0;
		int  tt_logsize1 = ins.size();
		ins<<tt_len;
		short tt_version=1;
		ins<<tt_version;
		int  tt_logsize2 = ins.size();
		ins<<v.sessionId;
		ins<<v.status;
		ins<<v.anchorName;
		ins<<v.anchorId;
		short  tt_logsize3 = (short)(ins.size() - tt_logsize2);
		memcpy(&(ins[tt_logsize1]),&tt_logsize3,sizeof(short));
		return ins;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& out, AnchorInfoObject& v)
	{
		short tt_len=0;
		out>>tt_len;
		short tt_version=0;
		out>>tt_version;
		if(tt_version == 1 || (tt_version==0))
		{
			out>>v.sessionId;
			out>>v.status;
			out>>v.anchorName;
			out>>v.anchorId;
		}
		else if(tt_version > 1)
		{
			int tt_logsize1 = out.GetCurOffsetPos();
			out>>v.sessionId;
			out>>v.status;
			out>>v.anchorName;
			out>>v.anchorId;
			int tt_logsize2 = out.GetCurOffsetPos();
			int tt_offset = tt_len - (tt_logsize2 - tt_logsize1);
			if(tt_offset>0)
			{
				out.CurOffset(tt_offset);
			}
		}
		else
		{
			if(tt_version>=1)
				out>>v.sessionId;
			else
				v.sessionId=0;
			if(tt_version>=1)
				out>>v.status;
			else
				v.status=0;
			if(tt_version>=1)
				out>>v.anchorName;
			if(tt_version>=1)
				out>>v.anchorId;
			else
				v.anchorId=0;
		}
		return out;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& out, AnchorInfoObject& v)
	{
		short tt_len=0;
		out>>tt_len;
		short tt_version=0;
		out>>tt_version;
		if(tt_version == 1 || (tt_version==0))
		{
			out>>v.sessionId;
			out>>v.status;
			out>>v.anchorName;
			out>>v.anchorId;
		}
		else if(tt_version > 1)
		{
			int tt_logsize1 = out.GetCurOffsetPos();
			out>>v.sessionId;
			out>>v.status;
			out>>v.anchorName;
			out>>v.anchorId;
			int tt_logsize2 = out.GetCurOffsetPos();
			int tt_offset = tt_len - (tt_logsize2 - tt_logsize1);
			if(tt_offset>0)
			{
				out.CurOffset(tt_offset);
			}
		}
		else
		{
			if(tt_version>=1)
				out>>v.sessionId;
			else
				v.sessionId=0;
			if(tt_version>=1)
				out>>v.status;
			else
				v.status=0;
			if(tt_version>=1)
				out>>v.anchorName;
			if(tt_version>=1)
				out>>v.anchorId;
			else
				v.anchorId=0;
		}
		return out;
	}
	//游戏房间信息列表
	typedef vector<AnchorInfoObject> AnchorInfoObjectList;
	inline WJS_InByteStream& operator << (WJS_InByteStream& ins,const AnchorInfoObjectList& v)
	{
		ins<<int(v.size());
		for(int i=0;i<(int)v.size();i++)
		{
			ins<<v[i];
		}
		return ins;
	}
	inline WJS_OutByteStream& operator >> (WJS_OutByteStream& out, AnchorInfoObjectList& v)
	{
		int count=0;
		out >> count;
		if(count<0)
		{
			throw("vecOverFlow");
			return out;
		}
		v.resize(count);
		for(int i=0;i<count;i++)
		{
			out>>v[i];
		}
		return out;
	}
	inline WJS_OutByteStream2& operator >> (WJS_OutByteStream2& out, AnchorInfoObjectList& v)
	{
		int count=0;
		out >> count;
		if(count<0)
		{
			throw("vecOverFlow");
			return out;
		}
		v.resize(count);
		for(int i=0;i<count;i++)
		{
			out>>v[i];
		}
		return out;
	}
	class SendData_VideoServer2HttpServer
	{
	public:
		//接收HttpServer服务器心跳
		static void Send_HttpServerHeart(WJS_InByteStream& ins)
		{
			ins.OnlySetUseSize0();
			ins<<VideoServer2HttpServer_No;
			ins<<(short)1;
			ins<<VideoServer2HttpServer::METHOD::ID_VideoServer2HttpServerRequest_HttpServerHeart;
		}
	};
	class IReceiver_VideoServer2HttpServer
	{
	public:
		//通知HttpServer服务器关闭
		virtual  void IReceiver_HttpServerWillClose(void* pExData)=0;
		//通知HttpServer服务器主播状态
		//result  0成功，1失败，-1操作失败
		//roomFlag  房间标识
		//AnchorInfoList  主播信息列表
		virtual  void IReceiver_NotifyAnchorStatusToHttpServer(int result,unsigned int roomFlag,const AnchorInfoObjectList& AnchorInfoList,void* pExData)=0;
	};
	class ReceiveCallback_VideoServer2HttpServer
	{
	protected:
		void _parser_HttpServerWillClose(WJS_OutByteStream2& out,IReceiver_VideoServer2HttpServer* receiver,void* pExData,short proversionno)
		{
			if(proversionno==1 || (proversionno==0))
			{
				receiver->IReceiver_HttpServerWillClose(pExData);
			}
			else if(proversionno>1)
			{
				receiver->IReceiver_HttpServerWillClose(pExData);
			}
			else
			{
				receiver->IReceiver_HttpServerWillClose(pExData);
			}
		}
		void _parser_NotifyAnchorStatusToHttpServer(WJS_OutByteStream2& out,IReceiver_VideoServer2HttpServer* receiver,void* pExData,short proversionno)
		{
			if(proversionno==1 || (proversionno==0))
			{
				int result;
				out>>result;
				unsigned int roomFlag;
				out>>roomFlag;
				AnchorInfoObjectList AnchorInfoList;
				out>>AnchorInfoList;
				receiver->IReceiver_NotifyAnchorStatusToHttpServer(result,roomFlag,AnchorInfoList,pExData);
			}
			else if(proversionno>1)
			{
				int result;
				out>>result;
				unsigned int roomFlag;
				out>>roomFlag;
				AnchorInfoObjectList AnchorInfoList;
				out>>AnchorInfoList;
				receiver->IReceiver_NotifyAnchorStatusToHttpServer(result,roomFlag,AnchorInfoList,pExData);
			}
			else
			{
				int result;
				if(proversionno>=1)
					out>>result;
				else
					result = 0;
				unsigned int roomFlag;
				if(proversionno>=1)
					out>>roomFlag;
				else
					roomFlag = 0;
				AnchorInfoObjectList AnchorInfoList;
				if(proversionno>=1)
					out>>AnchorInfoList;
				receiver->IReceiver_NotifyAnchorStatusToHttpServer(result,roomFlag,AnchorInfoList,pExData);
			}
		}
	public:
		bool Parser(const WJSByteSeq& byteStream,IReceiver_VideoServer2HttpServer* receiver,void* pExData)
		{
			int tt_prono=byteStream.readintonly();
			if(tt_prono!=VideoServer2HttpServer_No)
			  return false;
			WJS_OutByteStream2 out(byteStream);
			int prono=0;
			out>>prono;
			if(prono!=VideoServer2HttpServer_No)
			   return false;
			short proversionno=0;
			out>>proversionno;
			int methodid=0;
			out>>methodid;
			switch(methodid)
			{
			case VideoServer2HttpServer::METHOD::ID_VideoServer2HttpServerCallback_HttpServerWillClose :
				_parser_HttpServerWillClose(out,receiver,pExData,proversionno);
				return true;
			case VideoServer2HttpServer::METHOD::ID_VideoServer2HttpServerCallback_NotifyAnchorStatusToHttpServer :
				_parser_NotifyAnchorStatusToHttpServer(out,receiver,pExData,proversionno);
				return true;
			}
			return false;
		}
	};
}

//ID_VideoServer2HttpServerCallback_HttpServerWillClose|ID_VideoServer2HttpServerCallback_NotifyAnchorStatusToHttpServer;
//
