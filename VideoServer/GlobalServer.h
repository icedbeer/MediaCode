#pragma once
#include "base_typedef.h"
#include "base_code.h"
#include "IShareMedia.h"
#include "RUdpProtocol.pb.h"
#include "libevent_header.h"
#include "boost_log.h"
#include "boost/unordered_map.hpp"  
#include "boost/atomic.hpp"
#include "boost/asio.hpp"
#include "boost/date_time/posix_time/ptime.hpp"
#define BOOST_ASIO_DISABLE_STD_CHRONO


#ifdef _DEBUG
#pragma comment(lib, "libprotobufD.lib")
#else
#pragma comment(lib, "libprotobuf.lib")
#endif // _DEBUG

// 短整型大小端互换  
#define BigLittleSwap16(A)  ((((uint16_t)(A) & 0xff00) >> 8) | \
    (((uint16_t)(A) & 0x00ff) << 8)) 

using namespace std;
using namespace ProtoNameSpace;

class CGlobalServer
{
public:
    CGlobalServer(void);
    virtual ~CGlobalServer(void);

    static short    small_2_big_short(short num_);

public:
    static bool                         _big_endian;
};

extern boost::int64_t sev_get_epoch_time();

#define ALLOC_PROTOCAL_PACKET(pkt_, proto_num_, room_id_, user_id_) \
    NetPacket *pkt_ = NULL; \
    try{ \
    pkt_ = new NetPacket; \
    pkt_->set_proto_num(proto_num_); \
    pkt_->set_room_id(room_id_); \
    pkt_->set_user_id(user_id_); \
    pkt_->mutable_proto_pkt()->set_user_type(EUSER_PLAYER); \
    pkt_->mutable_proto_pkt()->add_vul_list(0);\
}catch(std::bad_alloc &) \
{;}

#define BigLittleSwap16(A)  ((((uint16_t)(A) & 0xff00) >> 8) | \
    (((uint16_t)(A) & 0x00ff) << 8))

extern list<IShareMedia *>  g_ShareMediaList;

extern void    add_extern_sharemedia_ptr(IShareMedia* ptr_);
extern void    clear_extern_sharemedia_ptr();
extern void    share_media_data(IShareMedia* src_ptr_, unsigned long long room_id_,
    unsigned char* data_, const int& len_);
extern void    add_shared_room(IShareMedia* src_ptr_, unsigned long long room_id_);
extern void    del_shared_room(IShareMedia* src_ptr_, unsigned long long room_id_);
//重置RUDP房间内的相关参数,主要是主播重新上线了，包序号等属性被重置
extern void    reinit_shared_room(IShareMedia* src_ptr_, unsigned long long room_id_);

