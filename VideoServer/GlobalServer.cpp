#include "GlobalServer.h"

list<IShareMedia* >   g_ShareMediaList;
bool CGlobalServer::_big_endian = false;

CGlobalServer::CGlobalServer(void)
{
    //判断字节序
    union
    {
        unsigned short	s16;
        unsigned char   s8[2];
    }un;
    un.s16 = 0x010a;
    _big_endian = (un.s8[0] == 0x01);
}

CGlobalServer::~CGlobalServer(void)
{
}

short CGlobalServer::small_2_big_short(short num_)
{
    short ret = num_;
    if (!_big_endian)
    {
        ret = BigLittleSwap16(num_);
    }
    return ret;
}

boost::int64_t sev_get_epoch_time()
{
    //首先获取时间点chrono::system_clock::now()
    //然后获取1970year至当前时间点的时间间隔duration: .time_since_epoch()
    //最好将duration转换成chrono::milliseconds,并获取总共有多少毫秒
    return boost::chrono::duration_cast<boost::chrono::milliseconds>(
        boost::chrono::system_clock::now().time_since_epoch()).count();
}

extern void add_extern_sharemedia_ptr(IShareMedia* ptr_)
{
    if (!ptr_ || 
        std::find(g_ShareMediaList.begin(), g_ShareMediaList.end(), ptr_) != g_ShareMediaList.end()) 
    {
        return;
    }
    g_ShareMediaList.push_back(ptr_);
}

extern void clear_extern_sharemedia_ptr()
{
    g_ShareMediaList.clear();
}

extern void add_shared_room(IShareMedia* src_ptr_, unsigned long long room_id_)
{
    if (g_ShareMediaList.empty())
    {
        return;
    }
    BOOST_AUTO(itor, g_ShareMediaList.begin());
    while (itor != g_ShareMediaList.end())
    {
        if ((*itor) != src_ptr_)
        {
            (*itor)->add_shared_room(room_id_);
        }
        ++itor;
    }
}

extern void del_shared_room(IShareMedia* src_ptr_, unsigned long long room_id_)
{
    if (g_ShareMediaList.empty())
    {
        return;
    }
    BOOST_AUTO(itor, g_ShareMediaList.begin());
    while (itor != g_ShareMediaList.end())
    {
        if ((*itor) != src_ptr_)
        {
            (*itor)->del_shared_room(room_id_);
        }
        ++itor;
    }
}

extern void reinit_shared_room(IShareMedia* src_ptr_, unsigned long long room_id_)
{
    BOOST_AUTO(itor, g_ShareMediaList.begin());
    while (itor != g_ShareMediaList.end())
    {
        if ((*itor) != src_ptr_)
        {
            (*itor)->reinit_room(room_id_);
        }
        ++itor;
    }
}

extern void share_media_data(IShareMedia* src_ptr_, unsigned long long room_id_, unsigned char* data_, const int& len_)
{
    if (g_ShareMediaList.empty())
    {
        return;
    }
    BOOST_AUTO(itor, g_ShareMediaList.begin());
    while (itor != g_ShareMediaList.end())
    {
        if ((*itor) != src_ptr_)
        {
            (*itor)->add_shared_media_data(room_id_, data_, len_);
        }
        ++itor;
    }
}
