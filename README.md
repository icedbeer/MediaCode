基于boost、libevent、liburl、ffmpeg开发的在线直播SDK。
音视频采集、编解码、传输、播放；
协议使用google的protobuf进行封装；
Client & Server;
数据传输支持Http 与 Udp协议，可互通；
Demo程序使用Http上行，Udp协议下行。