/*!
 * \file DSoundPlay.h
 *
 * \author Administrator
 * \date 二月 2017
 *
 * 本程序使用DirectSound播放PCM音频采样数据。
 * 此接口适合在需要PCM数据时去解码音频包，而不是解码音频包后主动写入声卡，与CSoundChannnel不同
 * 暂不使用此接口了
 *
 * 函数调用步骤如下：
 *
 * [初始化]
 * DirectSoundCreate8()：创建一个DirectSound对象。
 * SetCooperativeLevel()：设置协作权限，不然没有声音。
 * IDirectSound8->CreateSoundBuffer()：创建一个主缓冲区对象。
 * IDirectSoundBuffer->QueryInterface(IID_IDirectSoundBuffer8..)：
 *			创建一个副缓冲区对象，用来存储要播放的声音数据文件。
 * IDirectSoundBuffer8->QueryInterface(IID_IDirectSoundNotify..)：
 *			创建通知对象，通知应用程序指定播放位置已经达到。
 * IDirectSoundNotify8->SetNotificationPositions()：设置通知位置。
 * IDirectSoundBuffer8->SetCurrentPosition()：设置播放的起始点。
 * IDirectSoundBuffer8->Play()：开始播放。
 *
 * [循环播放数据]
 * IDirectSoundBuffer8->Lock()：锁定副缓冲区，准备写入数据。
 * fread()：读取数据。
 * IDirectSoundBuffer8->Unlock()：解锁副缓冲区。
 * WaitForMultipleObjects()：等待“播放位置已经达到”的通知。
 *
 * This software plays PCM raw audio data using DirectSound.
 * It's the simplest tutorial about DirectSound.
 *
 * The process is shown as follows:
 *
 * [Init]
 * DirectSoundCreate8(): Init DirectSound object.
 * SetCooperativeLevel(): Must set, or we won't hear sound.
 * IDirectSound8->CreateSoundBuffer(): Create primary sound buffer.
 * IDirectSoundBuffer->QueryInterface(IID_IDirectSoundBuffer8..):
 *			Create secondary sound buffer.
 * IDirectSoundBuffer8->QueryInterface(IID_IDirectSoundNotify..):
 *			Create Notification object.
 * IDirectSoundNotify8->SetNotificationPositions():
 *			Set Notification Positions.
 * IDirectSoundBuffer8->SetCurrentPosition(): Set position to start.
 * IDirectSoundBuffer8->Play(): Begin to play.
 *
 * [Loop to play data]
 * IDirectSoundBuffer8->Lock(): Lock secondary buffer.
 * fread(): get PCM data.
 * IDirectSoundBuffer8->Unlock(): UnLock secondary buffer.
 * WaitForMultipleObjects(): Wait for Notifications.
 */
#ifdef _WIN32
#pragma once

#include "IAudioOut.h"
#include <Windows.h>
#include <mmsystem.h>
#include <dsound.h>

#define MAX_AUDIO_BUF 4                         //播放缓冲的通知索引
#define BUFFERNOTIFYSIZE 8196                   //通知位置的大小
#define AVCODEC_MAX_AUDIO_FRAME_SIZE 192000     //音频PCM数据缓冲区大小

class CDSoundPlay : public IAudioOut
{
public:
    CDSoundPlay(void);
    ~CDSoundPlay(void);

    int                     InitAudioOutDev(int channel_num_ = 2, int sample_rate_ = 44100, 
        int sample_bits_ = 16, int nDevID = -1);
    void                    UninitAudioOutDev();
    //播放PCM音频数据
    int                     PlayAudio(void *data_, int size_);
    //获取声音输出设备个数
    //@param dev_name_：设备名称二维数组,如果不需要可以为NULL
    //@param max_num_：数组最多存储多少个，如果为0，则不存储
    //@return 音频输出设备个数
    int                     GetAudioOutDevice(char dev_name_[][256], const int max_num_);

    int                     GetCurDevNum();
    int                     GetVolumeDB();
    GUID                    GetDeviceGuid(int deviceID);
    //获取缓冲中未播放PCM字节数
    int                     GetBufferSize();

protected:
    void                    thread_loop();

public:
    int					    m_deviceNum;
    map<int , string >	    m_deviceNameMap;
    map<int , GUID >	    m_deviceGuidMap;
    int                     m_Volume;
    DWORD                   m_dwNextWriteOffset;
    string                  m_pcmBuffer;
    boost::thread           _dsound_thr;        //播放线程句柄
    bool                    _bplay_flag;        //线程结束标识
    boost::mutex            _mutex;             //PCM缓冲锁
    //DSound variable
    LPDIRECTSOUNDBUFFER8    m_pDSBuffer8;
    LPDIRECTSOUND8          m_pDsd;
    LPDIRECTSOUNDNOTIFY8    m_pDSNotify; 
    DSBPOSITIONNOTIFY       m_aPosNotify[MAX_AUDIO_BUF];
    HANDLE                  m_events[MAX_AUDIO_BUF];
};

#endif // _WIN32
