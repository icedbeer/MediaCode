#include "StdAfx.h"
#include "HttpInterface.h"
#include "session_mgr.h"

CHttpInterface::CHttpInterface(ILiveEvent* event_, USER_ID uid_, ROOM_ID room_id_)
    : _session_event(event_)
    , _http_client(new CSessionMgr(room_id_, uid_, event_))
{
    _session_flag = 0;
}


CHttpInterface::~CHttpInterface()
{
    if (_http_client)
    {
        delete _http_client;
        _http_client = NULL;
    }
}

boost::int32_t CHttpInterface::login(const char* ip_, const short port_, int player_type /*= 0*/)
{
    if (!_http_client)
    {
        return -1;
    }
    _session_flag = get_cur_time();
    string url_str = (boost::format("%s:%d/dump") % ip_ %port_).str();    
    if (1 == player_type)
    {
        return _http_client->Login(url_str, true);
    } else
    {
        return _http_client->Login(url_str, false);
    }
}

boost::int32_t CHttpInterface::logout()
{
    if (!_http_client)
    {
        return -1;
    }
    return _http_client->Logout();
}

bool CHttpInterface::is_login()
{
    if (!_http_client)
    {
        return false;
    }
    return _http_client->is_logined();
}

boost::int32_t CHttpInterface::send_video(NetPacket* pkt_)
{
    if (!_http_client)
    {
        return -1;
    }
    if (pkt_->pkt_seq() <= 1000)
    {
        //只在前1000个包中添加
        pkt_->set_session_flag(_session_flag);
    }
    return _http_client->send_media_frame(pkt_);
}

boost::int32_t CHttpInterface::send_audio(NetPacket* pkt_)
{
    if (!_http_client)
    {
        return -1;
    }
    if (pkt_->pkt_seq() <= 1000)
    {
        //只在前1000个包中添加
        pkt_->set_session_flag(_session_flag);
    }
    return _http_client->send_media_frame(pkt_);
}

void CHttpInterface::set_event(ILiveEvent* event_)
{
    if (!_http_client)
    {
        return;
    }
    _http_client->set_live_event(event_);
}

void CHttpInterface::set_video_cb(boost::function<void(boost::shared_ptr<NetPacket>)> func_)
{
    if (!_http_client)
    {
        return;
    }
    _http_client->set_video_cb(func_);
}

void CHttpInterface::set_audio_cb(boost::function<void(boost::shared_ptr<NetPacket>)> func_)
{
    if (!_http_client)
    {
        return;
    }
    _http_client->set_audio_cb(func_);
}
