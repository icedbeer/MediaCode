#include "stdafx.h"
//#include <afx.h>
//#include <afxwin.h>
#include "video_record_channel.h"
#include <Windef.h>
#include "dshow_video.h"
#include "video_codec.h"
#include "boost_log.h"
#include "media_record_thread.h"
#include "base_timer_value.h"
#include "corlor_convert.h"

#define  MAX_VIDEO_PACKET_SIZE 1400

//// Resize CImage
//void CreateStretchImage(CImage *pImage,CImage *ResultImage,int StretchWidth,int StretchHeight)
//{
//    if(pImage->IsDIBSection())
//    {
//        // 取得 pImage 的 DC
//        CDC* pImageDC1 = CDC::FromHandle(pImage->GetDC()); // Image 因為有自己的 DC, 所以必須使用 FromHandle 取得對應的 DC
//
//
//
//        CBitmap *bitmap1=pImageDC1->GetCurrentBitmap();
//        BITMAP bmpInfo;
//        bitmap1->GetBitmap(&bmpInfo);
//
//
//        // 建立新的 CImage
//        ResultImage->Create(StretchWidth,StretchHeight,bmpInfo.bmBitsPixel);
//        CDC* ResultImageDC = CDC::FromHandle(ResultImage->GetDC());
//
//
//        // 當 Destination 比較小的時候, 會根據 Destination DC 上的 Stretch Blt mode 決定是否要保留被刪除點的資訊
//        ResultImageDC->SetStretchBltMode(HALFTONE); // 使用最高品質的方式
//        ::SetBrushOrgEx(ResultImageDC->m_hDC,0,0,NULL); // 調整 Brush 的起點
//
//
//        // 把 pImage 畫到 ResultImage 上面
//        StretchBlt(*ResultImageDC,0,0,StretchWidth,StretchHeight,*pImageDC1,0,0,pImage->GetWidth(),pImage->GetHeight(),SRCCOPY);
//        // pImage->Draw(*ResultImageDC,0,0,StretchWidth,StretchHeight,0,0,pImage->GetWidth(),pImage->GetHeight());
//
//
//        pImage->ReleaseDC();
//        ResultImage->ReleaseDC();
//    }
//}

void load_img_data_thread(unsigned char* _default_img_data, CImage& _anchor_default_img)
{
    CImage srcImg;
    if (FAILED(srcImg.Load(GLOBAL_CONFIG()->_anchor_img)))
    {
        BOOST_WARNING << "Loat anchor default image failed. img path:" << GLOBAL_CONFIG()->_anchor_img;
    } else
    {
        //if (srcImg.GetWidth() == GLOBAL_CONFIG()->_preview_pic_width
        //    || srcImg.GetHeight() == GLOBAL_CONFIG()->_preview_pic_height)
        //{
            _anchor_default_img = srcImg;
        //} else
        //{
        //    CreateStretchImage(&srcImg, &_anchor_default_img,
        //        GLOBAL_CONFIG()->_preview_pic_width, GLOBAL_CONFIG()->_preview_pic_height);
        //}
        COLORREF colr;
        int nlen(640 * 480);
        for (int i = 0; i < GLOBAL_CONFIG()->_preview_pic_height; i++)//注意是nPtHeig 而不是nptWith ，因为这里是一行一行的获取图片RGB 
        {
            for (int j = GLOBAL_CONFIG()->_preview_pic_width - 1; j >= 0; j--)
            {
                colr = _anchor_default_img.GetPixel(j, i);//注意j 和i 不能交换；
                nlen--;
                *(_default_img_data + nlen * 3 + 2) = GetRValue(colr);
                *(_default_img_data + nlen * 3 + 1) = GetGValue(colr);
                *(_default_img_data + nlen * 3) = GetBValue(colr);
            }
        }
    }
}

CVideoRecordChannel::CVideoRecordChannel(CMediaRecordThread *media_thr_)
{
    _last_tv = 0;
    _video_frame_id = 0;
    _last_key_frame_id = 0;
    _pEncoder = NULL;
    _event = NULL;
    _max_buffer_size = VIDEO_MAX_WIDTH * VIDEO_MAX_HEIGHT * 3;
    _default_img_data = NULL;
    _bexit = false;

    _pRecThread = media_thr_;

    _default_img_data = new unsigned char[GLOBAL_CONFIG()->_preview_pic_width * GLOBAL_CONFIG()->_preview_pic_height * 3];
    //构造黑屏图片
    memset(_default_img_data, 0, GLOBAL_CONFIG()->_preview_pic_width * GLOBAL_CONFIG()->_preview_pic_height * 3);
    //读取图片数据
    _load_thr = boost::thread(load_img_data_thread, _default_img_data, _anchor_default_img);
    //开启线程
    _loop_thr = boost::thread(boost::bind(&CVideoRecordChannel::execute, this));
}

CVideoRecordChannel::~CVideoRecordChannel(void)
{
    if (_load_thr.joinable())
    {
        _load_thr.join();
    }
    _bexit = true;
    if (_loop_thr.joinable())
    {
        _loop_thr.join();
    }
    close_all();
    if (_default_img_data)
    {
        delete []_default_img_data;
    }
}

int CVideoRecordChannel::open(int32_t video_show_num, int32_t dev_id_, int32_t width_, int height_, 
                              int frame_rate_, int bit_rate_)
{
    BoostLog::Log(info, "CVideoRecordChannel::open()dev:%d, width:%d, height:%d, frame_rate:%d, bit_rate:%d",
        dev_id_, width_, height_, frame_rate_, bit_rate_);
    
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    VIDEODEVMAP::iterator itor = _video_dev_map.find(video_show_num);
    if (itor != _video_dev_map.end())
    {
        if (itor->second.get<0>()->GetCurDevID() == dev_id_)
        {
            //设备没有改变
            return 1;
        }else
        {
            delete itor->second.get<0>();
            delete itor->second.get<1>();
            _video_dev_map.erase(itor);
        }
    }

    CDShowVideoDevice *_pDShowVideoDevice = new(std::nothrow) CDShowVideoDevice;
    if(NULL == _pDShowVideoDevice || !_pDShowVideoDevice->InitCapture(
        GLOBAL_CONFIG()->_preview_pic_width, GLOBAL_CONFIG()->_preview_pic_height,
        frame_rate_, dev_id_))
    {
        BOOST_WARNING << "CVideoRecordChannel::open init_device() failed!";
        if (_pDShowVideoDevice)
        {
            delete _pDShowVideoDevice;
            _pDShowVideoDevice = NULL;
        }
    }
    if (_pDShowVideoDevice && !_pDShowVideoDevice->StartCapture())
    {
        BOOST_WARNING << "CVideoRecordChannel::open StartCapture() failed!";
        delete _pDShowVideoDevice;
        _pDShowVideoDevice = NULL;
        return -1;
    } 
    if (_pDShowVideoDevice)
    {
        _video_dev_map[video_show_num] = boost::make_tuple(_pDShowVideoDevice, new CCorlorConvert);
    }

    GLOBAL_CONFIG()->_codec_pic_width = width_;
    GLOBAL_CONFIG()->_codec_pic_height = height_;
    if (NULL != _pEncoder )
    {
        if (_pEncoder->get_width() != width_ || _pEncoder->get_height() != height_)
        {
            delete _pEncoder;
            _pEncoder = NULL;
        }
    }
    if (!_pEncoder)
    {
        _pEncoder = new(std::nothrow) H264Encoder;
        if(NULL == _pEncoder || !_pEncoder->init(width_, height_, frame_rate_, bit_rate_))
        {
            BOOST_INFO << "video encode init failed!";
            return -1;
        }
    }
    _video_frame_id = 0;
    _last_key_frame_id = 0;
    return 0;
}

int CVideoRecordChannel::open(int32_t video_show_num, string device_name_, int32_t width_, int height_, 
                              int frame_rate_, int bit_rate_)
{
    BoostLog::Log(info, "CVideoRecordChannel::open()dev:%s, width:%d, height:%d, frame_rate:%d, bit_rate:%d",
        device_name_.c_str(), width_, height_, frame_rate_, bit_rate_);

    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);

    VIDEODEVMAP::iterator itor = _video_dev_map.find(video_show_num);
    if (itor != _video_dev_map.end())
    {
        if (itor->second.get<0>()->GetCurDevName() == device_name_)
        {
            //设备没有改变
            return 1;
        }else
        {
            delete itor->second.get<0>();
            delete itor->second.get<1>();
            _video_dev_map.erase(itor);
        }
    }
    CDShowVideoDevice* _pDShowVideoDevice = new(std::nothrow) CDShowVideoDevice;
    if(NULL == _pDShowVideoDevice || !_pDShowVideoDevice->InitCapture(
        GLOBAL_CONFIG()->_preview_pic_width, GLOBAL_CONFIG()->_preview_pic_height,
        frame_rate_, device_name_))
    {
        BOOST_WARNING << "CVideoRecordChannel::open init_device() failed!";
        if (_pDShowVideoDevice)
        {
            delete _pDShowVideoDevice;
            _pDShowVideoDevice = NULL;
        }
        return -1;
    }
    if (_pDShowVideoDevice && !_pDShowVideoDevice->StartCapture())
    {
        BOOST_WARNING << "CVideoRecordChannel::open StartCapture() failed!";
        delete _pDShowVideoDevice;
        _pDShowVideoDevice = NULL;
        return -1;
    } 
    if (_pDShowVideoDevice)
    {
        _video_dev_map[video_show_num] = boost::make_tuple(_pDShowVideoDevice, new CCorlorConvert);
    }

    GLOBAL_CONFIG()->_codec_pic_width = width_;
    GLOBAL_CONFIG()->_codec_pic_height = height_;
    if (NULL != _pEncoder )
    {
        if (_pEncoder->get_width() != width_ || _pEncoder->get_height() != height_)
        {
            delete _pEncoder;
            _pEncoder = NULL;
        }
    }
    if (!_pEncoder)
    {
        _pEncoder = new(std::nothrow) H264Encoder;
        if(NULL == _pEncoder || !_pEncoder->init(width_, height_, frame_rate_, bit_rate_))
        {
            BOOST_INFO << "video encode init failed!";
            return -1;
        }
    }
    _video_frame_id = 0;
    _last_key_frame_id = 0;
    _last_tv = 0;
    return 0;
}

int CVideoRecordChannel::close(int32_t video_show_num)
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    VIDEODEVMAP::iterator itor = _video_dev_map.find(video_show_num);
    if(itor != _video_dev_map.end())
    {
        delete itor->second.get<0>();
        delete itor->second.get<1>();
        _video_dev_map.erase(itor);
    }
    //if (_pEncoder)
    //{
    //    delete _pEncoder;
    //    _pEncoder = NULL;
    //}
    _video_frame_id = 0;
    return 0;
}

int CVideoRecordChannel::close_all()
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    VIDEODEVMAP::iterator itor = _video_dev_map.begin();
    while(itor != _video_dev_map.end())
    {
        delete itor->second.get<0>();
        delete itor->second.get<1>();
        itor++;
    }
    _video_dev_map.clear();
    if (_pEncoder)
    {
        delete _pEncoder;
        _pEncoder = NULL;
    }
    _video_frame_id = 0;
    return 0;
}

void CVideoRecordChannel::execute()
{
    while (!_bexit)
    {
        if (!loop())
        {
#ifdef WIN32
            Sleep(1);
#else
            usleep(1000);
#endif // WIN32
        }
    }
}

bool CVideoRecordChannel::loop()
{
    if (_load_thr.joinable())
    {
        _load_thr.join();
    }
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);

    string str;
    bool ret_flag = false;
    int32_t width(0), height(0), raw_len(0);
    int32_t src_width(0), src_height(0), src_raw_len(0);
    VIDEODEVMAP::iterator itor = _video_dev_map.begin();
    GUID    media_sub_type;
    while(itor != _video_dev_map.end())
    {        
        raw_len = itor->second.get<0>()->ReadVideoFrame(_pRawBuffer, _max_buffer_size, width, height, media_sub_type);
        if (raw_len <= 0)
        {
            if (1 == itor->second.get<0>()->HandleEvent(str) && str.size() > 0) //检测设备是否被拔出
            {
                if (_event) //通知上层
                {
                    _event->on_video_dev_lost_cb(str.data(), str.size());
                }
            }
            itor++;
            continue;
        }
        ret_flag = true;
        //预览
        if (_event)
        {
            if (_rgb_data.size() < width * height * 3)
            {
                _rgb_data.resize(width * height * 3);
            }
            if (itor->second.get<1>()->img_convert(_pRawBuffer, width, height, 
                CCorlorConvert::GUID_2_ffmpeg_pixformat(media_sub_type), 
                (unsigned char *)_rgb_data.data(), width, height, (int)AV_PIX_FMT_YUV420P) > 0)
            {
                _event->on_video_preview_cb(itor->first, (unsigned char *)_rgb_data.data(), width, height);
            }
        }
        if (GLOBAL_CONFIG()->_cur_video_show_num == itor->first)
        {
            src_width = width;
            src_height= height;
            src_raw_len = raw_len;
            if (_raw_enc_data.size() < src_raw_len)
            {
                _raw_enc_data.resize(src_raw_len);
            }
            _raw_enc_data.assign((char *)_pRawBuffer, src_raw_len);
        }
        itor++;
    }
    if (-1 == GLOBAL_CONFIG()->_cur_video_show_num)
    {
        return ret_flag;
    }

    //判断是否需要发送自定义图片
    if (GLOBAL_CONFIG()->_send_def_img_flag) //编码默认图片
    {
        //确保固定的帧率发送
        if (::GetTickCount() < _last_tv)
        {
            _last_tv = ::GetTickCount();
        }else if ((::GetTickCount() - _last_tv) < (1000 / GLOBAL_CONFIG()->_frame_rate))
        {
            return ret_flag;
        }
        src_width = GLOBAL_CONFIG()->_preview_pic_width;
        src_height = GLOBAL_CONFIG()->_preview_pic_height;
        src_raw_len = src_width * src_height * 3;
        _raw_enc_data.assign((char *)_default_img_data, src_raw_len);
        media_sub_type = MEDIASUBTYPE_RGB24;
    }
    //编码、发送
    if (_pEncoder && src_width > 0 && src_height > 0 && src_raw_len > 0)
    {
        int encode_len_(sizeof(_pEncodeBuffer));
        int frame_type(0);
        if (_pEncoder && (encode_len_ = 
            _pEncoder->encode((unsigned char *)_raw_enc_data.data(), src_raw_len, src_width, src_height,
            CCorlorConvert::GUID_2_ffmpeg_pixformat(media_sub_type), 
            _pEncodeBuffer, encode_len_, &frame_type)) > 0)
        {
            //编码成功，发送
            vector<NetPacket* > vec_;
            split_packet(_pEncodeBuffer, encode_len_, frame_type, vec_);
            //send
            vector<NetPacket* >::iterator it = vec_.begin();
            while (it != vec_.end())
            {
                _pRecThread->send_media_frame((*it));
                delete *it;
                it++;
            }
            vec_.clear();
            ret_flag = true;
        }
    }

    return ret_flag;
}

int CVideoRecordChannel::split_packet(const unsigned char* in_buf_, const uint32_t& in_size_, 
                                      const int32_t& frame_type_, vector<NetPacket* > &vec_)
{
    _video_frame_id++; 
    if (1 == frame_type_)
    {
        _last_key_frame_id = _video_frame_id; 
    }
    unsigned char num(1);
    uint32_t tmp_len(in_size_);
    uint32_t cur_tv = get_cur_time();
    _last_tv = cur_tv;

    unsigned char split_count = (0 == in_size_%MAX_VIDEO_PACKET_SIZE)?
        (in_size_/MAX_VIDEO_PACKET_SIZE):(in_size_/MAX_VIDEO_PACKET_SIZE + 1);
    while (tmp_len > 0)
    {
        NetPacket* pkt = new NetPacket;
        pkt->set_proto_num(EPROTO_VIDEO);
        //pkt->set_pkt_seq(_pRecThread->get_new_pkt_id());
        pkt->set_room_id(0);
        pkt->set_user_id(0);
        pkt->set_send_tm(get_cur_time());
        pkt->mutable_video_pkt()->set_seq(_video_frame_id);
        pkt->mutable_video_pkt()->set_timestamp(cur_tv);
        pkt->mutable_video_pkt()->set_key_frame_id(_last_key_frame_id);
        pkt->mutable_video_pkt()->set_split_count(split_count);
        pkt->mutable_video_pkt()->set_split_seq(num);
        pkt->mutable_video_pkt()->set_frame_type(frame_type_);

        if (tmp_len >= MAX_VIDEO_PACKET_SIZE)
        {
            pkt->mutable_video_pkt()->set_media_data(in_buf_ + (num - 1)*MAX_VIDEO_PACKET_SIZE, MAX_VIDEO_PACKET_SIZE);
            vec_.push_back(pkt);
        }else
        {
            pkt->mutable_video_pkt()->set_media_data(in_buf_ + (num - 1)*MAX_VIDEO_PACKET_SIZE, tmp_len);
            vec_.push_back(pkt);
            break;
        }
        tmp_len -= MAX_VIDEO_PACKET_SIZE;
        num++;
    }
    return 0;
}
