/*********************************************************************************
*Copyright (C), 2005-2006, Cofire p2p. Co., Ltd.
*project name:  Cofire Zone v1.0
*Filename:		dib.cpp
*Author:		lian
*Version:		1.0
*Data:			2007-04
*Description:	DIB
*Funtion List:	
*历史记录:		2007-04 lian Created
*********************************************************************************/
#include "stdafx.h"
#include "dib.h"
#include <string>
#include <math.h>

//////////////////////////////////////////////////////////////////////////
//CDib
//////////////////////////////////////////////////////////////////////////
CDib::CDib()
{
	init();
}

CDib::~CDib()
{
	fini();
}

 void CDib::init()
 {
 	m_pDib = NULL;//dib buffer
 	m_pData = NULL;//buffer point to dib buffer
 	m_pRGB = NULL;//palette buffer
 	
 	m_pBmpInfo = NULL;//bmp info
 	m_pBmpInfoHeader = NULL;// bmp info header	
 
 	ZeroMemory(&m_BmpFileHeader, sizeof(BITMAPFILEHEADER));
 	m_BmpFileHeader.bfType = 0x4D42;//"BM"
 
 	m_valid = false;
 }
 
 void CDib::fini()
 {
 	if(is_valid())
 	{
 		if(m_pDib)
 			free(m_pDib);
 		
 		init();
 	}
 }
 
 //////////////////////////////////////////////////////////////////////////
 //bmp operate
 bool CDib::create( int _width, int _height, int _bits /* = 32 */ )
 {
 	fini();
 
 	long _linesize = _bits * _width;
 
 	while(_linesize % 4)
 		_linesize++;
 	
 	long _dibsize = sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * get_palette_size(_bits) \
 		+ _linesize * _height / 8;
 
 	//allocate dib buffer
 	m_pDib = (BYTE *)malloc(_dibsize);
 	if(!m_pDib)
 		return false;
 
 	//init
 	memset(m_pDib, 0, _dibsize);
 
 	//set info ptr
 	m_pBmpInfo = (BITMAPINFO*)m_pDib;
 	m_pBmpInfoHeader = &m_pBmpInfo->bmiHeader;
 	m_pData = (m_pDib) + sizeof(BITMAPINFOHEADER) \
 		+ sizeof(RGBQUAD) * get_palette_size(_bits);
 
 	if(get_palette_size(_bits) <= 0)
 		m_pRGB = NULL;
 	else
 		m_pRGB = (RGBQUAD *)(m_pDib + sizeof(BITMAPINFOHEADER)) ;
 
 	//init param
 	m_pBmpInfoHeader->biSize = sizeof(BITMAPINFOHEADER);
 	
 	//save param
 	m_pBmpInfoHeader->biWidth		= _width;
 	m_pBmpInfoHeader->biHeight		= _height;
 	m_pBmpInfoHeader->biBitCount	= _bits;
 	m_pBmpInfoHeader->biSizeImage	= _dibsize;
 	m_pBmpInfoHeader->biPlanes		= 1;
 	
 	m_BmpFileHeader.bfSize = sizeof(BITMAPFILEHEADER) + _dibsize;
 	m_BmpFileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) \
 		+ sizeof(BITMAPINFOHEADER) + get_palette_size();
 
 	//is valid now
 	m_valid = true;
 
 	return true;
 }
 
 bool CDib::destroy()
 {
 	fini();
 
 	return true;
 }
 
 void* CDib::get_dib_bits()
 {
 	return m_pData;
 }
 
 bool CDib::set_dib_bits( void* _data, long _len )
 {
 	if(is_valid() && _data)
 	{
 		memcpy(m_pData, _data, _len);
 		return true;
 	}
 
 	return false;
 }
 
 void CDib::reset_dib_bits( int _value )
 {
 	if(is_valid())
 	{
 		memset(m_pData, _value, get_dib_size());
 	}
 }
 
 
 
 //////////////////////////////////////////////////////////////////////////
 bool CDib::load( const char* _filename )
 {
 	fini();
 	
 	bool bret = false;
 	
 	return bret;
 }
 
 bool CDib::save( const char* _filename )
 {
 
 	
 	return false;
 }
 
 
 
 //////////////////////////////////////////////////////////////////////////
 //attr
 bool CDib::is_valid() const
 {
 	return m_valid;
 }
 
 int CDib::get_width() const
 {
 	if(is_valid())
 		return m_pBmpInfoHeader->biWidth;
 
 	return 0;
 }
 
 int CDib::get_height() const
 {
 	if(is_valid())
 		return m_pBmpInfoHeader->biHeight;
 
 	return 0;	
 }
 
 int CDib::get_palette_size() const
 {
 	if(is_valid())
 		return get_palette_size(m_pBmpInfoHeader->biBitCount);
 
 	return 0;
 }
 
 int CDib::get_palette_size( int _bits ) const
 {
 	switch(_bits)
 	{
 	case 1:
 		return 2;
 	case 4:
 		return 16;
 	case 8:
 		return 256;
 	default:
 		return 0;
 	}	
 }
 
 int CDib::get_dib_size() const
 {
 	if(is_valid())
 	{
 		long _linesize = m_pBmpInfoHeader->biBitCount / 8 * m_pBmpInfoHeader->biWidth;
 		
 		while(_linesize % 4)
 			_linesize++;
 
 		return _linesize * m_pBmpInfoHeader->biHeight;
 	}
 
 	return 0;
 }
 
 //just line size, line size should be times 4
 int CDib::get_dib_width() const
 {
 	if(is_valid())
 	{
 		long _linesize = m_pBmpInfoHeader->biBitCount / 8 * m_pBmpInfoHeader->biWidth;
 		
 		while(_linesize % 4)
 			_linesize++;
 
 		return _linesize;
 	}
 
 	return 0;
 }
 
 //////////////////////////////////////////////////////////////////////////
 //draw
 int CDib::bitblt( HDC hdc, int _xDest, int _yDest, int _width, int _height, \
 				  int _xSrc, int _ySrc ) const
 {
 	if(is_valid())
 	{
 		return ::SetDIBitsToDevice(hdc, _xDest, _yDest, _width, _height, \
 			_xSrc, m_pBmpInfoHeader->biHeight - _ySrc - _height, \
 			0, m_pBmpInfoHeader->biHeight, m_pData, m_pBmpInfo, DIB_RGB_COLORS);
 	}
 
 	return 0;
 }
 
 int CDib::bitblt( HDC hdc, int _xDest, int _yDest ) const
 {
 	if(is_valid())
 	{
 		return ::SetDIBitsToDevice(hdc, _xDest, _yDest, m_pBmpInfoHeader->biWidth, m_pBmpInfoHeader->biHeight, \
 			0, 0, \
 			0, m_pBmpInfoHeader->biHeight, m_pData, m_pBmpInfo, DIB_RGB_COLORS);
 	}
 	
 	return 0;
 }
 
 int CDib::stretch_blt( HDC hdc, int _dX, int _dY, int _dWidth, int _dHeight, \
 					   int _sX, int _sY, int _sWidth, int _sHeight ) const
 {
 	if(is_valid())
 	{
 		return ::StretchDIBits(hdc, _dX, _dY, _dWidth, _dHeight, \
 			_sX, _sY, _sWidth, _sHeight, m_pData, \
 			m_pBmpInfo, DIB_RGB_COLORS, SRCCOPY);
 	}
 
 	return 0;
 }
 
 //////////////////////////////////////////////////////////////////////////
 //color convert
 //计算常量
 #define BLACK_Y 0
 #define BLACK_U 128
 #define BLACK_V 128
 
 #define RGB2Y(b, g, r, y) \
 	y = (unsigned char)(((int)30 * (r)  + (int)59 * (g) + (int)11 * (b)) / 100)
 
 #define RGB2YUV(b, g, r, y, cb, cr) \
 	RGB2Y(b, g, r, y); \
 	cb = (unsigned char)(((int) - 17 * r  - (int)33 * g +(int)50 * b + 12800) / 100); \
 	cr = (unsigned)(((int)50 * r  - (int)42 *g  - (int)8 * b + 12800) / 100)
 
 #define LIMIT(x) (unsigned char) (((x > 0xffffff) ? 0xff0000 : ((x <= 0xffff) ? 0 : x & 0xff0000)) >> 16)
 
 
 BITMAPINFO* CDib::get_bmp_info() const
 {
 	return m_pBmpInfo;
 }
 
 bool CDib::RGB2YUV420(LPBYTE RgbBuf,UINT nWidth,UINT nHeight,LPBYTE yuvBuf,unsigned long *len)
 {
 	int i, j; 
 	unsigned char*bufY, *bufU, *bufV, *bufRGB,*bufYuv; 
 	memset(yuvBuf,0,(unsigned int )*len);
 	bufY = yuvBuf; 
 	bufV = yuvBuf + nWidth * nHeight; 
 	bufU = bufV + (nWidth * nHeight* 1/4); 
 	*len = 0; 
 	unsigned char y, u, v, r, g, b,testu,testv; 
 	unsigned int ylen = nWidth * nHeight;
 	unsigned int ulen = (nWidth * nHeight)/4;
 	unsigned int vlen = (nWidth * nHeight)/4; 
 	for (j = 0; j<nHeight;j++)
 	{
 		bufRGB = RgbBuf + nWidth * (nHeight - 1 - j) * 3 ; 
 		for (i = 0;i<nWidth;i++)
 		{
 			int pos = nWidth * i + j;
 			r = *(bufRGB++);
 			g = *(bufRGB++);
 			b = *(bufRGB++);
 			y = (unsigned char)( ( 66 * r + 129 * g + 25 * b + 128) >> 8) + 16 ;           
 			u = (unsigned char)( ( -38 * r - 74 * g + 112 * b + 128) >> 8) + 128 ;           
 			v = (unsigned char)( ( 112 * r - 94 * g - 18 * b + 128) >> 8) + 128 ;
 			*(bufY++) = max( 0, min(y, 255 )) + 10;
 			if(*(bufY++) > 255)
 			{
 				*(bufY++) = 255;
 			}
 
 			if (j%2==0&&i%2 ==0)
 			{
 				if (u>255)
 				{
 					u=255;
 				}
 				if (u<0)
 				{
 					u = 0;
 				}
 				*(bufU++) =u;
 				//存u分量
 			}
 			else
 			{
 				//存v分量
 				if (i%2==0)
 				{
 					if (v>255)
 					{
 						v = 255;
 					}
 					if (v<0)
 					{
 						v = 0;
 					}
 					*(bufV++) =v;
 
 				}
 			}
 		}
 	}
 	*len = nWidth * nHeight+(nWidth * nHeight)/2;
 	return true;
 }
 
 //RGB24 to bmp file
 void CDib::SaveRGB24AsBmp( BYTE * pData, int width, int height, const char * filename )
 
 {
 
 	int size = width*height*3; // 每个像素点3个字节
 
 	// 位图第一部分，文件信息
 
 	BITMAPFILEHEADER bfh;
 
 	bfh.bfType = 0x4d42; //bm
 
 	bfh.bfSize = size // data size
 
 		+ sizeof( BITMAPFILEHEADER ) // first section size
 
 		+ sizeof( BITMAPINFOHEADER ) // second section size
 
 		;
 
 	bfh.bfReserved1 = 0; // reserved
 
 	bfh.bfReserved2 = 0; // reserved
 
 	bfh.bfOffBits = bfh.bfSize - size;
 
 	// 位图第二部分，数据信息
 
 	BITMAPINFOHEADER bih;
 
 	bih.biSize = sizeof(BITMAPINFOHEADER);
 
 	bih.biWidth = width;
 
 	bih.biHeight = height;
 
 	bih.biPlanes = 1;
 
 	bih.biBitCount = 24;
 
 	bih.biCompression = 0;
 
 	bih.biSizeImage = size;
 
 	bih.biXPelsPerMeter = 0;
 
 	bih.biYPelsPerMeter = 0;
 
 	bih.biClrUsed = 0;
 
 	bih.biClrImportant = 0;
 
 
 
 	FILE * fp = NULL;
 	if (0 != fopen_s(&fp, filename, "wb") || !fp) 
 		return;
 
 	fwrite( &bfh, 1, sizeof(BITMAPFILEHEADER), fp );
 
 	fwrite( &bih, 1, sizeof(BITMAPINFOHEADER), fp );
 
 	fwrite( pData, 1, size, fp );
 
 	fclose( fp );
 
 }
 
 int CDib::SaveRGB32AsBMP (BYTE * pRGBBuffer, int width, int height, const char * filename, int bpp/* = 32*/)   
 {   
 	BITMAPFILEHEADER bmpheader;  
 	BITMAPINFOHEADER bmpinfo;  
 	FILE *fp = NULL;  
 
 	if (0 != fopen_s(&fp, filename, "wb") || fp == NULL)
 	{
 		return -1;  
 	}  
 
 	bmpheader.bfType = ('M' <<8)|'B';  
 	bmpheader.bfReserved1 = 0;  
 	bmpheader.bfReserved2 = 0;  
 	bmpheader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);  
 	bmpheader.bfSize = bmpheader.bfOffBits + width*height*bpp/8;  
 
 	bmpinfo.biSize = sizeof(BITMAPINFOHEADER);  
 	bmpinfo.biWidth = width;  
 	bmpinfo.biHeight = height;  
 	bmpinfo.biPlanes = 1;  
 	bmpinfo.biBitCount = bpp;  
 	bmpinfo.biCompression = BI_RGB;  
 	bmpinfo.biSizeImage = 0;  
 	bmpinfo.biXPelsPerMeter = 100;  
 	bmpinfo.biYPelsPerMeter = 100;  
 	bmpinfo.biClrUsed = 0;  
 	bmpinfo.biClrImportant = 0;  
 
 	fwrite(&bmpheader,sizeof(BITMAPFILEHEADER),1,fp);  
 	fwrite(&bmpinfo,sizeof(BITMAPINFOHEADER),1,fp);  
 	fwrite(pRGBBuffer,width*height*bpp/8,1,fp);  
 	fclose(fp);  
 	fp = NULL;  
 
 	return TRUE;  
 }   
 
 //亮度调节函数
 void CDib::process_bri_con(unsigned char *dest, int dstride, unsigned char *src, int sstride,
 	int w, int h, int brightness, int contrast)
 {
 	int i;
 	int pel;
 	int dstep = dstride-w;
 	int sstep = sstride-w;
 
 	contrast = ((contrast+100)*256*256)/100;
 	brightness = ((brightness+100)*511)/200-128 - contrast/512;
 
 	while (h--) {
 		for (i = w; i; i--)
 		{
 			pel = ((*src++* contrast)>>16) + brightness;
 			if(pel&768) pel = (-pel)>>31;
 			*dest++ = pel;
 		}
 		src += sstep;
 		dest += dstep;
 	}
 }
 
 /*色度和饱和度函数
 w、h为对应uv的宽度和高度，yv12为，width-图像宽度，height-图像高度，则w=width/2，h=height/2
 dststride和srcstride为同w
 */
 void CDib::process_hue_sat(unsigned char *udst, unsigned char *vdst, unsigned char *usrc, unsigned char *vsrc, 
 							int dststride, int srcstride,int w, int h, float hue, float sat)
 {
 	int i;
 	const int s= int(sin(hue) * (1<<16) * sat);
 	const int c= int(cos(hue) * (1<<16) * sat);
 
 	while (h--) {
 		for (i = 0; i<w; i++)
 		{
 			const int u= usrc[i] - 128;
 			const int v= vsrc[i] - 128;
 			int new_u= (c*u - s*v + (1<<15) + (128<<16))>>16;
 			int new_v= (s*u + c*v + (1<<15) + (128<<16))>>16;
 			if(new_u & 768) new_u= (-new_u)>>31;
 			if(new_v & 768) new_v= (-new_v)>>31;
 			udst[i]= new_u;
 			vdst[i]= new_v;
 		}
 		usrc += srcstride;
 		vsrc += srcstride;
 		udst += dststride;
 		vdst += dststride;
 	}
 }
 
 int CDib::get_bitmap_data(LPBYTE data, int size)
 {
 	if (NULL == data || NULL == m_pData || NULL == m_pBmpInfoHeader)
 	{
 		return -1;
 	}
 	int width = m_pBmpInfoHeader->biWidth;
 	int height = m_pBmpInfoHeader->biHeight;
 	int bpp = m_pBmpInfoHeader->biBitCount;
 	long bmp_size = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + width * height * bpp / 8;
 	if (bmp_size > size)
 	{
 		return -2;
 	}
 	BITMAPFILEHEADER bmpheader;  
 	BITMAPINFOHEADER bmpinfo;  
 
 	bmpheader.bfType = ('M' <<8)|'B';  
 	bmpheader.bfReserved1 = 0;  
 	bmpheader.bfReserved2 = 0;  
 	bmpheader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);  
 	bmpheader.bfSize = bmpheader.bfOffBits + width*height*bpp/8;  
 
 	bmpinfo.biSize = sizeof(BITMAPINFOHEADER);  
 	bmpinfo.biWidth = width;  
 	bmpinfo.biHeight = height;  
 	bmpinfo.biPlanes = 1;  
 	bmpinfo.biBitCount = bpp;  
 	bmpinfo.biCompression = BI_RGB;  
 	bmpinfo.biSizeImage = 0;  
 	bmpinfo.biXPelsPerMeter = 100;  
 	bmpinfo.biYPelsPerMeter = 100;  
 	bmpinfo.biClrUsed = 0;  
 	bmpinfo.biClrImportant = 0;  
 
 	memcpy(data, &bmpheader, sizeof(BITMAPFILEHEADER));
 	memcpy(data + sizeof(BITMAPFILEHEADER), &bmpinfo, sizeof(BITMAPINFOHEADER));
 	memcpy(data + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER), m_pData, m_pBmpInfoHeader->biWidth * m_pBmpInfoHeader->biHeight * m_pBmpInfoHeader->biBitCount / 8);
 	
 	return bmp_size;
 }