#pragma once
#include <map>
#include <list>
#include "live_event.h"
#include "global_config.h"
#include "ISocketInterface.h"
#include "LoadLibrary_Interface.h"

class CMediaRecordThread;
class CSessionMgr;
class BaseThreadMutex;

using namespace std;

class CRecordMgr
{
public:
    CRecordMgr(ILiveEvent* event_);
    virtual ~CRecordMgr(void);

    //添加服务器地址信息
    //@ ip_ : 服务器IP
    //@ type_ : 此IP运营商类型
    //@ my_type_ : 本机IP运营商类型
    int                     add_sev_ip_info(const unsigned long ip_, const int port_, 
        const short type_, const short my_type_);
    int                     del_sev_ip_info(const unsigned long ip_);

    void                    clear_sev_ip_info();

    int                     Login(const unsigned int uid_, const unsigned int room_id_, NETPROTO proto_ = _eHttp);
    int                     Login(const unsigned int uid_, const unsigned int room_id_, 
        const char* ip_, const short port_, NETPROTO proto_ = _eHttp);
    int                     Logout(const unsigned int room_id_);

    bool                    is_logined(const unsigned int room_id_);
    bool                    is_logining(const unsigned int room_id_);

    int                     preview_video(int video_show_num_, int dev_id_);
    int                     preview_audio(int dev_id_);
    //重置编解码器等
    void                    reinit();

    void                    set_event(ILiveEvent* event_);

private:
    typedef map<unsigned int, boost::shared_ptr<ISocketInterface> > SOCKETINTERFACEMAP;

    SOCKETINTERFACEMAP      _sock_interface_map;
    ILiveEvent*             _pEvent;
    CMediaRecordThread*     _pMediaRecThread;
    boost::recursive_mutex  _mutex;  
    boost::atomic_bool      _bstart_flag;       //记录采集线程，是否已经开启
    static list<boost::tuple<string, int> > _ip_addr_list;   //保存IP列表信息
};
