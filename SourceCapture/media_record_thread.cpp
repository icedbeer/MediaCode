#include "stdafx.h"
#include "media_record_thread.h"
#include "audio_record_channel.h"
#include "video_record_channel.h"
#include "dshow_video.h"
#include "global_config.h"
#include "base_guard.h"

//using namespace  MediaMessage;

CMediaRecordThread::CMediaRecordThread()
{
    _pAudioChannel = NULL;
    _pVideoChannel = NULL;
    try
    {
        //_pAudioChannel = new CAudioRecordChannel(this);
        _pVideoChannel = new CVideoRecordChannel(this);
    }
    catch (std::bad_alloc&)
    {        
    }
}

CMediaRecordThread::~CMediaRecordThread(void)
{
    if (_pAudioChannel)
    {
        delete _pAudioChannel;
    }
    if (_pVideoChannel)
    {
        delete _pVideoChannel;
    }
    _session_map.clear();
}

void CMediaRecordThread::add_session_mgr(ROOM_ID room_id_, boost::shared_ptr<ISocketInterface> session_mgr_)
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    _session_map[room_id_] = session_mgr_;
}

void CMediaRecordThread::del_session_mgr(ROOM_ID room_id_)
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    if (_session_map.find(room_id_) != _session_map.end())
    {
        _session_map.erase(_session_map.find(room_id_));
    }
}

int CMediaRecordThread::open()
{
    if (_pAudioChannel) 
    { 
        //_pAudioChannel->open(NULL, true);
    }
    if (_pVideoChannel)
    {
        _pVideoChannel->open(0, 0, 
            GLOBAL_CONFIG()->_codec_pic_width, GLOBAL_CONFIG()->_codec_pic_height, 
            GLOBAL_CONFIG()->_frame_rate, GLOBAL_CONFIG()->_video_bitrate);
    }
    return 0;
}

int CMediaRecordThread::close()
{
    if (_pAudioChannel)
    {
        _pAudioChannel->close();
    }
    if (_pVideoChannel)
    {
        _pVideoChannel->close_all();
    }
    return 0;
}

int CMediaRecordThread::send_media_frame(NetPacket* pkt_)
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    if (NULL == pkt_ || 0 == _session_map.size())
    {
        return -1;
    }
    BOOST_AUTO(itor, _session_map.begin());
    while (itor != _session_map.end())
    {
        if (itor->second && itor->second->is_login())
        {
            if (EPROTO_VIDEO == pkt_->proto_num())
            {
                pkt_->set_room_id(itor->first);
                itor->second->send_video(pkt_);
            } else if (EPROTO_AUDIO == pkt_->proto_num())
            {
                pkt_->set_room_id(itor->first);
                itor->second->send_audio(pkt_);
            }
        } else
        {
            itor++;
            continue;
        }
        itor++;
    }
    return 0;
}


void CMediaRecordThread::set_event(ILiveEvent* ev_)
{
    if(_pVideoChannel)
    {
        _pVideoChannel->set_video_display_event(ev_);
    }
}

//������ƵԤ��
int CMediaRecordThread::start_preview_video(int video_show_num, int dev_id_)
{
    if (!_pVideoChannel)
    {
        return -1;
    }
    if (-1 == dev_id_)
    {
        stop_preview_video(video_show_num);
        return 0;
    }
    if(GLOBAL_CONFIG()->_video_dev_count > dev_id_)
    {
        return _pVideoChannel->open(video_show_num, dev_id_, GLOBAL_CONFIG()->_codec_pic_width, GLOBAL_CONFIG()->_codec_pic_height,
            GLOBAL_CONFIG()->_frame_rate, GLOBAL_CONFIG()->_video_bitrate);
    }
    return -1;
}

//�ر���ƵԤ��
void CMediaRecordThread::stop_preview_video(int video_show_num)
{
    if (!_pVideoChannel)
    {
        return;
    }
    _pVideoChannel->close(video_show_num);
}

int CMediaRecordThread::open_audio_mic(int audio_dev_id_, bool bOpenAccompaniment_)
{
    if (_pAudioChannel)
    {
        return _pAudioChannel->open(audio_dev_id_, bOpenAccompaniment_);
    } else
    {
        return -1;
    }
}

int CMediaRecordThread::close_audio_mic()
{
    if (_pAudioChannel)
    {
        return _pAudioChannel->close();
    } else
    {
        return -1;
    }
}


