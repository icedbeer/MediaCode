/*!
 * \file HttpInterface.h
 *
 * \author Administrator
 * \date 一月 2017
 *
 * 使用HTTP协议传输音视频数据及相关的登陆协议
 */
#pragma once
#include "ISocketInterface.h"
#include "base_typedef.h"
#include "global_config.h"

class CSessionMgr;

class CHttpInterface :
    public ISocketInterface
{
public:
    CHttpInterface(ILiveEvent* event_, USER_ID uid_, ROOM_ID room_id_);
    ~CHttpInterface();

    virtual boost::int32_t      login(const char* ip_, const short port_, int player_type = 0);
    virtual boost::int32_t      logout();
    virtual bool                is_login();

    virtual boost::int32_t      send_video(NetPacket* pkt_);
    virtual boost::int32_t      send_audio(NetPacket* pkt_);

    //事件回调接口
    virtual void                set_event(ILiveEvent* event_);

    virtual void                set_video_cb(boost::function<void(boost::shared_ptr<NetPacket>)>);
    virtual void                set_audio_cb(boost::function<void(boost::shared_ptr<NetPacket>)>);

private:
    CSessionMgr*        _http_client;
    ILiveEvent*         _session_event;
};

