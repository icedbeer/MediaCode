/*!
 * \file dshow_video.h
 *
 * \author Administrator
 * Windows下通过DirectShow枚举视频采集设备，并实时采集，可创建多个
 * 通常情况下物理摄像头都是支持MEDIASUBTYPE_YUY2和MEDIASUBTYPE_RGB24的，
 * 但是MEDIASUBTYPE_YUY2的采集帧率比MEDIASUBTYPE_RGB24要高，本类中会优先使用MEDIASUBTYPE_YUY2进行初始化，
 * 如果输入设备不支持此格式（如虚拟摄像头），再尝试使用MEDIASUBTYPE_RGB24格式，一般这两种格式就足够了，
 * 如果需要采集各种物理、虚拟摄像头的话，那就直接使用MEDIASUBTYPE_RGB24吧
 */
#pragma once
#ifdef WIN32

#include <string>
#include <tchar.h>
#include <dshow.h>
#include <atlbase.h>
#include <vector>
#include <qedit.h>

#pragma warning(disable:4251)

using namespace std;

class CDShowVideoDevice : public ISampleGrabberCB
{
public:
    CDShowVideoDevice(void);
    ~CDShowVideoDevice(void);

    /*可用于全局获取视频采集设备*/
    static int              EnumVideoCaptureDevice(char device_list_[][256], const int &max_size_);

    /*初始化设备采集
    *@param width_   采集图像的宽度
    *@param height_  采集图像的高度
    *@param frame_rate_    采集帧率
    *@param device_name_  输入设备名称，如果为NULL，则使用默认设备
    *@return   true:success; false:failed.
    **/
    virtual bool            InitCapture(const unsigned& width_, const unsigned& height_, const unsigned& frame_rate_,
                                        const string &device_name_);

    /*初始化设备采集
     *@param width_   采集图像的宽度
     *@param height_  采集图像的高度
     *@param frame_rate_    采集帧率
     *@param dev_id_  输入设备序号，-1或0为默认
     *@return   true:success; false:failed.
     **/
    virtual bool            InitCapture(const unsigned& width_, const unsigned& height_, const unsigned& frame_rate_,
                                        const int &dev_id_);
    /*
     *停止采集，并释放相关资源
     **/
    virtual void            UninitCapture();

    /*开始采集*/
    virtual bool            StartCapture();
    /*
     *停止采集*/
    virtual bool            StopCapture();
    /*
    *外部读取数据接口
    *@param out_data_ 输出数据内存指针
    *@param max_size_ 输出数据内存大小
    *@param img_width_ 输出图像宽度
    *@param img_height_ 输出图像高度
    *@param media_sub_type_ 输出图像数据格式，RGB24或者YV12
    *@return >0 成功， <=0 失败
    **/
    virtual int             ReadVideoFrame(const unsigned char *out_data_, const int& max_size_,
                                        int &img_width_, int &img_height_, GUID& media_sub_type_);

    /*弹出摄像头设置窗口*/
    virtual bool            ShowVideoDialogBox(HWND hWndParent = NULL);
    /*判断是否有设置窗口*/
    virtual bool            HasSettingDialog(int nType);

    /*获取当前使用的输入设备*/
    virtual LPCSTR          GetCurrentCapVideoDeviceName();
    virtual string&         GetCurDevName() {return m_curDeviceName;}
    virtual int             GetCurDevID(){return m_curDeviceId;};

    /*
    *尝试获取采集设备事件，目前只抓取设备拔出事件
    *可在ReadVideoFrame调用失败后调用，判断是否已失去设备
    *@param dev_name_ 如果有设备被拔出，dev_name_被赋值为设备名
    *@retrun 0:没有拔出；1 : 被拔出
    **/
    virtual int              HandleEvent(__out string& dev_name_);

protected:
    /*初始化DShow图表*/
    virtual HRESULT         EnumDevice(vector<string > &devices_vec);
    /*枚举视频设备所支持的大小、帧率、和可采集格式，找到最符合自己的那一个*/
    virtual HRESULT         InitVideoFormat(int cx,int cy, GUID sub_type_);

    /*安全删除AM_MEDIA_TYPE */
    virtual void            DeleteMediaType(AM_MEDIA_TYPE *pmt);

    //通过friend name获取filter
    virtual IBaseFilter*    GetFilterByName(string filter_name_);

    virtual IPin*           GetInPin( IBaseFilter * pFilter, int nPin );
    virtual IPin*           GetOutPin( IBaseFilter * pFilter, int nPin );
    virtual HRESULT         GetPin( IBaseFilter * pFilter, PIN_DIRECTION dirrequired, int iNum, IPin **ppPin);
    
    /*继承自ISampleGrabberCB的数据采集回调接口*/
    virtual HRESULT STDMETHODCALLTYPE SampleCB( double SampleTime,IMediaSample *pSample);
    virtual HRESULT STDMETHODCALLTYPE BufferCB( double SampleTime,BYTE *pBuffer,long BufferLen);
    /*继承自ISampleGrabberCB的接口，可以实现为空，不采用*/
    virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void __RPC_FAR *__RPC_FAR *ppvObject)
    {
        return E_FAIL;
    }
    virtual ULONG STDMETHODCALLTYPE AddRef(void)
    {
        return 0;
    }
    virtual ULONG STDMETHODCALLTYPE Release(void)
    {
        return 0;
    }
    /*创建引擎，连接DirectShow各组件*/
    virtual bool            CreateEngine(int cx, int cy);
    /*销毁*/
    virtual bool            DestroyEngine();

protected:
    /*默认最多缓存多少个raw数据块*/
#define MAX_BUFFER_FRMAE    3

    IGraphBuilder           *m_pGB;
    ICaptureGraphBuilder2   *m_pCaptureGB2;
    IBaseFilter             *m_pCaptureFilter;
    IAMVfwCaptureDialogs    *m_pVfwDialogs;
    IMediaControl           *m_pMediaControl;
    IMediaEventEx           *m_pEvent;
    CComPtr<ISampleGrabber> m_pSampleGrabber;

    vector<unsigned char* > m_FrameList;                //缓冲视频采集数据
    CRITICAL_SECTION        m_Lock;                     //为缓冲区数据加锁，保证线程安全
    double                  m_dFrameInterval;
    double                  m_dbLastFrameTime;

    int                     m_nWidth;
    int                     m_nHeigth;
    int                     m_nFrameRate;
    bool                    m_bIsPlaying;
    int                     m_nNewFrameNum;             //新采集未被读取的图像帧数
    BITMAPINFOHEADER        m_bmpInfoHeader;
    int                     m_nCurrentSampleBufLen;
    string                  m_curDeviceName;
    int                     m_curDeviceId;
    vector<GUID >           m_SubType_Vec;              //视频采集Raw pic格式，按先后确定优先使用次序
    GUID                    m_cur_subtype;              //当前使用的视频采集格式
    static vector<string>   m_VideoDeviceList;          //视频采集设备列表

};
#endif //WIN32

