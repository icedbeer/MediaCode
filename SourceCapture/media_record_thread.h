#pragma once
#include <map>
#include "session_mgr.h"
#include "live_event.h"
#include "global_config.h"
#include "ISocketInterface.h"

class CAudioRecordChannel;
class CVideoRecordChannel;

using namespace std;

class CMediaRecordThread
{
public:
    CMediaRecordThread();
    virtual ~CMediaRecordThread(void);

    //初始化http
    virtual void    add_session_mgr(ROOM_ID room_id_, boost::shared_ptr<ISocketInterface> session_mgr_);
    virtual void    del_session_mgr(ROOM_ID room_id_);

    virtual int     send_media_frame(NetPacket* pkt_);

    //开启音视频默认设备的采集
    virtual int     open();
    virtual int     close();
    
    virtual void    set_event(ILiveEvent* ev_);

    //开启、关闭视频预览
    virtual int     start_preview_video(int video_show_num, int dev_id_);
    virtual void    stop_preview_video(int video_show_num);

    //开启、关闭输入音频采集，不同于视频，mic音频同时只能打开一路
    //@param bOpenAccompaniment_ : 是否打开伴奏设备，默认打开，建议打开，关闭可以设置伴奏的音量为0，即可
    virtual int     open_audio_mic(int audio_dev_id_, bool bOpenAccompaniment_);
    virtual int     close_audio_mic();
        
protected:
    typedef map<ROOM_ID, boost::shared_ptr<ISocketInterface> > SESSIOMMGR_MAP;

    SESSIOMMGR_MAP          _session_map;
    CAudioRecordChannel     *_pAudioChannel;
    CVideoRecordChannel     *_pVideoChannel;

    boost::recursive_mutex  _mutex;
};

