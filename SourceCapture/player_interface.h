#pragma once
#include "live_event.h"
#include "LoadLibrary_Interface.h"

class CPlayMgr;

class INTERFACE_API CPlayerInterface
    : public IPlayerInterface
{
public:
    CPlayerInterface(ILiveEvent* event_);
    ~CPlayerInterface(void);

    //添加服务器地址信息
    //@ ip_ : 服务器IP
    //@ type_ : 此IP运营商类型
    //@ my_type_ : 本机IP运营商类型
    int                     add_sev_ip_info(const unsigned long ip_, const int port_,
        const short type_, const short my_type_);
    int                     del_sev_ip_info(const unsigned long ip_);
    void                    clear_sev_ip_info();

    //IP地址内部自动轮换，需要先使用add_sev_ip_info添加服务器IP地址信息
    int                     Login(const unsigned int uid_, const unsigned int room_id_,
        NETPROTO proto_ = _eHttp);
    int                     Logout();

    bool                    is_playing();

    //设置播放音量0~100
    void                    set_volume(int volume_);
    //获取播放音量
    int                     get_volume();

    //拍照，目前只支持保存为位图
    void                    save_bmp(const char* file_name_, const int len_);
    //转换缩放图像
    int                     swscale_rgb24_img(unsigned char* src_data_, int src_width_, int src_height_, int src_format_,
        unsigned char* dst_data_, int dst_width_, int dst_height_, int dst_format_);
private:
    CPlayMgr*   _pPlayerMgr;
};
