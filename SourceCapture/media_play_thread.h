#pragma once
#include <stdint.h>
#include "live_event.h"
#include "global_config.h"

class CAudioPlayChannel;
class CVideoPlayChannel;
class CSessionMgr;

using namespace std;

class CMediaPlayThread
{
public:
    CMediaPlayThread(ILiveEvent* event_);
    virtual ~CMediaPlayThread(void);

    //init audio and video decoder and resource
    virtual int32_t     init(HWND hwnd_);

    virtual int32_t     set_video_hwnd(HWND hwnd_);

    //收到主播上下麦的消息，清空解码器缓冲
    virtual void        on_host_change();
    void                on_recv_video_packet(boost::shared_ptr<NetPacket> pkt_ptr_);
    void                on_recv_audio_packet(boost::shared_ptr<NetPacket> pkt_ptr_);

    //0:stop play;1:begin play; 2:playing.
    void                set_play_flag(int flag_);
    void                save_bmp(const char* file_name_, const int len_);
    virtual void        flush_codec();

protected:
    virtual void        execute();

protected:
    CAudioPlayChannel   *_pAudioChannel;
    CVideoPlayChannel   *_pVideoChannel;
    uint8_t             _media_buffer[128 * 1024];
    boost::atomic<int>  _bplay_flag;                //0:stop play;1:begin play; 2:playing.
    bool                _bexit_flag;
    boost::thread       _thread_thr;
};
