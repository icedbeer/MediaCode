#include "stdafx.h"
#include "session_mgr.h"
#include "base_code.h"
#include "live_event.h"
#include "play_mgr.h"
#ifdef _VS2008
#include "record_mgr.h"
#endif // _VS2008

struct SESSIONPROTOCAL
{
    uint8_t     protocal;
};

CSessionMgr::CSessionMgr(const ROOM_ID &room_id_, const USER_ID &id_, ILiveEvent* ev_)
:_pEvent(ev_)
,_bAnchor(false)
{
    _pHttpSession = NULL;
    _room_id = room_id_;
    _user_id = id_;
    _sev_url = "";
    _eState  = EHTTP_STATE_NONE;
#ifdef _GM_CLIENT
    _pGmEvent = NULL;
#endif // _GM_CLIENT
    //_pHttpSession = new CHttpClient(str_url_);
}

CSessionMgr::~CSessionMgr(void)
{
    BOOST_INFO << "CSessionMgr::~CSessionMgr:" << _sev_url << "; cur state:" << _eState;
    if (1)
    {
        //boost::lock_guard<boost::recursive_mutex > lock_(_mutex);
        if (_pHttpSession)
        {
            delete _pHttpSession;
            _pHttpSession = NULL;
        }
    }
    _pro_vec.clear();

    //如果网络通讯没有来得及通知上次登出成功，则在资源释放时主动告诉上层，保证此接口每次都被调用
    if (_pEvent.load() && (EHTTP_STATE_LOGOUTING == _eState || EHTTP_STATE_LOGINED == _eState))
    {
        //暂时不会走到这里，因为上层在退出时已经首先将_pEvent置为NULL了
        _pEvent.load()->on_logout_resualt_cb(2);
    }
}

//登入
int CSessionMgr::Login(const string &url_, bool bAnchor_/* = false*/)
{
    BOOST_INFO << "CSessionMgr::Login:" << url_ << "; cur state:" << _eState;
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    if (url_.empty())
    {
        return -1;
    }
    if (EHTTP_STATE_NONE != _eState && _pHttpSession)
    {
        delete _pHttpSession;
        _pHttpSession = NULL;
    }
    if (_pHttpSession)
    {
        if (_pHttpSession->get_url().compare(url_) == 0 && _eState == EHTTP_STATE_LOGINED)
        {
            return 1;
        } else
        {
            delete _pHttpSession;
            _pHttpSession = NULL;
        }
    }
    if (!_pHttpSession)
    {
        _pHttpSession = new (std::nothrow)CHttpClient(url_, this);
    }
    if (!_pHttpSession)
    {
        return -1;
    }

    _eState = EHTTP_STATE_LOGINING;
    _bAnchor.store(bAnchor_);
    _sev_url = url_;

    NetPacket pkt;
    pkt.set_proto_num(EPROTO_LOGIN);
    pkt.set_room_id(_room_id);
    pkt.set_user_id(_user_id);
    pkt.set_send_tm(get_cur_time());
    pkt.mutable_proto_pkt()->set_user_type(EUSER_PLAYER);
    if (_bAnchor)
    {
        pkt.mutable_proto_pkt()->set_user_type(EUSER_ANCHOR);
    }

    _pHttpSession->reset_tv();
    string str;
    pkt.SerializeToString(&str);
    return _pHttpSession->add_protocal_data((uint8_t *)str.data(), str.size());
}

//登出
int CSessionMgr::Logout()
{
    BOOST_INFO << "CSessionMgr::Logout:" << _sev_url << "; cur state:" << _eState;
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    if (EHTTP_STATE_LOGOUTING == _eState || EHTTP_STATE_NONE == _eState || NULL == _pHttpSession)
    {
        return -1;
    }
    if (EHTTP_STATE_LOGINING == _eState)
    {
        add_unprocessed_protocal(EPROTO_LOGOUT);
        return 1;
    }
    _eState = EHTTP_STATE_LOGOUTING;

    NetPacket pkt;
    pkt.set_proto_num(EPROTO_LOGOUT);
    pkt.set_room_id(_room_id);
    pkt.set_user_id(_user_id);
    pkt.set_send_tm(get_cur_time());
    pkt.mutable_proto_pkt()->set_user_type(EUSER_PLAYER);
    if (_bAnchor)
    {
        pkt.mutable_proto_pkt()->set_user_type(EUSER_ANCHOR);
    }

    _pHttpSession->reset_tv();
    string str;
    pkt.SerializeToString(&str);
    return _pHttpSession->PostEasyProtocalData(str);
}

int CSessionMgr::send_media_frame(NetPacket *pkt_)
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    if (NULL == _pHttpSession)
    {
        BOOST_ASSERT_MSG(0, "HTTPSesssion is NULL！");
        BOOST_DEBUG << "CSessionMgr::send_media_frame() http_session:" << (int )_pHttpSession 
            << "; current_state:" << _eState;
        delete pkt_;
        pkt_ = NULL;
        return -1;
    }
    if (_eState == EHTTP_STATE_LOGINED || EHTTP_STATE_LOGINING == _eState)
    {
        pkt_->set_room_id(_room_id);
        pkt_->set_user_id(_user_id);
        return _pHttpSession->add_media_frame(pkt_);
    }
    return -1;
}

//有时由于当前状态未更新，不能立即执行用户命令，将其缓存，带状态更新后再执行
void CSessionMgr::add_unprocessed_protocal(uint8_t pro_)
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    SESSIONPROTOCAL ses_pro;
    ses_pro.protocal = pro_;
    _pro_vec.push_back(ses_pro);
}

void CSessionMgr::update_state(const string &protocal_str_, const long &response_, bool flag_ /*= true*/)
{
    //BOOST_DEBUG << "CSessionMgr::update_state: data size:"  << protocal_str_.size()
    //    << "; response: " << response_;

    switch (response_)
    {
    case HTTP_PROTOCAL_CODE_ME:
        break;
    case HTTP_PROTOCAL_CODE_OTHER:
        break;
    case HTTP_MEDIA_CODE_ME:
            return;
        break;
    case HTTP_MEDIA_CODE_OTHER:
    case HTTP_HEATBEAT_CODE:        //接收到媒体数据
        //数据转移到on_recv_media_data处理
        {
            on_recv_media_data(protocal_str_);
            if (flag_)
            {
                check_unprocessed_protocal();
            }
            return;
        }
        break;
    case HTTP_HEATBEAT_CODE_NULL:   //暂时没有数据接收，保持心跳通信
        if (flag_)
        { 
            check_unprocessed_protocal();
            return;
        }
        break;
    case HTTP_HOST_ONMIC:
    case HTTP_HOST_OFFMIC:
        break;
    case HTTP_ERROR:
    case HTTP_UNKNOWN_COMMAND:
    case HTTP_UNKNOWN_PROTOCAL:
    default:
        return ;
    }

    NetPacket pkt;
    try
    {	
        if (!pkt.ParseFromArray((uint8_t *)protocal_str_.data() + 2, protocal_str_.size() - 2))
        {
            return;
        }
    }catch(google::protobuf::FatalException&)
    {
        return;
    }catch (...)
    {
        return;
    }


    switch (pkt.proto_num())
    {
    case EPROTO_LOGIN:
        {
            on_login_cb(pkt.user_id(), response_, pkt.proto_pkt().vul_list(0));
        }
    	break;
    case EPROTO_LOGOUT:
        on_logout_cb(pkt.user_id(), response_, pkt.proto_pkt().vul_list(0));
        break;
    default:
        BOOST_WARNING << "CSessionMgr::update_state: payload_type not exist:"  << pkt.proto_num()
            << "; response: " << response_;
        break;
    }
    if (flag_)
    {
        check_unprocessed_protocal();
    }
}

void CSessionMgr::on_recv_media_data(const string &media_data_)
{
#ifdef _NODES_DEBUG
    return;
#endif // _NODES_DEBUG
    if (0 == media_data_.size())
    {
        return;
    }
    uint8_t *pBuf = (uint8_t *)media_data_.data();
    int32_t src_size = media_data_.size();
    short data_len(0);
    NetPacket packet;
    while (src_size > 2 && pBuf)
    {
        memcpy(&data_len, pBuf, sizeof(short));
        data_len = CGlobalConfig::small_2_big_short(data_len);
        if (data_len > src_size - 2)
        {
            //包长度错误
            BOOST_ASSERT_MSG(data_len > src_size - 2, "包长度错误");
            break;
        }
        src_size -= 2;
        pBuf += 2;
        try
        {
            if (!packet.ParseFromArray(pBuf, data_len))
            {
                BOOST_WARNING << "protobuf parse data failed.";
                BOOST_ASSERT_MSG(0, "parse media packet failed.");
                break;
            }
        } catch (google::protobuf::FatalException& e)
        {
            BOOST_WARNING << "protobuf parse failed, exception msg:" << e.what();
            BOOST_ASSERT_MSG(0, "protobuf parse failed, throw a exception.");
            break;
        } catch (...)
        {
            break;
        }
        if (EPROTO_AUDIO == packet.proto_num())     //audio
        {
            if (!_audio_func_cb.empty())
            {
                _audio_func_cb(boost::shared_ptr<NetPacket>(new NetPacket(packet)));
            }
            pBuf += packet.ByteSize();
            src_size -= packet.ByteSize();

        } else if (EPROTO_VIDEO == packet.proto_num()) //video
        {
            if (!_video_func_cb.empty())
            {
                _video_func_cb(boost::shared_ptr<NetPacket>(new NetPacket(packet)));
            }

            pBuf += packet.ByteSize();
            src_size -= packet.ByteSize();
        } else
        {
            break;
        }
    }
}

//检测是否有未处理的命令
void CSessionMgr::check_unprocessed_protocal()
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    if (0 == _pro_vec.size())
    {
        if (EHTTP_STATE_LOGINED == _eState && !_bAnchor.load())
        {
            send_get_request();
        }
        return ;
    }
    uint8_t protocal_type = _pro_vec.front().protocal;
    switch(protocal_type)
    {
    case EPROTO_LOGOUT:
        if (EHTTP_STATE_NONE != _eState)
        {
            Logout();
        }
        break;
    default:
        break;
    }
    _pro_vec.erase(_pro_vec.begin());
}

void CSessionMgr::send_get_request()
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    if (NULL == _pHttpSession || EHTTP_STATE_NONE == _eState)
    {
        return;
    }
    NetPacket pkt;
    pkt.set_proto_num(EPROTO_GET);
    pkt.set_room_id(_room_id);
    pkt.set_user_id(_user_id);
    pkt.set_send_tm(get_cur_time());
    pkt.mutable_proto_pkt()->set_user_type(EUSER_PLAYER);
    if (_bAnchor)
    {
        pkt.mutable_proto_pkt()->set_user_type(EUSER_ANCHOR);
    }
    string str;
    pkt.SerializeToString(&str);
    _pHttpSession->add_protocal_data((uint8_t *)str.data(), str.size());
}

//HTTP状态回调
void CSessionMgr::on_login_cb(const USER_ID& uid_, const long& res_, uint8_t bflag_)
{
    BOOST_INFO << "CSessionMgr::on_login_cb() uid:" << uid_ <<"; res:" << res_;
    if (uid_ != _user_id)
    {
        BOOST_WARNING << "on_login_cb() recv uid:" << uid_ << "; resualt: " << res_ << " not mine.";
        return;
    }
    if (EHTTP_STATE_LOGINING == _eState && HTTP_PROTOCAL_CODE_ME == res_ && 0 == bflag_)
    {
        _eState = EHTTP_STATE_LOGINED;
        if (_pEvent.load())
        {
            _pEvent.load()->on_login_resualt_cb(0);
            _pEvent.load()->on_buffering_cb();
        }
    }else
    {
        _eState = EHTTP_STATE_LOGOUTED;
        if (_pEvent.load())
        {
            _pEvent.load()->on_login_resualt_cb(-1);
        }
    }
}

void CSessionMgr::on_logout_cb(const USER_ID& uid_, const long& res_, uint8_t bflag_)
{
    BOOST_INFO << "CSessionMgr::on_logout_cb() uid:" << uid_ <<"; res:" << res_;
    if (uid_ != _user_id)
    {
        BOOST_WARNING << "on_logout_cb() recv uid:" << uid_ << "; resualt: " << res_ << " not mine.";
        return;
    }
    if (EHTTP_STATE_LOGOUTING == _eState && HTTP_PROTOCAL_CODE_ME == res_ && 0 == bflag_)
    {
        _eState = EHTTP_STATE_LOGOUTED;
        if (_pEvent.load())
        {
            _pEvent.load()->on_logout_resualt_cb(0);
        }
    }else
    {
        _eState = EHTTP_STATE_LOGOUTED;
        if (_pEvent.load())
        {
            _pEvent.load()->on_logout_resualt_cb(0);
        }
    }
}

bool CSessionMgr::is_logined()
{
    if (EHTTP_STATE_LOGINED == _eState)
    {
        return true;
    }else
    {
        return false;
    }
}

bool CSessionMgr::is_logining()
{
    if (EHTTP_STATE_LOGINING == _eState)
    {
        return true;
    }else
    {
        return false;
    }
}

int CSessionMgr::relogin()
{
    if (EHTTP_STATE_LOGOUTING != _eState)
    {
        if (!_flush_codec_func.empty())
        {
            _flush_codec_func();
        }
        _eState = EHTTP_STATE_NONE;
    }
    //自发的重连
    _eState = EHTTP_STATE_LOGINING;

    NetPacket pkt;
    pkt.set_proto_num(EPROTO_LOGIN);
    pkt.set_room_id(_room_id);
    pkt.set_user_id(_user_id);
    pkt.set_send_tm(get_cur_time());
    pkt.mutable_proto_pkt()->set_user_type(EUSER_PLAYER);
    if (_bAnchor)
    {
        pkt.mutable_proto_pkt()->set_user_type(EUSER_ANCHOR);
    }

    _pHttpSession->reset_tv();
    string str;
    pkt.SerializeToString(&str);
    return _pHttpSession->add_protocal_data((uint8_t *)str.data(), str.size());
}

bool CSessionMgr::is_anchor()
{
    return _bAnchor.load();
}

void CSessionMgr::pre_release()
{
    if (_pHttpSession)
    {
        _pHttpSession->pre_release();
    }
}

void CSessionMgr::message_box(char* buf_)
{
}

void CSessionMgr::set_video_cb(boost::function<void(boost::shared_ptr<NetPacket>)> func_)
{
    _video_func_cb.swap(func_);
}

void CSessionMgr::set_audio_cb(boost::function<void(boost::shared_ptr<NetPacket>)> func_)
{
    _audio_func_cb.swap(func_);
}

void CSessionMgr::set_flush_codec_cb(boost::function<void()> func_)
{
    _flush_codec_func.swap(func_);
}
