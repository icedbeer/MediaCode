#include "inttypes.h"
#include "base_thread.h"
#include "base_singleton.h"
#include <rtmp/srs_protocol_rtmp.hpp>
#include <rtmp/srs_protocol_rtmp_stack.hpp>
#include <libs/srs_lib_simple_socket.hpp>
#include <core/srs_core_autofree.hpp>
#include <libs/srs_librtmp.hpp>
#include <process.h>

#pragma comment(lib, "librtmp.lib")

#define InitSockets()	{\
	WORD version;			\
	WSADATA wsaData;		\
	\
	version = MAKEWORD(1,1);	\
	WSAStartup(version, &wsaData);	}

#define	CleanupSockets()	WSACleanup()

class RtmpCaptureDataCallback;
class RtmpServer : public CThread
{
public:
	RtmpServer( );
	~RtmpServer();

	int StartRtmpServer( RtmpCaptureDataCallback* callback );
	void StopRtmpServer( );
	int fmle_publish(SrsRtmpServer* rtmp, SrsRequest* req);

	virtual int32_t terminate();

	virtual void execute ( );

private:
	RtmpCaptureDataCallback* callback_;
	short					m_port;
	SimpleSocketStream *	m_pSocket;

};

#define CREATE_RTMP_SERVER		CSingleton<RtmpServer>::instance
#define	RTMP_SERVER				CSingleton<RtmpServer>::instance
#define DESTROY_RTMP_SERVER		CSingleton<RtmpServer>::destroy
