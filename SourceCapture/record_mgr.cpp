#include "stdafx.h"
#include "record_mgr.h"
#include "media_record_thread.h"
#include "session_mgr.h"
#include "global_config.h"

list<boost::tuple<string, int> > CRecordMgr::_ip_addr_list;   //保存IP列表信息

CRecordMgr::CRecordMgr(ILiveEvent* ev_)
: _pEvent(ev_)
, _pMediaRecThread(0)
{
    GLOBAL_CONFIG()->enum_video_dev();

    _pMediaRecThread = new CMediaRecordThread;
    _pMediaRecThread->set_event(_pEvent);
}

CRecordMgr::~CRecordMgr(void)
{
    if (_pMediaRecThread)
    {
        delete _pMediaRecThread;
    }
    _sock_interface_map.clear();
    _ip_addr_list.clear();
}

int CRecordMgr::Login(const unsigned int uid_, const unsigned int room_id_, 
    const char* ip_, const short port_, NETPROTO proto_ /*= _eHttp*/)
{
    BOOST_INFO << "Login server uid :" << uid_ << ", room id:" << room_id_ 
        << ", ip:" << ip_ << ", port:" << port_;
    if (NULL == ip_ || port_ <= 0)
    {
        return -1;
    }
    BOOST_AUTO(itor, _sock_interface_map.find(room_id_));
    if (itor != _sock_interface_map.end())
    {
        _sock_interface_map.erase(_sock_interface_map.find(room_id_));
    }
    ISocketInterface* pTmp = NULL;
    if (_eHttp == proto_)
    {
        pTmp = GetHttpInterface(_pEvent, uid_, room_id_);
    }else if (_eUdp == proto_)
    {
        try
        {
            boost::asio::ip::udp::endpoint ep(boost::asio::ip::address::from_string(ip_), port_);
        } catch (boost::system::system_error& e)
        {
            BOOST_ERROR << "Login server throw exception:" << e.what();
            return -1;
        }
        pTmp = GetUdpSockInterface(_pEvent, uid_, room_id_);

    }else if (_eRtmp == proto_)
    {
        pTmp = NULL;
    }
    if (!pTmp)
    {
        return -1;
    }
    _sock_interface_map[room_id_] = boost::shared_ptr<ISocketInterface>(pTmp);
    if (_pMediaRecThread)
    {
        _pMediaRecThread->add_session_mgr(room_id_, _sock_interface_map[room_id_]);
    }

    return pTmp->login(ip_, port_, 1);
}

int CRecordMgr::Login(const unsigned int uid_, const unsigned int room_id_, 
    NETPROTO proto_ /*= _eHttp*/)
{
    BOOST_INFO << "Login server uid :" << uid_ << ", room id:" << room_id_;
    BOOST_AUTO(ip_addr, _ip_addr_list.front());
    _ip_addr_list.pop_front();
    _ip_addr_list.push_back(ip_addr);
    return Login(uid_, room_id_, ip_addr.get<0>().c_str(), ip_addr.get<1>(), proto_);
}

int CRecordMgr::Logout(const unsigned int room_id_)
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    BOOST_INFO << "CRecordInterface::Logout() : " << room_id_;
    BOOST_AUTO(itor, _sock_interface_map.find(room_id_));
    if (itor == _sock_interface_map.end())
    {
        return -1;
    }
    _pMediaRecThread->del_session_mgr(room_id_);

    _sock_interface_map.erase(itor);
    return 0;
}

bool CRecordMgr::is_logined(const unsigned int room_id_)
{
    BOOST_AUTO(itor, _sock_interface_map.find(room_id_));
    if (itor == _sock_interface_map.end())
    {
        return false;
    }
    if (itor->second)
    {
        return itor->second->is_login();
    }else 
    {
        return false;
    }
}

int CRecordMgr::preview_video(int video_show_num_, int dev_id_)
{
    BOOST_INFO << "CRecordMgr::preview_video() " << video_show_num_ << "; " <<dev_id_;
    return _pMediaRecThread->start_preview_video(video_show_num_, dev_id_);
}

int CRecordMgr::preview_audio(int dev_id_)
{
    BOOST_INFO << "CRecordMgr::preview_audio() dev id:" << dev_id_;
    return _pMediaRecThread->open_audio_mic(dev_id_, true);
}

int CRecordMgr::add_sev_ip_info(const unsigned long ip_, const int port_,
                              const short type_, const short my_type_)
{
    BOOST_INFO << "CRecordMgr::add_sev_ip_info ip:" << ip_ << "; port_:" << port_
        << ";type_:" << type_ << "; my_type_:" << my_type_;
    if (0 == ip_ || port_ <= 0)
    {
        return -1;
    }
    unsigned long ip_tmp = ntohl(ip_);
    struct in_addr in;
    memcpy(&in, &ip_tmp, sizeof(ip_));
    char *ip_str_ = inet_ntoa(in);
    if (NULL == ip_str_)
    {
        BOOST_WARNING << "inet_ntoa failed. from : " << ip_;
        return -1;
    }
    if (type_ == my_type_)
    {
        _ip_addr_list.push_front(boost::make_tuple(string(ip_str_), port_));
    } else
    {
        _ip_addr_list.push_back(boost::make_tuple(string(ip_str_), port_));
    }
    return 0;
}

int CRecordMgr::del_sev_ip_info(const unsigned long ip_)
{
    BOOST_INFO << "CRecordMgr::del_sev_ip_info() ip:" << ip_;
    int ret(-1);
    if (0 == ip_ || _ip_addr_list.empty())
    {
        return ret;
    }
    struct in_addr in;
    memcpy(&in, &ip_, sizeof(ip_));
    char *ip_str_ = inet_ntoa(in);
    if (NULL == ip_str_)
    {
        BOOST_WARNING << "inet_ntoa failed. from : " << ip_;
        return ret;
    }
    BOOST_AUTO(itor, _ip_addr_list.begin());
    while (itor != _ip_addr_list.end())
    {
        if (0 == (*itor).get<0>().compare(ip_str_))
        {
            _ip_addr_list.erase(itor++);
            ret = 0;
            continue;
        }
        ++itor;
    }
    return ret;
}

void CRecordMgr::clear_sev_ip_info()
{
}

void CRecordMgr::reinit()
{
    //_pMediaRecThread->stop();
    //_pMediaRecThread->start();
}

void CRecordMgr::set_event(ILiveEvent* event_)
{
    //自己保存
    _pEvent = event_;
    //用来画面预览回调；设备丢失事件通知
    if (_pMediaRecThread)
    {
        _pMediaRecThread->set_event(event_);
    }
    //设置给所有的网络接口
    std::pair<unsigned int, boost::shared_ptr<ISocketInterface> > itor;
    BOOST_FOREACH(itor, _sock_interface_map)
    {
        if (itor.second)
        {
            itor.second->set_event(event_);
        }
    }
}
