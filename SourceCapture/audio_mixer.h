#pragma once
#include "base_typedef.h"
#include <vector>

using namespace std;

typedef vector<int16_t*>	SrcAudioDataList;

class HeAudioMixer
{
public:
	HeAudioMixer(uint32_t audio_size);
	~HeAudioMixer();
	
	bool			mixer(const SrcAudioDataList& data_list, int32_t src_size, int16_t* dst, int32_t& dst_size);
	static uint16_t	get_average_signal_level(const int16_t* data, int32_t data_size);
	static void		audio_scale(int16_t* data, int32_t data_size, float scale);
protected:
	float			f_;
	int32_t*		values_;
	int32_t			audio_size_;		//一个固定单元的AUDIO长度
};

class AccompanyAverager
{
public:
	AccompanyAverager();
	~AccompanyAverager();

	void		averange_accompany(int16_t* data, int32_t data_size);
private:
	double		f_;  //均衡器参数
	uint32_t	count_;
	uint32_t	total_vol_;
};

