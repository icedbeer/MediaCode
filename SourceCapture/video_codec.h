#pragma once
#include "base_typedef.h"
#include "stdint.h"
extern "C"
{
#include "x264.h"
}

struct AVCodecContext;
struct AVFrame;
struct AVCodec;
struct SwsContext;

//////////////////////////////////////////////////////////////////////////
class IVideo_Codec
{
public:
	IVideo_Codec();
	virtual ~IVideo_Codec();

public:
	virtual int fini() = 0;


    virtual int encode(unsigned char* src_, int src_len_, int src_width_, int src_height_, int src_format_,
        unsigned char* dst_, int dst_len_, int *mark_, bool isRGB24Data_ = true) = 0;
	virtual int decode(unsigned char* src_, int src_len_, unsigned char* dst_, int dst_len_, 
        int *mark_, bool brotate_ = true) = 0;
	
protected:
	AVCodecContext *avctx_opts;
	AVFrame *picture;
	AVCodec *codec;
	SwsContext *_sws_context;

	int width, height;
	int bit_rate;
	int frame_rate;
	int gop_size;
	int qmin, qmax;
	int max_b_frames;
	int max_B_Frames;
};

// 由于ffmpeg编码264关键帧和码率控制的不好
// 视频编码直接使用x264，不使用ffmpeg封装的接口
class H264Encoder
{
public:
	H264Encoder();
	~H264Encoder();

	bool init(int iWidth, int iHeight , int nFrameRate, int nBitRate);
	void fini();
	
	/*
	* @param isRGB24Data 标识pOutBuf是否为RGB24格式的数据，如果为TRUE，则进行色彩空间的转换，否则不需要
	*/
	int encode(unsigned char *src_, int src_len_, int src_width_, int src_height_, int src_format_,
        unsigned char *dst_, int dst_len_, int *mark_, bool isRGB24Data_ = true);

    int  get_width() {return m_h264_codec_width;};
    int  get_height() {return m_h264_codec_height;};

protected:
	unsigned char*	m_pBuf;
    int             m_nFrameRate;
	unsigned int	m_h264_codec_width;		// Output Width
	unsigned int	m_h264_codec_height;	// Output Height

	unsigned int	m_nWidth;				// Input Width
	unsigned int	m_nHeight;				// Input Height

	SwsContext*		m_swsContext;

	//X264对象
	x264_picture_t	m_pic_out;
	x264_picture_t	m_en_picture;
	x264_t *		m_en_h;
	x264_param_t	m_en_param;
	unsigned int	m_en_nQP;
	int				m_nQIndex;

	//记录最后一个I帧出现的系统时间
	uint64_t        m_last_frame_time;
};


//////////////////////////////////////////////////////////////////////////
class CVideo_Decoder : public IVideo_Codec
{
public:
	CVideo_Decoder();
	virtual ~CVideo_Decoder();

public:
	virtual int init();
	virtual int fini();

    virtual int encode(unsigned char* src_, int src_len_, int src_width_, int src_height_, int src_format_,
        unsigned char* dst_, int dst_len_, int *mark_, bool brotate_ = true);
	virtual int decode(unsigned char* src_, int src_len_, unsigned char* dst_, int dst_len_, int *mark_, bool brotate_ = true);

	virtual int decode(unsigned char* src_, int src_len_, unsigned char* dst_, int dst_len_, int *mark_, 
        int &dst_width_, int &dst_height_, bool brotate_ = true);
    virtual int decode(unsigned char* src_, int src_len_, unsigned char* dst_, int dst_len_, int *mark_,
        int want_width_, int height_, int &dst_width_, int &dst_height_, bool brotate_ = true);

    virtual void flush_codec();
};
