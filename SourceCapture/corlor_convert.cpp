#include "StdAfx.h"
#include "corlor_convert.h"
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavutil/avutil.h"
#include "libswscale/swscale.h"
#include "libavutil/opt.h"
}

CCorlorConvert::CCorlorConvert(void) :
_sws_context(NULL),
_src_width(0),
_src_height(0),
_src_format(0),
_dst_width(0),
_dst_height(0),
_dst_format(0)
{
}

CCorlorConvert::~CCorlorConvert(void)
{
    uninit();
}

int CCorlorConvert::img_convert(unsigned char *src_data_, const int &src_width_, const int &src_height_, const int &src_format_,
    unsigned char *dst_data_, const int &dst_width_, const int &dst_height_, const int &dst_format_, bool brotate_/* = true*/)
{
    if (!src_data_ || !dst_data_)
    {
        return -1;
    }
    if (_src_width != src_width_ || _src_height != src_height_ || _src_format != src_format_
        || _dst_width != dst_width_ || _dst_height != dst_height_ || _dst_format != dst_format_)
    {
        uninit();
        init(src_width_, src_height_, src_format_, dst_width_, dst_height_, dst_format_);
    }
    if (src_width_ == dst_width_ && src_height_ == dst_height_ && src_format_ == dst_format_)
    {
        memcpy(dst_data_, src_data_, avpicture_get_size((AVPixelFormat)src_format_, src_width_, _src_height));
        return avpicture_get_size((AVPixelFormat)src_format_, src_width_, _src_height);
    }
    if (NULL == _sws_context)
    {
        return -1;
    }

    AVPicture srcPic;
    AVFrame dstFrame;
    avpicture_fill(&srcPic, src_data_, (AVPixelFormat)_src_format, _src_width, _src_height);
    avpicture_fill((AVPicture *)&dstFrame, dst_data_, (AVPixelFormat)_dst_format, _dst_width, _dst_height);

    //�ߵ�ͼ��
    if (brotate_)
    {
        srcPic.data[0] += srcPic.linesize[0] * (_src_height - 1);
        srcPic.linesize[0] *= -1;
        srcPic.data[1] += srcPic.linesize[1] * (_src_height / 2 - 1);
        srcPic.linesize[1] *= -1;
        srcPic.data[2] += srcPic.linesize[2] * (_src_height / 2 - 1);
        srcPic.linesize[2] *= -1;
    }

    return sws_scale(this->_sws_context, srcPic.data, srcPic.linesize, 0, _src_height, dstFrame.data, dstFrame.linesize);
}


bool CCorlorConvert::is_init()
{
    return (NULL != _sws_context);
}

int CCorlorConvert::init(const int &src_width_, const int &src_height_, const int &src_format_,
    const int &dst_width_, const int &dst_height_, const int &dst_format_)
{
    if (NULL != _sws_context)
    {
        uninit();
    }
    _src_width = src_width_;
    _src_height = src_height_;
    _src_format = src_format_;
    _dst_width = dst_width_;
    _dst_height = dst_height_;
    _dst_format = dst_format_;

    if (-1 == _src_format)
    {
        return -1;
    }
    
    this->_sws_context = sws_getContext(_src_width, _src_height, (AVPixelFormat)_src_format,
        dst_width_, dst_height_, (AVPixelFormat)_dst_format, SWS_BILINEAR, 0, 0, 0);

    return this->_sws_context ? 0 : -1;
}

void CCorlorConvert::uninit()
{
    if (this->_sws_context)
    {
        sws_freeContext(this->_sws_context);
        this->_sws_context = 0;
    }
}

int	CCorlorConvert::GUID_2_ffmpeg_pixformat(const GUID &format_)
{
    AVPixelFormat pix_format = AV_PIX_FMT_NONE;
    if (MEDIASUBTYPE_RGB24 == format_)
    {
        pix_format = AV_PIX_FMT_BGR24;
    }else if (MEDIASUBTYPE_YV12 == format_)
    {
        pix_format = AV_PIX_FMT_YUV420P;
    }else if (MEDIASUBTYPE_YUY2 == format_)
    {
        pix_format = AV_PIX_FMT_YUYV422;
    }
    return (int)pix_format;
}
