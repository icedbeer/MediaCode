#pragma once
#include <string>
#include <list>
#include <string>
#include "curl/curl.h"
#include "curl/multi.h"
#include "base_typedef.h"
#include "global_config.h"

class CSessionMgr;
class CEasyBuffer;

class CHttpClient
{
public:
	CHttpClient(const string & strUrl, CSessionMgr *session_mgr_);
	virtual ~CHttpClient(void);

public:
    /**
    * @brief 将需要发送的音视频数据添加到缓冲区中，达到一定数量后自动进行发送
    */
    virtual int         add_media_frame(NetPacket *pkt_);
    /**
    * @brief 将需要发送的协议包添加到缓冲区中,异步发送
    * @param bwhen_exit_ 是否在退出时才执行；比如logout命令是在退出时才会发出并执行，为保证可靠性，
    *                    放到单独的阻塞的接口中去执行
    */
    virtual int         add_protocal_data(const uint8_t *data_, const uint32_t &data_len_,
        bool bwhen_exit_ = false);

    virtual string&     get_protocal_str(){return _protocal_str;}
    
    virtual int32_t     terminate();
    virtual void        execute();

    //停止直播前清除所有媒体包
    virtual void        clear_media_frame();
    
    //重置http发送标识为false，即未发送数据状态
    void                reset_http_send_flag(){_bsending_flag = false;};
    
    CEasyBuffer*        get_send_easybuffer(){return _pEasyBufferSend;};
    CEasyBuffer*        get_recv_easybuffer(){ return _pEasyBufferRecv; };

    void                update_last_send_time();

    virtual void        pre_release();

    short               small_2_big_short(short num_);
    
    //使用已有的参数，重新初始化，用来重新登陆
    void                reinit();
    void                reset_tv(){ _last_send_tv = GetTickCount(); }

    const string&       get_url(){ return _url_str; }

    /**
    * @brief Post私有协议包,退出时，阻塞执行完所有未处理的协议包
    * @return 返回是否Post成功, 0:success
    */
    virtual int         PostEasyProtocalData(const string &protocal_data_);

protected:
    //curl_init("127.0.0.1:8080/upload");
    virtual int         curl_init(const string & strUrl);
    virtual void        curl_uninit();

    //检查是否有协议包需要发送
    virtual bool        send_protocal();
    //检查是否有媒体包需要发送
    virtual bool        send_media_frames();

    //发送put request上传数据
    //@ret success: >=0; failed: < 0
    virtual int32_t     put_http_data();

    /**
    * @brief Post私有协议包,长链接
    * @return 返回是否Post成功, 0:success
    */
    virtual int         PostProtocalData(const string &protocal_data_);

    //发送媒体数据前检测是否有关键帧，如果有，将关键帧和前面的数据分开来发送
    virtual int32_t     get_media_send_size();

protected:
    CURL                            *_curl;
    CURLM                           *_multi_curl;
    struct curl_slist               *_curl_headers;
    string                          _url_str;               //http server url
    string                          _protocal_str;          //保存正常发送的协议数据块内容
    list<string >                   _protocal_str_list;     //缓存私有协议数据块列表
    list<string >                   _exit_protocal_str_list;//退出时执行的协议，必达
    list<NetPacket *>               _media_pkt_list;          //需要发送的音视频包列表
    boost::recursive_mutex          _mutex;
    boost::atomic<CSessionMgr* >    _session_mgr;
    CEasyBuffer                     *_pEasyBufferRecv;
    CEasyBuffer                     *_pEasyBufferSend;
    bool                            _bfirst_flag;           //开始时必须保证第一次发出去的为视频包
    bool                            _bsending_flag;
    bool                            _bfirst_msg;             //只有在第一次提示主播上行带宽不足
    uint32_t                        _last_send_tv;           //最近一次发送数据的时间（目前只针对玩家）
    bool                            _bNeedReConnect;         //是否自动重连
    boost::thread                   _thread_thr;
    bool                            _bexit_flag;
    unsigned long long              _pkt_seq_id;             //包序号，在最终发送时赋值，防止多线程乱序
};

