#include "stdafx.h"
#include "DesktopAudioDevice.h"
#include <xmmintrin.h>
#include "base_guard.h"

const float dbMinus3    = 0.7071067811865476f;
const float dbMinus6    = 0.5f;
const float dbMinus9    = 0.3535533905932738f;

const float surroundMix = dbMinus3;
const float centerMix   = dbMinus6;
const float lowFreqMix  = dbMinus3;

const float surroundMix4 = dbMinus6;

const float attn5dot1 = 1.0f / (1.0f + centerMix + surroundMix);
const float attn4dotX = 1.0f / (1.0f + surroundMix4);

#define ISSUPPORT(bflag_, ret) \
    if (!bflag_) \
    { \
        return ret; \
    }

CDesktopAudioDevice::CDesktopAudioDevice(void)
{
	mmEnumerator_ = NULL;
	mmDevice_ = NULL;
	mmClient_ = NULL;
	mmCapture_ = NULL;
	m_pResampler = NULL;
	m_pDataOutputBuffer = NULL;
	m_pTmpBuffer = NULL;
	m_pBlankBuffer = NULL;
	m_pTempResampleBuffer = NULL;
	m_InputChannelMask = 0;
	m_InputBufferSize = 0;
    
    IsPlatFormSupport();
    ISSUPPORT(m_bPlatformSupport, );

	CoInitialize(0);
    _pEasyBuff = new CEasyBuffer(128 * 1024);
}


CDesktopAudioDevice::~CDesktopAudioDevice(void)
{
    ISSUPPORT(m_bPlatformSupport, );

	StopCapture();
	while (m_DeviceList.size())
	{
		AudioDeviceInfo *pInfo = *m_DeviceList.begin();
		m_DeviceList.pop_front();
		if (pInfo)
		{
			delete pInfo;
		}
	}
    if (_pEasyBuff)
    {
        delete _pEasyBuff;
    }
	CoUninitialize();
}

int CDesktopAudioDevice::GetAudioDevices(list<AudioDeviceInfo *> &deviceList, AudioDeviceType deviceType, bool bConnectedOnly)
{
    ISSUPPORT(m_bPlatformSupport, -1);

	const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
	const IID IID_IMMDeviceEnumerator    = __uuidof(IMMDeviceEnumerator);
	IMMDeviceEnumerator *mmEnumerator;
	HRESULT err;

	err = CoCreateInstance(CLSID_MMDeviceEnumerator, NULL, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void**)&mmEnumerator);
	if(FAILED(err))
	{
		return -1;
	}

	IMMDeviceCollection *collection;

	EDataFlow audioDeviceType;
	switch(deviceType)
	{
	case ADT_RECORDING:
		audioDeviceType = eCapture;
		break;
	case ADT_PLAYBACK:
		audioDeviceType = eRender;
		break;
	default:
		audioDeviceType = eAll;
		break;
	}

	DWORD flags = DEVICE_STATE_ACTIVE;
	if (!bConnectedOnly)
		flags |= DEVICE_STATE_UNPLUGGED;

	err = mmEnumerator->EnumAudioEndpoints(audioDeviceType, flags, &collection);
	if(FAILED(err))
	{
		SafeRelease(mmEnumerator);
		return -1;
	}

	uint32_t count;
	if(SUCCEEDED(collection->GetCount(&count)))
	{
		for(uint32_t i=0; i<count; i++)
		{
			IMMDevice *device;
			if(SUCCEEDED(collection->Item(i, &device)))
			{
				CWSTR wstrID;
				if(SUCCEEDED(device->GetId((LPWSTR*)&wstrID)))
				{
					IPropertyStore *store;
					if(SUCCEEDED(device->OpenPropertyStore(STGM_READ, &store)))
					{
						PROPVARIANT varName;

						PropVariantInit(&varName);
						if(SUCCEEDED(store->GetValue(PKEY_Device_FriendlyName, &varName)))
						{
							CWSTR wstrName = varName.pwszVal;

                            AudioDeviceInfo *info = new(std::nothrow) AudioDeviceInfo;
                            if (NULL == info)
                            {
                                continue;
                            }
							char *pGuid = WCharToMByte(CP_ACP, wstrID);
							info->strID = pGuid;
							delete []pGuid;

							char *pName = WCharToMByte(CP_ACP, wstrName);
							info->strName = pName;
							delete []pName;

							deviceList.push_back(info);
						}
					}
					CoTaskMemFree((LPVOID)wstrID);
				}

				SafeRelease(device);
			}
		}
	}

	SafeRelease(collection);
	SafeRelease(mmEnumerator);
	return deviceList.size();
}

bool CDesktopAudioDevice::GetDefaultDevice(string &strVal, AudioDeviceType deviceType)
{
    ISSUPPORT(m_bPlatformSupport, false);

	const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
	const IID IID_IMMDeviceEnumerator    = __uuidof(IMMDeviceEnumerator);
	IMMDeviceEnumerator *mmEnumerator;
	HRESULT err;

	err = CoCreateInstance(CLSID_MMDeviceEnumerator, NULL, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void**)&mmEnumerator);
	if(FAILED(err))
		return false;
	
	EDataFlow df;
	if (ADT_PLAYBACK == deviceType)
	{
		df = eRender;
	}else if(ADT_RECORDING == deviceType)
	{
		df = eCapture;
	}else
	{
		df = eAll;
	}
	
	IMMDevice *defDevice;
	if(FAILED(mmEnumerator->GetDefaultAudioEndpoint(df, eCommunications, &defDevice)))
	{
		SafeRelease(mmEnumerator);
		return false;
	}

	CWSTR wstrDefaultID;
	if(FAILED(defDevice->GetId((LPWSTR*)&wstrDefaultID)))
	{
		SafeRelease(defDevice);
		SafeRelease(mmEnumerator);
		return false;
	}
	char *pGuid = WCharToMByte(CP_ACP, wstrDefaultID);
	strVal = pGuid;
	delete []pGuid;

	CoTaskMemFree((LPVOID)wstrDefaultID);
	SafeRelease(defDevice);
	SafeRelease(mmEnumerator);

	return true;
}

bool CDesktopAudioDevice::GetDefaultMicID(string &strVal) 
{
    ISSUPPORT(m_bPlatformSupport, false);
	return GetDefaultDevice(strVal, ADT_RECORDING);
}

bool CDesktopAudioDevice::GetDefaultSpeakerID(string &strVal) 
{
    ISSUPPORT(m_bPlatformSupport, false);
	return GetDefaultDevice(strVal, ADT_PLAYBACK);
}

wchar_t* CDesktopAudioDevice::MByteToWChar(uint32_t CodePage,LPCSTR lpcszSrcStr)
{
	LPWSTR lpcwsStrDes=NULL;
	int   len=MultiByteToWideChar(CodePage,0,lpcszSrcStr,-1,NULL,0);
    lpcwsStrDes=new(std::nothrow) wchar_t[len+1];
	if(!lpcwsStrDes)
		return NULL;
	memset(lpcwsStrDes,0,sizeof(wchar_t)*(len+1));
	len=MultiByteToWideChar(CodePage,0,lpcszSrcStr,-1,lpcwsStrDes,len);
	if(len)
		return lpcwsStrDes;
	else
	{   
		delete[] lpcwsStrDes;
		return NULL;
	}
}

char* CDesktopAudioDevice::WCharToMByte(uint32_t CodePage,LPCWSTR lpcwszSrcStr)
{
	char* lpszDesStr=NULL;
	int len=WideCharToMultiByte(CodePage,0,lpcwszSrcStr,-1,NULL,0,NULL,NULL);
	lpszDesStr=new(std::nothrow) char[len+1];
	memset(lpszDesStr,0,sizeof(char)*(len+1));
	if(!lpszDesStr)
		return NULL;
	len=WideCharToMultiByte(CodePage,0,lpcwszSrcStr,-1,lpszDesStr,len,NULL,NULL);
	if(len)
		return lpszDesStr;
	else
	{   
		delete[] lpszDesStr;
		return NULL;
	}
} 

bool CDesktopAudioDevice::Init(bool isPlayBack, const string devGUID/* = "Default"*/)
{
    ISSUPPORT(m_bPlatformSupport, false);

	const IID IID_IMMDeviceEnumerator    = __uuidof(IMMDeviceEnumerator);
	const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
	const IID IID_IAudioClient           = __uuidof(IAudioClient);
	const IID IID_IAudioCaptureClient    = __uuidof(IAudioCaptureClient);

	HRESULT err = CoCreateInstance(CLSID_MMDeviceEnumerator, NULL, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void**)&mmEnumerator_);
	if(FAILED(err))
	{
		return false;
	}
	m_CurDeviceID = devGUID;

	if (devGUID.compare("Default") == 0)
	{
		err = mmEnumerator_->GetDefaultAudioEndpoint(isPlayBack ? eRender : eCapture, isPlayBack ? eConsole : eCommunications, &mmDevice_);
	} else
	{
		wchar_t *pGuid = MByteToWChar(CP_ACP, devGUID.c_str());
		err = mmEnumerator_->GetDevice(pGuid, &mmDevice_);
		delete []pGuid;
	}

	if(FAILED(err))
	{
		return false;
	}

	err = mmDevice_->Activate(IID_IAudioClient, CLSCTX_ALL, NULL, (void**)&mmClient_);
	if(FAILED(err))
	{
		return false;
	}

	IPropertyStore *store;
	if(SUCCEEDED(mmDevice_->OpenPropertyStore(STGM_READ, &store)))
	{
		PROPVARIANT varName;

		PropVariantInit(&varName);
		if(SUCCEEDED(store->GetValue(PKEY_Device_FriendlyName, &varName)))
		{
			char *pName = WCharToMByte(CP_ACP, varName.pwszVal);
			m_CurDeviceName = pName;
			delete []pName;
		}

		store->Release();
	}
	
	WAVEFORMATEX *pwfx;
	uint32_t  inputBitsPerSample;
	uint32_t  inputBlockSize;
	WAVEFORMATEXTENSIBLE *wfext = NULL;

	err = mmClient_->GetMixFormat(&pwfx);
	if(FAILED(err))
	{
		return false;
	}
	inputBitsPerSample = pwfx->wBitsPerSample;

	if(pwfx->wFormatTag == WAVE_FORMAT_EXTENSIBLE)
	{
		wfext = (WAVEFORMATEXTENSIBLE*)pwfx;
		m_InputChannelMask = wfext->dwChannelMask;

		if(wfext->SubFormat != KSDATAFORMAT_SUBTYPE_IEEE_FLOAT)
		{
			CoTaskMemFree(pwfx);
			return false;
		}
	} else if(pwfx->wFormatTag != WAVE_FORMAT_IEEE_FLOAT)
	{
		CoTaskMemFree(pwfx);
		return false;
	}

	m_bFloat                = true;
	m_InputChannels         = pwfx->nChannels;
	inputBitsPerSample    = 32;
	inputBlockSize        = pwfx->nBlockAlign;
	m_InputSamplesPerSec    = pwfx->nSamplesPerSec;
	m_SampleWindowSize      = (m_InputSamplesPerSec/100);   //每次采集10ms

	DWORD flags = isPlayBack ? AUDCLNT_STREAMFLAGS_LOOPBACK : 0;

	err = mmClient_->Initialize(AUDCLNT_SHAREMODE_SHARED, flags, 5000*1000*10, 0, pwfx, NULL);

	if(FAILED(err))
	{
		CoTaskMemFree(pwfx);
		return false;
	}
	
	err = mmClient_->GetService(IID_IAudioCaptureClient, (void**)&mmCapture_);
	if(FAILED(err))
	{
		CoTaskMemFree(pwfx);
		return false;
	}
	
	CoTaskMemFree(pwfx);

	if (m_pResampler)
	{
		src_delete(m_pResampler->resampler);
		delete m_pResampler;
		m_pResampler = NULL;
	}

	m_ResampleRatio = double(DEFAULT_SAMPLE_RATE) / double(m_InputSamplesPerSec);

	if (1.0f == m_ResampleRatio)
	{
		return true;
	}	

	m_pResampler = new(std::nothrow) NotAResampler;
    if (NULL == m_pResampler)
    {
        return false;
    }
	m_pResampler->jumpRange = 70;
	int errVal;

	int converterType = SRC_SINC_FASTEST;
	m_pResampler->resampler = src_new(converterType, 2, &errVal);
	if(!m_pResampler->resampler)
		return false;

	if (m_pBlankBuffer)
	{
		free(m_pBlankBuffer);
		m_pBlankBuffer = NULL;
	}	
	m_pBlankBuffer = (uint8_t *)malloc(m_InputSamplesPerSec/100*2 * sizeof(float) * 2);
    if (NULL == m_pBlankBuffer)
    {
        return false;
    }
    

	uint32_t frameAdjust = uint32_t((double(m_InputSamplesPerSec/100) * m_ResampleRatio)/* + 1.0*/);
	uint32_t newFrameSize = frameAdjust*2;

	if (m_pTempResampleBuffer)
	{
		free(m_pTempResampleBuffer);
		m_pTempResampleBuffer = NULL;
	}	
	m_pTempResampleBuffer = (uint8_t *)malloc(newFrameSize * sizeof(float) * 2);
    if (NULL == m_pTempResampleBuffer)
    {
        return false;
    }

	return true;
}

void CDesktopAudioDevice::StartCapture()
{
    ISSUPPORT(m_bPlatformSupport, );

	//Init(true);
	if(mmClient_) {
		mmClient_->Start();
	}
    start();
}

void CDesktopAudioDevice::StopCapture()
{
    ISSUPPORT(m_bPlatformSupport, );

    terminate();
	if(mmClient_)
		mmClient_->Stop();
	FreeData();
	SafeRelease(mmEnumerator_);
}

int CDesktopAudioDevice::GetAudioBuffer(unsigned char* out_buf_, int size_)
{
    ISSUPPORT(m_bPlatformSupport, -1);
    BASE_GUARD_RETURN(BaseThreadMutex, cf_mon, m_mutext, -1);
    if (!_pEasyBuff || _pEasyBuff->GetUsedSize() < size_)
    {
        return -1;
    }
    _pEasyBuff->PopFront(out_buf_, size_);
    return size_;
}

int CDesktopAudioDevice::QueryAudioBuffer(unsigned char* out_buf_, const int max_size_)
{
    ISSUPPORT(m_bPlatformSupport, -1);

    //string _buffer_str;
    uint32_t numAudioFrames(0);
    int32_t buff_size(0);
    if(GetNextBuffer(numAudioFrames, buff_size))
    {
        if (max_size_ < numAudioFrames * 2 * sizeof(short))
        {
            return -1;
        }
        float *captureBuffer;

        if(!m_bFloat)
        {
            uint32_t totalSamples = numAudioFrames * m_InputChannels;

            if(m_InputBitsPerSample == 8)
            {
                float *tempConvert = (float *)m_NextBuffer;
                char *tempSByte = (char*)m_NextBuffer;

                while(totalSamples--)
                {
                    *(tempConvert++) = float(*(tempSByte++))/127.0f;
                }
            }
            else if(m_InputBitsPerSample == 16)
            {
                float *tempConvert = (float *)m_NextBuffer;
                short *tempShort = (short*)m_NextBuffer;

                while(totalSamples--)
                {
                    *(tempConvert++) = float(*(tempShort++))/32767.0f;
                }
            }
            else if(m_InputBitsPerSample == 24)
            {
                float *tempConvert = (float *)m_NextBuffer;
                BYTE *tempTriple = (BYTE*)m_NextBuffer;
                TripleToLong valOut;

                while(totalSamples--)
                {
                    TripleToLong &valIn  = (TripleToLong&)tempTriple;

                    valOut.wVal = valIn.wVal;
                    valOut.tripleVal = valIn.tripleVal;
                    if(valOut.tripleVal > 0x7F)
                        valOut.lastByte = 0xFF;

                    *(tempConvert++) = float(double(valOut.val)/8388607.0);
                    tempTriple += 3;
                }
            }
            else if(m_InputBitsPerSample == 32)
            {
                float *tempConvert = (float *)m_NextBuffer;
                long *tempShort = (long*)m_NextBuffer;

                while(totalSamples--)
                {
                    *(tempConvert++) = float(double(*(tempShort++))/2147483647.0);
                }
            }

            captureBuffer = (float *)m_NextBuffer;
        }
        else
            captureBuffer = (float*)m_NextBuffer;
		
		if (NULL == m_pDataOutputBuffer)
		{
            if (NULL == (m_pDataOutputBuffer = (uint8_t *)malloc(numAudioFrames * 2 * sizeof(float) * 2)))
            {
                return -1;
            }
		}
		//memset(m_pDataOutputBuffer, 0, numAudioFrames * 2 * sizeof(float));

        if(m_InputChannels == 1)
        {
            uint32_t  numFloats   = numAudioFrames;
            float *inputTemp  = (float*)captureBuffer;
            float *outputTemp = (float *)m_pDataOutputBuffer;

            if((UPARAM(inputTemp) & 0xF) == 0 && (UPARAM(outputTemp) & 0xF) == 0)
            {
                uint32_t alignedFloats = numFloats & 0xFFFFFFFC;
                for(uint32_t i=0; i<alignedFloats; i += 4)
                {
                    __m128 inVal   = _mm_load_ps(inputTemp+i);

                    __m128 outVal1 = _mm_unpacklo_ps(inVal, inVal);
                    __m128 outVal2 = _mm_unpackhi_ps(inVal, inVal);

                    _mm_store_ps(outputTemp+(i*2),   outVal1);
                    _mm_store_ps(outputTemp+(i*2)+4, outVal2);
                }

                numFloats  -= alignedFloats;
                inputTemp  += alignedFloats;
                outputTemp += alignedFloats*2;
            }

            while(numFloats--)
            {
                float inputVal = *inputTemp;
                *(outputTemp++) = inputVal;
                *(outputTemp++) = inputVal;

                inputTemp++;
            }
        }
        else if(m_InputChannels == 2) //straight up copy
        {
            memcpy((uint8_t *)m_pDataOutputBuffer, (uint8_t *)captureBuffer, numAudioFrames*2*sizeof(float));
		}
        else
        {
            float *inputTemp  = (float *)m_NextBuffer;
            float *outputTemp = (float *)m_pDataOutputBuffer;

            if(m_InputChannelMask == KSAUDIO_SPEAKER_QUAD)
            {
                uint32_t numFloats = numAudioFrames*4;
                float *endTemp = inputTemp+numFloats;

                while(inputTemp < endTemp)
                {
                    float left      = inputTemp[0];
                    float right     = inputTemp[1];
                    float rearLeft  = inputTemp[2]*surroundMix4;
                    float rearRight = inputTemp[3]*surroundMix4;

                    *(outputTemp++) = (left  + rearLeft)  * attn4dotX;
                    *(outputTemp++) = (right + rearRight) * attn4dotX;

                    inputTemp  += 4;
                }
            }
            else if(m_InputChannelMask == KSAUDIO_SPEAKER_2POINT1)
            {
                uint32_t numFloats = numAudioFrames*3;
                float *endTemp = inputTemp+numFloats;

                while(inputTemp < endTemp)
                {
                    float left      = inputTemp[0];
                    float right     = inputTemp[1];

                    *(outputTemp++) = left;
                    *(outputTemp++) = right;

                    inputTemp  += 3;
                }
            }
            else if(m_InputChannelMask == KSAUDIO_SPEAKER_3POINT1)
            {
                uint32_t numFloats = numAudioFrames*4;
                float *endTemp = inputTemp+numFloats;

                while(inputTemp < endTemp)
                {
                    float left          = inputTemp[0];
                    float right         = inputTemp[1];
                    float frontCenter   = inputTemp[2];
                    float lowFreq       = inputTemp[3];

                    *(outputTemp++) = left;
                    *(outputTemp++) = right;

                    inputTemp  += 4;
                }
            }
            else if(m_InputChannelMask == KSAUDIO_SPEAKER_4POINT1)
            {
                uint32_t numFloats = numAudioFrames*5;
                float *endTemp = inputTemp+numFloats;

                while(inputTemp < endTemp)
                {
                    float left      = inputTemp[0];
                    float right     = inputTemp[1];
					
                    float rearLeft  = inputTemp[3]*surroundMix4;
                    float rearRight = inputTemp[4]*surroundMix4;

                    *(outputTemp++) = (left  + rearLeft)  * attn4dotX;
                    *(outputTemp++) = (right + rearRight) * attn4dotX;

                    inputTemp  += 5;
                }
            }
            else if(m_InputChannelMask == KSAUDIO_SPEAKER_SURROUND)
            {
                uint32_t numFloats = numAudioFrames*4;
                float *endTemp = inputTemp+numFloats;

                while(inputTemp < endTemp)
                {
                    float left      = inputTemp[0];
                    float right     = inputTemp[1];
                    float frontCenter    = inputTemp[2];
                    float rearCenter     = inputTemp[3];
                    
                    *(outputTemp++) = left;
                    *(outputTemp++) = right;

                    inputTemp  += 4;
                }
            }
            else if(m_InputChannelMask == KSAUDIO_SPEAKER_5POINT1 || m_InputChannelMask == KSAUDIO_SPEAKER_5POINT1_SURROUND)
            {
                uint32_t numFloats = numAudioFrames*6;
                float *endTemp = inputTemp+numFloats;

                while(inputTemp < endTemp)
                {
                    float left      = inputTemp[0];
                    float right     = inputTemp[1];
                    float center    = inputTemp[2]*centerMix;

                    float rearLeft  = inputTemp[4]*surroundMix;
                    float rearRight = inputTemp[5]*surroundMix;
                    
                    *(outputTemp++) = (left  + center  + rearLeft) * attn5dot1;
                    *(outputTemp++) = (right + center  + rearRight) * attn5dot1;

                    inputTemp  += 6;
                }
            } else if(m_InputChannelMask == KSAUDIO_SPEAKER_7POINT1)
            {
                uint32_t numFloats = numAudioFrames*8;
                float *endTemp = inputTemp+numFloats;

                while(inputTemp < endTemp)
                {
                    float left          = inputTemp[0];
                    float right         = inputTemp[1];
                    
                    float center        = inputTemp[2] * centerMix;                                        
                    float rearLeft      = inputTemp[4] * surroundMix;
                    float rearRight     = inputTemp[5] * surroundMix;
					                    
                    // Downmix from 5.1 to stereo
                    *(outputTemp++) = (left  + center  + rearLeft)  * attn5dot1;
                    *(outputTemp++) = (right + center  + rearRight) * attn5dot1;

                    inputTemp  += 8;
                }
            } else if(m_InputChannelMask == KSAUDIO_SPEAKER_7POINT1_SURROUND)
            {
                uint32_t numFloats = numAudioFrames*8;
                float *endTemp = inputTemp+numFloats;

                while(inputTemp < endTemp)
                {
                    float left      = inputTemp[0];
                    float right     = inputTemp[1];
                    float center    = inputTemp[2] * centerMix;
					
                    float rearLeft  = inputTemp[4];
                    float rearRight = inputTemp[5];
                    float sideLeft  = inputTemp[6];
                    float sideRight = inputTemp[7];

                    rearLeft  = (rearLeft  + sideLeft)  * 0.5f;
                    rearRight = (rearRight + sideRight) * 0.5f;

                    *(outputTemp++) = (left  + center + rearLeft  * surroundMix) * attn5dot1;
                    *(outputTemp++) = (right + center + rearRight * surroundMix) * attn5dot1;

                    inputTemp  += 8;
                }
            }
        }
        //return -1;
        if (1.0f == m_ResampleRatio && NULL == m_pResampler)
		{
			if (NULL == m_pTmpBuffer)
			{
				if(NULL == (m_pTmpBuffer = (uint8_t *)malloc(numAudioFrames * 2 * 2 * 2)))
                {
                    return -1;
                }
			}
			memset(m_pTmpBuffer, 0, numAudioFrames * 2 * 2);
			src_float_to_short_array((float *)m_pDataOutputBuffer, (short *)m_pTmpBuffer, numAudioFrames * 2);
            memcpy((void *)out_buf_, m_pTmpBuffer, numAudioFrames * 2 * sizeof(short));
            return numAudioFrames * 2 * sizeof(short);
		}else
		{
			uint32_t frameAdjust = uint32_t((double(numAudioFrames) * m_ResampleRatio));

			memset((uint8_t *)m_pTempResampleBuffer, 0, frameAdjust * 8);

			SRC_DATA data;
			data.src_ratio = m_ResampleRatio;

			data.data_in = (float *)m_pDataOutputBuffer;
			data.input_frames = numAudioFrames;

			data.data_out = (float *)m_pTempResampleBuffer;
			data.output_frames = frameAdjust;

			data.end_of_input = 0;

			int err = src_process(m_pResampler->resampler, &data);
			if(err)
			{
				return false;
			}

			if(data.input_frames_used != numAudioFrames)
			{
				return false;
			}

			numAudioFrames = data.output_frames_gen;

			if (NULL == m_pTmpBuffer)
			{
				if(NULL == (m_pTmpBuffer = (uint8_t *)malloc(frameAdjust * 2 * 2 * 2)))
                {
                    return -1;
                }
			}
			memset(m_pTmpBuffer, 0, frameAdjust * 2 * 2);
			src_float_to_short_array((float *)m_pTempResampleBuffer, (short *)m_pTmpBuffer, frameAdjust * 2);
			memcpy((void *)out_buf_, m_pTmpBuffer, numAudioFrames * 2 * sizeof(short));
            return numAudioFrames * 2 * sizeof(short);
		}
	}else
	{
			return -1;
	}

    return 0;
}

void CDesktopAudioDevice::FreeData()
{
    ISSUPPORT(m_bPlatformSupport, );
	SafeRelease(mmCapture_);
	SafeRelease(mmClient_);
	SafeRelease(mmDevice_);
	if (m_pResampler)
	{
		src_delete(m_pResampler->resampler);
		delete m_pResampler;
		m_pResampler = NULL;
	}
	if (m_pBlankBuffer)
	{
		free(m_pBlankBuffer);
		m_pBlankBuffer = NULL;
	}	
	if (m_pTempResampleBuffer)
	{
		free(m_pTempResampleBuffer);
		m_pTempResampleBuffer = NULL;
	}	
	if (m_pDataOutputBuffer)
	{
		free(m_pDataOutputBuffer);
		m_pDataOutputBuffer = NULL;
	}
	if (m_pTmpBuffer)
	{
		free(m_pTmpBuffer);
		m_pTmpBuffer = NULL;
	}	
}

bool CDesktopAudioDevice::GetNextBuffer(uint32_t &numFrames, int32_t &size_)
{
    ISSUPPORT(m_bPlatformSupport, false);

    uint32_t captureSize = 0;
    bool bFirstRun = true;
    HRESULT hRes;
    uint64_t devPosition, qpcTimestamp;
    LPBYTE captureBuffer;
    UINT32 numFramesRead;
    DWORD dwFlags = 0;

    while (true) {
        if (m_InputBufferSize >= m_SampleWindowSize*m_InputChannels) 
		{
            break;
        }
        if (NULL == mmCapture_)
        {
            break;
        }

        hRes = mmCapture_->GetNextPacketSize(&captureSize);

        if (FAILED(hRes)) 
		{
   //         if (hRes == AUDCLNT_E_DEVICE_INVALIDATED) 
			//{
   //             FreeData();
			//	StopCapture();
			//	Init(true, m_CurDeviceID);
			//	StartCapture();
   //             return false;
   //         }
            return false;
        }

        if (!captureSize)
            return false;

        hRes = mmCapture_->GetBuffer(&captureBuffer, &numFramesRead, &dwFlags, &devPosition, &qpcTimestamp);

        if (FAILED(hRes)) {
            return false;
        }
		uint32_t totalFloatsRead = numFramesRead * m_InputChannels;

        memcpy((void *)m_NextBuffer, (void *)captureBuffer, totalFloatsRead * sizeof(float));
		numFrames = numFramesRead;
        size_ = totalFloatsRead * sizeof(float);

		mmCapture_->ReleaseBuffer(numFramesRead);
		return true;
    }

    return false;
}

void CDesktopAudioDevice::IsPlatFormSupport()
{
    OSVERSIONINFOEX osver;  
    osver.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);  
    //获取版本信息  
    //if (! GetVersionEx((LPOSVERSIONINFOW)&osver))  
    if (!GetVersionEx((LPOSVERSIONINFOA)&osver))
    {  
        m_bPlatformSupport = false;
        return ;
    }  
    if (osver.dwMajorVersion >= 6)  //高于windows vista
    {
        m_bPlatformSupport = true;
    }else
    {
        m_bPlatformSupport = false;
    }     
}

void CDesktopAudioDevice::execute()
{
    int ret = 0;
    while(!terminated_)
    {
        if (_pEasyBuff && (ret = QueryAudioBuffer(_pcm_tmp_buffer, sizeof(_pcm_tmp_buffer))) >0)
        {
            BASE_GUARD(BaseThreadMutex, cf_mon, m_mutext);
            if (_pEasyBuff->GetUsedSize() >= 1920 * 5)
            {
                _pEasyBuff->reset();
            }
            _pEasyBuff->PushBack(_pcm_tmp_buffer, ret);
        }else
        {
            Sleep(1);
        }
    }
}
