#pragma once
#include "base_typedef.h"
#include <vector>
#include <string>
#include "type_def.h"

#define	DEFAULT_PIC_WIDTH		320
#define DEFAULT_PIC_HEIGHT		240

#define DEFAULT_FRAME_RATE		15
#define MAX_VIDEO_H_SIZE		1200
#define MAX_VIDEO_W_SIZE		1920
#define MAX_VIDEO_DATA_SIZE		(MAX_VIDEO_W_SIZE * MAX_VIDEO_H_SIZE * 3 / 2)


using namespace std;



enum device_status
{
	DEVICE_CLOSE	= 0,
	DEVICE_OPENING	= 1,
	DEVICE_OPEN		= 2,
	DEVICE_CLOSEING = 3,
};

//定义媒体数据类型
enum DATA_TYPE
{
	H264_DATA_TYPE		=	0,
	YUV420_DATA_TYPE	=	1,
	RGB32_DATA_TYPE		=	2,
	RGB24_DATA_TYPE     =   3,

	AMR_DATA_TYPE       =  10,
	AAC_DATA_TYPE       =  11,
	MP3_DATA_TYPE       =  12,
	PCM_DATA_TYPE       =  13,
};

class IVideoInDevice
{
public:
	IVideoInDevice(int32_t dev_id, uint16_t code_id ) : status_(DEVICE_CLOSE)
	{
		dev_id_ = dev_id;
		code_id_= code_id;
		switch( code_id_ )
		{
			case X264_240P: //320p编码器
				{
					width_		=320;
					height_		=240;
					fps_		=15;
				}
				break;

			case X264_360P://480p编码器
				{
					width_		=480;
					height_		=360;
					fps_		=15;
				}
				break;

			case X264_480P://640p编码器
				{
					width_		=640;
					height_		=480;
					fps_		=15;
				}
				break;
			case X264_600P:
				{
					width_		=800;
					height_		=600;
					fps_		=15;
				}
				break;
			case X264_540P:
				{
					width_		=960;
					height_		=540;
					fps_		=15;
				}
				break;
			default:
				{
					width_		=-1;
					height_		=-1;
					fps_		=-1;
				}
				break;
		}
	};

	virtual ~IVideoInDevice(){};
	uint8_t				is_open() const{ return status_;};
	uint32_t			get_width(){ return width_;};
	uint32_t			get_height(){ return height_; };
	uint32_t			get_fps(){ return fps_; };
	int					get_dev_id( ){return dev_id_; };
	uint16_t			get_code_id(){return code_id_; };
	virtual int			init_device( )= 0;
	virtual int			open( uint16_t frame= DEFAULT_FRAME_RATE ) = 0;
	virtual void		close() = 0;
	virtual bool		read(uint8_t* data, uint32_t &data_size, int& frame_type, int& data_type, uint64_t& ts )=0;
protected:
	uint8_t				status_;		//0：关闭 1：打开中 2：打开 3：关闭中
	int32_t				dev_id_;
	uint16_t			code_id_;
	uint32_t			width_; 
	uint32_t			height_;
	uint32_t			fps_;

};

