#include "stdafx.h"
#include "audio_codec.h"
#include "EasyBuffer.h"

extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavutil/avutil.h"
#include "libswscale/swscale.h"
#include "libavutil/opt.h"
}
#include "global_config.h"

#ifdef WIN32
#pragma comment(lib, "avcodec.lib")
#pragma comment(lib, "avformat.lib")
#pragma comment(lib, "avutil.lib")
#endif // WIN32

CAudioEncoder::CAudioEncoder(void)
{
    _pCodecContext = NULL;
    _pAudioSourceFrame = NULL;
    _pAvCodec = NULL;
    _nSampleRate = 0;
    _nChannelCount = 0;
    _nBitPerSample = 0;
    _eCodecId = EAUDIOCODEC_AAC;
    _nSizePerFrame = 0;
    _pEasyBuffer = NULL;
    _pcm_buffer = NULL;

    static bool init_av_ = false;
    if(false == init_av_)
    {
        init_av_ = true;
        avcodec_register_all();
    }
}

CAudioEncoder::~CAudioEncoder(void)
{
    close();
}

int CAudioEncoder::open(int sample_rate_, int channel_num_, int bit_per_sample_, 
                         ENUM_AUDIO_CODEC codec_id_/* = EAUDIOCODEC_AAC*/)
{
    BoostLog::Log(info, "CAudioEncoder::open() %d, %d, %d, %d", 
        sample_rate_, channel_num_, bit_per_sample_, codec_id_);
    
    if (NULL != _pAvCodec)
    {
        close();
    }
    do 
    {
        boost::lock_guard<boost::recursive_mutex > lock_(GLOBAL_CONFIG()->_codec_mutex);

        /* find the encoder */
        switch(codec_id_)
        {
        case EAUDIOCODEC_AAC:
            _pAvCodec = avcodec_find_encoder(AV_CODEC_ID_AAC);
            break;
        case EAUDIOCODEC_MP3:
            _pAvCodec = avcodec_find_encoder(AV_CODEC_ID_MP3);
            break;
        default:
            BOOST_WARNING << "CAudioEncoder::open no codec_id:" << codec_id_;
            break;
        }
        if (!_pAvCodec) 
        {
            BOOST_WARNING << "CAudioEncoder::open find _pAvCodec:"<< codec_id_ << " failed!";
            break;
        }

        _pCodecContext = avcodec_alloc_context3(_pAvCodec);
        if (!_pCodecContext) 
        {
            break;
        }

        /* put sample parameters */
        _pCodecContext->bit_rate = GLOBAL_CONFIG()->_audio_bitrate;

        /* check that the encoder supports s16 pcm input */
        _pCodecContext->sample_fmt = AV_SAMPLE_FMT_S16;
        bool  bsupportflag = false;
        const enum AVSampleFormat *p = _pAvCodec->sample_fmts;
        while (*p != AV_SAMPLE_FMT_NONE) 
        {
            if (*p == AV_SAMPLE_FMT_S16)
            {
                bsupportflag = true;
                break;
            }
            p++;
        }
        if (!bsupportflag)
        {
            BOOST_WARNING << "CAudioEncoder::open audio encoder not support fmt:" << 
                av_get_sample_fmt_name(_pCodecContext->sample_fmt);
            break;
        }    

        /* select other audio parameters supported by the encoder */
        _pCodecContext->sample_rate    = sample_rate_;
        _pCodecContext->channel_layout = av_get_default_channel_layout(channel_num_);
        _pCodecContext->channels       = av_get_channel_layout_nb_channels(_pCodecContext->channel_layout);

        /* open it */
        if (avcodec_open2(_pCodecContext, _pAvCodec, NULL) < 0) 
        {
            BOOST_WARNING << "CAudioEncoder::open avcodec_open2 failed.";
            break;
        }

        /* frame containing input raw audio */
        _pAudioSourceFrame = avcodec_alloc_frame();
        if (!_pAudioSourceFrame) 
        {
            BOOST_WARNING << "CAudioEncoder::open avcodec_alloc_frame failed.";
            break;
        }

        _pAudioSourceFrame->nb_samples     = _pCodecContext->frame_size;
        _pAudioSourceFrame->format         = _pCodecContext->sample_fmt;
        _pAudioSourceFrame->channel_layout = _pCodecContext->channel_layout;

        /* the _pAvCodec gives us the frame size, in samples,
        * we calculate the size of the samples buffer in bytes */
        _nSizePerFrame = av_samples_get_buffer_size(NULL, _pCodecContext->channels, _pCodecContext->frame_size,
            _pCodecContext->sample_fmt, 0);
        _pcm_buffer = (unsigned char *)av_malloc(_nSizePerFrame);
        if (NULL == _pcm_buffer)
        {
            break;
        }
        _pEasyBuffer = new(std::nothrow) CEasyBuffer(192000);
        if (NULL == _pEasyBuffer)
        {
            break;
        }
        this->_nSampleRate = sample_rate_;
        this->_nChannelCount = channel_num_;
        this->_nBitPerSample = bit_per_sample_;
        this->_eCodecId = codec_id_;

        return 0;
    } while (false);
    
    //open failed
    close();
    return -1;
}

int CAudioEncoder::close()
{
    boost::lock_guard<boost::recursive_mutex > lock_(GLOBAL_CONFIG()->_codec_mutex);
    if (_pAudioSourceFrame)
    {
        avcodec_free_frame(&_pAudioSourceFrame);
        _pAudioSourceFrame = NULL;
    }
    if (_pCodecContext)
    {
        avcodec_close(_pCodecContext);
        _pCodecContext = NULL;
    }
    if (_pCodecContext)
    {
        av_free(_pCodecContext);
        _pCodecContext = NULL;
    }
    if (_pEasyBuffer)
    {
        delete _pEasyBuffer;
        _pEasyBuffer = NULL;
    }
    if (_pcm_buffer)
    {
        av_freep(&_pcm_buffer);
        _pcm_buffer = NULL;
    }
    this->_nSampleRate = 0;
    this->_nChannelCount = 0;
    this->_nBitPerSample = 0;
    this->_eCodecId = (ENUM_AUDIO_CODEC)0;
    this->_nSizePerFrame =0;
    return 0;
}

int CAudioEncoder::enc(short *pcm_data_, int pcm_data_size_, 
                        short *out_data_, int max_out_data_size_)
{
    int ret(-1);
    if (NULL == pcm_data_ || NULL == out_data_ || 0 >= pcm_data_size_ || 0 >= max_out_data_size_)
    {
        BoostLog::Log(warn, "CAudioEncoder::enc() param error! src_data:0x%x, src_len:%d, \
                            out_data:0x%x, out_size:%d", pcm_data_, pcm_data_size_, out_data_, max_out_data_size_);
        return ret;
    }
    if (NULL == _pCodecContext || NULL == _pAvCodec)
    {
        return ret;
    }
    _pEasyBuffer->PushBack((void *)pcm_data_, pcm_data_size_ * sizeof(short ));
    if (_pEasyBuffer->GetUsedSize() < _nSizePerFrame)
    {
        return ret;
    }
    _pEasyBuffer->PopFront(_pcm_buffer, _nSizePerFrame);
    
    /* the _pAvCodec gives us the frame size, in samples,
     * we calculate the size of the samples buffer in bytes */
    /* setup the data pointers in the AVFrame */
    ret = avcodec_fill_audio_frame(_pAudioSourceFrame, _pCodecContext->channels, _pCodecContext->sample_fmt,
                                   _pcm_buffer, _nSizePerFrame, 0);
    if (ret < 0) 
    {
        BOOST_WARNING << "CAudioEncoder::open avcodec_fill_audio_frame failed.";
        return ret;
    }
    int got_output(0);
    AVPacket pkt;
    av_init_packet(&pkt);
    pkt.data = NULL; // packet data will be allocated by the encoder
    pkt.size = 0;
    /* encode the samples */
    ret = avcodec_encode_audio2(_pCodecContext, &pkt, _pAudioSourceFrame, &got_output);

    if (ret >= 0 && got_output && max_out_data_size_ >= pkt.size)  //success
    {
        memcpy((void *)out_data_, pkt.data, pkt.size);
        ret = pkt.size;

        av_free_packet(&pkt);
    }

    return ret;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
CAudioDecoder::CAudioDecoder(void)
{
    _pAvCodec = NULL;
    _pCodecContext = NULL;
    _pAudioSourceFrame = NULL;
    _eCodecId = (ENUM_AUDIO_CODEC )0;

    static bool init_av_ = false;
    if(false == init_av_)
    {
        init_av_ = true;
        avcodec_register_all();
    }
}

CAudioDecoder::~CAudioDecoder(void)
{
    close();
}

int CAudioDecoder::open(ENUM_AUDIO_CODEC codec_id_ /*= EAUDIOCODEC_AAC*/)
{
    AVPacket avpkt;
    AVFrame *decoded_frame = NULL;

    av_init_packet(&avpkt);

    do 
    {
        boost::lock_guard<boost::recursive_mutex > lock_(GLOBAL_CONFIG()->_codec_mutex);
        /* find the mpeg audio decoder */
        switch(codec_id_)
        {
        case EAUDIOCODEC_AAC:
            _pAvCodec = avcodec_find_decoder(AV_CODEC_ID_AAC);
            break;
        case EAUDIOCODEC_MP3:
            _pAvCodec = avcodec_find_decoder(AV_CODEC_ID_MP3);
            break;
        default:
            BOOST_WARNING << "CAudioDecoder::open no codec_id:" << codec_id_;
            break;
        }
        if (!_pAvCodec) 
        {
            BOOST_WARNING << "CAudioDecoder::open find _pAvCodec:"<< codec_id_ << " failed!";
            break;
        }
        _pCodecContext = avcodec_alloc_context3(_pAvCodec);
        if (!_pCodecContext) 
        {
            BOOST_WARNING << "CAudioDecoder::open Could not allocate audio _pAvCodec context";
            break;
        }
        /* open it */
        if (avcodec_open2(_pCodecContext, _pAvCodec, NULL) < 0) 
        {
            BOOST_WARNING << "CAudioDecoder::open avcodec_open2 failed.";
            break;
        }
        if(NULL == (_pAudioSourceFrame = avcodec_alloc_frame()))
        {
            BOOST_WARNING << "CAudioDecoder::open avcodec_alloc_frame failed.";
            break;
        }
        return 0;
    } while (false);
    //failed
    close();
    return -1;
}

int CAudioDecoder::close()
{
    boost::lock_guard<boost::recursive_mutex > lock_(GLOBAL_CONFIG()->_codec_mutex);

    if (_pCodecContext)
    {
        avcodec_close(_pCodecContext);
        _pCodecContext = NULL;
    }
    if (_pCodecContext)
    {
        av_free(_pCodecContext);
        _pCodecContext = NULL;
    }
    if (_pAudioSourceFrame)
    {
        avcodec_free_frame(&_pAudioSourceFrame);
        _pAudioSourceFrame = NULL;
    }
    _eCodecId = (ENUM_AUDIO_CODEC )0;
    return 0;
}

//需以帧为单位进行解码
int CAudioDecoder::dec(unsigned char *src_data_, int src_size_, 
                        unsigned char *out_data_, int max_out_data_size_)
{
    if (NULL == src_data_ || src_size_ <= 0 || NULL == out_data_ || max_out_data_size_ <= 0)
    {
        //BoostLog::Log(warn, "CAudioDecoder::dec() params error! 0x%x, %d, 0x%x, %d", 
        //    src_data_, src_size_, out_data_, max_out_data_size_);
        return -1;
    }
    if (NULL == _pAvCodec)
    {
        return -1;
    }
    int len(0);
    AVPacket avpkt;
    av_init_packet(&avpkt);
    avpkt.data = src_data_;
    avpkt.size = src_size_;

    int got_frame = 0;
    avcodec_get_frame_defaults(_pAudioSourceFrame);

    len = avcodec_decode_audio4(_pCodecContext, _pAudioSourceFrame, &got_frame, &avpkt);
    if (len >= 0 && got_frame) 
    {
        /* if a frame has been decoded, output it */
        int data_size = av_samples_get_buffer_size(NULL, _pCodecContext->channels,
            _pAudioSourceFrame->nb_samples,
            _pCodecContext->sample_fmt, 1);
        if (data_size <= max_out_data_size_)
        {
            memcpy(out_data_, _pAudioSourceFrame->data[0], data_size);
            return data_size;
        }
    }
    return -1;
}

void CAudioDecoder::flush_codec()
{
    boost::lock_guard<boost::recursive_mutex > lock_(GLOBAL_CONFIG()->_codec_mutex);
    if (this->_pCodecContext)
    {
        avcodec_flush_buffers(_pCodecContext);
    }
}

