#ifdef DLL_EXPORTS
#define INTERFACE_API __declspec(dllexport)
#else
#define  INTERFACE_API __declspec(dllimport)
#endif

#pragma once
#include "live_event.h"
enum NETPROTO
{
    _eHttp = 0,
    _eUdp,
    _eRtmp,
};

class IRecordInterface
{
public:
    virtual         ~IRecordInterface(){};
    
    //添加服务器地址信息,根据IP列表，客户端自动选择登陆IP
    //@ ip_ : 服务器IP
    //@ type_ : 此IP运营商类型
    //@ my_type_ : 本机IP运营商类型
    virtual int                     add_sev_ip_info(const unsigned long ip_, const int port_,
        const short type_, const short my_type_) = 0;
    virtual int                     del_sev_ip_info(const unsigned long ip_) = 0;
    virtual void                    clear_sev_ip_info() = 0;

    virtual int                     Login(const unsigned int uid_, const unsigned int room_id_,
        NETPROTO proto_ = _eHttp) = 0;
    virtual int                     Login(const unsigned int uid_, const unsigned int room_id_,
        const char* ip_, const short port_, NETPROTO proto_ = _eHttp) = 0;
    virtual int                     Logout(const unsigned int room_id_) = 0;

    //打开视频设备dev_id_，在第video_show_num_路预览，dev_id_为-1，则关闭设备
    virtual int                     preview_video(int video_show_num_, int dev_id_) = 0;
    //打开音频输入设备MIC，默认打开伴奏，可以通过set_acc_flag（false）关闭伴奏
    virtual int                     preview_audio(int dev_id_) = 0;

    virtual void                    set_video_show_num(int num_) = 0;
    virtual int                     get_video_show_num() = 0;

    //获取视频采集设备列表
    virtual int                     get_video_dev_list(char video_dev_[][256], const int max_size_ = 10) = 0;
    //获取视频采集设备个数
    virtual int                     get_video_dev_count() = 0;

    virtual int&                    get_preview_video_width() = 0;
    virtual int&                    get_preview_video_height() = 0;

    //设置主播头像图片
    virtual void                    set_default_anchor_img(const char* path_name_, const int& max_len_) = 0;
    virtual void                    set_default_anchor_img_flag(bool flag_) = 0;

    //设置是否启用伴奏标志
    virtual void                    set_acc_flag(bool bflag_) = 0;
    //设置是否启用麦克风标志
    virtual void                    set_mic_flag(bool bflag_) = 0;

    virtual bool                    is_logined(const unsigned int room_id_) = 0;

    //设置视频采集编解码参数，将重启音视频采集及编解码器组件
    virtual void                    set_global_cfg(int cap_width_, int cap_height_,
        int codec_width_, int codec_height_, int gop_time_, int video_bitrate_,
        int audio_bitrate_, char* anchor_img_str_) = 0;
    //设置事件通知回调接口
    virtual void                    set_live_event(ILiveEvent* event_) = 0;
};

class IPlayerInterface
{
public:
    virtual                         ~IPlayerInterface(){}
    //添加服务器地址信息
    //@ ip_ : 服务器IP
    //@ type_ : 此IP运营商类型
    //@ my_type_ : 本机IP运营商类型
    virtual int                     add_sev_ip_info(const unsigned long ip_, const int port_,
        const short type_, const short my_type_) = 0;
    virtual int                     del_sev_ip_info(const unsigned long ip_) = 0;
    virtual void                    clear_sev_ip_info() = 0;

    //IP地址内部自动轮换，需要先使用add_sev_ip_info添加服务器IP地址信息
    virtual int                     Login(const unsigned int uid_, const unsigned int room_id_,
        NETPROTO proto_ = _eHttp) = 0;
    virtual int                     Logout() = 0;

    virtual bool                    is_playing() = 0;

    //设置播放音量0~100
    virtual void                    set_volume(int volume_) = 0;
    //获取播放音量
    virtual int                     get_volume() = 0;

    //拍照，目前只支持保存为位图
    virtual void                    save_bmp(const char* file_name_, const int len_) = 0;
    //转换缩放图像
    virtual int                     swscale_rgb24_img(unsigned char* src_data_, int src_width_, int src_height_, int src_format_,
        unsigned char* dst_data_, int dst_width_, int dst_height_, int dst_format_) = 0;
};

extern "C" INTERFACE_API IRecordInterface*      GetRecordInterface(ILiveEvent*);
extern "C" INTERFACE_API void                   ReleaseRecordInterface(IRecordInterface**);
extern "C" INTERFACE_API IPlayerInterface*      GetPlayerInterface(ILiveEvent*);
extern "C" INTERFACE_API void                   ReleasePlayerInterface(IPlayerInterface**);
extern "C" INTERFACE_API void                   ReleaseDllResource();
