#include "stdafx.h"
#include <io.h>
#include "global_config.h"
#include "dshow_video.h"
#include "curl/curl.h"

using namespace ProtoNameSpace;
#ifdef _DEBUG
#pragma comment(lib, "libprotobufD.lib")
#else
#pragma comment(lib, "libprotobuf.lib")
#endif // _DEBUG

#define VOLUMEMAX   32767
#define VOLUMEMIN	-32768

bool CGlobalConfig::_big_endian = false;

CGlobalConfig::CGlobalConfig(void)
{
    //�ж��ֽ���
    union
    {
        unsigned short	s16;
        unsigned char   s8[2];
    }un;
    un.s16 = 0x010a;
    _big_endian = (un.s8[0] == 0x01);

    /* Check for existence */
    if ((_access("medialog", 0)) != -1)
    {
#ifdef _DEBUG
        BoostLog::Init_Release("log", "KTVMedia", debug);
#else
        BoostLog::Init_Release("log", "KTVMedia", debug);
#endif // _DEBUG
    }
    //curl_global_init(CURL_GLOBAL_ALL);

    _preview_pic_width = 640;
    _preview_pic_height = 480;
    _codec_pic_width = 320;
    _codec_pic_height = 240;
    _frame_rate = 15;
    _video_bitrate = 150;
    _audio_bitrate = 64000;
    _key_frame_interval = 3;
    _cur_video_show_num = -1;
    _local_uid = 0;
    _video_dev_count = 0;
    _send_def_img_flag = false;
    _benable_accompany = true;
    _benable_mic = true;
    _play_volume_scale = 1.0f;
    _brecord_handle_init_flag = false;

    memset(_anchor_img, 0, sizeof(_anchor_img));
    set_anchor_img(".\\Games\\KTVServer\\ui\\AnchorWaiting.jpg"); //����һ��Ĭ�ϵ�����ͼ��

    memset(_video_dev_list, 0, sizeof(_video_dev_list));
#ifndef _MEDIA_PLAY_CLIENT
    //_video_dev_count = enum_video_dev(_video_dev_list, MAX_VIDEO_DEV_COUNT);
#endif // _MEDIA_PLAY_CLIENT
}

CGlobalConfig::~CGlobalConfig(void)
{
    //curl_global_cleanup();
    // Optional:  Delete all global objects allocated by libprotobuf.
    google::protobuf::ShutdownProtobufLibrary();
    if ((_access("medialog", 0)) != -1)
    {
        BoostLog::Uninit();
    }
}

#ifndef _MEDIA_PLAY_CLIENT
int CGlobalConfig::enum_video_dev(__out char dev_[][256], __in const int& max_count_)
{
    return CDShowVideoDevice::EnumVideoCaptureDevice(dev_, max_count_);
}

void CGlobalConfig::enum_video_dev()
{
    _video_dev_count = enum_video_dev(_video_dev_list, MAX_VIDEO_DEV_COUNT);
}

#endif // _MEDIA_PLAY_CLIENT

void CGlobalConfig::set_anchor_img(const char* img_str_)
{
    sprintf_s(_anchor_img, sizeof(_anchor_img), "%s", img_str_);
}

short CGlobalConfig::small_2_big_short(short num_)
{
    short ret = num_;
    if (!_big_endian)
    {
        ret = BigLittleSwap16(num_);
    }
    return ret;
}

extern long long get_cur_time()
{
#ifdef WIN32
    return GetTickCount();
#else
    struct  timeval tv;
    gettimeofday(&tv, NULL);
    return (tv.tv_sec * 1000 + tv.tv_usec/1000);
#endif
}
