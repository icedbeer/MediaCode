#pragma once
#include <deque>
#include <string>
#include <stdint.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>
#include <process.h>
#include <io.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <mswsock.h>
#include <direct.h>
#include <mmdeviceapi.h>
#include "global_config.h"

#ifdef _MSC_VER
#define _WIN32_WINNT 0x0501
#endif

using namespace std;

//声音采集设备类型
enum AUDIO_DEVICE_TYPE
{
    ENONE,
    ESOUNDWAVE,         //传统的音频采集，PCM->AAC
    EWEBRTC,            //WEBRTC->AMR
    EDESKTOP,           //输出声卡
};

#define MAX_PCM_BUFFER_SIZE     1920  //48000HZ*(16bit/8)*2channel/100    10ms
#define DEFAULT_SAMPLE_RATE		48000
#define DEFAULT_CHANNEL_NUM		2
#define DEFAULT_CHANNEL_BIT		16
#define DEFAULT_RAW_SIZE		4096  //声卡每次填充数据大小

class CSoundChannel;
class CDesktopAudioDevice;
class HeAudioMixer;
class CAudioEncoder;
class CMediaRecordThread;

class CAudioRecordChannel : public IMMNotificationClient
{
public:
    CAudioRecordChannel(CMediaRecordThread *media_thr_);
    virtual ~CAudioRecordChannel(void);

    //根据cap_type_类型，内部自动配置相应的采集编码参数,success即开始采集
    //param @cap_type_ 音频采集类型，ESOUNDWAVE或EWEBRTC
    //param @dev_id_ 使用音频输入设备序号，默认为0
    //param @baccompaniment_ 是否启用伴奏
    virtual int     open(int dev_id_, bool baccompaniment_);
    virtual int     open(char *dev_name_, bool baccompaniment_);
    //停止采集，并关闭设备
    virtual int     close();

protected:
    // IUnknown methods -- AddRef, Release, and QueryInterface
    LONG _cRef;
    ULONG STDMETHODCALLTYPE AddRef()
    {
        return 0;
    }

    ULONG STDMETHODCALLTYPE Release()
    {
        ULONG ulRef = 0;
        return ulRef;
    }

    HRESULT STDMETHODCALLTYPE QueryInterface(
        REFIID riid, VOID **ppvInterface)
    {
        return S_OK;
    }

    HRESULT STDMETHODCALLTYPE OnDefaultDeviceChanged(
        EDataFlow flow, ERole role,
        LPCWSTR pwstrDeviceId);

    HRESULT STDMETHODCALLTYPE OnDeviceAdded(LPCWSTR pwstrDeviceId)
    {
        return S_OK;
    };

    HRESULT STDMETHODCALLTYPE OnDeviceRemoved(LPCWSTR pwstrDeviceId)
    {
        return S_OK;
    }

    HRESULT STDMETHODCALLTYPE OnDeviceStateChanged(
        LPCWSTR pwstrDeviceId,
        DWORD dwNewState)
    {
        return S_OK;
    }

    HRESULT STDMETHODCALLTYPE OnPropertyValueChanged(
        LPCWSTR pwstrDeviceId,
        const PROPERTYKEY key)
    {
        return S_OK;
    }

    void                        execute();    
    virtual bool                loop();
    void                        reopen();

protected:
    CSoundChannel               *_pMicDevice;                       //mic采集传统设备
    CDesktopAudioDevice         *_pDesktopDevice;                   //输出声卡采集设备(visit 以上版本)
    HeAudioMixer                *_pAudioMixer;                      //混音器
    CAudioEncoder               *_pAudioEncoder;
    uint8_t                     _pcm_tmp_buffer[192000];
    uint8_t                     _pcm_buffer[MAX_PCM_BUFFER_SIZE];   //PCM数据缓冲区
    uint8_t                     _encode_buffer[32*1024];            //编码数据缓冲区
    float                       _mic_volume;                        //mic的音频音量值
    float                       _accompany_volume;                  //伴奏的音频音量值
    float                       _volume;                            //整体的音频音量值
    uint16_t                    _signal_level;                      //记录音频实时能量值
    bool                        _baccompany_flag;                   //伴奏标识
    boost::recursive_mutex      _mutex;                             //设备锁

    CMediaRecordThread          *_pRecThread;
    uint32_t                    _last_cap_mic_tv;                   //记录最后一次采集到麦克风数据的时间,防止用户切换设备或者插拔设备，无法采集数据
    boost::atomic<bool>         _breopen_flag;
    IMMDeviceEnumerator*        _pEnumerator;
    uint32_t                    _audio_frame_id;                           //音频帧ID
    deque<string >              _enc_deque;                        //存储编码好的音频数据
    boost::thread               _loop_thr;
    bool                        _bexit;
};
