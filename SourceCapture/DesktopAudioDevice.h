#pragma once

#include "base_thread.h"
#include <list>
#include <string>
#include <stdint.h>
#include <mmdeviceapi.h>
#include <propsys.h>
#include <Functiondiscoverykeys_devpkey.h>
#ifdef _VS2013
#include <ks.h>
#include <ksmedia.h>
#endif // _VS2013
#include <Audioclient.h>
#include "../thirdpart/libsamplerate/samplerate.h"
#include "base_thread_mutex.h"
#include "EasyBuffer.h"

#if defined _DEBUG
#pragma comment(lib, "libsamplerateD.lib")
#else
#pragma comment(lib, "libsamplerate.lib")
#endif // _DEBUG

using namespace std;

#ifdef C64
typedef long long           PARAM;
typedef unsigned long long  UPARAM;
#else
typedef long                PARAM;
typedef unsigned long       UPARAM;
#endif


typedef char                *LPSTR;
typedef const char          *LPCSTR;
typedef wchar_t             *WSTR;
typedef const wchar_t       *CWSTR;
typedef TCHAR               *TSTR;
typedef const TCHAR         *CTSTR;

#define DEFAULT_SAMPLE_RATE 48000

#define KSAUDIO_SPEAKER_4POINT1     (KSAUDIO_SPEAKER_QUAD|SPEAKER_LOW_FREQUENCY)
#define KSAUDIO_SPEAKER_3POINT1     (KSAUDIO_SPEAKER_STEREO|SPEAKER_FRONT_CENTER|SPEAKER_LOW_FREQUENCY)
#define KSAUDIO_SPEAKER_2POINT1     (KSAUDIO_SPEAKER_STEREO|SPEAKER_LOW_FREQUENCY)

#define SafeRelease(var) if(var) {var->Release(); var = NULL;}

enum edges {
	edgeLeft = 0x01,
	edgeRight = 0x02,
	edgeTop = 0x04,
	edgeBottom = 0x08,
};

struct AudioDeviceInfo
{
	string strID;
	string strName;

	~AudioDeviceInfo() {strID.empty(); strName.empty();}
};

union TripleToLong
{
	LONG val;
	struct 
	{
		WORD wVal;
		BYTE tripleVal;
		BYTE lastByte;
	};
};

struct NotAResampler
{
	SRC_STATE	 *resampler;
	uint64_t     jumpRange;
};

enum AudioDeviceType {
	ADT_PLAYBACK,
	ADT_RECORDING
};

class CDesktopAudioDevice : CThread
{
public:
	CDesktopAudioDevice(void);
	~CDesktopAudioDevice(void);

	bool		Init(bool isPlayBack, const string devGUID = "Default");

	void		StartCapture();
	void		StopCapture(); 

    int         GetAudioBuffer(unsigned char* out_buf_, int size_);

	int			GetAudioDevices(list<AudioDeviceInfo *> &deviceList, AudioDeviceType deviceType, bool bConnectedOnly);
	bool		GetDefaultDevice(string &strVal, AudioDeviceType deviceType);

	bool		GetDefaultMicID(string &strVal);
	bool		GetDefaultSpeakerID(string &strVal);

protected:
	wchar_t*	MByteToWChar(uint32_t CodePage,LPCSTR lpcszSrcStr);
	char*		WCharToMByte(uint32_t CodePage,LPCWSTR lpcwszSrcStr);

	bool		GetNextBuffer(uint32_t &numFrames, int32_t &size_);

	void		FreeData();

    void        IsPlatFormSupport();

    int			QueryAudioBuffer(unsigned char* out_buf_, const int max_size_);

    void        execute();
	
protected:
	list<AudioDeviceInfo *> m_DeviceList;
	
	string					m_CurDeviceID;
	string					m_CurDeviceName;

	IMMDeviceEnumerator		*mmEnumerator_;
	IMMDevice				*mmDevice_;
	IAudioClient			*mmClient_;
	IAudioCaptureClient		*mmCapture_;

	uint32_t				m_SampleWindowSize;
	DWORD					m_InputChannelMask;
	WORD					m_InputChannels;
	uint32_t				m_InputSamplesPerSec;
	uint32_t				m_InputBufferSize;

	uint32_t				m_InputBitsPerSample;
	bool					m_bFloat;

	NotAResampler			*m_pResampler;
	double					m_ResampleRatio;

    uint8_t                 m_NextBuffer[19200];
	uint8_t					*m_pBlankBuffer, *m_pTempResampleBuffer;
	uint8_t					*m_pDataOutputBuffer;
	uint8_t					*m_pTmpBuffer;
    bool                    m_bPlatformSupport;     //标识平台是否支持(window vista以上版本)
    uint8_t                 _pcm_tmp_buffer[192000];
    CEasyBuffer*            _pEasyBuff;             
    BaseThreadMutex         m_mutext;
};

