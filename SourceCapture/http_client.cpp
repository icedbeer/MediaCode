#include "stdafx.h"
#include "http_client.h"
#include <assert.h>
#include <conio.h>
#include "session_mgr.h"
#include "EasyBuffer.h"
#include "base_code.h"

#ifdef _DEBUG
#pragma comment(lib, "libcurld.lib")
#else
#pragma comment(lib, "libcurl.lib")
#endif // _DEBUG
#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "wldap32.lib")

#define MEDIA_PACKET_SIZE       30000  //打包发送阀值大小

static size_t OnWriteData(void* buffer, size_t size, size_t nmemb, void* lpVoid)
{
    BOOST_DEBUG << "OnWriteData size: " << nmemb * size;
    CHttpClient* parent = (CHttpClient*)lpVoid;
    if (!parent)
    {
        return -1;
    }
    CEasyBuffer *pEasyBuffer = (CEasyBuffer *)parent->get_recv_easybuffer();
    if (NULL == pEasyBuffer || NULL == buffer)
    {
        return 0;
    }
    if (!pEasyBuffer->PushBack(buffer, size * nmemb))
    {
        BOOST_WARNING << "easy buffer push failed.";
        pEasyBuffer->reset();
    }
    return size * nmemb;
}

// CURLOPT_CLOSESOCKETFUNCTION
// socket断开,不在此处重连，由超时机制去重连
static int closesocket_cb(void *clientp, curl_socket_t item)
{
    BOOST_DEBUG << "closesocket_cb  sock:" << item;
    closesocket(item);
    return 0;
}

CHttpClient::CHttpClient(const string & strUrl, CSessionMgr *session_mgr_)
{
    BOOST_DEBUG << "CHttpClient construct." << strUrl;

    _curl = NULL;
    _multi_curl = NULL;
    _url_str = strUrl;
    _session_mgr = session_mgr_;
    _curl_headers = NULL;
    _pEasyBufferRecv = NULL;
    _bsending_flag = false;
    _pEasyBufferRecv = new(std::nothrow) CEasyBuffer(1024 * 1024);
    _pEasyBufferSend = new(std::nothrow) CEasyBuffer(1024 * 1024);
    _bfirst_msg = true;
    _bfirst_flag = true;
    _last_send_tv = ::GetTickCount();
    _bNeedReConnect = true;
    _bexit_flag = false;
    _pkt_seq_id = 0;

    if (_pEasyBufferSend && _pEasyBufferRecv && curl_init(_url_str) >= 0)
    {
        //开始线程
        _thread_thr = boost::thread(boost::bind(&CHttpClient::execute, this));
    }
}

CHttpClient::~CHttpClient(void)
{
    BOOST_DEBUG << "CHttpClient disconstruct.";
    _session_mgr.store(NULL);
    //首先终止线程
    if (0 == terminate())
    {
        //卸载curl资源
        curl_uninit();
        if (_pEasyBufferRecv)
        {
            delete _pEasyBufferRecv;
        }
        if (_pEasyBufferSend)
        {
            delete _pEasyBufferSend;
        }
    }
}

int32_t CHttpClient::terminate()
{
    BOOST_DEBUG << "CHttpClient terminate.";
    _bNeedReConnect = false;
    _bexit_flag = true;
    _thread_thr.join();

    return 0;
}

void CHttpClient::execute()
{
    int32_t     still_running(0);
    CURLcode    res(CURLE_OK);
    //获取执行结果
    int         msgs_left;
    CURLMsg *   msg;
    struct timeval timeout;
    int         rc; /* select() return code */
    CURLMcode   mc; /* curl_multi_fdset() return code */
    fd_set      fdread;
    fd_set      fdwrite;
    fd_set      fdexcep;
    int         maxfd = -1;
    long        curl_timeo = -1;

    //we start some action by calling perform right away 
    curl_multi_perform(_multi_curl, &still_running);
    BOOST_DEBUG << "CHttpClient start thread.";
    uint32_t max_wait_tv = 0;
    while (!_bexit_flag)
    {
        if (!_bsending_flag)
        {
            if (!send_protocal())
            {
                send_media_frames();
            }
        }
        do {
            FD_ZERO(&fdread);
            FD_ZERO(&fdwrite);
            FD_ZERO(&fdexcep);
            /* set a suitable timeout to play around with */
            timeout.tv_sec = 0;
            timeout.tv_usec = 10000;   //10ms
            curl_multi_timeout(_multi_curl, &curl_timeo);
            if (curl_timeo >= 0)
            {
                timeout.tv_sec = curl_timeo / 1000;
                if (timeout.tv_sec > 1)
                    timeout.tv_sec = 1;
                else
                    timeout.tv_usec = (curl_timeo % 1000) * 1000;
            }
            /* get file descriptors from the transfers */
            mc = curl_multi_fdset(_multi_curl, &fdread, &fdwrite, &fdexcep, &maxfd);
            if (mc != CURLM_OK)
            {
                BOOST_ERROR << "curl_multi_fdset() failed, code :" << mc;
                break;
            }
            if (maxfd == -1)
            {
#ifdef _WIN32
                Sleep(1);
                rc = 0;
#else
                /* Portable sleep for platforms other than Windows. */
                struct timeval wait = { 0, 1 * 1000 }; /* 1ms */
                rc = select(0, NULL, NULL, NULL, &wait);
#endif
            } else
            {
                rc = select(maxfd + 1, &fdread, &fdwrite, &fdexcep, &timeout);
            }
            switch (rc)
            {
            case -1:
                /* select error */
                break;
            case 0:
            default:
                /* timeout or readable/writable sockets */
                curl_multi_perform(_multi_curl, &still_running);
                break;
            }
        } while (/*still_running*/false);
        //获取执行结果
        while (!_bexit_flag && (msg = curl_multi_info_read(_multi_curl, &msgs_left)))
        {
            if (CURLMSG_DONE == msg->msg)
            {
                long response_code;
                res = curl_easy_getinfo(msg->easy_handle, CURLINFO_RESPONSE_CODE, &response_code);
                if (CURLE_OK == res && _session_mgr.load())
                {
                    string str;
                    str.append((char *)_pEasyBufferRecv->GetBuffer(), _pEasyBufferRecv->GetUsedSize());
                    _session_mgr.load()->update_state(str, response_code);
                    _pEasyBufferRecv->reset();
                    _pEasyBufferSend->reset();
                    _bsending_flag = false;

                    if (HTTP_MEDIA_CODE_ME == response_code)
                    {
                        //BOOST_DEBUG << "Get Http response code 230. send buf size:" << _pEasyBufferSend->GetUsedSize();
                        _pEasyBufferSend->reset();
                        _bsending_flag = false;
                    } 
                }
            }
        }
        if (_bNeedReConnect)
        {
            if (_last_send_tv > 0 && GetTickCount() - _last_send_tv >= 8000)
            {
                BOOST_DEBUG << "CHttpClient will reconnect. last send tv:" << _last_send_tv
                    << "; cur tv:" << GetTickCount() << "; media list size:" << _media_pkt_list.size()
                    << "; send buf used size:" << _pEasyBufferSend->GetUsedSize();
                //重置链接
                reinit();
                //we start some action by calling perform right away 
                curl_multi_perform(_multi_curl, &still_running);
            }
        }
    }
    BOOST_DEBUG << "CHttpClient end thread.";
}

void CHttpClient::reinit()
{
    curl_uninit();
    //切换IP地址
    curl_init(_url_str);
    update_last_send_time();
    _bsending_flag = false;

    _session_mgr.load()->relogin();
}

void CHttpClient::update_last_send_time()
{
    _last_send_tv = GetTickCount();
}

void CHttpClient::pre_release()
{
    if (_curl)
    {
        //curl_easy_setopt(_curl, CURLOPT_CLOSESOCKETFUNCTION, NULL);
        curl_easy_setopt(_curl, CURLOPT_CLOSESOCKETDATA, NULL);
        curl_easy_setopt(_curl, CURLOPT_WRITEDATA, NULL);
        curl_easy_setopt(_curl, CURLOPT_READDATA, NULL);
    }
}

void CHttpClient::clear_media_frame()
{
    BOOST_DEBUG << "clear  media frames.";
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    list<NetPacket *>::iterator itor = _media_pkt_list.begin();
    while (itor != _media_pkt_list.end())
    {
        if (*itor)
        {
            delete *itor;
        }
        itor++;
    }
    _media_pkt_list.clear();
}

int CHttpClient::add_media_frame(NetPacket *pkt_)
{
    if (!pkt_)
    {
        return -1;
    }
    if (_bfirst_flag) //第一个包保证为视频关键帧
    {
//         if (pkt_->has_video_pkt() && 1 != pkt_->video_pkt().frame_type()) //直至等到一个关键帧数据包
//         {
//             return -1;
//         }
        _bfirst_flag = false;
    }
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    //重置包序号
    NetPacket* pNewPkt = new NetPacket(*pkt_);
    pNewPkt->set_pkt_seq(++_pkt_seq_id);

    if (_media_pkt_list.size() >= 600)
    {
        BOOST_ASSERT_MSG(0, "采集端HTTP上传数据不及时,将丢弃部分数据包！");
        BOOST_DEBUG << "media pkt size too more : " << _media_pkt_list.size();
        //从头开始删除一个切片的长度，即遇到第二个关键帧的时候停止删除，防止后面的画面花屏
        uint32_t first_key_frame_id(0);
        NetPacket *pTmp = NULL;
        while (_media_pkt_list.size() >= 100)
        {
            pTmp = NULL;
            pTmp = _media_pkt_list.front();
            if (pTmp)
            {
                if (EPROTO_VIDEO == pTmp->proto_num()) //视频关键帧
                {
                    if (0 == first_key_frame_id)
                    {
                        first_key_frame_id = pTmp->video_pkt().seq();
                    } else if (first_key_frame_id == pTmp->video_pkt().seq())
                    {
                    } else
                    {
                        break;
                    }
                }
                delete pTmp;
            }
            //delete _media_pkt_list.front();
            _media_pkt_list.pop_front();
        }
//         if (_bfirst_msg)
//         {
//             _bfirst_msg = false;
//             if (_session_mgr.load())
//             {
//                 _session_mgr.load()->message_box("您的上行带宽不足或不稳定，将导致玩家视频卡顿，请检查您的宽带！");
//             }
//         }
        BOOST_INFO << "media pkt size too more after clear : " << _media_pkt_list.size();
    }
    _media_pkt_list.push_back(pNewPkt);

    return 0;
}

int CHttpClient::add_protocal_data(const uint8_t *data_, const uint32_t &data_len_,
    bool bwhen_exit_ /*= false*/)
{
    BOOST_DEBUG << "add_protocal_data  data len: " << data_len_
        << "; flag : " << bwhen_exit_?"true":"false";
    if (NULL == data_ || data_len_ <= 0)
    {
        return -1;
    }
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    short size_len = CGlobalConfig::small_2_big_short((short)data_len_);
    string str;
    str.append((char *)&size_len, sizeof(size_len));
    str.append((char *)data_, data_len_);

    if (bwhen_exit_)
    {
        _protocal_str_list.clear();
    }
    _protocal_str_list.push_back(str);
    _bNeedReConnect = !bwhen_exit_;

    return 0;
}

bool CHttpClient::send_protocal()
{
    if (0 == _protocal_str_list.size())
    {
        return false;
    }
    char buf[50] = { 0 };
    memcpy(buf, (*_protocal_str_list.begin()).c_str(), (*_protocal_str_list.begin()).length());
    NetPacket pkt;
    bool flag = pkt.ParseFromArray(buf, (*_protocal_str_list.begin()).length());
    flag = pkt.ParseFromArray(buf + 2, (*_protocal_str_list.begin()).length());
    PostProtocalData(*_protocal_str_list.begin());
    _protocal_str_list.pop_front();
    return true;
}

bool CHttpClient::send_media_frames()
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    if (_media_pkt_list.size() < 10 || 0 != _pEasyBufferSend->GetUsedSize())
    {
        return false;
    }
    if (put_http_data() >= 0)
    {
        return true;
    } else
    {
        return false;
    }
}

/**
* @brief Post私有协议包
* @return 返回是否Post成功
*/
int CHttpClient::PostProtocalData(const string &protocal_data_)
{
    BOOST_DEBUG << "PostProtocalData data len:" << protocal_data_.size();
    if (NULL == _curl)
    {
        return -1;
    }
    this->_protocal_str = protocal_data_;
    curl_multi_remove_handle(_multi_curl, _curl);
    
    CURLMcode res = CURLM_OK;
    curl_easy_setopt(_curl, CURLOPT_URL, _url_str.c_str());
    curl_easy_setopt(_curl, CURLOPT_POSTFIELDS, _protocal_str.c_str());
    curl_easy_setopt(_curl, CURLOPT_POSTFIELDSIZE, _protocal_str.size());

    //recv http server response data
    curl_easy_setopt(_curl, CURLOPT_WRITEFUNCTION, OnWriteData);
    curl_easy_setopt(_curl, CURLOPT_WRITEDATA, this);

    res = curl_multi_add_handle(_multi_curl, _curl);
    update_last_send_time();
    _bsending_flag = true;
    return res;
}

/**
* @brief Post私有协议包,退出时，阻塞执行完所有未处理的协议包
* @return 返回是否Post成功, 0:success
*/
int CHttpClient::PostEasyProtocalData(const string &protocal_data_)
{
    terminate();
    if (NULL == _curl)
    {
        return -1;
    }
    curl_multi_remove_handle(_multi_curl, _curl);
    curl_easy_reset(_curl);

    short data_len = protocal_data_.size();
    data_len = CGlobalConfig::small_2_big_short(data_len);
    string str;
    str.append((char *)&data_len, sizeof(data_len));
    str += protocal_data_;
    curl_easy_setopt(_curl, CURLOPT_URL, _url_str.c_str());
    curl_easy_setopt(_curl, CURLOPT_POSTFIELDS, str.c_str());
    curl_easy_setopt(_curl, CURLOPT_POSTFIELDSIZE, str.size());

    curl_easy_setopt(_curl, CURLOPT_TIMEOUT_MS, 10);
    curl_easy_perform(_curl);
     
//     CURLcode res = curl_easy_perform(_curl);
//     if (CURLE_OK == res)
//     {
//         long response_code;
//         res = curl_easy_getinfo(_curl, CURLINFO_RESPONSE_CODE, &response_code);
//         if (CURLE_OK == res && _pSessionMgr)
//         {
//             string str_res;
//             str_res.assign((char *)_pEasyBufferRecv->GetBuffer() + 4, _pEasyBufferRecv->GetUsedSize() - 8);
//             _pSessionMgr->update_state(str_res, response_code, false);
//         }
//    }
    return 0;
}

//发送put request上传数据
//@param response_  http回应码，如200 OK
//@param bAudioFlag_ false:send audio data; true:send video data;
//@ret success: >=0; failed: < 0
int32_t CHttpClient::put_http_data()
{
    CURLcode res = CURLE_OK;
    if (NULL == _curl)
    {
        return -1;
    }
    get_media_send_size();
    //BOOST_DEBUG << "put_http_data send buf len:" << _pEasyBufferSend->GetUsedSize();

    //获取此次数据发送字节数,保证关键帧在数据包的头部
    curl_multi_remove_handle(_multi_curl, _curl);

    //curl_easy_setopt(_curl, CURLOPT_READFUNCTION, OnReadMediaData);

    curl_easy_setopt(_curl, CURLOPT_POSTFIELDS, _pEasyBufferSend->GetBuffer());
    curl_easy_setopt(_curl, CURLOPT_POSTFIELDSIZE, _pEasyBufferSend->GetUsedSize());

    //recv http server response data
    curl_easy_setopt(_curl, CURLOPT_WRITEFUNCTION, OnWriteData);
    curl_easy_setopt(_curl, CURLOPT_WRITEDATA, this);

    curl_multi_add_handle(_multi_curl, _curl);
    _bsending_flag = true;
    update_last_send_time();
    return res;
}

int32_t CHttpClient::get_media_send_size()
{
    if (!_pEasyBufferSend || _media_pkt_list.size() == 0)
    {
        return 0;
    }
    _pEasyBufferSend->reset();

    short len(0);
    NetPacket *pkt = NULL;
    while (_media_pkt_list.size() > 0)
    {
        pkt = _media_pkt_list.front();
        //寻找视频关键帧为分割点
        if (EPROTO_VIDEO == pkt->proto_num())
        {
            if (_pEasyBufferSend->GetUsedSize() > 0 && 1 == pkt->video_pkt().split_seq()) //首帧既是key frame
            {
                break;
            }
        }
        if (_pEasyBufferSend->GetUsedSize() + pkt->ByteSize() >= MEDIA_PACKET_SIZE)
        {
            break;
        }
        len = pkt->ByteSize();
        len = CGlobalConfig::small_2_big_short(len);
        _pEasyBufferSend->PushBack(&len, sizeof(short));
        pkt->SerializeToArray(_pEasyBufferSend->GetFreeBuffer(),
            _pEasyBufferSend->GetBufferSize() - _pEasyBufferSend->GetUsedSize());
        _pEasyBufferSend->SetUsedSize(_pEasyBufferSend->GetUsedSize() + pkt->ByteSize());

        delete pkt;
        _media_pkt_list.pop_front();
    }
    return _pEasyBufferSend->GetUsedSize();
}

//curl_init("127.0.0.1:8080/upload");
int CHttpClient::curl_init(const string & strUrl)
{
    _curl = curl_easy_init();
    if (NULL == _curl)
    {
        return CURLE_FAILED_INIT;
    }
    //http请求头
    _curl_headers = curl_slist_append(_curl_headers, "Connection:keep-alive");
    _curl_headers = curl_slist_append(_curl_headers, "Expect:");
    _curl_headers = curl_slist_append(_curl_headers, "Content-Type:");
    //发送http请求头
    curl_easy_setopt(_curl, CURLOPT_HTTPHEADER, _curl_headers);
    curl_easy_setopt(_curl, CURLOPT_URL, strUrl.c_str());
    //curl_easy_setopt(_curl, CURLOPT_PUT, 1);
    curl_easy_setopt(_curl, CURLOPT_POST, 1);
    curl_easy_setopt(_curl, CURLOPT_TIMECONDITION, 30);
    curl_easy_setopt(_curl, CURLOPT_TIMEOUT, 8);
    curl_easy_setopt(_curl, CURLOPT_CLOSESOCKETFUNCTION, closesocket_cb);
    curl_easy_setopt(_curl, CURLOPT_CLOSESOCKETDATA, this);
    curl_easy_setopt(_curl, CURLOPT_NOSIGNAL, 1L);

    _multi_curl = curl_multi_init();

    return 0;
}

void CHttpClient::curl_uninit()
{
    pre_release();

    if (_curl_headers)
    {
        curl_slist_free_all(_curl_headers);
        _curl_headers = NULL;
    }
    if (_multi_curl)
    {
        if (_curl) curl_multi_remove_handle(_multi_curl, _curl);
        curl_multi_cleanup(_multi_curl);
        _multi_curl = NULL;
    }
    if (_curl)
    {
        curl_easy_cleanup(_curl);
        _curl = NULL;
    }
    _pEasyBufferRecv->reset();
    _pEasyBufferSend->reset();

    clear_media_frame();
}

