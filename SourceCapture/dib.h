/*********************************************************************************
*Copyright (C), 2005-2006, Cofire p2p. Co., Ltd.
*project name:  Cofire Zone v1.0
*Filename:		dib.h
*Author:		lian
*Version:		1.0
*Data:			2007-04
*Description:	DIB
*Funtion List:	
*��ʷ��¼:		2007-04 lian Created
*********************************************************************************/
#ifndef __DIB_H
#define __DIB_H

#include <winsock2.h>
#include <windows.h>


//////////////////////////////////////////////////////////////////////////
typedef struct  tagColor
{
	char blue;
	char green;
	char red;	
}Color_t;


//////////////////////////////////////////////////////////////////////////
class CDib
{
public:
	CDib();
	virtual ~CDib();

 public:
 	//bmp operate
 	bool create(int _width, int _height, int _bits = 32);
 	bool destroy();
 
 	//bmp data
 	BITMAPINFO* get_bmp_info() const;
 
 	void*	get_dib_bits();
 	bool	set_dib_bits(void* _data, long _len);
 	void	reset_dib_bits(int _value);
 
 	//bmp file
 	bool load(const char* _filename);
 	bool save(const char* _filename);

 	//draw
 	int bitblt(HDC hdc, int _xDest, int _yDest) const;
 	int bitblt(HDC hdc, int _xDest, int _yDest, int _width, int _height, \
 				  int _xSrc, int _ySrc) const;
 
 	int stretch_blt(HDC hdc, int _dX, int _dY, int _dWidth, int _dHeight, \
 		int _sX, int _sY, int _sWidth, int _sHeight) const;

 	//attr
 	bool is_valid() const;
 
 	int get_width() const;
 	int get_height() const;
 	
 	int get_palette_size() const;
 	int get_palette_size(int _bits) const;
 
 	int get_dib_size() const;
 	int get_dib_width() const;
 
 	int get_bitmap_data(LPBYTE data, int size);
 
 public:
 	static bool RGB2YUV420(LPBYTE RgbBuf,UINT nWidth,UINT nHeight,LPBYTE yuvBuf,unsigned long *len);
 	static void SaveRGB24AsBmp( BYTE * pData, int width, int height, const char * filename );
 	static int SaveRGB32AsBMP (BYTE * pRGBBuffer, int width, int height, const char * filename, int bpp = 32);
 	static void CDib::process_bri_con(unsigned char *dest, int dstride, unsigned char *src, int sstride,
 					int w, int h, int brightness, int contrast);
 	static void CDib::process_hue_sat(unsigned char *udst, unsigned char *vdst, unsigned char *usrc, unsigned char *vsrc, 
 					int dststride, int srcstride,int w, int h, float hue, float sat);
 protected:
 	void init();
 	void fini();
 		
 protected:
 	BYTE* m_pDib;//dib buffer
 	BYTE* m_pData;//buffer point to dib buffer
 	RGBQUAD* m_pRGB;//palette buffer
 	
 	BITMAPFILEHEADER m_BmpFileHeader;//bmp file header
 	BITMAPINFO* m_pBmpInfo;//bmp info
 	BITMAPINFOHEADER* m_pBmpInfoHeader;// bmp info header
 
 	bool m_valid;
};


#endif//__DIB_H
