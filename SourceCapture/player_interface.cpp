#include "stdafx.h"
#include "player_interface.h"
#include "play_mgr.h"
#include "boost_log.h"
#include "global_config.h"

CPlayerInterface::CPlayerInterface(ILiveEvent* event_)
{
    CREATE_GLOBAL_CONFIG();
    _pPlayerMgr = new CPlayMgr(event_);
}

CPlayerInterface::~CPlayerInterface(void)
{
    if (_pPlayerMgr) 
    {
        delete _pPlayerMgr;
    }
    DESTROY_GLOBAL_CONFIG();
}

int CPlayerInterface::add_sev_ip_info(const unsigned long ip_, const int port_, 
    const short type_, const short my_type_)
{
    BOOST_INFO << "CPlayerInterface::add_sev_ip_info() ip:" << ip_ << "; port_:" << port_ 
        << ";type_:" << type_ << "; my_type_:" << my_type_;
    if (_pPlayerMgr)
    {
        return _pPlayerMgr->add_sev_ip_info(ip_, port_, type_, my_type_);
    }
    else
    {
        return -1;
    }
}

int CPlayerInterface::del_sev_ip_info(const unsigned long ip_)
{
    BOOST_INFO << "CPlayerInterface::del_sev_ip_info() ip:" << ip_;
    if (_pPlayerMgr)
    {
        return _pPlayerMgr->del_sev_ip_info(ip_);
    }
    else
    {
        return -1;
    }
}

void CPlayerInterface::clear_sev_ip_info()
{
    BOOST_INFO << "CPlayerInterface::clear_sev_ip_info()";
    if (_pPlayerMgr)
    {
        _pPlayerMgr->clear_sev_ip_info();
    }
}

int CPlayerInterface::Login(const unsigned int uid_, const unsigned int room_id_,
    NETPROTO proto_ /*= _eHttp*/)
{
    BOOST_INFO << "CPlayerInterface::Login() uid:" << uid_ << "; room id:"<<room_id_;
    if (_pPlayerMgr)
    {
        srand(time(NULL));
        return _pPlayerMgr->Login(uid_, room_id_, proto_);
    }else
    { 
        return -1; 
    }
} 

int CPlayerInterface::Logout()
{
    BOOST_INFO << "CPlayerInterface::Logout()";
    try
    {
        if (_pPlayerMgr)
        {
            return _pPlayerMgr->Logout();
        }
        else
        {
            return -1;
        }
    }catch (...)
    {
        BOOST_ERROR << "CPlayerInterface::Logout() exception.";
    }
    return -1;
}

bool CPlayerInterface::is_playing()
{
    if (_pPlayerMgr)
    {
        return _pPlayerMgr->is_logined();
    } else
    {
        return false;
    }
}

void CPlayerInterface::set_volume(int volume_)
{
    if (volume_ < 0)
    {
        volume_ = 0;
    }else if (volume_ > 100)
    {
        volume_ =100;
    }
    GLOBAL_CONFIG()->_play_volume_scale = volume_ / 100.0f;
}

int CPlayerInterface::get_volume()
{
    return GLOBAL_CONFIG()->_play_volume_scale * 100;
}

void CPlayerInterface::save_bmp(const char* file_name_, const int len_)
{
    BOOST_INFO << "save bmp file:" << file_name_;
    if (_pPlayerMgr)
    {
        _pPlayerMgr->save_bmp(file_name_, len_);
    }
}

int CPlayerInterface::swscale_rgb24_img(unsigned char* src_data_, int src_width_, int src_height_, int src_format_, unsigned char* dst_data_, int dst_width_, int dst_height_, int dst_format_)
{
    if (_pPlayerMgr)
    {
        return _pPlayerMgr->swscale_rgb24_img(src_data_, src_width_, src_height_, src_format_, 
            dst_data_, dst_width_, dst_height_, dst_format_);
    } else
    {
        return -1;
    }
}

