#pragma once
#include <list>
#include <string>
#include "base_typedef.h"
#include "base_singleton.h"
#include "boost/atomic.hpp"
#include "boost_log.h"
#include "boost/typeof/typeof.hpp"
#include "boost/foreach.hpp"
#include "boost/shared_ptr.hpp"
#include "boost/scoped_ptr.hpp"
#include "boost/thread.hpp"
#include "boost/thread/mutex.hpp"
#include "boost/thread/recursive_mutex.hpp"
#include "boost/thread/lock_guard.hpp"
#include "boost/assert.hpp"
#include "boost/format.hpp"
#include "boost/tuple/tuple.hpp"
#include "boost/array.hpp"
#include "boost/any.hpp"
#include "boost/asio.hpp"
#include "boost/tuple/tuple.hpp"

#include "RUdpProtocol.pb.h"

using namespace std;
//using namespace MEDIA_MESSAGE;
using namespace ProtoNameSpace;

class /*__declspec(dllexport) */CGlobalConfig
{
public:
    CGlobalConfig(void);
    virtual ~CGlobalConfig(void);

#ifndef _MEDIA_PLAY_CLIENT
    void            enum_video_dev();
    //获取所有的视频采集设备，返回设备个数
    virtual int     enum_video_dev(__out char dev_[][256], __in const int& max_count_);
#endif // _MEDIA_PLAY_CLIENT
    //设置主播静态画面
    virtual void    set_anchor_img(const char* img_str_);

    static short    small_2_big_short(short num_);

private:
#define             MAX_VIDEO_DEV_COUNT     10

public:
    boost::atomic_int32_t               _preview_pic_width;
    boost::atomic_int32_t               _preview_pic_height;
    boost::atomic_int32_t               _codec_pic_width;
    boost::atomic_int32_t               _codec_pic_height;
    boost::atomic_int32_t               _frame_rate;
    boost::atomic_int32_t               _video_bitrate;
    boost::atomic_int32_t               _audio_bitrate;
    boost::atomic_int32_t               _key_frame_interval;        //关键帧间隔（秒）
    boost::atomic_int32_t               _cur_video_show_num;        //标志当前使用哪个视频框中的视频
    boost::atomic_uint32_t              _local_uid;
    char                                _video_dev_list[MAX_VIDEO_DEV_COUNT][256];
    boost::atomic_int32_t               _video_dev_count;
    char                                _anchor_img[256];
    boost::atomic_bool                  _send_def_img_flag;         //是否发送主播自定义图片标志
    boost::atomic_bool                  _benable_accompany;         //是否启用伴奏
    boost::atomic_bool                  _benable_mic;               //是否启用本地麦克风
    boost::atomic<float >               _play_volume_scale;         //播放音量级别，默认1.0,0.0为静音
    boost::atomic_bool                  _brecord_handle_init_flag;  //CRecordInterface是否初始化标识
    boost::recursive_mutex              _codec_mutex;               //编解码器锁，fffmpeg线程不安全

    static bool                         _big_endian;

private:
    boost::mutex                        _video_sev_ip_mutex; 
};

#define CREATE_GLOBAL_CONFIG		CSingleton<CGlobalConfig>::instance
#define GLOBAL_CONFIG				CSingleton<CGlobalConfig>::instance
#define DESTROY_GLOBAL_CONFIG		CSingleton<CGlobalConfig>::destroy

#define NOTHROW_NEW(ClassType, obj_, RECTION, ret_) \
    ClassType *obj_ = new(std::nothrow) ClassType; \
    if (NULL == obj_) \
            { \
        RECTION ret_; \
            }

#define BigLittleSwap16(A)  ((((uint16_t)(A) & 0xff00) >> 8) | \
    (((uint16_t)(A) & 0x00ff) << 8))

extern long long get_cur_time();