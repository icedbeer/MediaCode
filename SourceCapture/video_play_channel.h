#pragma once
#include <string>
#include <vector>
#include <winsock2.h>
#include <windows.h>
#include "global_config.h"
#include "live_event.h"

using namespace std;

#define MAX_VIDEO_FRAME_SIZE 640*480* 3

class CVideo_Decoder;
class CEasyBuffer;

class CVideoPlayChannel
{
public:
    CVideoPlayChannel(ILiveEvent* event_);
    ~CVideoPlayChannel(void);

    //put video encoded frame to buffer list
    virtual int         put_data(boost::shared_ptr<NetPacket> pkt_);
    //get video raw picture data, if video ts >= audio_ts_;
    virtual bool        play_frame(uint32_t audio_ts_, int &width_, int &height_);

    virtual int         set_video_hwnd(HWND hwnd_);

    virtual void        flush_codec();

    void                save_bmp(const char* file_name_, const int len_);

#ifdef _GM_CLIENT
    void                set_gm_event(IGMEvent* event_){ _pGmEvent = event_; };
#endif // _GM_CLIENT

    void                set_room_id(ROOM_ID room_id_){ _room_id = room_id_; };

    void                set_play_flag(bool bplay_);

protected:
    //open video decoder
    virtual int         open();
    //close video decoder
    virtual int         close();

    //将缓冲区中的数据帧组合成一个整体的视频帧，如果时间戳大于等于音频时间戳，则给dst_data_赋值，并返回true
    virtual bool        get_frame(uint32_t audio_ts_, string &data_);

    void                clear_frames();

protected:
    ILiveEvent*                 _pEvent;
    CVideo_Decoder*             _pDecoder;
    vector<boost::shared_ptr<NetPacket> >  _frame_list;
    boost::recursive_mutex      _mutex;
    SEQ_ID                      _last_key_frame_id;
    HWND                        _video_play_wnd;
    HDC                         _video_paly_hdc;
    int                         _width;
    int                         _height;
    uint8_t                     _raw_frame_buf[MAX_VIDEO_FRAME_SIZE];
    string                      _bmp_file_name;
    uint32_t                    _last_play_tv;
    ROOM_ID                     _room_id;
    boost::atomic_bool          _bplay;
#ifdef _GM_CLIENT
    IGMEvent*                   _pGmEvent;
#endif // _GM_CLIENT
};
