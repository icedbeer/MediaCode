#pragma once
#include "live_event.h"
#include "LoadLibrary_Interface.h"

class CRecordMgr;

class INTERFACE_API CRecordInterface
    : public IRecordInterface
{
public:
    CRecordInterface(ILiveEvent* event_);
    ~CRecordInterface();

    //添加服务器地址信息
    //@ ip_ : 服务器IP
    //@ type_ : 此IP运营商类型
    //@ my_type_ : 本机IP运营商类型
    int                     add_sev_ip_info(const unsigned long ip_, const int port_,
        const short type_, const short my_type_);
    int                     del_sev_ip_info(const unsigned long ip_);
    void                    clear_sev_ip_info();

    int                     Login(const unsigned int uid_, const unsigned int room_id_, NETPROTO proto_ = _eHttp);
    int                     Login(const unsigned int uid_, const unsigned int room_id_,
        const char* ip_, const short port_, NETPROTO proto_ = _eHttp);
    int                     Logout(const unsigned int room_id_);

    int                     preview_video(int video_show_num_, int dev_id_);
    //打开音频输入设备MIC，默认打开伴奏，可以通过set_acc_flag（false）关闭伴奏
    virtual int             preview_audio(int dev_id_);

    void                    set_video_show_num(int num_);
    int                     get_video_show_num();

    //获取视频采集设备列表
    int                     get_video_dev_list(char video_dev_[][256], const int max_size_ = 10);
    //获取视频采集设备个数
    int                     get_video_dev_count();
    
    int&                    get_preview_video_width();
    int&                    get_preview_video_height();

    //设置主播头像图片
    void                    set_default_anchor_img(const char* path_name_, const int& max_len_);
    void                    set_default_anchor_img_flag(bool flag_);

    //设置是否启用伴奏标志
    void                    set_acc_flag(bool bflag_);
    //设置是否启用麦克风标志
    void                    set_mic_flag(bool bflag_);

    bool                    is_logined(const unsigned int room_id_);

    //设置视频采集编解码参数
    void                    set_global_cfg(int cap_width_, int cap_height_,
        int codec_width_, int codec_height_, int gop_time_, int video_bitrate_,
        int audio_bitrate_, char* anchor_img_str_);
    void                    set_live_event(ILiveEvent* event_);

private:
    CRecordMgr*     _pRecordMgr;
};

