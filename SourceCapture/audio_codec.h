#pragma once

enum ENUM_AUDIO_CODEC{
    EAUDIOCODEC_AAC = 1,
    EAUDIOCODEC_MP3,
};

struct AVCodecContext;
struct AVFrame;
struct AVCodec;
class CEasyBuffer;

//CAudioEncoder 不提供从采样的功能，需上层提供open指定的pcm数据进行编码
class CAudioEncoder
{
public:
    CAudioEncoder(void);
    ~CAudioEncoder(void);

    virtual int         open(int sample_rate_, int channel_num_, int bit_per_sample_, 
        ENUM_AUDIO_CODEC codec_id_ = EAUDIOCODEC_AAC);
    virtual int         close();

    virtual int         enc(short *pcm_data_, int pcm_data_size_, 
        short *out_data_, int max_out_data_size_);

protected:
    AVCodec             *_pAvCodec;
    AVCodecContext      *_pCodecContext;
    AVFrame             *_pAudioSourceFrame;
    CEasyBuffer         *_pEasyBuffer;          //PCM数据缓冲区
    unsigned char       *_pcm_buffer;           //pcm buffer

    int                 _nSampleRate;
    int                 _nChannelCount;
    int                 _nBitPerSample;
    ENUM_AUDIO_CODEC    _eCodecId;
    int                 _nSizePerFrame;       //每帧的字节数
};

//CAudioEncoder 不提供从采样的功能，需上层提供open指定的pcm数据进行编码
class CAudioDecoder
{
public:
    CAudioDecoder(void);
    ~CAudioDecoder(void);

    virtual int         open(ENUM_AUDIO_CODEC codec_id_ = EAUDIOCODEC_AAC);
    virtual int         close();

    //需以帧为单位进行解码
    virtual int         dec(unsigned char *src_data_, int src_size_, 
        unsigned char *out_data_, int max_out_data_size_);

    virtual void        flush_codec();

protected:
    AVCodec             *_pAvCodec;
    AVCodecContext      *_pCodecContext;
    AVFrame             *_pAudioSourceFrame;
    CEasyBuffer         *_pEasyBuffer;          //PCM数据缓冲区
    unsigned char       *_pcm_buffer;           //pcm buffer

    int                 _nSampleRate;
    int                 _nChannelCount;
    int                 _nBitPerSample;
    ENUM_AUDIO_CODEC    _eCodecId;
    int                 _nSizePerFrame;       //每帧的字节数
};
