#include "stdafx.h"
#include "audio_record_channel.h"
#include "media_record_thread.h"
#include "Sound.h"
#include "DesktopAudioDevice.h"
#include "audio_mixer.h"
#include "audio_codec.h"
//#include "media_packet.h"
#include "base_timer_value.h"

//using namespace MediaMessage;

CAudioRecordChannel::CAudioRecordChannel(CMediaRecordThread *media_thr_)
{
    _pMicDevice = NULL;
    _pDesktopDevice = NULL;
    _pAudioMixer = NULL;
    _signal_level = 0;
    _baccompany_flag = true;
    _mic_volume = 1.0f;
    _accompany_volume = 1.0f;
    _volume = 1.0f;
    _pAudioEncoder = NULL;
    _last_cap_mic_tv = 0;
    _breopen_flag = false;
    _audio_frame_id = 0;
    _bexit = false;

    _pRecThread = media_thr_;

    HRESULT hr = S_OK;
    _pEnumerator = NULL;
    ::CoInitialize(NULL);

    hr = CoCreateInstance(
        __uuidof(MMDeviceEnumerator), NULL,
        CLSCTX_ALL, __uuidof(IMMDeviceEnumerator),
        (void**)&_pEnumerator);
    if (SUCCEEDED(hr))
    {
        hr = _pEnumerator->RegisterEndpointNotificationCallback((IMMNotificationClient*)this);
        if (hr==S_OK)
        {
            //"注册成功";
        }else
        {
            //"注册失败";
        }
    }
}

CAudioRecordChannel::~CAudioRecordChannel(void)
{
    if (_pEnumerator)
    {
        _pEnumerator->UnregisterEndpointNotificationCallback((IMMNotificationClient*)this);
        _pEnumerator->Release();
    }
    close();
    ::CoUninitialize();
}

int CAudioRecordChannel::open(int dev_id_, bool baccompaniment_)
{
    BoostLog::Log(info, "CAudioRecordChannel::open() dev_id:%d, accompaniment:%s",
        dev_id_, baccompaniment_?"true":"false");

    do 
    {
        boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
        if (_pMicDevice || _pDesktopDevice || _pAudioMixer || _pAudioEncoder)
        {
            close();
        }
        //输入
        _pMicDevice = new(std::nothrow) CSoundChannel;
        if (_pMicDevice)
        {
            if(!_pMicDevice->m_bOpenAudio((dev_id_ >= 0)?dev_id_:0, CSoundChannel::e_Recorder, 2, 48000, 16))
            {
                BOOST_WARNING <<"CAudioRecordChannel::open m_bOpenAudio failed! ";
                if (_pMicDevice)
                {
                    delete _pMicDevice;
                    _pMicDevice = NULL;
                }
            }else
            {
                _pMicDevice->m_bSetBuffers(MAX_PCM_BUFFER_SIZE, 20);
            }
        }

        //输出
        _pDesktopDevice = new(std::nothrow) CDesktopAudioDevice;
        if(NULL == _pDesktopDevice || !_pDesktopDevice->Init(true))
        {
            BOOST_WARNING <<"CAudioRecordChannel::open _pDesktopDevice->Init(true) failed!";
            if (_pDesktopDevice)
            {
                delete _pDesktopDevice;
                _pDesktopDevice = NULL;
            }
            if (!_pMicDevice)
            {
                //如果mic也为空，则不再继续
                break;
            }
        }
        if (_pDesktopDevice)
        {
            _pDesktopDevice->StartCapture();
        }
        //混音器
        _pAudioMixer = new(std::nothrow) HeAudioMixer(MAX_PCM_BUFFER_SIZE/2);
        if (NULL == _pAudioMixer)
        {
            break;
        }
        //编码器
        _pAudioEncoder = new(std::nothrow) CAudioEncoder;
        if(NULL == _pAudioEncoder || _pAudioEncoder->open(48000, 2, 16, EAUDIOCODEC_AAC) < 0)
        {
            BOOST_WARNING <<"CAudioRecordChannel::open _pAudioEncoder->open failed!";
            break;
        }

        //success
        _baccompany_flag = baccompaniment_;

        _bexit = false;
        _loop_thr = boost::thread(boost::bind(&CAudioRecordChannel::execute, this));

        return 0;

    } while (false);
    BOOST_ERROR <<"CAudioRecordChannel::open failed!";
    close();
    return -1;
}

int CAudioRecordChannel::open(char *dev_name_, bool baccompaniment_)
{
    BoostLog::Log(info, "CAudioRecordChannel::open() dev_name:%s, accompaniment:%s",
        dev_name_, baccompaniment_?"true":"false");
    do 
    {
        boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
        if (_pMicDevice || _pDesktopDevice ||_pAudioMixer || _pAudioEncoder)
        {
            close();
        }

        //如果设备名为NULL，则打开默认音频输入设备.
        bool flag = false;
        _pMicDevice = new(std::nothrow) CSoundChannel;
        if (_pMicDevice)
        {
            if (NULL == dev_name_)
            {
                flag = _pMicDevice->m_bOpenAudio(0, CSoundChannel::e_Recorder, 2, 48000, 16);
            }else
            {
                flag = _pMicDevice->m_bOpenAudio(dev_name_, CSoundChannel::e_Recorder, 2, 48000, 16);
            }
        }
        if (flag)
        {
            _pMicDevice->m_bSetBuffers(MAX_PCM_BUFFER_SIZE, 20);
        }else
        {
            BOOST_WARNING <<"CAudioRecordChannel::open failed!";
            if (_pMicDevice)
            {
                delete _pMicDevice;
                _pMicDevice = NULL;
            }
        }
        //输出
        _pDesktopDevice = new(std::nothrow) CDesktopAudioDevice;
        if (NULL == _pDesktopDevice  || !_pDesktopDevice->Init(true))
        {
            if (_pDesktopDevice)
            {
                delete _pDesktopDevice;
                _pDesktopDevice = NULL;
            }
            if (!_pMicDevice)
            {
                //如果mic也为空，则不再继续
                break;
            }
        }
        if (_pDesktopDevice)
        {
            _pDesktopDevice->StartCapture();
        }
        //混音器
        _pAudioMixer = new(std::nothrow) HeAudioMixer(MAX_PCM_BUFFER_SIZE/2);

        //编码器
        _pAudioEncoder = new(std::nothrow) CAudioEncoder;
        if(NULL == _pAudioEncoder || _pAudioEncoder->open(48000, 2, 16, EAUDIOCODEC_AAC) < 0)
        {
            BOOST_WARNING <<"CAudioRecordChannel::open _pAudioEncoder->open failed!";
            break;
        }
        //success
        _baccompany_flag = baccompaniment_;

        _bexit = false;
        _loop_thr = boost::thread(boost::bind(&CAudioRecordChannel::execute, this));

        return 0;
    } while (false);
    
    BOOST_ERROR << "CAudioRecordChannel::open failed.";
    close();
    return -1;
}

//停止采集，并关闭设备
int CAudioRecordChannel::close()
{
    BOOST_WARNING <<"CAudioRecordChannel::close";

    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);

    _bexit = true;
    if (_loop_thr.joinable())
    {
        _loop_thr.join();
    }
    if (_pMicDevice)
    {
        delete _pMicDevice;
        _pMicDevice = NULL;
    }
    if (_pDesktopDevice)
    {
        delete _pDesktopDevice;
        _pDesktopDevice = NULL;
    }
    if (_pAudioMixer)
    {
        delete _pAudioMixer;
        _pAudioMixer = NULL;
    }
    if (_pAudioEncoder)
    {
        delete _pAudioEncoder;
        _pAudioEncoder = NULL;
    }
    return 0;
}

bool CAudioRecordChannel::loop()
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);

    if (_breopen_flag)
    {
        _breopen_flag.store(false);
        reopen();
    }

    //尚未打开任何设备
    if ((NULL == _pMicDevice) & (NULL == _pDesktopDevice))
    {
        return false;
    }
    memset(_pcm_buffer, 0, sizeof(_pcm_buffer));
    SrcAudioDataList prt_list;
    if (_pMicDevice)
    {
        memset(_pcm_buffer, 0, sizeof(_pcm_buffer));
        //采集数据，每帧10ms
        if(_pMicDevice->m_bRead(_pcm_buffer, MAX_PCM_BUFFER_SIZE))
        {
            prt_list.push_back((int16_t *)_pcm_buffer);
            _last_cap_mic_tv = ::GetTickCount();
        }else
        {
            if (_last_cap_mic_tv > 0 && ::GetTickCount() - _last_cap_mic_tv >= 1000)
            {
                //重新打开设备
                delete _pMicDevice;
                _pMicDevice = NULL;

                //如果设备名为NULL，则打开默认音频输入设备.
                bool flag = false;
                _pMicDevice = new(std::nothrow) CSoundChannel;
                if (_pMicDevice)
                {
                    flag = _pMicDevice->m_bOpenAudio(0, CSoundChannel::e_Recorder, 2, 48000, 16);
                }
                if (flag)
                {
                    _pMicDevice->m_bSetBuffers(MAX_PCM_BUFFER_SIZE, 20);
                }else
                {
                    BOOST_WARNING <<"CAudioRecordChannel::open failed!";
                    if (_pMicDevice)
                    {
                        delete _pMicDevice;
                        _pMicDevice = NULL;
                    }
                }
                _last_cap_mic_tv = 0;
            }
            return false;
        }
        if (!GLOBAL_CONFIG()->_benable_mic) //如果不启用麦克风，则置为静音
        {
            memset(_pcm_buffer, 0, MAX_PCM_BUFFER_SIZE);
        }
    }
    //伴奏
    if (_baccompany_flag)
    {
        //输出声卡音频采集
        if (_pDesktopDevice)
        {
            int32_t ret(0);
            if((ret =  _pDesktopDevice->GetAudioBuffer(_pcm_tmp_buffer, MAX_PCM_BUFFER_SIZE)) >= 0)
            {
                //首先调整伴奏音量后
                HeAudioMixer::audio_scale((int16_t *)_pcm_tmp_buffer, 
                    ret/sizeof(int16_t ), _accompany_volume);
                prt_list.push_back((int16_t *)_pcm_tmp_buffer);
                if (!GLOBAL_CONFIG()->_benable_accompany) //如果不启用伴奏，则置为静音
                {
                    memset(_pcm_tmp_buffer, 0, MAX_PCM_BUFFER_SIZE);
                }
            }

        }
        //混音
        int32_t dst_len(MAX_PCM_BUFFER_SIZE/2);
        if (NULL == _pAudioMixer || !_pAudioMixer->mixer(prt_list, 
            MAX_PCM_BUFFER_SIZE/sizeof(int16_t ), (int16_t *)_pcm_buffer, dst_len))
        {
            //BOOST_DEBUG << "audio loop mixer failed!";
            return false;
        }
    }
    //将音频数据置为静音
    if (GLOBAL_CONFIG()->_send_def_img_flag)
    {
        memset(_pcm_buffer, 0, sizeof(_pcm_buffer));
    }
    //音量变化
    if (1.0f != _volume)
    {
        HeAudioMixer::audio_scale((int16_t *)_pcm_buffer, MAX_PCM_BUFFER_SIZE/sizeof(int16_t ), _volume);
    }
    ////获取音量值
    _signal_level = HeAudioMixer::get_average_signal_level((int16_t *)_pcm_buffer,
        MAX_PCM_BUFFER_SIZE/sizeof(int16_t ));

    if (-1 == GLOBAL_CONFIG()->_cur_video_show_num)
    {
        return false;
    }
    //编码
    int len = _pAudioEncoder->enc((int16_t *)_pcm_buffer, MAX_PCM_BUFFER_SIZE/sizeof(int16_t ), 
        (int16_t *)_encode_buffer, sizeof(_encode_buffer)/sizeof(int16_t ));
    if (len > 0)
    {
        //发送
        NetPacket* pkt = new NetPacket;
        pkt->set_proto_num(EPROTO_AUDIO);
        //pkt->set_pkt_seq(_pRecThread->get_new_pkt_id());
        pkt->set_room_id(0);
        pkt->set_user_id(0);
        pkt->set_send_tm(get_cur_time());
        pkt->add_audio_pkt();
        pkt->mutable_audio_pkt(0)->set_seq(++_audio_frame_id);
        pkt->mutable_audio_pkt(0)->set_timestamp(get_cur_time());
        pkt->mutable_audio_pkt(0)->set_media_data(_encode_buffer, len);
        _pRecThread->send_media_frame(pkt);
        delete pkt;
    }
    return true;
}

HRESULT STDMETHODCALLTYPE CAudioRecordChannel::OnDefaultDeviceChanged(EDataFlow flow, ERole role, LPCWSTR pwstrDeviceId)
{
    _breopen_flag = true;
    return S_OK;
}

void CAudioRecordChannel::execute()
{
    while (!_bexit)
    {
        if (!loop())
        {
            Sleep(1);
        }
    }
}
void CAudioRecordChannel::reopen()
{
    if (_pMicDevice)
    {
        delete _pMicDevice;
        _pMicDevice = NULL;
    }
    if (_pDesktopDevice)
    {
        delete _pDesktopDevice;
        _pDesktopDevice = NULL;
    }
    //输入
    _pMicDevice = new(std::nothrow) CSoundChannel;
    if (_pMicDevice)
    {
        if (!_pMicDevice->m_bOpenAudio(0, CSoundChannel::e_Recorder, 2, 48000, 16))
        {
            BOOST_WARNING << "CAudioRecordChannel::open m_bOpenAudio failed! ";
            if (_pMicDevice)
            {
                _pMicDevice->m_bCloseAudio();
                delete _pMicDevice;
                _pMicDevice = NULL;
            }
        } else
        {
            _pMicDevice->m_bSetBuffers(MAX_PCM_BUFFER_SIZE, 20);
        }
    }

    //输出
    _pDesktopDevice = new(std::nothrow) CDesktopAudioDevice;
    if (NULL == _pDesktopDevice || !_pDesktopDevice->Init(true))
    {
        BOOST_WARNING << "CAudioRecordChannel::open _pDesktopDevice->Init(true) failed!";
        if (_pDesktopDevice)
        {
            delete _pDesktopDevice;
            _pDesktopDevice = NULL;
        }
        if (!_pMicDevice)
        {
            //如果mic也为空，则不再继续
            return;
        }
    }
    if (_pDesktopDevice)
    {
        _pDesktopDevice->StartCapture();
    }
}