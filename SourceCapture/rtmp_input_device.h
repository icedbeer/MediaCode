 #pragma once

#include <atlcomcli.h>
#include <stdio.h>
#include <string>

#include "base_timer_value.h"
#include "base_thread_mutex.h"
#include "base_guard.h"
#include "type_def.h"
#include "video_device.h"
#include "EasyBuffer.h"

#ifdef __cplusplus
extern "C" {
#endif
#include "libavcodec/avcodec.h"
#include "libavutil/avutil.h"
#include "libavutil/avstring.h"
#include "libavutil/pixdesc.h"
#include "libavutil/imgutils.h"
#include "libavutil/dict.h"
#include "libavutil/parseutils.h"
#include "libavutil/samplefmt.h"
#include "libswresample/swresample.h"
#include "libavutil/opt.h"
#include "libavcodec/avfft.h"
#include "libavutil/audioconvert.h"
#include "libavutil/mathematics.h"

#ifdef __cplusplus
};
#endif

//该类中目前仅使用ffmpeg进行音频的解码而已
#pragma comment(lib, "avcodec.lib")
#pragma comment(lib, "avutil.lib")
#pragma comment(lib, "swresample.lib")

using namespace std;

#define AVCODEC_MAX_AUDIO_FRAME_SIZE 19200
#define MP3_AUDIO_INPUT_BUFFER_SIZE  4092


class RtmpCaptureDataCallback
{
public :
	virtual void OnIncomingVideoFrame( uint32_t ts, uint8_t* data, int size, uint8_t frame_type ) = 0;
	virtual void OnIncomingAudioFrame( uint32_t ts, uint8_t* data, int size, uint8_t data_type ) = 0;
	virtual void RtmpCaptureError( int code, char* info )=0;
	virtual void OnIncomingFirstVideoFrame() = 0;
	virtual void OnIncomingFirstAudioFrame() = 0;
};


/************************************************************************/
/* 接收RTMP协议数据，音视频数据存储在本类的缓冲区中，
   视频直接存储ES流（加入头之后）;
   音频做实时的转码，并存储PCM数据；                                    */
/************************************************************************/
class __declspec(dllexport) RtmpInputDevice : public IVideoInDevice, public RtmpCaptureDataCallback
{
public:
	RtmpInputDevice( int32_t dev_id, uint16_t code_id );
	~RtmpInputDevice();
	virtual int		init_device( );
	virtual int		open( uint16_t frame = DEFAULT_FRAME_RATE );
	virtual void	close( );
	virtual bool	read(uint8_t* data, uint32_t &data_size, int& frame_type, int& data_type, uint64_t& ts );

	virtual void	OnIncomingVideoFrame( uint32_t ts, uint8_t* data, int size, uint8_t frame_type ) ;
	virtual void	OnIncomingAudioFrame( uint32_t ts, uint8_t* data, int size, uint8_t data_type ) ;
	virtual void	RtmpCaptureError( int code, char* info );

	//做回调通知使用
	virtual void    OnIncomingFirstVideoFrame();
	virtual void    OnIncomingFirstAudioFrame();

	virtual int     get_video_data(uint8_t *buffer, uint32_t uLen, int& frame_type, uint64_t& ts );
	virtual int     get_audio_data(uint8_t *buffer, uint32_t uLen);

	//音频解码
	virtual int		InitDecoder();
	virtual int 	UnInitDecoder();
	virtual int 	ResetDecoder();
	virtual int 	decode_data( );

protected:
	BaseThreadMutex mutex_;

	CEasyBuffer		audio_buffer;
	CEasyBuffer		audio_decode_buffer; //PCM data
	CEasyBuffer		video_buffer;

	//音频解码，由RTMP接收到的音频数据目前限制为MP3格式，需要进行解码，再编码为AAC格式
	AVCodec			*m_pCodec;
	AVCodecContext	*m_pContext;
	SwrContext		*m_pAudioResample;
	AVFrame			*m_pFrame;
	//解码得到的PCM数据的临时存储
	uint8_t			audio_pcm_buffer[AVCODEC_MAX_AUDIO_FRAME_SIZE];

	struct video_head
	{
		uint32_t	size;
		int			frame_type;
		uint64_t	ts;
	};
};

