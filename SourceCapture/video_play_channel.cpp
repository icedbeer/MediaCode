#include "stdafx.h"
#include "video_play_channel.h"
#include "video_codec.h"
#include "EasyBuffer.h"
#include "dib.h"
#include <conio.h>
#include "global_config.h"

CVideoPlayChannel::CVideoPlayChannel(ILiveEvent* event_)
:_pEvent(event_)
{
    _last_key_frame_id = 0;
    _video_play_wnd = NULL;
    _video_paly_hdc = NULL;
    _width = _height = 0;
    _pDecoder = NULL;
    _room_id = 0;
    _bplay = false;
#ifdef _GM_CLIENT
    _pGmEvent = NULL;
#endif // _GM_CLIENT
    memset(_raw_frame_buf, 0, sizeof(_raw_frame_buf));
    open();
}

CVideoPlayChannel::~CVideoPlayChannel(void)
{
    close();
    if (_video_play_wnd && _video_paly_hdc)
    {
        ::ReleaseDC(_video_play_wnd, _video_paly_hdc);
    }
}

//put video encoded frame to buffer list
int CVideoPlayChannel::put_data(boost::shared_ptr<NetPacket> pkt_)
{
    boost::lock_guard<boost::recursive_mutex > lock_(_mutex);
    if (_frame_list.size() > 1000) //15fps
    {
        _frame_list.erase(_frame_list.begin());
    }
    _frame_list.push_back(pkt_);
    return _frame_list.size();
}

//get video raw picture data, if video ts >= audio_ts_; 
bool CVideoPlayChannel::play_frame(uint32_t audio_ts_, int &width_, int &height_)
{
    if (!_pDecoder || !_bplay)
    {
        return false;
    }
    boost::lock_guard<boost::recursive_mutex > lock_(_mutex);
    //截图
    if (_bmp_file_name.size() > 0)
    {
        uint8_t val;
        for (int h(0); h < height_; h++)
        {
            for (int w(0); w < width_; w++)
            {
                val = _raw_frame_buf[width_ * h * 3 + w * 3];
                _raw_frame_buf[width_ * h * 3 + w * 3] = _raw_frame_buf[width_ * h * 3 + w * 3 + 2];;
                _raw_frame_buf[width_ * h * 3 + w * 3 + 2] = val;
            }
        }
        uint8_t tmp_buf[3] = { 0 };
        int i(0);
        while (i < width_ * height_ / 2)
        {
            memcpy(tmp_buf, _raw_frame_buf + i * 3, 3);
            memcpy(_raw_frame_buf + i * 3, _raw_frame_buf + (width_ * height_ - i - 1) * 3, 3);
            memcpy(_raw_frame_buf + (width_ * height_ - i - 1) * 3, tmp_buf, 3);
            i++;
        }
        for (int h(0); h < height_; h++)
        {
            for (int w(0); w < width_ / 2; w++)
            {
                memcpy(tmp_buf, _raw_frame_buf + width_ * h * 3 + w * 3, 3);
                memcpy(_raw_frame_buf + width_ * h * 3 + w * 3,
                    _raw_frame_buf + width_ * h * 3 + (width_ - w - 1) * 3, 3);
                memcpy(_raw_frame_buf + width_ * h * 3 + (width_ - w - 1) * 3, tmp_buf, 3);
            }
        }

        CDib::SaveRGB24AsBmp(_raw_frame_buf, width_, height_, _bmp_file_name.c_str());
        _bmp_file_name.clear();
    }
    //解码播放
    string str;
    int ret(0);
    if (get_frame(audio_ts_, str))
    {
        if (0 == _last_play_tv && _pEvent)
        {
            _pEvent->on_begin_playing_cb();    //通知开始播放
        }
        _last_play_tv = ::GetTickCount();
#ifndef  _NODES_DEBUG
        //decode
        int frame_type(0), ret(0);
        if ((ret = _pDecoder->decode((uint8_t *)str.data(), str.size(), _raw_frame_buf, 
            MAX_VIDEO_FRAME_SIZE, &frame_type, width_, height_, true)) > 0)
        {
            if (_pEvent)
            {
                static int num(0);
                _pEvent->on_video_play_cb(_raw_frame_buf, width_, height_);
            }
#ifdef _GM_CLIENT
            if (_pGmEvent)
            {
                _pGmEvent->on_video_play_cb(_raw_frame_buf, width_, height_, _room_id);
            }
#endif // _GM_CLIENT
            //if (this->_width != width_ || this->_height != height_)
            //{
            //    _dib.destroy();
            //    if(!_dib.create(width_, height_, 24))
            //    {
            //        BOOST_ERROR << "create dib failed." << width_ <<"; " << height_;
            //        return true;
            //    }
            //    this->_width = width_;
            //    this->_height = height_;
            //}
            //if(_dib.set_dib_bits(_raw_frame_buf, ret) && _video_paly_hdc)
            //{
            //    _dib.stretch_blt(_video_paly_hdc, 0, 0, _width, _height, 0, 0, _width, _height);
            //}
            return true;
        }
#endif // _NODES_DEBUG
    }
    return false;
}

void CVideoPlayChannel::set_play_flag(bool bplay_)
{
    _bplay = bplay_;
    //_bplay.store(bplay_);
}

int CVideoPlayChannel::open()
{
#ifndef  _NODES_DEBUG
    _last_play_tv = 0;
    _pDecoder = new(std::nothrow) CVideo_Decoder();
    if (NULL == _pDecoder)
    {
        return -1;
    }
    if(_pDecoder->init() < 0)
    {
        BOOST_ERROR << "init video decoder failed.";
        delete _pDecoder;
        _pDecoder = NULL;
        return -1;
    }
#endif // _NODES_DEBUG
    return 0;
}

int CVideoPlayChannel::close()
{
#ifndef _NODES_DEBUG
    clear_frames();
    if (_pDecoder)
    {
        delete _pDecoder;
        _pDecoder = NULL;
    }
#endif // _NODES_DEBUG
    return 0;
}

//将缓冲区中的数据帧组合成一个整体的视频帧，如果时间戳大于等于音频时间戳，则给dst_data_赋值，并返回true
bool CVideoPlayChannel::get_frame(uint32_t audio_ts_, string &data_)
{
    if (_frame_list.empty()) //no data
    {
        return false;
    }
    data_.clear();
    VideoPacket pkt;
    BOOST_AUTO(itor, _frame_list.begin());
    bool flag = false;
    //首先保证数据的正确性
    while (itor != _frame_list.end())
    {
        pkt = (*itor)->video_pkt();
        if (pkt.timestamp() > (audio_ts_ - 0) || pkt.split_count() > _frame_list.size())
        {
            if (0 == audio_ts_ && get_cur_time() - _last_play_tv >= (1000.0f / GLOBAL_CONFIG()->_frame_rate))
            {
            } else
            {
                //视频时间戳过快，或者该帧尚未全部到达
                return false;
            }
        }
        //first time,get key frame id
        if (0 == _last_key_frame_id)
        {
            if (/*1 != pkt.frame_type*/!IS_X264_TYPE_I(pkt.frame_type()))
            {
                _frame_list.erase(itor);
                itor = _frame_list.begin();
                BOOST_DEBUG << "first video frame is not key frame.";
                continue;
            }else
            {
                //确保I帧的完整性
                if (_frame_list.size() >= pkt.split_count() && 
                    _frame_list.at(pkt.split_count() - 1)->video_pkt().split_seq() == pkt.split_count())
                {
                    _last_key_frame_id = pkt.seq();
                    flag = true;
                    break;
                }else
                {
                    BOOST_DEBUG << "关键帧不完整!";
                    _frame_list.erase(itor);
                    itor = _frame_list.begin();
                    continue;
                }
            }
        }else  //判断即将读取的视频帧的完整性
        {
            //确保视频帧的完整性
            if (_frame_list.size() >= pkt.split_count() && 
                _frame_list.at(pkt.split_count() - 1)->video_pkt().split_seq() == pkt.split_count())
            {
                if (1 == pkt.frame_type()/*IS_X264_TYPE_I(pkt.frame_type)*/)
                {
                    _last_key_frame_id = pkt.seq();
                }else if(_last_key_frame_id != pkt.key_frame_id()) //中间关键帧丢失
                {
                    _frame_list.erase(itor);
                    itor = _frame_list.begin();
                    BOOST_DEBUG << "中间关键帧丢失.";
                    continue;
                }
                flag = true;
                break;
            }else
            {
                _frame_list.erase(itor);
                itor = _frame_list.begin();
                BOOST_DEBUG << "此视频帧不完整.";
                continue;
            }
        }
    }
    if (!flag)  //组帧失败
    {
        BOOST_DEBUG << "视频组帧失败！";
        return false;
    }
    SEQ_ID cur_seq = 0;
    itor = _frame_list.begin();
    while (itor != _frame_list.end())
    {
        pkt = (*itor)->video_pkt();
        if (0 == cur_seq)
        {
            cur_seq = pkt.seq();

            //if (g_req_id == 0)
            //{
            //    g_req_id = cur_seq;
            //}else
            //{
            //    if (cur_seq != ++g_req_id)
            //    {
            //        MessageBox(NULL, NULL, NULL, 0);
            //    }
            //    g_req_id = cur_seq;
            //}

        }else if(pkt.seq() == cur_seq && pkt.split_seq() == pkt.split_count())
        {
            data_.append(pkt.media_data().data(), pkt.media_data().size());

            //BOOST_DEBUG <<"get video frame ts:" << pkt.timestamp() << ";audio ts:" << audio_ts_;

            _frame_list.erase(itor);
            return true;
        }else if (pkt.seq() != cur_seq)
        {
            //BOOST_DEBUG <<"get video frame ts:" << pkt.timestamp() << ";audio ts:" << audio_ts_;
           return true;
        }
        data_.append(pkt.media_data().data(), pkt.media_data().size());
        _frame_list.erase(itor);
        itor = _frame_list.begin();
    }
    return false;
}

void CVideoPlayChannel::clear_frames()
{
    boost::lock_guard<boost::recursive_mutex > lock_(_mutex);

    _frame_list.clear();
}

int CVideoPlayChannel::set_video_hwnd(HWND hwnd_)
{
    BOOST_INFO << "set video hwnd():" << hwnd_;
    if( hwnd_ == NULL )
    {
        BOOST_ERROR <<"video play hwnd is NULL";
        return -1;
    }
    if( _video_paly_hdc != NULL )
    {
        ::ReleaseDC(hwnd_, _video_paly_hdc);
        _video_paly_hdc = NULL;
    }
    _video_play_wnd = hwnd_;
    _video_paly_hdc = ::GetDC(_video_play_wnd);
    return 0;
}

void CVideoPlayChannel::flush_codec()
{
    boost::lock_guard<boost::recursive_mutex > lock_(_mutex);
    clear_frames();
    if (_pDecoder)
    {
        _pDecoder->flush_codec();
    }
    _last_play_tv = 0;
}

void CVideoPlayChannel::save_bmp(const char* file_name_, const int len_)
{
    boost::lock_guard<boost::recursive_mutex > lock_(_mutex);
    if (NULL == file_name_ || 0 >= len_)
    {
        return;
    }
    _bmp_file_name.assign(file_name_, len_);
}

