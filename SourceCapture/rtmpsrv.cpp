#include "base_timer_value.h"
#include "rtmpsrv.h"
#include "rtmp_input_device.h"
#include "../common/revolver/boost_log.h"

RtmpServer::RtmpServer( )
{
	callback_	= NULL;
	m_port		= 1935;
	m_pSocket	= NULL;
}

RtmpServer::~RtmpServer()
{
	return;
}


int RtmpServer::fmle_publish(SrsRtmpServer* rtmp, SrsRequest* req )
{
	if (NULL == rtmp || NULL == req )
	{
		return -1;
	}
	int ret = ERROR_SUCCESS;

	bool hasHead = false, bVFirst = true, bAFirst = true;
	char head[128] = {0};
	char commonHead[4] = {0x00, 0x00, 0x00, 0x01};
	string video_data, audio_data;
	int headLen = 0;

	while ( false ==  get_terminated() )
	{
		Sleep(5);

		SrsMessage* msg = NULL;
		if ((ret = rtmp->recv_message(&msg)) != ERROR_SUCCESS) 
		{
			BOOST_ERROR << "rtmp recv identify client message failed. ret=" << ret;
			break;
		}

		SrsAutoFree(SrsMessage, msg);

		if (msg->header.is_amf0_command() || msg->header.is_amf3_command()) 
		{
			SrsPacket* pkt = NULL;
			if ((ret = rtmp->decode_message(msg, &pkt)) != ERROR_SUCCESS) {
				BOOST_ERROR << "fmle decode unpublish message failed. ret=" << ret;
				break;
			}

			SrsAutoFree(SrsPacket, pkt);

			if (dynamic_cast<SrsFMLEStartPacket*>(pkt)) {
				SrsFMLEStartPacket* unpublish = dynamic_cast<SrsFMLEStartPacket*>(pkt);
				if ((ret = rtmp->fmle_unpublish(10, unpublish->transaction_id)) != ERROR_SUCCESS) {
					break;
				}
				break;
			}

			BOOST_ERROR << "fmle ignore AMF0/AMF3 command message.";
			continue;
		}

		if (msg->header.is_audio()) 
		{
			audio_data.clear();
			if (bAFirst)
			{
				callback_->OnIncomingFirstAudioFrame();
				audio_data.append((char *)msg->payload + 1, msg->size - 1);
				bAFirst = false;
				BOOST_INFO << "RtmpServer received audio data";
			}else
				audio_data.append((char *)msg->payload, msg->size -1);
			callback_->OnIncomingAudioFrame( (uint32_t)msg->header.timestamp, (uint8_t*)audio_data.data(), audio_data.size(), 0 );
		}else if (msg->header.is_video()) 
		{
			if (!hasHead)
			{
				memcpy(head, commonHead, 4);
				int8_t *skip = msg->payload;
				int8_t *skip2 = msg->payload;
				while(*(skip++) != 0x67);
				while(*(skip2++) != 0x68);
				int diff = skip2 - skip;
				if (diff <= 0)
				{
					BOOST_ERROR << "RtmpServer received video data error! header is error! Not find PPS SPS!";
					continue;
				}

				memcpy(head + 4, skip - 1, diff - 4);	//copy sps
				memcpy(head + 4 + diff - 4, commonHead, 4);
				memcpy(head + 4 + diff - 4 + 4, skip2 - 1, 4); //copy pps
				hasHead = true;
				headLen = 4 + diff - 4 + 4 + 4;
			}else
			{
				video_data.clear();
				if (bVFirst)
				{
					video_data.append(head, headLen);
					video_data.append((const char *)msg->payload+9, msg->size - 9);
					callback_->OnIncomingFirstVideoFrame();
					callback_->OnIncomingVideoFrame((uint32_t)msg->header.timestamp, (uint8_t*)video_data.data(), video_data.length(), 1);
					bVFirst = false;
					BOOST_INFO << "RtmpServer received video data";
				} else
				{
					if (msg->payload[0] == 0x17)	//I frame
					{
						video_data.append(head, headLen);
					}
					video_data.append(commonHead + 1, 3);
					video_data.append((const char *)msg->payload+9, msg->size - 9);
					if (msg->payload[0] == 0x17)
						callback_->OnIncomingVideoFrame((uint32_t)msg->header.timestamp, (uint8_t*)video_data.data(), video_data.length(), 1);
					else
						callback_->OnIncomingVideoFrame((uint32_t)msg->header.timestamp, (uint8_t*)video_data.data(), video_data.length(), 0);
				}
			}
		}
	}
	return ret;
}

void RtmpServer::execute( )
{
	if (NULL == m_pSocket)
	{
		return ;
	}
	m_pSocket->listen("0.0.0.0", m_port, 10);

	SimpleSocketStream* server = new SimpleSocketStream;
	while ( false ==  get_terminated() )
	{
		if (NULL == server)
		{
			SimpleSocketStream* server = new SimpleSocketStream;
		}
		if (NULL != m_pSocket && 0 == m_pSocket->accept(*server))
		{
			BOOST_INFO << "RtmpServer thread accept obs connected!";

			SrsRtmpServer* rtmp = new SrsRtmpServer(server);
			SrsRequest* req = new SrsRequest();

			int ret = ERROR_SUCCESS;

			if ((ret = rtmp->handshake()) != ERROR_SUCCESS) {
				break;
			}

			SrsMessage* msg = NULL;
			SrsConnectAppPacket* pkt = NULL;
			if ((ret = srs_rtmp_expect_message<SrsConnectAppPacket>(rtmp->get_protocol(), &msg, &pkt)) != ERROR_SUCCESS) {
				break;
			}
			if ((ret = rtmp->response_connect_app(req, 0)) != ERROR_SUCCESS) {
				break;
			}
			while ( false ==  get_terminated()  ) {
				SrsRtmpConnType type;
				if ((ret = rtmp->identify_client(10, type, req->stream, req->duration)) != ERROR_SUCCESS){
					callback_->RtmpCaptureError( -1, "RtmpServer thread peer shutdown!" );
					terminate();
					BOOST_INFO << "RtmpServer thread peer shutdown!";
					break;
				}
				assert(type == SrsRtmpConnFMLEPublish);
				req->strip();
				rtmp->start_fmle_publish(10);
				int ret = fmle_publish( rtmp, req );
				if ((ret != ERROR_CONTROL_REPUBLISH) && (ret != ERROR_CONTROL_RTMP_CLOSE)) //OBS主动停止串流
				{
					break;
				}
			}
			if (NULL != rtmp)
			{
				delete rtmp;
				rtmp = NULL;
			}
			//req在debug模式下暂不释放，否则会导致中断；初步估计与内存对其有关
#ifndef _DEBUG
			if(NULL != req)
			{
				delete req;
				req = NULL;
			}
#endif
			if (NULL != server)
			{
				server->close_socket();
			}
		}else
		{
			Sleep(5);
		}
	} 
	if (NULL != server)
	{
		delete server;
		server = NULL;
	}
	BOOST_INFO << "RtmpServer thread quit!";
}

int RtmpServer::StartRtmpServer( RtmpCaptureDataCallback* callback )
{
	callback_ = callback;

	InitSockets();
	m_port = 1935;

	if (NULL != m_pSocket)
	{
		m_pSocket->close_socket();
		delete m_pSocket;
		m_pSocket = NULL;
	}
	m_pSocket = new SimpleSocketStream;
	m_pSocket->create_socket();

	return start();
}

void RtmpServer::StopRtmpServer( )
{
	BOOST_INFO << "StopRtmpServer begin";
	terminate();

	if (NULL != m_pSocket)
	{
		m_pSocket->close_socket();
		delete m_pSocket;
		m_pSocket = NULL;
	}
	BOOST_INFO << "StopRtmpServer end";
	CleanupSockets();
};

int32_t RtmpServer::terminate()
{
	terminated_ = true;
	if(thr_handle_ != NULL)
	{
		if(WAIT_TIMEOUT == WaitForSingleObject(thr_handle_, 100))
		{
			BOOST_INFO << "StopRtmpServer thread timeout";
			TerminateThread(thr_handle_, -1);
		}
		thr_handle_ = NULL;
	}
	return 0;
}

