#pragma once
#include "stdafx.h"
#include <vector>
#include <stdint.h>
#include "global_config.h"
#include "http_client.h"

enum HTTP_STATE{
    EHTTP_STATE_NONE        = 0x00,
    EHTTP_STATE_LOGINING    = 0x01,
    EHTTP_STATE_LOGINED     = 0x02,
    EHTTP_STATE_LOGOUTING   = 0x03,
    EHTTP_STATE_LOGOUTED    = EHTTP_STATE_NONE,
};

struct SESSIONPROTOCAL;

class ILiveEvent;
#ifdef _GM_CLIENT
class CGMMgr;
#endif // _GM_CLIENT

class CSessionMgr
{
public:
    CSessionMgr(const ROOM_ID &room_id_, const USER_ID &user_id_, ILiveEvent* ev_);
    virtual ~CSessionMgr(void);

    void            set_live_event(ILiveEvent* event_){ _pEvent.store(event_); };

#ifdef _GM_CLIENT
    void            set_gm_event(CGMMgr* event_);
#endif // _GM_GLIENT

    //登入
    virtual int     Login(const string &url_, bool bAnchor_ = false);
    //登出
    virtual int     Logout();

    virtual int     send_media_frame(NetPacket *pkt_);

    //格局http回应更新http state
    //@pragma protocal_str_:发往server的协议包
    //@pragma response_ : server 的回应值
    virtual void    update_state(const string &protocal_str_, const long &response_, bool flag_ = true);
    //媒体包专门处理接口
    virtual void    on_recv_media_data(const string &media_data_);

    virtual bool    is_logined();
    virtual bool    is_logining();

    //http断线，重新连接
    virtual int     relogin();

    bool            is_anchor();
    ROOM_ID    get_room_id(){ return _room_id; };

    //释放前，设置一些http client属性
    virtual void    pre_release();

    virtual void    message_box(char* buf_);

    void            set_video_cb(boost::function<void(boost::shared_ptr<NetPacket>)>);
    void            set_audio_cb(boost::function<void(boost::shared_ptr<NetPacket>)>);
    void            set_flush_codec_cb(boost::function<void()>);

protected:
    //有时由于当前状态未更新，不能立即执行用户命令，将其缓存，带状态更新后再执行
    virtual void    add_unprocessed_protocal(uint8_t pro_);
    //检查是否有未处理命令
    virtual void    check_unprocessed_protocal();

    virtual void    send_get_request();

    //HTTP状态回调
    virtual void    on_login_cb(const USER_ID& uid_, const long& res_, uint8_t bflag_);
    virtual void    on_logout_cb(const USER_ID& uid_, const long& res_, uint8_t bflag_);

protected:
    boost::atomic<ILiveEvent* >         _pEvent;
    ROOM_ID                             _room_id;
    USER_ID                             _user_id;
    string                              _sev_url;
    HTTP_STATE                          _eState;            //current http session state
    CHttpClient                         *_pHttpSession;
    vector<SESSIONPROTOCAL >            _pro_vec;           //save unprocessed protocal orders
    boost::atomic_bool                  _bAnchor;           //记录自己是否是主播
    boost::recursive_mutex              _mutex;
    boost::function<void(boost::shared_ptr<NetPacket>)>    _video_func_cb;         //数据上传回调
    boost::function<void(boost::shared_ptr<NetPacket>)>    _audio_func_cb;         //数据上传回调
    boost::function<void()>                                _flush_codec_func;      //清理解码器缓冲区
};
