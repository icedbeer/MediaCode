#include "stdafx.h"
#include "record_interface.h"
#include "record_mgr.h"
#include "global_config.h"

CRecordInterface::CRecordInterface(ILiveEvent* event_)
{
    _pRecordMgr = new CRecordMgr(event_);
    GLOBAL_CONFIG()->_brecord_handle_init_flag.store(true);
}

CRecordInterface::~CRecordInterface()
{
    GLOBAL_CONFIG()->_brecord_handle_init_flag.store(false);
    delete _pRecordMgr;
}

int CRecordInterface::Login(const unsigned int uid_, const unsigned int room_id_, NETPROTO proto_ /*= _eHttp*/)
{
    if (_pRecordMgr || !_pRecordMgr->is_logined(room_id_))
    {
        return _pRecordMgr->Login(uid_, room_id_, proto_);
        return -1;
    }else
    {
        return -1;
    }
}

int CRecordInterface::Login(const unsigned int uid_, const unsigned int room_id_,
    const char* ip_, const short port_, NETPROTO proto_ /*= _eHttp*/)
{
    if (_pRecordMgr)
    {
        return _pRecordMgr->Login(uid_, room_id_, ip_, port_, proto_);
    } else
    {
        return -1;
    }
}

int CRecordInterface::Logout(const unsigned int room_id_)
{
    if (_pRecordMgr)
    {
        return _pRecordMgr->Logout(room_id_);
    }else
    {
        return -1;
    }
}

int CRecordInterface::preview_video(int video_show_num_, int dev_id_)
{
    if (_pRecordMgr)
    {
        return _pRecordMgr->preview_video(video_show_num_, dev_id_);
    }else
    {
        return -1;
    }
}

int CRecordInterface::preview_audio(int dev_id_)
{
    if (_pRecordMgr)
    {
        return _pRecordMgr->preview_audio(dev_id_);
    } else
    {
        return -1;
    }
}

void CRecordInterface::set_video_show_num(int num_)
{
    GLOBAL_CONFIG()->_cur_video_show_num.store(num_);    
}

int CRecordInterface::get_video_show_num()
{
    return GLOBAL_CONFIG()->_cur_video_show_num.storage();
}

int CRecordInterface::get_video_dev_list(char video_dev_[][256], const int max_size_/* = 10*/)
{
    int size_(0);
    if (GLOBAL_CONFIG()->_video_dev_count > max_size_)
    {
        size_ = max_size_;
    }else
    {
        size_ = GLOBAL_CONFIG()->_video_dev_count;
    }
    for (int i(0); i < size_; i++)
    {
        memcpy(video_dev_[i], GLOBAL_CONFIG()->_video_dev_list[i], 256);
    }
    return size_;
}

int CRecordInterface::get_video_dev_count()
{
    return GLOBAL_CONFIG()->_video_dev_count.storage();
}

int& CRecordInterface::get_preview_video_width()
{
    return GLOBAL_CONFIG()->_preview_pic_width.storage();
}

int& CRecordInterface::get_preview_video_height()
{
    return GLOBAL_CONFIG()->_preview_pic_height.storage();
}

void CRecordInterface::set_default_anchor_img(const char* path_name_, const int& max_len_)
{
    if (max_len_ > sizeof(GLOBAL_CONFIG()->_anchor_img) || NULL == path_name_)
    {
        return;
    }
    memset(GLOBAL_CONFIG()->_anchor_img, 0, sizeof(GLOBAL_CONFIG()->_anchor_img));
    memcpy(GLOBAL_CONFIG()->_anchor_img, path_name_, max_len_);
}

void CRecordInterface::set_default_anchor_img_flag(bool flag_)
{
    GLOBAL_CONFIG()->_send_def_img_flag = flag_;
}

void CRecordInterface::set_acc_flag(bool bflag_)
{
    GLOBAL_CONFIG()->_benable_accompany = bflag_;
}

void CRecordInterface::set_mic_flag(bool bflag_)
{
    GLOBAL_CONFIG()->_benable_mic = bflag_;
}

bool CRecordInterface::is_logined(const unsigned int room_id_)
{
    if (_pRecordMgr)
    {
        return _pRecordMgr->is_logined(room_id_);
    }else
    {
        return false;
    }
}

int CRecordInterface::add_sev_ip_info(const unsigned long ip_, const int port_, 
                                      const short type_, const short my_type_)
{
    if (_pRecordMgr)
    {
        return _pRecordMgr->add_sev_ip_info(ip_, port_, type_, my_type_);
    }
    else
    {
        return -1;
    }
}

int CRecordInterface::del_sev_ip_info(const unsigned long ip_)
{
    if (_pRecordMgr)
    {
        return _pRecordMgr->del_sev_ip_info(ip_);
    }
    else
    {
        return -1;
    }
}

void CRecordInterface::clear_sev_ip_info()
{
    if (_pRecordMgr)
    {
        _pRecordMgr->clear_sev_ip_info();
    }
}

void CRecordInterface::set_global_cfg(int cap_width_, int cap_height_,
    int codec_width_, int codec_height_, int gop_time_, int video_bitrate_,
    int audio_bitrate_, char* anchor_img_str_)
{
    GLOBAL_CONFIG()->_preview_pic_width = cap_width_;
    GLOBAL_CONFIG()->_preview_pic_height = cap_height_;
    GLOBAL_CONFIG()->_codec_pic_width = codec_width_;
    GLOBAL_CONFIG()->_codec_pic_height = codec_height_;
    GLOBAL_CONFIG()->_key_frame_interval = gop_time_;
    GLOBAL_CONFIG()->_video_bitrate = video_bitrate_;
    GLOBAL_CONFIG()->set_anchor_img(anchor_img_str_);

    if (_pRecordMgr)
    {
        //���³�ʼ��������
        //if (_pRecordMgr->is_start())
        {
            _pRecordMgr->reinit();
        }
    }
}

void CRecordInterface::set_live_event(ILiveEvent* event_)
{
    if (_pRecordMgr)
    {
        _pRecordMgr->set_event(event_);
    }
}

