#ifndef __BMP_PROCESSOR_H
#define __BMP_PROCESSOR_H

#ifdef __cplusplus
extern "C" {
#endif

void smooth(int video_width, int video_height, void *_src, void *_dst, int _size);
void sharpen(int video_width, int video_height, unsigned char *_src, unsigned char *_dst, int _size);

#ifdef __cplusplus
}
#endif

#endif