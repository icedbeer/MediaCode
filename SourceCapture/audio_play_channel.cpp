#include "stdafx.h"
#include "audio_play_channel.h"
#include <conio.h>
#include "audio_record_channel.h"
#include "base_guard.h"
#include "audio_codec.h"
#include "Sound.h"
#include "DSoundPlay.h"
#include "audio_mixer.h"

#define  MAX_BUFFER_PACKET  50
#define  MIN_BUFFER_PACKET  3

const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
const IID IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);

CAudioPlayChannel::CAudioPlayChannel(void)
{
    _pSoundDev = NULL;
    _pDecoder  = NULL;
    _dev_id    = 0;
    _audio_ts  = 0;
    _bplay_flag = false;
    _breopen_flag = false;
    _bexit_flag = false;
    _cur_pcm_buf_size = 0;

    HRESULT hr = S_OK;
    _pEnumerator = NULL;
    ::CoInitialize(NULL);

    hr = CoCreateInstance(
        CLSID_MMDeviceEnumerator, NULL,
        CLSCTX_ALL, IID_IMMDeviceEnumerator,
        (void**)&_pEnumerator);
    if (SUCCEEDED(hr) && _pEnumerator)
    {
        hr = _pEnumerator->RegisterEndpointNotificationCallback((IMMNotificationClient*)this);
        if (hr==S_OK)
        {
            cout<<"注册成功";
        }else
        {
            cout<<"注册失败";
        }
    }
    init(0);
    _thread_thr = boost::thread(boost::bind(&CAudioPlayChannel::execute, this));
}

CAudioPlayChannel::~CAudioPlayChannel(void)
{
    if (_pEnumerator)
    {
        _pEnumerator->UnregisterEndpointNotificationCallback((IMMNotificationClient*)this);
        _pEnumerator->Release();
    }
    _bexit_flag = true;
    _thread_thr.join();
    fini();
    ::CoUninitialize();
}

void CAudioPlayChannel::execute()
{
    while(!_bexit_flag)
    {
        if (!is_playing() || !_bplay_flag.load())
        {
#ifdef WIN32
            Sleep(1);
#else
            usleep(1000);
#endif // WIN32
        }else
        {
#ifdef _NODES_DEBUG
            Sleep(1);
#endif //_NODES_DEBUG
        }
        if (_bplay_flag.load())
        {
            if (!play_audio())
            {
#ifdef WIN32
                Sleep(1);
#else
                usleep(1000);
#endif // WIN32
            }
        }
        if (_breopen_flag.load())
        {
            BOOST_INFO << "Will reinit audio device.";
            init(0);
            _breopen_flag.store(false);
        }
    }
}

//init audio decoder, and open audio out device, if open failed, open default
int32_t CAudioPlayChannel::init(int32_t audio_dev_id_)
{
    BOOST_ERROR << "CAudioPlayChannel::init() audio dev id:" << audio_dev_id_;
    do
    {
        _audio_ts = 0;
#ifndef _NODES_DEBUG
        if (!_pDecoder)
        {
            _pDecoder  = new(std::nothrow) CAudioDecoder;
            if (NULL == _pDecoder)
            {
                break;
            }
            if(_pDecoder->open(EAUDIOCODEC_AAC) < 0)
            {
                delete _pDecoder;
                _pDecoder = NULL;
            }
        }
#endif // _NODES_DEBUG
        if (_pSoundDev)
        {
            delete _pSoundDev;
            _pSoundDev  = NULL;
        }
        _pSoundDev = new(std::nothrow) CSoundChannel;
        if (NULL == _pSoundDev)
        {
            break;
        }
        if (_pSoundDev->InitAudioOutDev(DEFAULT_CHANNEL_NUM, DEFAULT_SAMPLE_RATE, DEFAULT_CHANNEL_BIT, audio_dev_id_) >= 0)
        {
            _dev_id = audio_dev_id_;
        }else
        {
            BOOST_ERROR << "CAudioPlayChannel::init() open audio device failed.dev id:" << audio_dev_id_;
            break;
        }
        _bplaying_flag = false;
        return 0;
    }while(false);

    BOOST_ERROR << "CAudioPlayChannel::init() audio dev id:" << audio_dev_id_ << " failed.";
    fini();
    return -1;
}

void CAudioPlayChannel::fini()
{
    clear_media_packets();
    if (_pDecoder)
    {
        delete _pDecoder;
        _pDecoder = NULL;
    }
    if (_pSoundDev)
    {
        delete _pSoundDev;
        _pSoundDev  = NULL;
    }
}

void CAudioPlayChannel::clear_media_packets()
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    _packet_list.clear();
}

HRESULT STDMETHODCALLTYPE CAudioPlayChannel::OnDefaultDeviceChanged(EDataFlow flow, ERole role, LPCWSTR pwstrDeviceId)
{
    BOOST_INFO << "CAudioPlayChannel::OnDefaultDeviceChanged in.";
    if (flow != eRender)
    {
        return S_OK;
    }
    BOOST_INFO << "CAudioPlayChannel::OnDefaultDeviceChanged render device change.";
    _breopen_flag.store(true);
    return S_OK;
}

int32_t CAudioPlayChannel::set_audio_device(int32_t audio_dev_id_)
{
    BOOST_INFO << "CAudioPlayChannel::set_audio_device() old dev id:" << _dev_id
        << "; new audio dev id:" << audio_dev_id_;
    if (_dev_id == audio_dev_id_)
    {
        return 1;
    }
    if (_pSoundDev)
    {
        delete _pSoundDev;
        _pSoundDev  = NULL;
    }
    _pSoundDev = new(std::nothrow) CSoundChannel;
    if (NULL == _pSoundDev)
    {
        return -1;
    }
    if (_pSoundDev->InitAudioOutDev(DEFAULT_CHANNEL_NUM, DEFAULT_SAMPLE_RATE, DEFAULT_CHANNEL_BIT, audio_dev_id_))
    {
        _dev_id = audio_dev_id_;
    }else
    {
        BOOST_ERROR << "CAudioPlayChannel::set_audio_device() open audio device failed.dev id:" 
            << audio_dev_id_;
        return -1;
    }
    return 0;
}

void CAudioPlayChannel::flush_codec()
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    clear_media_packets();
    if (_pDecoder)
    {
        _pDecoder->flush_codec();
    }
}

void CAudioPlayChannel::set_play_flag(bool flag_)
{
    _bplay_flag.store(flag_);
}

void CAudioPlayChannel::set_play_event_cb(boost::function<void(int)> func_)
{
    _play_state_change_func.swap(func_);
}

void CAudioPlayChannel::put_data(boost::shared_ptr<AudioPacket> pkt_)
{
    boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
    if (_packet_list.size() > 1500) //15s,10ms/audio frame
    {
        _packet_list.pop_front();
    }
    _packet_list.push_back(pkt_);
}

bool CAudioPlayChannel::is_playing()
{
    if (!_bplaying_flag.load())
    {
        if (_packet_list.size() >= MAX_BUFFER_PACKET)
        {
            _bplaying_flag.store(true);
            if (!_play_state_change_func.empty())
            {
                _play_state_change_func(1);
            }
        }
    }else
    {
        if (0 == _packet_list.size())
        {
            _bplaying_flag.store(false);
            if (!_play_state_change_func.empty())
            {
                _play_state_change_func(0);
            }
        }
    }

    return _bplaying_flag.load();
}

bool CAudioPlayChannel::play_audio()
{
    if (NULL == _pDecoder || NULL == _pSoundDev || !is_playing() || _pSoundDev->GetBufferSize() > 8196 * 2)
    {
        return false;
    }
    int32_t len(0);
    if (1)
    {
        boost::lock_guard<boost::recursive_mutex> lock_(_mutex);
        if (_packet_list.empty())
        {
            return false;
        }
#ifndef _NODES_DEBUG
        if ((_cur_pcm_buf_size = 
            _pDecoder->dec((uint8_t *)_packet_list.front()->media_data().data(), 
            _packet_list.front()->media_data().size(), _pcm_buf, 192000)) <= 0)
        {
            _packet_list.pop_front();
            return false;
        }
        //音量变化
        if (1.0f != GLOBAL_CONFIG()->_play_volume_scale)
        {
            HeAudioMixer::audio_scale((int16_t *)_pcm_buf, _cur_pcm_buf_size / sizeof(int16_t),
                GLOBAL_CONFIG()->_play_volume_scale);
        }
#endif // _NODES_DEBUG
        _audio_ts = _packet_list.front()->timestamp();
        _packet_list.pop_front();
    }
#ifndef _NODES_DEBUG
    _pSoundDev->PlayAudio(_pcm_buf, _cur_pcm_buf_size);
#endif // _NODES_DEBUG

    return true;
}

int CAudioPlayChannel::get_pcm_data(void **data_, DWORD& len_)
{
    //注意此处，如果使用CSoundChannnel进行播放的话，则直接赋给*data_ = _pcm_buf即可
    //如果是使用DSound的话，需要拷贝_pcm_buf数据到*data_
    //目前未实现DSound播放， 因为DSound每次要求传入其固定大小的PCM数据，属于拉模式，而CSound是推模式
    *data_ = _pcm_buf;
    len_ = _cur_pcm_buf_size;
    return len_;
}
