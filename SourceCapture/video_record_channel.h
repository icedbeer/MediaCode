/************************************************************************/
/* 下辖管理所有的视频采集设备类型，同时进行编码，然后通过外部接口进行发送*/
/************************************************************************/
#pragma once
#include "base_typedef.h"
#include <string>
#include <vector>
#include <map>
#include <atlimage.h>
//#include "media_packet.h"
#include "live_event.h"
#include "global_config.h"
#include "RUdpProtocol.pb.h"


#define VIDEO_MAX_WIDTH     1920
#define VIDEO_MAX_HEIGHT    1200

class CDShowVideoDevice;
class CVideo_Encoder;
class H264Encoder;
class CMediaRecordThread;
class CCorlorConvert;

//using namespace MediaMessage;
using namespace ProtoNameSpace;
using namespace std;

class CVideoRecordChannel
{
public:
    CVideoRecordChannel(CMediaRecordThread *media_thr_);
    virtual ~CVideoRecordChannel(void);

    virtual int         open(int32_t video_show_num, int dev_id_, 
        int32_t width_, int height_, int frame_rate_, int bit_rate_);
    virtual int         open(int32_t video_show_num, string device_name_, 
        int32_t width_, int height_, int frame_rate_, int bit_rate_);
    virtual int         close(int32_t video_show_num);
    virtual int         close_all();
    
    virtual void        set_video_display_event(ILiveEvent* ev_){_event = ev_;}

protected:
    virtual void        execute();
    //外部线程调用，读取数据，并编码打包传输，否则返回false
    virtual bool        loop();

    virtual int         split_packet(const unsigned char* in_buf_, const uint32_t& in_size_, 
        const int32_t& frame_type_, vector<NetPacket* > &vec_);

protected:
    typedef map<int, boost::tuple<CDShowVideoDevice* , CCorlorConvert* > > VIDEODEVMAP;

    VIDEODEVMAP             _video_dev_map;
    H264Encoder             *_pEncoder;
    CMediaRecordThread      *_pRecThread;
    ILiveEvent              *_event;

    uint64_t                _last_tv;           //last video frame timestamp
    int32_t                 _max_buffer_size;
    unsigned char           _pRawBuffer[VIDEO_MAX_WIDTH * VIDEO_MAX_HEIGHT * 3];
    string                  _rgb_data;
    string                  _raw_enc_data;
    unsigned char           _pEncodeBuffer[1024*1024];
    boost::recursive_mutex  _mutex;
    SEQ_ID                  _video_frame_id;            //packet seq id
    SEQ_ID                  _last_key_frame_id; //last key frame id
    CImage                  _anchor_default_img;
    unsigned char*          _default_img_data;  //存储主播默认图像数据
    boost::thread           _load_thr;
    boost::thread           _loop_thr;
    bool                    _bexit;
};
