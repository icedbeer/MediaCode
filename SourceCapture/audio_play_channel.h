#pragma once
#include <list>
#include <stdint.h>
#include "global_config.h"
#include <mmdeviceapi.h>

class CAudioDecoder;
class IAudioOut;

//using namespace MediaMessage;

class CAudioPlayChannel : public IMMNotificationClient
{
public:
    CAudioPlayChannel(void);
    ~CAudioPlayChannel(void);

    //add audio frame data to buffer
    virtual void            put_data(boost::shared_ptr<AudioPacket> pkt_);

    virtual int32_t         set_audio_device(int32_t audio_dev_id_);
    inline  const uint32_t  &get_audio_ts() {return _audio_ts;};

    virtual void            flush_codec();

    virtual void            set_play_flag(bool flag_);

    //播放状态变化通知接口
    void                    set_play_event_cb(boost::function<void (int )> func_);

    //获取已解码的PCM数据
    int                     get_pcm_data(void **data_, DWORD& len_);

protected:
    virtual void            execute();

    virtual bool            is_playing();

    //decode frame to pcm data
    virtual bool            play_audio();

    //init audio decoder, and open audio out device, if open failed, open default
    virtual int32_t         init(int32_t audio_dev_id_);
    //close decoder and audio out device
    virtual void            fini();

    void                    clear_media_packets();

protected:
    // IUnknown methods -- AddRef, Release, and QueryInterface
    LONG _cRef;
    ULONG STDMETHODCALLTYPE AddRef()
    {
        return 0;
    }

    ULONG STDMETHODCALLTYPE Release()
    {
        ULONG ulRef = 0;
        return ulRef;
    }

    HRESULT STDMETHODCALLTYPE QueryInterface(
        REFIID riid, VOID **ppvInterface)
    {
        return S_OK;
    }

    HRESULT STDMETHODCALLTYPE OnDefaultDeviceChanged(
        EDataFlow flow, ERole role,
        LPCWSTR pwstrDeviceId);

    HRESULT STDMETHODCALLTYPE OnDeviceAdded(LPCWSTR pwstrDeviceId)
    {
        return S_OK;
    };

    HRESULT STDMETHODCALLTYPE OnDeviceRemoved(LPCWSTR pwstrDeviceId)
    {
        return S_OK;
    }

    HRESULT STDMETHODCALLTYPE OnDeviceStateChanged(
        LPCWSTR pwstrDeviceId,
        DWORD dwNewState)
    {
        return S_OK;
    }

    HRESULT STDMETHODCALLTYPE OnPropertyValueChanged(
        LPCWSTR pwstrDeviceId,
        const PROPERTYKEY key)
    {
        return S_OK;
    }

protected:
    CAudioDecoder*              _pDecoder;
    list<boost::shared_ptr<AudioPacket> >    _packet_list;
    IAudioOut*                  _pSoundDev;
    uint8_t                     _pcm_buf[192000];
    DWORD                       _cur_pcm_buf_size;  //_pcm_buf当前有效字节数
    boost::recursive_mutex      _mutex;
    int32_t                     _dev_id;        //audio out device id, default is 0.
    uint32_t                    _audio_ts;      //audio current play time stamp.
    boost::atomic<bool>         _bplaying_flag;
    boost::atomic<bool>         _bplay_flag;
    boost::atomic<bool>         _breopen_flag;
    IMMDeviceEnumerator*        _pEnumerator;
    bool                        _bexit_flag;
    boost::thread               _thread_thr;
    boost::function<void(int)>  _play_state_change_func;    //播放状态变化通知接口
};
