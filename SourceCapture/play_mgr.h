#pragma once
#include <list>
#include "live_event.h"
#include "global_config.h"
#include "LoadLibrary_Interface.h"

class CMediaPlayThread;
class ISocketInterface;
class CCorlorConvert;

using namespace std;

class  CPlayMgr
{
public:
    CPlayMgr(ILiveEvent* event_);
    virtual ~CPlayMgr(void);

    //添加服务器地址信息
    //@ ip_ : 服务器IP
    //@ type_ : 此IP运营商类型
    //@ my_type_ : 本机IP运营商类型
    int                     add_sev_ip_info(const unsigned long ip_, const int port_, 
        const short type_, const short my_type_);
    int                     del_sev_ip_info(const unsigned long ip_);

    void                    clear_sev_ip_info();

    //IP地址内部自动轮换，需要先使用add_sev_ip_info添加服务器IP地址信息
    int                     Login(const unsigned int uid_, const unsigned int room_id_,
        NETPROTO proto_ /*= _eHttp*/);
    int                     Logout();

    bool                    is_logined();

    void                    save_bmp(const char* file_name_, const int len_);

    //转换缩放图像
    int                     swscale_rgb24_img(unsigned char* src_data_, int src_width_, int src_height_, int src_format_,
        unsigned char* dst_data_, int dst_width_, int dst_height_, int dst_format_);

private:
    ILiveEvent*                         _pEvent;
    CMediaPlayThread*                   _pMediaPlayThd;
    boost::shared_ptr<ISocketInterface> _pSessionMgr;
    CCorlorConvert*                     _corlor_convert;
    static list<boost::tuple<string, int> > _ip_addr_list;   //保存IP列表信息
};
