#include "stdafx.h"
#ifdef WIN32
#include "dshow_video.h"
#include <assert.h>
#include "boost_log.h"
#include "charactor_convert.h"

#pragma comment(lib,"quartz.lib")
#pragma comment(lib,"winmm.lib")
#pragma comment(lib,"msacm32.lib")
#pragma comment(lib,"olepro32.lib")
#pragma comment(lib,"strmiids.lib")

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p) if(p){p->Release();p=NULL;}
#endif

vector<string>   CDShowVideoDevice::m_VideoDeviceList;

//判断两个浮点数是否相等,保持小数点后3位有效数字
bool is_float_equal(float ft1, float ft2)
{
    if (ft1 <= (ft2 + 0.001f) && ft1 >= (ft2 - 0.001f))
    {
        return true;
    }
    return false;
}

CDShowVideoDevice::CDShowVideoDevice(void)
{
    ::CoInitialize(NULL);
    m_pGB = 0;
    m_pCaptureGB2 = 0;
    m_pCaptureFilter = 0;
    m_pMediaControl = NULL;
    m_pEvent = NULL;
    m_nWidth = 0;
    m_nHeigth = 0;
    m_dbLastFrameTime = 0.0;
    m_nCurrentSampleBufLen = 0;
    m_pVfwDialogs = 0;
    m_bIsPlaying = false;
    m_nNewFrameNum = 0;
    m_dFrameInterval = 0.0f;
    m_curDeviceId = -1;
    ::InitializeCriticalSection(&m_Lock);

    //添加采集图像格式，根据自己的需要添加或注释
    m_SubType_Vec.push_back(MEDIASUBTYPE_YUY2);
    m_SubType_Vec.push_back(MEDIASUBTYPE_RGB24);
}

CDShowVideoDevice::~CDShowVideoDevice(void)
{
    m_SubType_Vec.clear();
    DestroyEngine();
    ::DeleteCriticalSection(&m_Lock);
    ::CoUninitialize();
}

bool CDShowVideoDevice::InitCapture(const unsigned& width_, const unsigned& height_, const unsigned& frame_rate_,
    const string &device_name_)
{
    if (0 == m_VideoDeviceList.size())
    {
        if (FAILED(EnumDevice(m_VideoDeviceList)) || 0 == m_VideoDeviceList.size())
        {
            BOOST_WARNING << "enum video device failed, or no video capture device.";
            return false;
        }
    }

    m_nWidth = width_;
    m_nHeigth = height_;
    //许多摄像头采集帧率很低，并不能按照我们帧率去采集，我们将按照最高帧率去采集，
    //如果高于我们想要的帧率，则由时间戳去控制以达到我们需要的帧率
    m_nFrameRate = frame_rate_;
    m_dFrameInterval = 1.0 / m_nFrameRate;

    //device_name_如果为空，则使用默认采集设备
    m_curDeviceId = -1;
    if (device_name_.empty())
    {
        m_curDeviceName = m_VideoDeviceList.front();
    } else
    {
        for (size_t i(0); i < m_VideoDeviceList.size(); i++)
        {
            if (0 == device_name_.compare(m_VideoDeviceList.at(i)))
            {
                m_curDeviceId = i;
                break;
            }
        }
        if (m_curDeviceId < 0)
        {
            //指定设备不存在，使用默认设备
            m_curDeviceName = m_VideoDeviceList.front();
            m_curDeviceId = 0;
        } else
        {
            m_curDeviceName = device_name_;
        }
    }
    return CreateEngine(width_, height_);
}

bool CDShowVideoDevice::InitCapture(const unsigned& width_, const unsigned& height_, const unsigned& frame_rate_,
    const int &dev_id_)
{
    if (0 == m_VideoDeviceList.size())
    {
        if (FAILED(EnumDevice(m_VideoDeviceList)))
        {
            return false;
        }
    }
    if (0 == m_VideoDeviceList.size())
    {
        m_curDeviceName = "";
    } else if (m_VideoDeviceList.size() < (dev_id_ - 1))
    {
        m_curDeviceName = m_VideoDeviceList.front();
    } else
    {
        m_curDeviceName = m_VideoDeviceList.at(dev_id_);
    }
    return InitCapture(width_, height_, frame_rate_, m_curDeviceName);
}

void CDShowVideoDevice::UninitCapture()
{
    DestroyEngine();
}

bool  CDShowVideoDevice::CreateEngine(int cx, int cy)
{
    //初始化接口
    //Create the Capture Graph Builder  
    HRESULT hr = CoCreateInstance(CLSID_CaptureGraphBuilder2, NULL, CLSCTX_INPROC_SERVER,
        IID_ICaptureGraphBuilder2, (void**)&m_pCaptureGB2);
    if (FAILED(hr))
    {
        return false;
    }
    //Create the Filter Graph Manager  
    if (SUCCEEDED(hr = CoCreateInstance(CLSID_FilterGraph, 0, CLSCTX_INPROC_SERVER,
        IID_IGraphBuilder, (void**)&m_pGB)))
    {
        hr = m_pGB->QueryInterface(IID_IMediaControl, (void**)&m_pMediaControl);
        if (FAILED(hr))
        {
            return false;
        }
        //Initialize the Capture Graph Builder  
        m_pCaptureGB2->SetFiltergraph(m_pGB);
    } else
    {
        SAFE_RELEASE(m_pCaptureGB2);
        return false;          //Failed  
    }
    //获取source capture filter
    m_pCaptureFilter = GetFilterByName(m_curDeviceName);
    if (NULL == m_pCaptureFilter)
    {
        return false;
    }
    if (FAILED(hr = m_pGB->AddFilter(m_pCaptureFilter, L"Capture Filter")))
    {
        return false;
    }
    if (FAILED(hr = m_pGB->QueryInterface(IID_IMediaEventEx, (void **)&m_pEvent)))
    {
        return false;
    }
    //获取ISampleGrabber filter
    if (FAILED(m_pSampleGrabber.CoCreateInstance(CLSID_SampleGrabber)) || !m_pSampleGrabber)
    {
        return false;
    }
    m_nWidth = cx;
    m_nHeigth = cy;

    //设置视频采集图像格式
    vector<GUID >::iterator itor = m_SubType_Vec.begin();
    while (itor != m_SubType_Vec.end())
    {
        m_cur_subtype = *itor;
        do
        {
            //设置视频采集规格参数
            if (FAILED(InitVideoFormat(cx, cy, m_cur_subtype)))
            {
                break;
            }

            CComQIPtr<IBaseFilter, &IID_IBaseFilter> pGrabBase(m_pSampleGrabber);
            //set video format
            AM_MEDIA_TYPE mt;
            memset(&mt, 0, sizeof(mt));
            mt.majortype = MEDIATYPE_Video;
            mt.subtype = m_cur_subtype;

            if (FAILED(hr = m_pSampleGrabber->SetMediaType(&mt)))
            {
                break;
            }
            if (FAILED(hr = m_pGB->AddFilter(pGrabBase, L"Grabber")))
            {
                break;
            }
            //开始连接graph
            CComPtr< IPin > pSourcePin = GetOutPin(m_pCaptureFilter, 0);
            CComPtr< IPin > pGrabPin = GetInPin(pGrabBase, 0);
            if (FAILED(hr = m_pGB->Connect(pSourcePin, pGrabPin)))
            {
                break;
            }
            //获取连接媒体格式
            if (FAILED(hr = m_pSampleGrabber->GetConnectedMediaType(&mt)))
            {
                break;
            }
            VIDEOINFOHEADER *vih = (VIDEOINFOHEADER*)mt.pbFormat;
            m_bmpInfoHeader = vih->bmiHeader;

            hr = m_pSampleGrabber->SetBufferSamples(false);
            hr = m_pSampleGrabber->SetOneShot(false);
            hr = m_pSampleGrabber->SetCallback(static_cast<ISampleGrabberCB*>(this), 1);
            if (FAILED(hr))
            {
                break;
            }

            //获取camera配置窗口filter
            m_pVfwDialogs = NULL;
            m_pCaptureGB2->FindInterface(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, m_pCaptureFilter,
                IID_IAMVfwCaptureDialogs, (void **)&m_pVfwDialogs);

            //建立内存缓冲区
            for (int i(0); i < MAX_BUFFER_FRMAE; ++i)
            {
                m_FrameList.push_back(new unsigned char[m_nCurrentSampleBufLen + 1]);
            }

            return true;
        } while (false);

        itor++;
    }
    return false;
}

bool  CDShowVideoDevice::DestroyEngine()
{
    if (m_pMediaControl)
    {
        m_pMediaControl->/*StopWhenReady*/Stop();
    }
    SAFE_RELEASE(m_pEvent);
    SAFE_RELEASE(m_pCaptureFilter);
    m_pSampleGrabber = NULL;
    SAFE_RELEASE(m_pMediaControl);
    if (m_pCaptureGB2)
    {
        m_pCaptureGB2->SetFiltergraph(NULL);
    }
    SAFE_RELEASE(m_pCaptureGB2);
    SAFE_RELEASE(m_pGB);
    SAFE_RELEASE(m_pVfwDialogs);

    m_dbLastFrameTime = 0.0;
    m_nCurrentSampleBufLen = 0;
    m_pVfwDialogs = 0;

    m_bIsPlaying = false;
    int i(0);
    while (i < m_FrameList.size())
    {
        delete[]m_FrameList[i];
        ++i;
    }
    m_FrameList.clear();

    return true;
}

void CDShowVideoDevice::DeleteMediaType(AM_MEDIA_TYPE *pmt)
{
    if (pmt != NULL)
    {
        if (pmt->cbFormat != 0)
        {
            CoTaskMemFree((PVOID)pmt->pbFormat);
            pmt->cbFormat = 0;
            pmt->pbFormat = NULL;
        }
        if (pmt->pUnk != NULL)
        {
            // Unecessary because pUnk should not be used, but safest.
            pmt->pUnk->Release();
            pmt->pUnk = NULL;
        }
        CoTaskMemFree(pmt);
    }
}

HRESULT CDShowVideoDevice::GetPin(IBaseFilter * pFilter, PIN_DIRECTION dirrequired, int iNum, IPin **ppPin)
{
    CComPtr< IEnumPins > pEnum;
    *ppPin = NULL;

    HRESULT hr = pFilter->EnumPins(&pEnum);
    if (FAILED(hr))
        return hr;

    ULONG ulFound;
    IPin *pPin;
    hr = E_FAIL;

    while (S_OK == pEnum->Next(1, &pPin, &ulFound))
    {
        PIN_DIRECTION pindir = (PIN_DIRECTION)3;

        pPin->QueryDirection(&pindir);
        if (pindir == dirrequired)
        {
            if (iNum == 0)
            {
                *ppPin = pPin;  // Return the pin's interface
                hr = S_OK;      // Found requested pin, so clear error
                break;
            }
            iNum--;
        }

        pPin->Release();
    }
    return hr;
}

IPin* CDShowVideoDevice::GetInPin(IBaseFilter * pFilter, int nPin)
{
    CComPtr<IPin> pComPin = 0;
    GetPin(pFilter, PINDIR_INPUT, nPin, &pComPin);
    return pComPin;
}

IPin* CDShowVideoDevice::GetOutPin(IBaseFilter * pFilter, int nPin)
{
    CComPtr<IPin> pComPin = 0;
    GetPin(pFilter, PINDIR_OUTPUT, nPin, &pComPin);
    return pComPin;
}

// 枚举可用视频采集设备
HRESULT CDShowVideoDevice::EnumDevice(vector<string > &devices_vec)
{
    devices_vec.clear();

    ICreateDevEnum *pDevEnum = NULL;
    IEnumMoniker   *pEnum = NULL;

    //Create the system device enumerator  
    HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,
        IID_ICreateDevEnum, reinterpret_cast<void**>(&pDevEnum));
    if (SUCCEEDED(hr) && pDevEnum)
    {
        //创建一个枚举器，枚举视频设备  
        hr = pDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &pEnum, 0);
    } else
    {
        return E_FAIL;
    }
    if (FAILED(hr) || NULL == pEnum)
    {
        return E_FAIL;
    }

    IMoniker *pMoniker = NULL;
    IPropertyBag *pPropBag;
    VARIANT varName;

    while (S_OK == pEnum->Next(1, &pMoniker, NULL))
    {
        if (FAILED(hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void**)(&pPropBag))))
        {
            pMoniker->Release();
            continue;
        }
        if (SUCCEEDED(hr))
        {
            //有效可用设备
            IBaseFilter *filter;
            if (SUCCEEDED(hr = pMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&filter)))
            {
                //hr = m_pGB->AddFilter(m_pCaptureFilter, L"Capture Filter");  
                if (SUCCEEDED(hr))
                {
                    varName.vt = VT_BSTR;
                    if (FAILED(hr = pPropBag->Read(L"Description", &varName, 0)))
                    {
                        hr = pPropBag->Read(L"FriendlyName", &varName, 0);
                    }
                    USES_CONVERSION;
                    if (SUCCEEDED(hr))
                    {
                        devices_vec.push_back(unicode2utf(OLE2T(varName.bstrVal)));
                    }
                    VariantClear(&varName);
                }
                filter->Release();
            }
        }
        SAFE_RELEASE(pPropBag);
        SAFE_RELEASE(pMoniker);
    }
    pEnum->Release();
    pDevEnum->Release();
    return S_OK;
}

IBaseFilter *CDShowVideoDevice::GetFilterByName(string filter_name_)
{
    if (filter_name_.empty())
    {
        return NULL;
    }
    ICreateDevEnum *pDevEnum = NULL;
    IEnumMoniker   *pEnum = NULL;

    //Create the system device enumerator  
    HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,
        IID_ICreateDevEnum, reinterpret_cast<void**>(&pDevEnum));
    if (SUCCEEDED(hr) && pDevEnum)
    {
        //创建一个枚举器，枚举视频设备  
        hr = pDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &pEnum, 0);
    } else
    {
        return NULL;
    }
    if (FAILED(hr) || NULL == pEnum)
    {
        return NULL;
    }

    IMoniker *pMoniker = NULL;
    IPropertyBag *pPropBag;
    VARIANT varName;
    IBaseFilter *filter = NULL;

    while (S_OK == pEnum->Next(1, &pMoniker, NULL))
    {
        if (FAILED(hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void**)(&pPropBag))))
        {
            pMoniker->Release();
            continue;
        }
        varName.vt = VT_BSTR;
        if (SUCCEEDED(hr = pPropBag->Read(L"FriendlyName", &varName, 0)))
        {
            USES_CONVERSION;
            char tmp_str[256] = { 0 };
            WideCharToMultiByte(CP_ACP, 0, varName.bstrVal, -1,
                tmp_str, 256, NULL, NULL);
            if (0 == _stricmp(tmp_str, filter_name_.c_str()))
            {
                //有效可用设备
                if (FAILED(hr = pMoniker->BindToObject(0, 0, IID_IBaseFilter, (void**)&filter)))
                {
                    filter = NULL;
                    break;
                }
            }
            VariantClear(&varName);
        }

        SAFE_RELEASE(pPropBag);
        SAFE_RELEASE(pMoniker);
    }
    return filter;
}

//枚举所有的视频设备
int CDShowVideoDevice::EnumVideoCaptureDevice(char device_list_[][256], const int &max_size_)
{
    m_VideoDeviceList.clear();

    ICreateDevEnum *pDevEnum = NULL;
    IEnumMoniker *pEnum = NULL;

    //Create the system device enumerator  
    HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER,
        IID_ICreateDevEnum, reinterpret_cast<void**>(&pDevEnum));
    if (FAILED(hr))
    {
        return 0;
    }
    //创建一个枚举器，枚举视频设备  
    if (FAILED(hr = pDevEnum->CreateClassEnumerator(CLSID_VideoInputDeviceCategory,
        &pEnum, 0)) || NULL == pEnum)
    {
        SAFE_RELEASE(pDevEnum);
        return 0;
    }

    IMoniker *pMoniker = NULL;
    VARIANT varName;
    int dev_count(0);
    while (S_OK == pEnum->Next(1, &pMoniker, NULL))
    {
        IPropertyBag *pPropBag;
        if (FAILED(hr = pMoniker->BindToStorage(0, 0, IID_IPropertyBag, (void**)(&pPropBag))))
        {
            pMoniker->Release();
            continue;       //Skip this one, maybe the next one will work  
        }
        VariantInit(&varName);
        varName.vt = VT_BSTR;
        if (FAILED(hr = pPropBag->Read(L"Description", &varName, 0)))
        {
            hr = pPropBag->Read(L"FriendlyName", &varName, 0);
        }
        if (SUCCEEDED(hr))
        {
            IBaseFilter *filter;;
            if (SUCCEEDED(hr = pMoniker->BindToObject(NULL, 0, IID_IBaseFilter, (void**)&filter)))
            {
                SAFE_RELEASE(filter);

                USES_CONVERSION;
                //设备可用，添加到设备列表
                //devices_vec.push_back(OLE2T(varName.bstrVal));
                string dev_name = unicode2utf(OLE2T(varName.bstrVal));
                strncpy_s(device_list_[dev_count], dev_name.c_str(), dev_name.length());
                dev_count++;
                if (dev_count >= max_size_)
                {
                    break;
                }
                m_VideoDeviceList.push_back(dev_name);
            }
        }
        VariantClear(&varName);
        pPropBag->Release();
        pMoniker->Release();
    }
    SAFE_RELEASE(pEnum);
    SAFE_RELEASE(pDevEnum);
    return dev_count;
}

HRESULT CDShowVideoDevice::InitVideoFormat(int cx_, int cy_, GUID sub_type_)
{
    if (m_pCaptureGB2 == NULL)
    {
        return E_FAIL;
    }

    IAMStreamConfig *pConfig = NULL;
    AM_MEDIA_TYPE   *pMediaType = NULL;

    HRESULT hr = m_pCaptureGB2->FindInterface(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, m_pCaptureFilter,
        IID_IAMStreamConfig, (void**)&pConfig);
    if (FAILED(hr) || NULL == pConfig)
    {
        hr = m_pCaptureGB2->FindInterface(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Interleaved,
            m_pCaptureFilter, IID_IAMStreamConfig, (void **)&pConfig);
        if (FAILED(hr) || NULL == pConfig)
        {
            return E_FAIL;
        }
    }
    int cap_num(0);
    VIDEO_STREAM_CONFIG_CAPS scc;
    float fPercent = float(cx_) / (float)cy_;   //用户要求的宽高比
    SIZE like_fpercent_size;                    //相同宽高比，但是最接近，且大于需求宽高的尺寸
    SIZE unlike_fpercent_size;                  //不同宽高比，但是最接近，且大于需求宽高的尺寸
    memset(&like_fpercent_size, 0, sizeof(like_fpercent_size));
    memset(&unlike_fpercent_size, 0, sizeof(unlike_fpercent_size));

    //获取总共支持多少种媒体格式及大小
    int nCount(0), nSize(0);
    if (FAILED(pConfig->GetNumberOfCapabilities(&nCount, &nSize)))
    {
        return E_FAIL;
    }

    do
    {
        if (FAILED(pConfig->GetStreamCaps(cap_num++, &pMediaType, (BYTE *)&scc)) || !pMediaType)
        {
            break;
        }
        if (pMediaType->formattype == FORMAT_VideoInfo && pMediaType->subtype == sub_type_)
        {
            //         RECT rc = {0, 0, cx_, cy_};
            //((VIDEOINFOHEADER* )pMediaType->pbFormat)->rcSource = rc;
            //((VIDEOINFOHEADER*)pMediaType->pbFormat )->rcTarget = rc;
            //HEADER(pMediaType->pbFormat)->biWidth               = cx_;  
            //HEADER(pMediaType->pbFormat)->biHeight              = cy_;  
            //         HEADER(pMediaType->pbFormat)->biSizeImage           = sample_size;
            //         ((VIDEOINFOHEADER*)pMediaType->pbFormat)->AvgTimePerFrame = scc.MinFrameInterval; //设置最大帧率

            //配置camera采集参数,如果配置失败，则查找最相似的配置
            //if(FAILED(hr = pConfig->SetFormat(pMediaType)))
            if (scc.InputSize.cx != cx_ || scc.InputSize.cy != cy_)
            {
                if (HEADER(pMediaType->pbFormat)->biWidth >= cx_
                    && HEADER(pMediaType->pbFormat)->biHeight >= cy_
                    && is_float_equal(fPercent, float(scc.InputSize.cx)
                    / float(scc.InputSize.cy))) //相同宽高比的
                {
                    if (like_fpercent_size.cx == 0)
                    {
                        like_fpercent_size.cx = scc.InputSize.cx;
                        like_fpercent_size.cy = scc.InputSize.cy;
                    } else
                    {
                        if (scc.InputSize.cx < like_fpercent_size.cx ||
                            scc.InputSize.cy < like_fpercent_size.cy)
                        {
                            //找一个最接近需求宽高，且大于其的尺寸
                            like_fpercent_size.cx = scc.InputSize.cx;
                            like_fpercent_size.cy = scc.InputSize.cy;
                        }
                    }

                } else if (scc.InputSize.cx >= cx_
                    && scc.InputSize.cy >= cy_
                    && !is_float_equal(fPercent, float(scc.InputSize.cx)
                    / float(scc.InputSize.cy))) //不同宽高比的
                {
                    if (unlike_fpercent_size.cx == 0)
                    {
                        unlike_fpercent_size.cx = scc.InputSize.cx;
                        unlike_fpercent_size.cy = scc.InputSize.cy;
                    } else
                    {
                        if (scc.InputSize.cx < unlike_fpercent_size.cx ||
                            scc.InputSize.cy < unlike_fpercent_size.cy)
                        {
                            //找一个最接近需求宽高，且大于其的尺寸
                            unlike_fpercent_size.cx = scc.InputSize.cx;
                            unlike_fpercent_size.cy = scc.InputSize.cy;
                        }
                    }
                }
                DeleteMediaType(pMediaType);
                //继续比对
                continue;
            } else
            {
                //format OK
                memset(&like_fpercent_size, 0, sizeof(like_fpercent_size));
                memset(&unlike_fpercent_size, 0, sizeof(unlike_fpercent_size));
                m_nWidth = scc.InputSize.cx;
                m_nHeigth = scc.InputSize.cy;
                break;
            }
        } else
        {
            DeleteMediaType(pMediaType);
            pMediaType = NULL;
            continue;
        }
        DeleteMediaType(pMediaType);
        SAFE_RELEASE(pConfig);
        return S_OK;
    } while (true);

    //按照最终选定的格式进行配置dshow采集
    if (like_fpercent_size.cx != 0 && like_fpercent_size.cy != 0)
    {
        //优先设置相同宽高比的尺寸
        m_nWidth = like_fpercent_size.cx;
        m_nHeigth = like_fpercent_size.cy;
    } else if (unlike_fpercent_size.cx != 0 && unlike_fpercent_size.cy != 0)
    {
        //设置不同宽高比的尺寸
        m_nWidth = unlike_fpercent_size.cx;
        m_nHeigth = unlike_fpercent_size.cy;
    } else if (m_nWidth == 0 || m_nHeigth == 0)
    {
        //找不到合适的格式和尺寸，释放资源，返回fail
        DeleteMediaType(pMediaType);
        SAFE_RELEASE(pConfig);
        return E_FAIL;
    }
    //根据选定的采集格式，计算每一帧的图像大小
    int sample_size(0);
    if (MEDIASUBTYPE_RGB24 == sub_type_)
    {
        sample_size = m_nWidth * m_nHeigth * 3;
    } else if (MEDIASUBTYPE_YUY2 == sub_type_)
    {
        sample_size = m_nWidth * m_nHeigth * 2;
    } else
    {
        return E_FAIL;
    }
    m_nCurrentSampleBufLen = sample_size;

    hr = m_pCaptureGB2->FindInterface(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Video, m_pCaptureFilter,
        IID_IAMStreamConfig, (void**)&pConfig);
    if (FAILED(hr) || NULL == pConfig)
    {
        hr = m_pCaptureGB2->FindInterface(&PIN_CATEGORY_CAPTURE, &MEDIATYPE_Interleaved,
            m_pCaptureFilter, IID_IAMStreamConfig, (void **)&pConfig);
        if (FAILED(hr) || NULL == pConfig)
        {
            return E_FAIL;
        }
    }
    if (FAILED(pConfig->GetFormat(&pMediaType)))
    {
        return E_FAIL;
    }
    RECT rc = { 0, 0, m_nWidth, m_nHeigth };
    pMediaType->formattype = FORMAT_VideoInfo;
    pMediaType->subtype = sub_type_;
    ((VIDEOINFOHEADER*)pMediaType->pbFormat)->rcSource = rc;
    ((VIDEOINFOHEADER*)pMediaType->pbFormat)->rcTarget = rc;
    HEADER(pMediaType->pbFormat)->biWidth = m_nWidth;
    HEADER(pMediaType->pbFormat)->biHeight = m_nHeigth;
    HEADER(pMediaType->pbFormat)->biSizeImage = sample_size;
    ((VIDEOINFOHEADER*)pMediaType->pbFormat)->AvgTimePerFrame = 330000; //设置最大帧率

    //配置camera采集参数,如果配置失败，则查找最相似的配置
    if (FAILED(hr = pConfig->SetFormat(pMediaType)))
    {
        DeleteMediaType(pMediaType);
        SAFE_RELEASE(pConfig);
        return E_FAIL;
    }
    DeleteMediaType(pMediaType);
    SAFE_RELEASE(pConfig);
    return S_OK;;
}

bool  CDShowVideoDevice::StartCapture()
{
    if (m_pMediaControl)
    {
        if (SUCCEEDED(m_pMediaControl->Run()))
        {
            m_dbLastFrameTime = 0.0;
            m_bIsPlaying = true;
            return true;
        }
    }
    return false;
}

bool  CDShowVideoDevice::StopCapture()
{
    if (m_pMediaControl)
    {
        if (SUCCEEDED(m_pMediaControl->Stop()))
        {
            m_bIsPlaying = false;
            return true;
        }
    }
    return false;
}

HRESULT STDMETHODCALLTYPE CDShowVideoDevice::SampleCB(double SampleTime, IMediaSample *pSample)
{
    return S_OK;
}

HRESULT STDMETHODCALLTYPE CDShowVideoDevice::BufferCB(double SampleTime, BYTE *pBuffer, long BufferLen)
{
    if (0 != SampleTime && 0 != m_dbLastFrameTime)
    {
        if (SampleTime - m_dbLastFrameTime < m_dFrameInterval)
        {
            return S_OK;//控制帧频率
        }
    }
    if (m_nCurrentSampleBufLen != BufferLen)
    {
        //获取到的图像与自己初始化设置的参数不一致，异常
        return E_FAIL;
    }
    m_dbLastFrameTime += m_dFrameInterval;

    ::EnterCriticalSection(&m_Lock);

    //最多缓冲 MAX_BUFFER_FRMAE 帧
    if (m_nNewFrameNum >= MAX_BUFFER_FRMAE)
    {
        //覆盖第一个
        unsigned char* buf = m_FrameList.front();
        memcpy(buf, pBuffer, BufferLen);
        m_FrameList.erase(m_FrameList.begin());
        m_FrameList.push_back(buf);
    } else
    {
        memcpy(m_FrameList[m_nNewFrameNum], pBuffer, BufferLen);
        ++m_nNewFrameNum;
    }
    ::LeaveCriticalSection(&m_Lock);
    return S_OK;
}


//尝试获取采集设备时间，目前只抓取设备拔出事件,
//@param dev_name_ : 如果有设备拔出，输出设备名称
//@return 0:没有拔出；1 : 被拔出
int CDShowVideoDevice::HandleEvent(__out string& dev_name_)
{
    dev_name_.clear();
    if (m_pEvent)
    {
        LONG eventCode = 0, eventParam1 = 0, eventParam2 = 0;
        while (SUCCEEDED(m_pEvent->GetEvent(&eventCode, &eventParam1, &eventParam2, 0)))
        {
            m_pEvent->FreeEventParams(eventCode, eventParam1, eventParam2);
            switch (eventCode)
            {
            case EC_COMPLETE:
            case EC_USERABORT:
            case EC_ERRORABORT:
                break;

            case EC_DEVICE_LOST:
            {
                dev_name_ = m_curDeviceName;
                return 1;
            }
            break;

            default:
                break;
            }
        }
    }
    return 0;
}

int CDShowVideoDevice::ReadVideoFrame(const unsigned char *out_data_, const int& max_size_,
    int &img_width_, int &img_height_, GUID& media_sub_type_)
{
    int ret = -1;
    if (NULL == out_data_ || max_size_ <= 0 || 0 == m_FrameList.size() || m_nNewFrameNum <= 0)
    {
        return -1;
    }
    ::EnterCriticalSection(&m_Lock);
    if (m_nCurrentSampleBufLen > max_size_)
    {
        ret = -2;
    } else
    {
        unsigned char* buf = m_FrameList.front();
        memcpy((void *)out_data_, (void *)buf, m_nCurrentSampleBufLen);
        --m_nNewFrameNum;

        m_FrameList.erase(m_FrameList.begin());
        m_FrameList.push_back(buf);

        img_width_ = m_nWidth;
        img_height_ = m_nHeigth;
        ret = m_nCurrentSampleBufLen;
        media_sub_type_ = m_cur_subtype;
    }
    ::LeaveCriticalSection(&m_Lock);
    return ret;
}

LPCSTR  CDShowVideoDevice::GetCurrentCapVideoDeviceName()
{
    return m_curDeviceName.c_str();
}

bool CDShowVideoDevice::HasSettingDialog(int nType)
{
    if (m_pVfwDialogs)
        return SUCCEEDED(m_pVfwDialogs->HasDialog(nType));
    else
    {
        //尝试vga驱动
        ISpecifyPropertyPages *pSpec = 0;
        CAUUID cauuid;

        bool bHas = false;

        HRESULT hr = m_pCaptureFilter->QueryInterface(IID_ISpecifyPropertyPages,
            (void **)&pSpec);
        if (hr == S_OK)
        {
            hr = pSpec->GetPages(&cauuid);
            if (cauuid.cElems > 0)
            {
                //有
                bHas = true;
            }
            CoTaskMemFree(cauuid.pElems);
        }
        if (pSpec)
            pSpec->Release();
        return bHas;
    }
}
bool  CDShowVideoDevice::ShowVideoDialogBox(HWND hWndParent)
{
    if (NULL == hWndParent)
    {
        hWndParent = ::GetDesktopWindow();
    }
    HRESULT hr = E_FAIL;
    if (m_pVfwDialogs)
    {
        // Check if the device supports this dialog box.
        //必须暂停捕捉,否则vfw对话框不能正常工作
        if (m_bIsPlaying)
        {
            StopCapture();
        }

        if (SUCCEEDED(m_pVfwDialogs->HasDialog(0)))
        {
            // Show the dialog box.
            hr = m_pVfwDialogs->ShowDialog(0, hWndParent);
        }
        if (m_bIsPlaying)
        {
            StartCapture();
        }
        return SUCCEEDED(hr);
    } else if (m_pCaptureFilter)
    {
        //尝试vga驱动
        ISpecifyPropertyPages *pSpec = 0;
        CAUUID cauuid;


        hr = m_pCaptureFilter->QueryInterface(IID_ISpecifyPropertyPages,
            (void **)&pSpec);
        if (hr == S_OK)
        {
            hr = pSpec->GetPages(&cauuid);
            if (cauuid.cElems > 0)
            {
                hr = OleCreatePropertyFrame(hWndParent, 0, 0, L"视频调节", 1,
                    (IUnknown **)&m_pCaptureFilter, cauuid.cElems,
                    (GUID *)cauuid.pElems, 0, 0, NULL);
            }

            CoTaskMemFree(cauuid.pElems);
        }
        if (pSpec)
            pSpec->Release();

        return SUCCEEDED(hr);
    }
    return false;
}

#endif //WIN32
