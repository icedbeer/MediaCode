#include "stdafx.h"
#include "LoadLibrary_Interface.h"
#include "record_interface.h"
#include "player_interface.h"
#include "global_config.h"

IRecordInterface* GetRecordInterface(ILiveEvent* event_)
{
    return new CRecordInterface(event_);
}

void ReleaseRecordInterface(IRecordInterface** interface_)
{
    if (*interface_)
    {
        delete *interface_;
        *interface_ = 0;
    }
}

IPlayerInterface* GetPlayerInterface(ILiveEvent* event_)
{
    return new CPlayerInterface(event_);
}

void ReleasePlayerInterface(IPlayerInterface** interface_)
{
    if (*interface_)
    {
        delete *interface_;
        *interface_ = 0;
    }
}

extern "C" INTERFACE_API void ReleaseDllResource()
{
    DESTROY_GLOBAL_CONFIG();
}
