#pragma once
#include <stdint.h>
#include <map>
using namespace std;

#define MAX_AUDIO_BUF 4 //播放缓冲的通知索引
#define BUFFERNOTIFYSIZE 8192 //通知位置的大小，请参见DirectSound应用程序开发快速入门
#define AVCODEC_MAX_AUDIO_FRAME_SIZE 192000

#ifndef _max
#define _max(a, b) a > b ? a : b
#endif

#ifndef _min
#define _min(a, b) a > b ? b : a
#endif

typedef void *HANDLE;

typedef struct _FFMPEGPLAYER *PFFMPEGPLAYER;
typedef struct _TFIFOBUFFER *PTFIFOBUFFER;

#ifndef GUID_DEFINED
#define GUID_DEFINED
#if defined(__midl)
typedef struct {
	unsigned long  Data1;
	unsigned short Data2;
	unsigned short Data3;
	byte           Data4[ 8 ];
} GUID;
#else
typedef struct _GUID {
	unsigned long  Data1;
	unsigned short Data2;
	unsigned short Data3;
	unsigned char  Data4[ 8 ];
} GUID;
#endif
#endif

class _declspec(dllexport) Mp3Player
{
public:
	Mp3Player();
	~Mp3Player();

	//播放MP3文件
	int Play(const char *fileName, int nDeviceID = -1, bool loop_play = true);	
	int Stop();

	uint32_t GetVolume();

	//获取声音输出设备个数，用于设备ID时，从0递增
	int GetAudioOutDevice();
	string GetDeviceName(int deviceID);

	int GetDeviceNum(){return m_deviceNum;}

protected:
	bool InitDSound(int nDevID = -1);
	bool DestoryDSound();
	GUID GetDeviceGuid(int deviceID);
	int MediaAudioDiaplayCallBack(uint8_t *pu8Buffer, uint32_t *pu32Length);
	friend void DecodeThread(void *arg);

public:
	int					m_deviceNum;
	map<int , string >	m_deviceNameMap;
	map<int , GUID >	m_deviceGuidMap;

protected:
	bool				m_bPlaying, m_bLoop, m_bStop;
	string				m_curFileName;
	HANDLE				m_hThreadHandle;
	PFFMPEGPLAYER		m_pPlayer;
	uint32_t					m_Volume;
};
