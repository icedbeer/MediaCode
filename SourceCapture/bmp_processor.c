#ifdef __cplusplus
extern "C" {
#endif

#include "bmp_processor.h"
#include <string.h>

void smooth(int video_width, int video_height, void *_src, void *_dst, int _size)
{
	unsigned char *pSrc = (unsigned char *)_src;
	unsigned char *pDst = (unsigned char *)_dst;

  
	register int i = 0;
	register int j = 0;
	
	unsigned int	sum_value_r = 0;
	unsigned int	sum_value_g = 0;
	unsigned int	sum_value_b = 0;

	int bottom = 0;

	if(video_width * video_height * 3 != _size  || _src == NULL || _dst == NULL)
	{
		return ;
	}

	memcpy(_dst, _src, video_width * video_height * sizeof(unsigned char) * 3); 

	bottom = (video_height - 1) * video_width * 3;
	//上下边框
	for(i = 0; i < video_width * 3; i ++)
	{
		*(pDst + i) = *(pSrc + i);
		*(pDst + i + bottom) = *(pSrc + i + bottom);
	}


	//左右边框
	for(i = 1; i < video_height - 1; i++)
	{
		*(pDst + i * video_width * 3) = *(pSrc + i * video_width * 3);
		*(pDst + i * video_width * 3 + 1) = *(pSrc + i * video_width * 3 + 1);
		*(pDst + i * video_width * 3 + 2) = *(pSrc + i * video_width * 3 + 2);


		*(pDst + (i + 1) * video_width * 3 - 3) = *(pSrc + (i + 1) * video_width * 3 - 3);
		*(pDst + (i + 1) * video_width * 3 - 2) = *(pSrc + (i + 1) * video_width * 3 - 2);
		*(pDst + (i + 1) * video_width * 3 - 1) = *(pSrc + (i + 1) * video_width * 3 - 1);
	}

	for(i = 1; i < video_height - 1; ++i)
	{
		for(j = 1; j < video_width - 1; ++j)
		{
			//上一行
			sum_value_r = *(pSrc + ((i-1) * video_width + j - 1) * 3);
			sum_value_g = *(pSrc + ((i-1) * video_width + j - 1) * 3 + 1);
			sum_value_b = *(pSrc + ((i-1) * video_width + j - 1) * 3 + 2);

			sum_value_r += *(pSrc + ((i-1) * video_width + j) * 3) * 2;
			sum_value_g += *(pSrc + ((i-1) * video_width + j) * 3 + 1) * 2;
			sum_value_b += *(pSrc + ((i-1) * video_width + j) * 3 + 2) * 2;


			sum_value_r += *(pSrc + ((i-1) * video_width + j + 1) * 3);
			sum_value_g += *(pSrc + ((i-1) * video_width + j + 1) * 3 + 1);
			sum_value_b += *(pSrc + ((i-1) * video_width + j + 1) * 3 + 2);


			//本行
			sum_value_r += *(pSrc + ((i) * video_width + j - 1) * 3) * 2;
			sum_value_g += *(pSrc + ((i) * video_width + j - 1) * 3 + 1) * 2;
			sum_value_b += *(pSrc + ((i) * video_width + j - 1) * 3 + 2) * 2;

			sum_value_r += *(pSrc + ((i) * video_width + j) * 3) * 24;
			sum_value_g += *(pSrc + ((i) * video_width + j) * 3 + 1) * 24;
			sum_value_b += *(pSrc + ((i) * video_width + j) * 3 + 2) * 24;


			sum_value_r += *(pSrc + ((i) * video_width + j + 1) * 3) * 2;
			sum_value_g += *(pSrc + ((i) * video_width + j + 1) * 3 + 1) * 2;
			sum_value_b += *(pSrc + ((i) * video_width + j + 1) * 3 + 2) * 2;


			//下一行
			sum_value_r += *(pSrc + ((i+1) * video_width + j - 1) * 3);
			sum_value_g += *(pSrc + ((i+1) * video_width + j - 1) * 3 + 1);
			sum_value_b += *(pSrc + ((i+1) * video_width + j - 1) * 3 + 2);

			sum_value_r += *(pSrc + ((i+1) * video_width + j) * 3) * 2;
			sum_value_g += *(pSrc + ((i+1) * video_width + j) * 3 + 1) * 2;
			sum_value_b += *(pSrc + ((i+1) * video_width + j) * 3 + 2) * 2;


			sum_value_r += *(pSrc + ((i+1) * video_width + j + 1) * 3);
			sum_value_g += *(pSrc + ((i+1) * video_width + j + 1) * 3 + 1);
			sum_value_b += *(pSrc + ((i+1) * video_width + j + 1) * 3 + 2);


			*(pDst + ((i) * video_width + j) * 3) = sum_value_r / 36;
			*(pDst + ((i) * video_width + j) * 3 + 1) = sum_value_g / 36;
			*(pDst + ((i) * video_width + j) * 3 + 2) = sum_value_b / 36;
		}
	}
}


void sharpen(int video_width, int video_height, unsigned char *_src, unsigned char *_dst, int _size)
{
	int sum_r, sum_g, sum_b;
	int index, pos, dst_pos;

	int m, n, j, i;

	int templates[9] = { -1, -2, -1,
						 -2, 36, -2,
						 -1, -2, -1,};          


	memcpy(_dst, _src, video_width * video_height * sizeof(unsigned char) * 3);   


	for(j=2; j < video_height-2; ++j)   
	{   
		for(i=2; i < video_width-2; ++i)   
		{   
			sum_r = 0;   
			sum_g = 0;
			sum_b = 0;
			index = 0;   


			for(m = j-1; m<j + 2; ++ m)   
			{   
				for(n = i-1; n<i + 2; ++ n)   
				{   
					pos  = (m * video_width + n) * 3;
					sum_r += _src[pos] * templates[index];   
					sum_g += _src[pos + 1] * templates[index];   
					sum_b += _src[pos + 2] * templates[index];   
					++ index;
				}   
			}   


			sum_r /= 24;   
			if (sum_r > 255)   
				sum_r = 255;   
			else if (sum_r <0)   
				sum_r = 0;   


			sum_g /= 24;   
			if (sum_g > 255)   
				sum_g = 255;   
			else if (sum_g <0)   
				sum_g = 0;   



			sum_b /= 24;   
			if (sum_b > 255)   
				sum_b = 255;   
			else if (sum_b < 0)  
				sum_b = 0; 


			dst_pos = (j * video_width + i) * 3;
			_dst[dst_pos] = sum_r;   
			_dst[dst_pos + 1] = sum_g;   
			_dst[dst_pos + 2] = sum_b;   
		}   
	}   
} 

#ifdef __cplusplus
}
#endif