// Mp3Player.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <stdint.h>
#include "Mp3Player.h"
#include <io.h>
#include <process.h>

typedef struct _TFIFOBUFFER
{
	uint8_t *pu8Buffer, *pu8Read, *pu8Write, *pu8End;
}TFIFOBUFFER, *PTFIFOBUFFER;

PTFIFOBUFFER FifoBufferCreate(int s32Size);
void FifoBufferDestroy(PTFIFOBUFFER pHandle);
void FifoBufferReset(PTFIFOBUFFER pHandle);
int FifoBufferSize(PTFIFOBUFFER pHandle);
int FifoBufferWrite(PTFIFOBUFFER pHandle, uint8_t *pu8Buffer, int s32Size);
int FifoBufferRead(PTFIFOBUFFER pHandle, uint8_t *pu8Buffer, int *ps32Size);

DWORD g_dwNextWriteOffset;

typedef struct _FFMPEGPLAYER
{
	AVFormatContext *ptAVFormatContextIn;
	AVStream *ptAVStreamA;
	PTFIFOBUFFER hFifoBufferHandleA;
	uint8_t *pu8BufferConvertA;
	SwrContext *pAudioResample;
	AVFrame *ptDecodeFrameA;
	int audiostreamindex;

	LPDIRECTSOUNDBUFFER8 m_pDSBuffer8;
	LPDIRECTSOUND8 g_pDsd;
	LPDIRECTSOUNDNOTIFY8 g_pDSNotify; 
	DSBPOSITIONNOTIFY g_aPosNotify[MAX_AUDIO_BUF];
	HANDLE m_events[MAX_AUDIO_BUF];
}FFMPEGPLAYER, *PFFMPEGPLAYER;

BOOL CALLBACK DSEnumProc(LPGUID lpGUID,	LPCTSTR lpszDesc,LPCTSTR lpszDrvName,LPVOID lpContext );

Mp3Player::Mp3Player()
{
	m_deviceNum = -1;
	m_pPlayer = NULL;
	m_bPlaying = false;
	m_bLoop = true;
	m_bStop = true;
	m_hThreadHandle = NULL;
	m_Volume = 0;
	GetAudioOutDevice();
	av_register_all();	
}
Mp3Player::~Mp3Player()
{
	Stop();
	m_deviceGuidMap.clear();
	m_deviceNameMap.clear();
}

int Mp3Player::GetAudioOutDevice()
{
	m_deviceNum = 0;
	m_deviceGuidMap.clear();
	m_deviceNameMap.clear();
	if (FAILED(DirectSoundEnumerate((LPDSENUMCALLBACK)DSEnumProc, (VOID *)this)))
	{
		return -1;
	}
	return m_deviceNum;
}

string Mp3Player::GetDeviceName(int deviceID)
{
	if (deviceID < 0 || deviceID >= m_deviceNum)
	{
		return "";
	}
	return m_deviceNameMap[deviceID];
}
GUID Mp3Player::GetDeviceGuid(int deviceID)
{
	if (deviceID < 0 || deviceID >= m_deviceNum)
	{
		return GUID_NULL;
	}
	return m_deviceGuidMap[deviceID+1];
}

int Mp3Player::Play(const char *fileName, int nDevID/* = -1*/, bool loop_play/* = true*/)
{
	if (NULL != m_pPlayer)
	{
		Stop();
	}
	if (NULL == fileName || 0 != _access(fileName, 00))
	{
		FILE *fp = fopen("mp3Play.log", "a+");
		char buf[_MAX_PATH] = {0};
		sprintf(buf, "Mp3Player Player failed!  file:%s not existed! deviceId:%d", fileName, nDevID);
		fwrite(buf, 1, _MAX_PATH, fp);
		fclose(fp);
		return -1;
	}
	m_curFileName.assign(fileName);
	m_bLoop = loop_play;

	m_pPlayer = new _FFMPEGPLAYER;
	memset(m_pPlayer, 0, sizeof(_FFMPEGPLAYER));
	m_pPlayer->ptAVStreamA = NULL;
	m_pPlayer->hFifoBufferHandleA = NULL;
	m_pPlayer->pu8BufferConvertA = NULL;
	m_pPlayer->pAudioResample = NULL;
	m_pPlayer->ptDecodeFrameA = NULL;
	m_pPlayer->audiostreamindex = -1;
	m_pPlayer->ptAVFormatContextIn = NULL;
	int ret = 0;
	do 
	{
		m_pPlayer->ptAVFormatContextIn = avformat_alloc_context();
		ret = avformat_open_input(&m_pPlayer->ptAVFormatContextIn, fileName, NULL, NULL);
		if (ret < 0)
		{
			char buff[256] = {0};
			av_strerror(ret, buff, 256);
			FILE *fp = fopen("mp3Play.log", "a+");
			char buf[_MAX_PATH] = {0};
			sprintf(buf, "Mp3Player Player failed!  file:%s  deviceId:%d, error1:%s", fileName, nDevID, buff);
			fwrite(buf, 1, _MAX_PATH, fp);
			fclose(fp);
			ret = -1;
			break;
		}
		ret = avformat_find_stream_info(m_pPlayer->ptAVFormatContextIn, NULL);
		if(ret < 0)
		{
			char buff[256] = {0};
			av_strerror(ret, buff, 256);
			FILE *fp = fopen("mp3Play.log", "a+");
			char buf[_MAX_PATH] = {0};
			sprintf(buf, "Mp3Player Player failed!  file:%s  deviceId:%d, error2:%s", fileName, nDevID, buff);
			fwrite(buf, 1, _MAX_PATH, fp);
			fclose(fp);
			ret = -2;
			break;
		}
		m_pPlayer->audiostreamindex = av_find_best_stream(m_pPlayer->ptAVFormatContextIn, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);
		if (m_pPlayer->audiostreamindex >= 0)
		{
			m_pPlayer->ptAVStreamA = m_pPlayer->ptAVFormatContextIn->streams[m_pPlayer->audiostreamindex];
			AVCodecContext *ptAVCodecContext = m_pPlayer->ptAVStreamA->codec;
			AVCodec *ptAVCodec = avcodec_find_decoder(ptAVCodecContext->codec_id);
			if (NULL != ptAVCodec && 0 == avcodec_open2(ptAVCodecContext, ptAVCodec, NULL))
			{
				m_pPlayer->hFifoBufferHandleA = FifoBufferCreate(AVCODEC_MAX_AUDIO_FRAME_SIZE * 3);
				m_pPlayer->pu8BufferConvertA = (uint8_t *)malloc(AVCODEC_MAX_AUDIO_FRAME_SIZE);
				if(NULL == m_pPlayer->hFifoBufferHandleA || NULL == m_pPlayer->pu8BufferConvertA)
				{
					ret = -3;
					break;
				}
				m_pPlayer->ptDecodeFrameA = avcodec_alloc_frame();
				if (NULL == m_pPlayer->ptDecodeFrameA)
				{
					ret = -4;
					break;
				}
				m_pPlayer->pAudioResample = swr_alloc_set_opts(NULL, AV_CH_LAYOUT_STEREO, AV_SAMPLE_FMT_S16, 44100, 
					ptAVCodecContext->channel_layout, ptAVCodecContext->sample_fmt, ptAVCodecContext->sample_rate, 0 , NULL);
				if (NULL == m_pPlayer->pAudioResample)
				{
					ret = -5;
					break;
				}
				if (0 > swr_init(m_pPlayer->pAudioResample))
				{
					ret = -6;
					break;
				}
				m_bPlaying = true;
				m_bStop = false;
				InitDSound(nDevID);
				m_hThreadHandle = (HANDLE )_beginthread(DecodeThread, 0, this);
				return ret;
			}else
			{
				ret = -7;
				break;
			}
		}else
		{
			ret = -8;
			break;
		}
	} while (false);
	if (NULL != m_pPlayer)
	{
		if (NULL != m_pPlayer->hFifoBufferHandleA)
		{
			FifoBufferDestroy(m_pPlayer->hFifoBufferHandleA);
			m_pPlayer->hFifoBufferHandleA = NULL;
		}
		if (NULL != m_pPlayer->pu8BufferConvertA)
		{
			free(m_pPlayer->pu8BufferConvertA);
			m_pPlayer->pu8BufferConvertA = NULL;
		}
		if (NULL != m_pPlayer->ptDecodeFrameA)
		{
			avcodec_free_frame(&m_pPlayer->ptDecodeFrameA);
			m_pPlayer->ptDecodeFrameA = NULL;
		}
		if (NULL != m_pPlayer->pAudioResample)
		{
			swr_free(&m_pPlayer->pAudioResample);
			m_pPlayer->pAudioResample = NULL;
		}
		if(NULL != m_pPlayer->ptAVStreamA && NULL != m_pPlayer->ptAVStreamA->codec)
		{
			avcodec_close(m_pPlayer->ptAVStreamA->codec);
		}
		if (NULL != m_pPlayer->ptAVFormatContextIn)
		{
			avformat_close_input(&m_pPlayer->ptAVFormatContextIn);
			m_pPlayer->ptAVFormatContextIn = NULL;
		}
		delete m_pPlayer;
		m_pPlayer = NULL;
	}

	return ret;
}

int Mp3Player::Stop()
{
	m_bStop = true;
	m_bPlaying = false;
	if (NULL != m_pPlayer)
	{
		if (NULL != m_hThreadHandle)
		{
			::WaitForSingleObject(m_hThreadHandle, INFINITE);
			m_hThreadHandle = NULL;
		}
		if (NULL != m_pPlayer && NULL != m_pPlayer->hFifoBufferHandleA)
		{
			FifoBufferDestroy(m_pPlayer->hFifoBufferHandleA);
			m_pPlayer->hFifoBufferHandleA = NULL;
		}
		if (NULL != m_pPlayer && NULL != m_pPlayer->pu8BufferConvertA)
		{
			free(m_pPlayer->pu8BufferConvertA);
			m_pPlayer->pu8BufferConvertA = NULL;
		}
		if (NULL != m_pPlayer && NULL != m_pPlayer->ptDecodeFrameA)
		{
			avcodec_free_frame(&m_pPlayer->ptDecodeFrameA);
			m_pPlayer->ptDecodeFrameA = NULL;
		}
		if (NULL != m_pPlayer && NULL != m_pPlayer->pAudioResample)
		{
			swr_free(&m_pPlayer->pAudioResample);
			m_pPlayer->pAudioResample = NULL;
		}
		if(NULL != m_pPlayer && NULL != m_pPlayer->ptAVStreamA && NULL != m_pPlayer->ptAVStreamA->codec)
		{
			avcodec_close(m_pPlayer->ptAVStreamA->codec);
		}
		if (NULL != m_pPlayer && NULL != m_pPlayer->ptAVFormatContextIn)
		{
			avformat_close_input(&m_pPlayer->ptAVFormatContextIn);
			m_pPlayer->ptAVFormatContextIn = NULL;
		}
		if (NULL != m_pPlayer)
		{
			delete m_pPlayer;
		}
		m_pPlayer = NULL;
		DestoryDSound();
	}	
	return 0;
}

#define VOLUMEMAX   32767
#define VOLUMEMIN	-32768

#ifndef core_min
#define core_min(a, b)		((a) < (b) ? (a) : (b))
#endif

uint32_t get_average_signal_level(const int16_t* data, int32_t data_size)
{
	uint32_t ret = 0;

	if(data_size > 0)
	{
		int32_t sum = 0;
		int16_t* pos = (int16_t *)data;
		for (int i=0; i < data_size; i++)
		{
			sum += abs(*pos);
			pos ++;
		}

		ret = sum * 500.0 / (data_size * VOLUMEMAX);
		ret = core_min(ret, 100);
	}

	return ret;
}

int Mp3Player::MediaAudioDiaplayCallBack(uint8_t *pu8Buffer, uint32_t *pu32Length)
{
	if(NULL != m_pPlayer)
	{
		int s32Temp = 0, s32Return = 0, s32DecodedSize = 0, s32ConvertSize = 0, s32Got = 0;
		uint8_t *pu8Temp1 = NULL;

		memset(pu8Buffer, 0, (int)(*pu32Length));
		do
		{
			AVPacket tPacket;
			int s32Return = av_read_frame(m_pPlayer->ptAVFormatContextIn, &tPacket);
			if(0 > s32Return)
			{
				if(AVERROR_EOF == (uint32_t)s32Return
					|| (m_pPlayer->ptAVFormatContextIn->pb && m_pPlayer->ptAVFormatContextIn->pb->eof_reached)
					|| (m_pPlayer->ptAVFormatContextIn->pb && m_pPlayer->ptAVFormatContextIn->pb->error))
				{
					//重头播放
					if (m_bLoop)
					{
						FifoBufferReset(m_pPlayer->hFifoBufferHandleA);
						av_seek_frame(m_pPlayer->ptAVFormatContextIn, m_pPlayer->audiostreamindex, 0, -1);
						continue;
					}else
					{
						m_bStop = true;
						*pu32Length = 0;
						return 0;
					}
				}
			} else
			{
				if(m_pPlayer->audiostreamindex >= 0 && tPacket.stream_index == m_pPlayer->ptAVStreamA->index)
				{
					
				}else
				{
					av_free_packet(&tPacket);
					continue;
				}
			}
			//开始解码
			pu8Temp1 = tPacket.data;
			do
			{
				avcodec_get_frame_defaults(m_pPlayer->ptDecodeFrameA);
				s32DecodedSize = avcodec_decode_audio4(m_pPlayer->ptAVStreamA->codec, m_pPlayer->ptDecodeFrameA, &s32Got, &tPacket);
				if(0 > s32DecodedSize || 0 == s32Got)
				{
					av_free_packet(&tPacket);
					break;
				}
				av_free_packet(&tPacket);
				tPacket.data += s32DecodedSize;
				tPacket.size -= s32DecodedSize;
				if(NULL != m_pPlayer->pAudioResample && NULL != m_pPlayer->pu8BufferConvertA)
				{
					int32_t resamplenum     = 0;
					memset(m_pPlayer->pu8BufferConvertA, 0, AVCODEC_MAX_AUDIO_FRAME_SIZE);
					resamplenum = swr_convert(m_pPlayer->pAudioResample, &m_pPlayer->pu8BufferConvertA, (*pu32Length)/4, (const uint8_t **)m_pPlayer->ptDecodeFrameA->data,
						m_pPlayer->ptDecodeFrameA->nb_samples);
					FifoBufferWrite(m_pPlayer->hFifoBufferHandleA, m_pPlayer->pu8BufferConvertA, resamplenum * 4);
				}
				else
				{
					FifoBufferWrite(m_pPlayer->hFifoBufferHandleA, m_pPlayer->ptDecodeFrameA->extended_data[0],
						m_pPlayer->ptDecodeFrameA->nb_samples * m_pPlayer->ptAVStreamA->codec->channels * av_get_bytes_per_sample(m_pPlayer->ptAVStreamA->codec->sample_fmt));
				}
			}while(tPacket.size > 0);
			tPacket.data = pu8Temp1;
			s32Temp = FifoBufferSize(m_pPlayer->hFifoBufferHandleA);
			if((int)(*pu32Length) <= s32Temp)
			{
				FifoBufferRead(m_pPlayer->hFifoBufferHandleA, pu8Buffer, (int *)pu32Length);
				m_Volume = get_average_signal_level((int16_t *)pu8Buffer, (*pu32Length)/2);
				return 1;
			}
			else
			{
				continue;
			}
		}while(1);
	}
	return 0;
}

bool Mp3Player::InitDSound(int nDevID/* = -1*/)
{
	if (NULL == m_pPlayer)
	{
		return false;
	}
	m_pPlayer->m_pDSBuffer8=NULL; //buffer
	m_pPlayer->g_pDsd=0; //dsound
	m_bStop = FALSE; //是否正在播放
	m_pPlayer->g_pDSNotify=0; 
	HRESULT hr;
	if (nDevID >= 0)
	{
		GUID g = GetDeviceGuid(nDevID);
		if(FAILED(hr=DirectSoundCreate8(&GetDeviceGuid(nDevID),&m_pPlayer->g_pDsd,NULL)))
			return FALSE;
	}else
		if(FAILED(hr=DirectSoundCreate8(NULL,&m_pPlayer->g_pDsd,NULL)))
			return FALSE;
	if(FAILED(hr=m_pPlayer->g_pDsd->SetCooperativeLevel(::GetDesktopWindow(),DSSCL_NORMAL)))
	{
		return FALSE;
	}
	DSBUFFERDESC dsbd;
	printf("%d\n",sizeof(dsbd));
	memset(&dsbd,0,sizeof(dsbd));
	dsbd.dwSize=sizeof(dsbd);
	dsbd.dwFlags=DSBCAPS_GLOBALFOCUS | DSBCAPS_CTRLPOSITIONNOTIFY |DSBCAPS_GETCURRENTPOSITION2;
	dsbd.dwBufferBytes=MAX_AUDIO_BUF*BUFFERNOTIFYSIZE; 
	dsbd.lpwfxFormat=(WAVEFORMATEX*)malloc(sizeof(WAVEFORMATEX));
	dsbd.lpwfxFormat->wFormatTag=WAVE_FORMAT_PCM;   
	/* format type */
	(dsbd.lpwfxFormat)->nChannels=2;    
	(dsbd.lpwfxFormat)->nSamplesPerSec=44100;    
	(dsbd.lpwfxFormat)->nAvgBytesPerSec=44100*(16/8)*2;
	(dsbd.lpwfxFormat)->nBlockAlign=(16/8)*2;
	(dsbd.lpwfxFormat)->wBitsPerSample=16;   
	(dsbd.lpwfxFormat)->cbSize=0;
	//创建DirectSound辅助缓冲区
	LPDIRECTSOUNDBUFFER lpbuffer;
	if(DS_OK != (hr=(m_pPlayer->g_pDsd->CreateSoundBuffer(&dsbd,&lpbuffer,NULL))))
	{   
		return FALSE;
	}
	if( FAILED(hr=(lpbuffer->QueryInterface(IID_IDirectSoundBuffer8,(LPVOID*)&m_pPlayer->m_pDSBuffer8))))
	{
		return false ;
	}
	lpbuffer->Release();
	//设置DirectSound通知 机制
	if(FAILED(hr=(m_pPlayer->m_pDSBuffer8->QueryInterface(IID_IDirectSoundNotify,(LPVOID*)&m_pPlayer->g_pDSNotify))))
	{
		return false ;
	}
	for(int i =0;i<MAX_AUDIO_BUF;i++)
	{
		m_pPlayer->g_aPosNotify[i].dwOffset =i*BUFFERNOTIFYSIZE;
		m_pPlayer->m_events[i]=::CreateEvent(NULL,false,true,NULL); 
		m_pPlayer->g_aPosNotify[i].hEventNotify=m_pPlayer->m_events[i];
	}
	m_pPlayer->g_pDSNotify->SetNotificationPositions(MAX_AUDIO_BUF,m_pPlayer->g_aPosNotify);
	m_pPlayer->g_pDSNotify->Release();
	return true;
}

void DecodeThread(void *arg)
{
	Mp3Player *pParent = (Mp3Player *)arg;
	if (NULL == pParent && NULL != pParent->m_pPlayer)
	{
		return;
	}
	pParent->m_bStop =false;
	DWORD res=5;
	DWORD len;
	int flag=1;
	pParent->m_pPlayer->m_pDSBuffer8->Play(0,0,DSBPLAY_LOOPING);
	g_dwNextWriteOffset=BUFFERNOTIFYSIZE;
	while(!pParent->m_bStop)
	{
		res = WaitForMultipleObjects (MAX_AUDIO_BUF, pParent->m_pPlayer->m_events, FALSE, INFINITE);
		if((res >=WAIT_OBJECT_0)&&(res <=WAIT_OBJECT_0+3)) 
		{
			VOID* pDSLockedBuffer = NULL;
			VOID* pDSLockedBuffer2 = NULL;
			DWORD dwDSLockedBufferSize;
			DWORD dwDSLockedBufferSize2;
			HRESULT hr = 0; 
			int ret = 0;
			hr=pParent->m_pPlayer->m_pDSBuffer8->Lock(g_dwNextWriteOffset,BUFFERNOTIFYSIZE,&pDSLockedBuffer
				,&dwDSLockedBufferSize, &pDSLockedBuffer2,&dwDSLockedBufferSize2,0);
			if(hr == DSERR_BUFFERLOST)
			{
				pParent->m_pPlayer->m_pDSBuffer8->Restore();
				pParent->m_pPlayer->m_pDSBuffer8->Lock(g_dwNextWriteOffset,BUFFERNOTIFYSIZE,&pDSLockedBuffer
					,&dwDSLockedBufferSize,&pDSLockedBuffer2,&dwDSLockedBufferSize2,0);
			}
			if(SUCCEEDED(hr))
			{
				ret = pParent->MediaAudioDiaplayCallBack((uint8_t *)pDSLockedBuffer, (uint32_t *)&dwDSLockedBufferSize);
				g_dwNextWriteOffset+=dwDSLockedBufferSize;
				if (NULL != pDSLockedBuffer2) 
				{
					ret =pParent->MediaAudioDiaplayCallBack((uint8_t *)pDSLockedBuffer2, (uint32_t *)&dwDSLockedBufferSize2);
					g_dwNextWriteOffset+=dwDSLockedBufferSize2;
				} 
				g_dwNextWriteOffset %= (BUFFERNOTIFYSIZE * MAX_AUDIO_BUF);
				hr = pParent->m_pPlayer->m_pDSBuffer8->Unlock(pDSLockedBuffer,dwDSLockedBufferSize,
					pDSLockedBuffer2,dwDSLockedBufferSize2);
			}
			res=5;
		}
	}
	pParent->m_pPlayer->m_pDSBuffer8->Stop();
	pParent->m_pPlayer->m_pDSBuffer8 = NULL;
	pParent->m_hThreadHandle = NULL;
	if (!pParent->m_bLoop)
	{
		pParent->Stop();
	}
	return;
}

bool Mp3Player::DestoryDSound()
{
	if (NULL == m_pPlayer)
	{
		return false;
	}
	if (NULL != m_pPlayer->m_pDSBuffer8)
	{
		m_pPlayer->m_pDSBuffer8->Stop();
	}
	if (NULL != m_pPlayer->g_pDsd)
	{
		m_pPlayer->g_pDsd->Release();
		m_pPlayer->g_pDsd = NULL;
	}
	if (NULL != m_pPlayer->g_pDsd)
	{
		m_pPlayer->g_pDsd->Release();
		m_pPlayer->g_pDsd = NULL;
	}
	for(int i =0;i<MAX_AUDIO_BUF;i++)
	{
		if (NULL != m_pPlayer->m_events[i])
		{
			CloseHandle(m_pPlayer->m_events[i]);
			m_pPlayer->m_events[i] = NULL;
		}
	}
	return true;
}

uint32_t Mp3Player::GetVolume()
{
	if (m_Volume > 100)
	{
		m_Volume = 100;
	}
	return m_Volume;
}

BOOL CALLBACK DSEnumProc(LPGUID lpGUID,
	LPCTSTR lpszDesc,
	LPCTSTR lpszDrvName,
	LPVOID lpContext )
{
	string drvName;
	Mp3Player *pParent = (Mp3Player *)lpContext;
	char *lpName = NULL;
	if (NULL == pParent)
	{
		return FALSE;
	}
	pParent->m_deviceNum++;
	if (lpGUID != NULL)  //NULL only for "Primary Sound Driver".
	{
		pParent->m_deviceGuidMap[pParent->m_deviceNum] = *lpGUID;
	}else
	{
		pParent->m_deviceGuidMap[pParent->m_deviceNum] = GUID_NULL;
	}
	int num = WideCharToMultiByte(CP_OEMCP,NULL,lpszDesc,-1,NULL,0,NULL,FALSE);
	if ((lpName = (char *)malloc(num)) == NULL)
	{
		return(TRUE);
	}
	WideCharToMultiByte (CP_OEMCP,NULL,lpszDesc,-1,lpName,num,NULL,FALSE);

	drvName.assign(lpName, sizeof(GUID));
	free(lpName);
	pParent->m_deviceNameMap[pParent->m_deviceNum] = drvName;
	return(TRUE);
}


PTFIFOBUFFER FifoBufferCreate(int s32Size)
{
	PTFIFOBUFFER ptFifoBuffer = NULL;
	do
	{
		ptFifoBuffer = (PTFIFOBUFFER)malloc(sizeof(TFIFOBUFFER));
		if(NULL == ptFifoBuffer)
		{
			break;
		}
		memset(ptFifoBuffer, 0, sizeof(TFIFOBUFFER));
		ptFifoBuffer->pu8Buffer = (uint8_t *)malloc(s32Size);
		if(NULL == ptFifoBuffer->pu8Buffer)
		{
			break;
		}
		memset(ptFifoBuffer->pu8Buffer, 0, s32Size);
		ptFifoBuffer->pu8Write = ptFifoBuffer->pu8Read = ptFifoBuffer->pu8Buffer;
		ptFifoBuffer->pu8End = ptFifoBuffer->pu8Buffer + s32Size;
		if(NULL == ptFifoBuffer->pu8Buffer)
		{
			break;
		}
		return (PTFIFOBUFFER)ptFifoBuffer;
	}while(false);
	if(NULL != ptFifoBuffer)
	{
		if(NULL != ptFifoBuffer->pu8Buffer)
		{
			free(ptFifoBuffer->pu8Buffer);
			ptFifoBuffer->pu8Buffer = NULL;
		}
		free(ptFifoBuffer);
		ptFifoBuffer = NULL;
	}
	return NULL;
}

void FifoBufferDestroy(PTFIFOBUFFER pHandle)
{
	PTFIFOBUFFER ptFifoBuffer = (PTFIFOBUFFER)pHandle;
	if(NULL != ptFifoBuffer)
	{
		if(NULL != ptFifoBuffer->pu8Buffer)
		{
			free(ptFifoBuffer->pu8Buffer);
			ptFifoBuffer->pu8Buffer = NULL;
		}
		free(ptFifoBuffer);
		ptFifoBuffer = NULL;
	}
}

void FifoBufferReset(PTFIFOBUFFER pHandle)
{
	PTFIFOBUFFER ptFifoBuffer = (PTFIFOBUFFER)pHandle;
	if(NULL != ptFifoBuffer)
	{
		ptFifoBuffer->pu8Write = ptFifoBuffer->pu8Read = ptFifoBuffer->pu8Buffer;
	}
}

int FifoBufferSize(PTFIFOBUFFER pHandle)
{
	int s32Size = 0;
	PTFIFOBUFFER ptFifoBuffer = (PTFIFOBUFFER)pHandle;
	if(NULL != ptFifoBuffer)
	{
		s32Size = ptFifoBuffer->pu8Write - ptFifoBuffer->pu8Read;
		if(s32Size < 0)
		{
			s32Size += ptFifoBuffer->pu8End - ptFifoBuffer->pu8Buffer;
		}
	}
	return s32Size;
}

int FifoBufferWrite(PTFIFOBUFFER pHandle, uint8_t *pu8Buffer, int s32Size)
{
	int s32Length = 0;
	PTFIFOBUFFER ptFifoBuffer = (PTFIFOBUFFER)pHandle;
	if(NULL == ptFifoBuffer)
	{
		return 0;
	}
	do
	{
		s32Length = _min(ptFifoBuffer->pu8End - ptFifoBuffer->pu8Write, s32Size);
		memcpy(ptFifoBuffer->pu8Write, pu8Buffer, s32Length);
		pu8Buffer = pu8Buffer + s32Length;
		ptFifoBuffer->pu8Write += s32Length;
		if(ptFifoBuffer->pu8Write >= ptFifoBuffer->pu8End)
		{
			ptFifoBuffer->pu8Write = ptFifoBuffer->pu8Buffer;
		}
		s32Size -= s32Length;
	}while(s32Size > 0);
	return 1;
}

int FifoBufferRead(PTFIFOBUFFER pHandle, uint8_t *pu8Buffer, int *ps32Size)
{
	int s32Length = 0, pTempSize = (*ps32Size);
	PTFIFOBUFFER ptFifoBuffer = (PTFIFOBUFFER)pHandle;
	if(NULL == ptFifoBuffer || NULL == pu8Buffer || 0 > pTempSize)
	{
		return 0;
	}
	if(0 == pTempSize)
	{
		(*ps32Size) = 0;
		return 1;
	}
	(*ps32Size) = 0;
	do
	{
		s32Length = _min(FifoBufferSize(pHandle), pTempSize);
		s32Length = _min(ptFifoBuffer->pu8End - ptFifoBuffer->pu8Read, s32Length);
		if(0 == s32Length)
		{
			break;
		}
		memcpy(pu8Buffer, ptFifoBuffer->pu8Read, s32Length);
		pu8Buffer = pu8Buffer + s32Length;
		ptFifoBuffer->pu8Read = ptFifoBuffer->pu8Read + s32Length;
		if(ptFifoBuffer->pu8Read >= ptFifoBuffer->pu8End)
		{
			ptFifoBuffer->pu8Read = ptFifoBuffer->pu8Buffer;
		}
		pTempSize -= s32Length;
		(*ps32Size) += s32Length;
	}while(pTempSize > 0);
	return 1;
}