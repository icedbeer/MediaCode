#include "stdafx.h"
#include "audio_mixer.h"
#include <math.h>

#define VOLUMEMAX   32767
#define VOLUMEMIN	-32768

HeAudioMixer::HeAudioMixer(uint32_t audio_size)
{
	f_ = 1.0;
    values_ = NULL;
	values_ = new(std::nothrow) int32_t[audio_size];
    if (NULL == values_)
    {
        audio_size_ = 0;
    }else
    {
    	audio_size_ = audio_size;
    }
}

HeAudioMixer::~HeAudioMixer()
{
	if(values_ != NULL)
	{
		delete []values_;
		values_ = NULL;
	}
}

bool HeAudioMixer::mixer(const SrcAudioDataList& data_list, int32_t src_size, int16_t* dst, int32_t& dst_size)
{
	if(data_list.size() <= 0 || dst == NULL || src_size > dst_size 
		|| src_size == 0 || src_size > audio_size_)
	{
		return false;
	}

	int16_t* dst_pos = dst;
	int16_t* src_pos = NULL;

	int32_t	mix_value = 0;
	int32_t	max_sample = 0;
	register int32_t i = 0;

	int32_t	sample_num = data_list.size();

	if(sample_num == 1)
	{
		src_pos = data_list[0];

		for(i = 0; i < src_size; ++ i)
		{
			dst_pos[i] = src_pos[i];
		}

		f_ = 1.0;
	}
	else if(sample_num > 0)
	{
		for(i = 0; i < src_size; ++ i)
		{
			mix_value = 0;

			for(register int32_t v_index = 0; v_index < sample_num; ++ v_index)
			{
				src_pos = data_list[v_index];
				if(src_pos != NULL)
					mix_value += src_pos[i];
			}

			values_[i] = mix_value;

			if(max_sample < abs(mix_value)) //求出一个最大的样本
			{
				max_sample = abs(mix_value);
			}
		}

		if(max_sample * f_ > VOLUMEMAX)
		{
			f_ = VOLUMEMAX * 1.0 / max_sample;
		}

		for(i = 0; i < src_size; ++ i)
		{	
			dst_pos[i] = (int16_t)(values_[i] * f_);
		}

		//让f_接近1.0
		if(f_ < 1.0)
		{
			f_ = (1.0  - f_) / 32 + f_; 
		}
		else if(f_ > 1.0)
		{
			f_ = 1.0;
		}

		dst_size = src_size;
	}

	return true;
}

uint16_t HeAudioMixer::get_average_signal_level(const int16_t* data, int32_t data_size)
{
	uint16_t ret = 0;

	if(data_size > 0)
	{
		int32_t sum = 0;
		int16_t* pos = (int16_t *)data;
		for (int i=0; i < data_size; i++)
		{
			sum += abs(*pos);
			pos ++;
		}

		ret = sum * 500.0 / (data_size * VOLUMEMAX);
		ret = core_min(ret, 100);
	}

	return ret;
}

void HeAudioMixer::audio_scale(int16_t* data, int32_t data_size, float scale)
{
    if (scale >= 0.999999f && scale <= 1.000001f)
    {
        return;
    }
	int32_t temp_data = 0;

	for(int i = 0; i < data_size; i++) 
	{
		temp_data = static_cast<int32_t>(scale * data[i]);
		if (temp_data < VOLUMEMIN) 
			data[i] = VOLUMEMIN;
		else if (temp_data > VOLUMEMAX) 
			data[i] = VOLUMEMAX; 
		else 
			data[i] = static_cast<int16_t>(temp_data);
	}
}

AccompanyAverager::AccompanyAverager() : f_(1.0), count_(0), total_vol_(0)
{

}

AccompanyAverager::~AccompanyAverager()
{

}

void AccompanyAverager::averange_accompany(int16_t* data, int32_t data_size)
{
	if(data_size == 0)
		return ;

	int32_t sum = 0;
	int16_t* pos = (int16_t *)data;
	float v = 0;
	for(int i=0; i < data_size; i++)
	{
		sum += abs(*pos);
		v = (*pos) * f_;
		*pos = (int16_t)v;

		pos ++;
	}

	total_vol_ += (uint32_t)(sum / data_size);
	count_ ++;

	if(count_ >= 20)
	{
		//调节系数
		uint32_t cur_vol = total_vol_ / count_;

		if(cur_vol > 12288 && f_ > 0.0625)
			f_ = f_ - 0.0625;

		if (cur_vol < 6144 && f_ < 0.9375)
			f_ = f_ + 0.0625;

		count_ = 0;
		total_vol_ = 0;
	}
}




