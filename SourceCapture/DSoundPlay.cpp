#include "stdafx.h"
#include "DSoundPlay.h"

#pragma   comment(lib,"winmm.lib")
#pragma   comment(lib,"dsound.lib") 
#pragma   comment(lib,"dxguid.lib")

//音频设备枚举回调
BOOL CALLBACK AudioDevEnumCB(LPGUID lpGUID,	LPCTSTR lpszDesc,LPCTSTR lpszDrvName,LPVOID lpContext )
{
    string drvName;
    CDSoundPlay *pParent = (CDSoundPlay *)lpContext;
    if (NULL == pParent)
    {
        return false;
    }
    pParent->m_deviceNum++;
    if (lpGUID !=  NULL)  //NULL only for "Primary Sound Driver".
    {
        pParent->m_deviceGuidMap[pParent->m_deviceNum] = *lpGUID;
    }else
    {
        pParent->m_deviceGuidMap[pParent->m_deviceNum] = GUID_NULL;
    }
#ifdef UNICODE
    char *lpName = NULL;
    int num = WideCharToMultiByte(CP_OEMCP,NULL,lpszDesc,-1,NULL,0,NULL,false);
    if ((lpName = (char *)malloc(num)) == NULL)
    {
        return(TRUE);
    }
    WideCharToMultiByte (CP_OEMCP,NULL,lpszDesc,-1,lpName,num,NULL,false);

    drvName.assign(lpName, sizeof(GUID));
    free(lpName);
#else
    drvName.assign(lpszDesc, sizeof(GUID));
#endif //UNICODE
    pParent->m_deviceNameMap[pParent->m_deviceNum] = drvName;
    return(TRUE);
}

CDSoundPlay::CDSoundPlay(void)
{
    m_pDSBuffer8 = NULL;
    m_pDsd = NULL;
    m_pDSNotify = NULL; 
    for(int i = 0; i < MAX_AUDIO_BUF; i++)
    {
        m_events[i] = NULL;
    }
    _bplay_flag = true;
}


CDSoundPlay::~CDSoundPlay(void)
{
    UninitAudioOutDev();
}

int CDSoundPlay::GetAudioOutDevice(char dev_name_[][256], const int max_num_)
{
    m_deviceNum = 0;
    m_deviceGuidMap.clear();
    m_deviceNameMap.clear();
    if (FAILED(DirectSoundEnumerate((LPDSENUMCALLBACK)AudioDevEnumCB, (VOID *)this)))
    {
        return -1;
    }
    if (m_deviceNameMap.empty())
    {
        return 0;
    }
    if (dev_name_ && max_num_ > 0)
    {
        int num(0);
        map<int , string >::iterator itor = m_deviceNameMap.begin();
        while(itor != m_deviceNameMap.end())
        {
            memcpy(dev_name_[num++], itor->second.data(), itor->second.length());
            ++itor;
            if (num >= max_num_)
            {
                return max_num_;
            }
        }
    }
    return m_deviceNameMap.size();
}

int CDSoundPlay::GetCurDevNum()
{
    return m_deviceNum;
}

int CDSoundPlay::InitAudioOutDev(int channel_num_ /*= 2*/, int sample_rate_ /*= 44100*/, 
    int sample_bits_ /*= 16*/, int nDevID/*  =  -1*/)
{
    UninitAudioOutDev();

    GetAudioOutDevice(NULL, 0);
    m_pDSBuffer8 = NULL; //buffer
    m_pDsd = 0; //dsound
    m_pDSNotify = 0; 
    HRESULT hr;
    if (nDevID >=  0)
    {
        if(FAILED(hr = DirectSoundCreate8(&GetDeviceGuid(nDevID),&m_pDsd,NULL)))
            return -1;
    }else
        if(FAILED(hr = DirectSoundCreate8(NULL,&m_pDsd,NULL)))
            return -1;
    //设置协作权限，不然没有声音
    if(FAILED(hr = m_pDsd->SetCooperativeLevel(::GetDesktopWindow(),DSSCL_NORMAL)))
    {
        return -1;
    }
    DSBUFFERDESC dsbd;
    memset(&dsbd,0,sizeof(dsbd));
    dsbd.dwSize = sizeof(dsbd);
    dsbd.dwFlags = DSBCAPS_GLOBALFOCUS | DSBCAPS_CTRLPOSITIONNOTIFY |DSBCAPS_GETCURRENTPOSITION2;
    dsbd.dwBufferBytes = MAX_AUDIO_BUF*BUFFERNOTIFYSIZE; 
    dsbd.lpwfxFormat = (WAVEFORMATEX*)malloc(sizeof(WAVEFORMATEX));
    dsbd.lpwfxFormat->wFormatTag = WAVE_FORMAT_PCM;   
    /* format type */
    (dsbd.lpwfxFormat)->nChannels = channel_num_;    
    (dsbd.lpwfxFormat)->nSamplesPerSec = sample_rate_;    
    (dsbd.lpwfxFormat)->nAvgBytesPerSec = sample_rate_*(sample_bits_/8)*channel_num_;
    (dsbd.lpwfxFormat)->nBlockAlign = (sample_bits_/8)*channel_num_;
    (dsbd.lpwfxFormat)->wBitsPerSample = sample_bits_;   
    (dsbd.lpwfxFormat)->cbSize = 0;
    //创建DirectSound辅助缓冲区
    LPDIRECTSOUNDBUFFER lpbuffer;
    if(DS_OK !=  (hr = (m_pDsd->CreateSoundBuffer(&dsbd,&lpbuffer,NULL))))
    {   
        return -1;
    }
    if( FAILED(hr = (lpbuffer->QueryInterface(IID_IDirectSoundBuffer8,(LPVOID*)&m_pDSBuffer8))))
    {
        return -1;
    }
    lpbuffer->Release();
    //设置DirectSound通知 机制
    if(FAILED(hr = (m_pDSBuffer8->QueryInterface(IID_IDirectSoundNotify,(LPVOID*)&m_pDSNotify))))
    {
        return -1;
    }
    for(int i  = 0;i<MAX_AUDIO_BUF;i++)
    {
        m_aPosNotify[i].dwOffset  = i*BUFFERNOTIFYSIZE;
        m_events[i] = ::CreateEvent(NULL,false,true,NULL); 
        m_aPosNotify[i].hEventNotify = m_events[i];
    }
    m_pDSNotify->SetNotificationPositions(MAX_AUDIO_BUF, m_aPosNotify);
    m_pDSNotify->Release();
    m_pDSBuffer8->Play(0,0,DSBPLAY_LOOPING);
    m_dwNextWriteOffset = BUFFERNOTIFYSIZE;

    //开启播放线程
    _bplay_flag = true;
    _dsound_thr = boost::thread(boost::bind(&CDSoundPlay::thread_loop, this));

    return 0;
}

int CDSoundPlay::PlayAudio(void *data_, int size_)
{
    if (!data_ || size_ <= 0)
    {
        return -1;
    }
    boost::lock_guard<boost::mutex> lock_(_mutex);
//     if (m_pcmBuffer.length() <= 8196)
//     {
        m_pcmBuffer.append((char *)data_, size_);
        return 0;
//     } else
//     {
//         return -2;
//     }
}

void CDSoundPlay::UninitAudioOutDev()
{
    //结束播放线程
    _bplay_flag = false;
    _dsound_thr.join();

    if (NULL != m_pDSBuffer8)
    {
        m_pDSBuffer8->Stop();
    }
    if (NULL != m_pDsd)
    {
        m_pDsd->Release();
        m_pDsd = NULL;
    }
    if (NULL != m_pDsd)
    {
        m_pDsd->Release();
        m_pDsd = NULL;
    }
    for(int i = 0; i < MAX_AUDIO_BUF; i++)
    {
        if (NULL != m_events[i])
        {
            CloseHandle(m_events[i]);
            m_events[i] = NULL;
        }
    }
}

GUID CDSoundPlay::GetDeviceGuid(int deviceID)
{
    if (deviceID < 0 || deviceID >= m_deviceNum)
    {
        return GUID_NULL;
    }
    return m_deviceGuidMap[deviceID];
}

int CDSoundPlay::GetBufferSize()
{
    boost::lock_guard<boost::mutex> lock_(_mutex);
    return m_pcmBuffer.length();
}

void CDSoundPlay::thread_loop()
{
    bool bSleepFlag = false;
    while (_bplay_flag)
    {
        bSleepFlag = true;
        //等待声卡中缓存数据播放结束
        DWORD res = WaitForMultipleObjects(MAX_AUDIO_BUF, m_events, false, INFINITE);
        if ((res >= WAIT_OBJECT_0) && (res <= WAIT_OBJECT_0 + 3))
        {
            VOID* pDSLockedBuffer = NULL;
            DWORD dwDSLockedBufferSize;
            int ret = 0;
            //创建主副缓冲区
            HRESULT hr = m_pDSBuffer8->Lock(m_dwNextWriteOffset, BUFFERNOTIFYSIZE,
                &pDSLockedBuffer, &dwDSLockedBufferSize, NULL, NULL, 0);
            if (hr == DSERR_BUFFERLOST)
            {
                //如果创建失败，则重置，然后再创建
                m_pDSBuffer8->Restore();
                m_pDSBuffer8->Lock(m_dwNextWriteOffset, BUFFERNOTIFYSIZE, &pDSLockedBuffer,
                    &dwDSLockedBufferSize, NULL, NULL, 0);
            }
            if (SUCCEEDED(hr))
            {
                boost::lock_guard<boost::mutex> lock_(_mutex);
                if (m_pcmBuffer.length() >= dwDSLockedBufferSize)
                {
                    //数据足够
                    memcpy(pDSLockedBuffer, m_pcmBuffer.data(), dwDSLockedBufferSize);
                    m_pcmBuffer.erase(0, dwDSLockedBufferSize);
                    m_dwNextWriteOffset += dwDSLockedBufferSize;
                    //记录播放位置,算法不能改
                    m_dwNextWriteOffset %= (BUFFERNOTIFYSIZE * MAX_AUDIO_BUF);
                    //解锁后自动开始播放
                    hr = m_pDSBuffer8->Unlock(pDSLockedBuffer, dwDSLockedBufferSize, NULL, 0);
                    bSleepFlag = false;
                } else
                {
                    //数据不足，暂不播放
                    hr = m_pDSBuffer8->Unlock(pDSLockedBuffer, 0, NULL, 0);
                }
            }
        }
        if (bSleepFlag)
        {
            Sleep(1);
        }
    }
    m_pcmBuffer.clear();
}

int CDSoundPlay::GetVolumeDB()
{
    if (m_Volume > 100)
    {
        m_Volume = 100;
    }
    return m_Volume;
}
