/*!
 * \class CCorlorConvert
 *
 * \brief 
 * 
 * 通过fffmeg的sws_scale库实现raw图像格式的转换
 * \author Administrator
 * \date 十月 2016
 */
#pragma once
#include <dshow.h>
extern "C"
{
#include "libavutil/pixfmt.h"
}

struct AVPicture;
struct AVFrame;
struct SwsContext;

class CCorlorConvert
{
public:
    CCorlorConvert();
    virtual ~CCorlorConvert(void);

    virtual int	    img_convert(unsigned char *src_data_, const int &src_width_, const int &src_height_, const int &src_format_,
        unsigned char *dst_data_, const int &dst_width_, const int &dst_height_, const int &dst_format_, bool brotate_ = true);

    bool            is_init();

    static  int	    GUID_2_ffmpeg_pixformat(const GUID &format_);

protected:
    virtual int	    init(const int &src_width_, const int &src_height_, const int &src_format_,
        const int &dst_width_, const int &dst_height_, const int &dst_format_);
    virtual void	uninit();


protected:
    SwsContext		*_sws_context;

    int			    _src_width;
    int			    _src_height;
    int			    _src_format;
    int			    _dst_width;
    int			    _dst_height;
    int			    _dst_format;
};

