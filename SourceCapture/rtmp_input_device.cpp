#include "rtmp_input_device.h"
#include "rtmpsrv.h"
#include "../common/revolver/boost_log.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

RtmpInputDevice::RtmpInputDevice( int32_t dev_id, uint16_t code_id ) : IVideoInDevice(dev_id, code_id)
	,audio_buffer( 32*1000), audio_decode_buffer(1024*1024), video_buffer(1024* 1024)
{
	m_pContext = NULL;
	m_pCodec = NULL;
	m_pAudioResample = NULL;
	m_pFrame= NULL;
	InitDecoder();
}

RtmpInputDevice::~RtmpInputDevice()
{
	close();
	UnInitDecoder();
}

int RtmpInputDevice::init_device()
{
	int ret= 0;
	switch( code_id_ )
	{
		case X264_240P: //320p编码器
		case X264_360P://480p编码器
		case X264_480P://640p编码器
		case X264_600P:
		case X264_540P:
		case X264_AUTO:
			{
				ret= open( 15 );
			}
			break;
	}
	return ret;
}

int RtmpInputDevice::open( uint16_t fps )
{
	if (dev_id_ != 0xFF )
	{
		return -1;
	}

	if( DEVICE_CLOSE != status_ )
	{
		BOOST_INFO << " <RtmpInputDevice::open> device status error status " << status_ ;
		return 1;
	}

	BOOST_INFO << "<RtmpInputDevice::open>";

	//CREATE_OBS_AUDIO( );

	CREATE_RTMP_SERVER( );
	int ret = RTMP_SERVER()->StartRtmpServer( this );
	if( 0 != ret )
	{
		BOOST_INFO << "<RtmpInputDevice::open> StartRtmpServer error code " << ret ;
		//DESTROY_RTMP_SERVER();
		return ret;
	}
	
	status_ = DEVICE_OPEN;

	return 0;
}

void RtmpInputDevice::close()
{
	if( DEVICE_CLOSE != status_ )
	{
		BOOST_INFO << "<RtmpInputDevice::close>!";
		RTMP_SERVER()->StopRtmpServer( );
		DESTROY_RTMP_SERVER();
		//DESTROY_OBS_AUDIO();
		status_ = DEVICE_CLOSE;
	}
	return;
}

//外部获取RTMP数据
bool RtmpInputDevice::read(uint8_t* data, uint32_t &data_size, int& frame_type, int& data_type, uint64_t& ts )
{
	if( DEVICE_OPEN != status_ )
	{
		return false;
	}
	switch (data_type)
	{
	case H264_DATA_TYPE:
		get_video_data(data, data_size, frame_type, ts);
		break;
	case PCM_DATA_TYPE:
		get_audio_data(data, data_size);
		break;
	default:
		return false;
	}
	return true;
}

void RtmpInputDevice::OnIncomingVideoFrame( uint32_t ts, uint8_t* data, int size, uint8_t frame_type )
{
	BASE_GUARD(BaseThreadMutex, cf_mon, mutex_ );
	video_head head={0};
	head.size=  size;
	head.frame_type= frame_type;
	head.ts = ts;
	video_buffer.PushBack(&head, sizeof(head) );
	video_buffer.PushBack( data, size );
}
void RtmpInputDevice::OnIncomingAudioFrame( uint32_t ts, uint8_t* data, int size, uint8_t data_type )
{
	BASE_GUARD(BaseThreadMutex, cf_mon, mutex_);

	audio_buffer.PushBack( data, size );

	//实时解码为PCM数据
	decode_data( );
}

void RtmpInputDevice::RtmpCaptureError( int code, char* info )
{
	status_ = DEVICE_CLOSE;
	return;
}

//做回调通知使用
void RtmpInputDevice::OnIncomingFirstVideoFrame()
{

}

//做回调通知使用
void RtmpInputDevice::OnIncomingFirstAudioFrame()
{

}

int RtmpInputDevice::get_video_data(uint8_t *buffer, uint32_t uLen, int& frame_type, uint64_t& ts )
{
	int size= 0;
	BASE_GUARD_RETURN(BaseThreadMutex, cf_mon, mutex_, 0);

	video_head head={0};
	if( video_buffer.GetUsedSize() > sizeof(video_head) )
	{
		video_buffer.PopFront( &head, sizeof(head) );
		if( head.size  <= video_buffer.GetUsedSize() )
		{
			video_buffer.PopFront( buffer, head.size );
			frame_type= head.frame_type;
			ts= head.ts;
			return head.size;
		}
		else
		{
			BOOST_ERROR "<CObsAudioHandle::get_video_data> error buffer reset packet_size = " << size << "data_size = " << video_buffer.GetUsedSize() ;
			video_buffer.SetUsedSize( 0 );
		}
	}
	return 0;
}

//获取音频PCM解码数据,时间戳另打
int RtmpInputDevice::get_audio_data(uint8_t *buffer, uint32_t uLen)
{
	BASE_GUARD_RETURN(BaseThreadMutex, cf_mon, mutex_, 0);

	if( audio_decode_buffer.GetUsedSize() >= uLen )
	{
		audio_decode_buffer.PopFront( buffer, uLen );
		return uLen;
	}
	return 0;
}

int RtmpInputDevice::InitDecoder()
{
	avcodec_register_all();
	m_pCodec = avcodec_find_decoder(AV_CODEC_ID_MP3);
	if(NULL == m_pCodec)
	{
		return -1;
	}
	m_pContext = avcodec_alloc_context3(m_pCodec);
	if (NULL == m_pContext)
	{
		return -1;
	}
	int ret = avcodec_open2(m_pContext, m_pCodec, NULL);
	if (0 != ret)
	{
		return -1;
	}
	m_pFrame = avcodec_alloc_frame();
	return 0;
}

int RtmpInputDevice::UnInitDecoder()
{
	if (NULL != m_pAudioResample)
	{
		swr_free(&m_pAudioResample);
		m_pAudioResample = NULL;
	}
	m_pCodec = avcodec_find_decoder(AV_CODEC_ID_MP3);
	if(NULL != m_pCodec && NULL != m_pContext)
	{
		avcodec_close(m_pContext);
		av_free(m_pContext);
		m_pCodec = NULL;
		m_pContext = NULL;
	}
	if (NULL != m_pFrame)
	{
		avcodec_free_frame(&m_pFrame);
		m_pFrame = NULL;
	}
	audio_buffer.SetUsedSize(0);
	audio_decode_buffer.SetUsedSize(0);
	return 0;
}

int RtmpInputDevice::ResetDecoder()
{
	UnInitDecoder();
	return InitDecoder();
}

int RtmpInputDevice::decode_data( )
{
	int leftsize = 0;
	AVPacket pkt;
	av_init_packet(&pkt);
	pkt.data = audio_buffer.GetBuffer();
	pkt.size = audio_buffer.GetUsedSize();

	avcodec_get_frame_defaults(m_pFrame);
	int got_picture = 0;
	int ret = avcodec_decode_audio4(m_pContext, m_pFrame, &got_picture, &pkt);
	if (ret <=  0 || 0 == got_picture)
	{
		return -1;
	}

	if (NULL == m_pAudioResample)
	{
		if (!m_pContext->channel_layout)
		{
			m_pContext->channel_layout = av_get_default_channel_layout(m_pContext->channels);
		}
		if (!m_pContext->channel_layout) 
		{
			return -2;
		}

		m_pAudioResample = swr_alloc_set_opts(NULL, AV_CH_LAYOUT_STEREO, AV_SAMPLE_FMT_S16, 44100, m_pContext->channel_layout, m_pContext->sample_fmt, m_pContext->sample_rate, 0, NULL);
		if (NULL == m_pAudioResample)
		{
			return -3;
		}
		if (0 > swr_init(m_pAudioResample))
		{
			return -4;
		}
	}

	int32_t resamplenum     = 0;
	memset(audio_pcm_buffer, 0, AVCODEC_MAX_AUDIO_FRAME_SIZE);
	uint8_t* in_put[2]={NULL};
	in_put[0]= audio_pcm_buffer;
	resamplenum = swr_convert(m_pAudioResample, in_put, 8192, (const uint8_t **)m_pFrame->data, m_pFrame->nb_samples);
	audio_decode_buffer.PushBack( audio_pcm_buffer, resamplenum * 4 );

	pkt.data += ret;
	pkt.size -= ret;
	pkt.dts = pkt.pts = AV_NOPTS_VALUE;
	audio_buffer.PopFront( m_pAudioResample, ret);
	return 0;
}

