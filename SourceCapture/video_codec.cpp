#include "stdafx.h"
#include "video_codec.h"
#include "global_config.h"

extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavutil/avutil.h"
#include "libswscale/swscale.h"
#include "libavutil/opt.h"
}

#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef WIN32
#include <strmif.h>
#include <amvideo.h>
#endif //WIN32

#ifdef WIN32
#pragma comment(lib, "avcodec.lib")
#pragma comment(lib, "avformat.lib")
#pragma comment(lib, "avutil.lib")
#pragma comment(lib, "swscale.lib")
#pragma comment(lib, "libx264.lib")
#endif // WIN32

//////////////////////////////////////////////////////////////////////////
IVideo_Codec::IVideo_Codec() :
avctx_opts(0),
picture(0),
codec(0),
_sws_context(0),
width(0),
height(0),
bit_rate(0),
frame_rate(0),
gop_size(20),
qmin(25),
qmax(40),
max_b_frames(3)
{
    static bool init_av_ = false;
    if(false == init_av_)
    {
        init_av_ = true;
        avcodec_register_all();
    }
}

IVideo_Codec::~IVideo_Codec()
{
}


///////////////////////////////////////////////////////////////////////////////
#ifdef WIN32
//获取CPU个数
typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);
DWORD GetNumberOfProcessors()
{
	SYSTEM_INFO si;
	PGNSI pfnGNSI = (PGNSI) GetProcAddress(GetModuleHandleA("kernel32.dll"), "GetNativeSystemInfo");
	if(pfnGNSI)
	{
		pfnGNSI(&si);
	}else
	{ 
		GetSystemInfo(&si);
	}
	return si.dwNumberOfProcessors;
}
#else
int GetNumberOfProcessors()
{
	return 2;
}
#endif

H264Encoder::H264Encoder()
{
	m_nWidth				= 0;
	m_nHeight				= 0;

	m_h264_codec_width		= 0;
	m_h264_codec_height		= 0;

    m_pBuf					= NULL;
	m_swsContext			= NULL;
	m_en_h					= NULL;
	m_nQIndex				= 3;

	memset(&m_en_picture, 0, sizeof(x264_picture_t));
	memset(&m_en_param, 0, sizeof(m_en_param));

	m_last_frame_time = 0;
}

H264Encoder::~H264Encoder()
{
	fini();

	if(m_pBuf)
	{
		delete []m_pBuf;
		m_pBuf = NULL;
	}
}

bool H264Encoder::init(int iWidth, int iHeight, int nFrameRate, int nBitRate)
{
    do 
    {
        //由于ffmpeg为C开发，使用全局变量，所以如果同一进程内如果，需要打开多个编解码器
        //则需要加锁，否则可能会有一场崩溃问题
        boost::lock_guard<boost::recursive_mutex > lock_(GLOBAL_CONFIG()->_codec_mutex);

        m_nWidth = iWidth;
        m_nHeight				= iHeight;
        m_h264_codec_width		= iWidth;
        m_h264_codec_height		= iHeight;
        m_nFrameRate            = nFrameRate;
        m_nQIndex		        = 3;
        m_pBuf			        = new(std::nothrow) unsigned char[m_nWidth * m_nHeight * 3];
        if (NULL == m_pBuf)
        {
            break;
        }
        bool    faster = true; 
        int     CPUNum = GetNumberOfProcessors();
        if(CPUNum >= 4)  
        {
            faster		= false;
            m_nQIndex	= 2;
        }
        x264_param_default(&m_en_param);
        if (x264_param_default_preset(&m_en_param, faster ? "medium" : "superfast", "zerolatency") != 0)//medium,veryslow
            break;
        //OBS Setting
        //x264_param_default_preset(&m_en_param, "superfast", "");
        //m_en_param.b_deterministic = false;
        //m_en_param.rc.i_vbv_max_bitrate = nBitRate; //vbv-maxrate
        //m_en_param.rc.i_vbv_buffer_size = nBitRate; //vbv-bufsize
        //m_en_param.rc.i_bitrate = nBitRate;
        //m_en_param.rc.b_filler = 1;
        //m_en_param.rc.i_rc_method = X264_RC_ABR;
        //m_en_param.rc.f_rf_constant = 0.0f;;
        //m_en_param.b_vfr_input = false;
        //m_en_param.vui.b_fullrange = 0;
        //m_en_param.vui.i_colorprim = 1;
        //m_en_param.vui.i_transfer = 13;
        //m_en_param.vui.i_colmatrix = 6;
        //m_en_param.i_keyint_max = m_nFrameRate * 3;
        //m_en_param.i_fps_num = m_nFrameRate;
        //m_en_param.i_fps_den = 1;
        //m_en_param.i_timebase_num = 1;
        //m_en_param.i_timebase_den = 1000;
        //m_en_param.i_log_level = X264_LOG_NONE;
        //m_en_param.i_csp = X264_CSP_I420;
        //m_en_param.i_width = m_h264_codec_width;
        //m_en_param.i_height = m_h264_codec_height;
        //x264_param_apply_profile(&m_en_param, "high");

        //设置编码器参数
        m_en_param.i_log_level		     = X264_LOG_NONE;
        m_en_param.rc.i_rc_method	     = X264_RC_ABR;
        m_en_param.rc.f_rf_constant      = 0.0f;
         m_en_param.rc.i_vbv_max_bitrate = nBitRate; //vbv-maxrate
         m_en_param.rc.i_vbv_buffer_size = nBitRate; //vbv-bufsize
        m_en_param.rc.i_bitrate          = nBitRate;
        m_en_param.rc.b_filler           = 1;
        m_en_param.rc.f_rf_constant      = 0.0f;
        m_en_param.b_vfr_input           = 0;
        m_en_param.i_width               = m_h264_codec_width;
        m_en_param.i_height              = m_h264_codec_height;
        m_en_param.vui.b_fullrange       = 0;
        m_en_param.vui.i_colorprim       = 1;
        m_en_param.vui.i_transfer        = 13;
        m_en_param.vui.i_colmatrix       = 6;
        m_en_param.i_keyint_max          = m_nFrameRate * 3;
        m_en_param.i_keyint_min          = m_nFrameRate * 3;
        m_en_param.i_fps_num             = m_nFrameRate;
        m_en_param.i_fps_den             = 1;
        m_en_param.i_timebase_num        = 1;
        m_en_param.i_timebase_den        = 1000;
        m_en_param.i_csp                 = X264_CSP_I420;
        m_en_param.b_deterministic       = false;
        //m_en_param.i_level_idc = 41; // 编码质量为main时使用
        x264_param_apply_profile (&m_en_param, "high");

        if ((m_en_h = x264_encoder_open(&m_en_param)) == NULL)
            break;

        if (x264_picture_alloc(&m_en_picture, X264_CSP_I420, m_h264_codec_width, m_h264_codec_height) != 0)
            break;

        //构造颜色空间转换器
        m_swsContext = sws_getContext(m_nWidth, m_nHeight, AV_PIX_FMT_RGB24, 
            m_h264_codec_width, m_h264_codec_height, AV_PIX_FMT_YUV420P, SWS_BICUBIC, 
            NULL, NULL, NULL);
        //success
        return true;
    } while (false);

    BOOST_ERROR << "H264Encoder::init failed.";
    fini();
    return false;
}

void H264Encoder::fini()
{
    boost::lock_guard<boost::recursive_mutex > lock_(GLOBAL_CONFIG()->_codec_mutex);

    if (m_en_h != NULL)
	{
		x264_picture_clean(&m_en_picture);
		x264_encoder_close(m_en_h);
		sws_freeContext(m_swsContext);
		m_en_h = NULL;
	}
	if (m_pBuf != NULL)
	{
		delete [] m_pBuf;
		m_pBuf = NULL;
	}
}

int H264Encoder::encode(unsigned char *src_, int src_len_, int src_width_, int src_height_, int src_format_,
                        unsigned char *dst_, int dst_len_, int *mark_, bool isRGB24Data_)
{
	if(m_en_h == NULL || m_swsContext == NULL)
		return -1;

	if (isRGB24Data_)	//进行色彩空间转换
	{
        if (m_nWidth != src_width_ || m_nHeight != src_height_)
        {
            if (m_swsContext)
            {
                sws_freeContext(m_swsContext);
            }
            m_nWidth = src_width_;
            m_nHeight = src_height_;
            //构造颜色空间转换器
            m_swsContext = sws_getContext(m_nWidth, m_nHeight, (AVPixelFormat )src_format_,
                m_h264_codec_width, m_h264_codec_height, AV_PIX_FMT_YUV420P, SWS_BICUBIC, 
                NULL, NULL, NULL);
        }
        int srcStride = src_width_ * 
            (avpicture_get_size((AVPixelFormat)src_format_, src_width_, src_height_) / (src_width_ * src_height_));
		sws_scale(m_swsContext, &src_, &srcStride, 0, src_height_, 
			m_en_picture.img.plane, m_en_picture.img.i_stride);
	}else	//当传递过来的数据格式为YUV420P时，不进行转换
	{
		memcpy(m_en_picture.img.plane[0], src_, m_h264_codec_width * m_h264_codec_height);
		memcpy(m_en_picture.img.plane[1], 
            src_ + m_h264_codec_width * m_h264_codec_height, 
            m_h264_codec_width * m_h264_codec_height/4);
		memcpy(m_en_picture.img.plane[2], src_ + 
            m_h264_codec_width * m_h264_codec_height + m_h264_codec_width * m_h264_codec_height / 4, 
            m_h264_codec_width * m_h264_codec_height/4);
		m_en_picture.img.plane[3] = 0;

		m_en_picture.img.i_stride[0] = m_h264_codec_width;
		m_en_picture.img.i_stride[1] = m_h264_codec_width/ 2;
		m_en_picture.img.i_stride[2] = m_h264_codec_width / 2;
		m_en_picture.img.i_stride[3] =  0;
	}

	m_en_param.i_frame_total++;
	m_en_picture.i_pts = (int64_t)m_en_param.i_frame_total * m_en_param.i_fps_den;

	x264_nal_t *nal = NULL;
	int i_nal		= 0;

	m_en_picture.i_type = X264_TYPE_AUTO;
	m_pic_out.i_type = X264_TYPE_AUTO;

    if (0 != m_last_frame_time && (GetTickCount() - m_last_frame_time) >= 
        (GLOBAL_CONFIG()->_key_frame_interval * 1000 + 100))	//当超过0.1秒钟没有编码关键帧
	{
		m_en_picture.i_type = X264_TYPE_IDR;
		m_pic_out.i_type = X264_TYPE_IDR;
		m_en_picture.b_keyframe = X264_TYPE_IDR;
		m_pic_out.b_keyframe = X264_TYPE_IDR;
	}

	//X.264 编码
	int ret = x264_encoder_encode(m_en_h, &nal, &i_nal, &m_en_picture, &m_pic_out);
	if (ret > 0 && ret <= dst_len_)
	{
        //success
		memcpy(dst_, nal[0].p_payload, ret);
        *mark_ = m_pic_out.i_type;
        if (1 == *mark_)
        {
            //防止关键帧间隔太近,将关键帧频率控制在0.5~0.1s之间
            if ((GetTickCount() - m_last_frame_time) < (GLOBAL_CONFIG()->_key_frame_interval * 1000 - 500))
            {
                *mark_ = 5;
            }else
            {
                m_last_frame_time = GetTickCount();
            }
        }
    }else
    {
        //failed
        BOOST_WARNING << "video encode failed.";
    }
    return ret;
}

//////////////////////////////////////////////////////////////////////////
CVideo_Decoder::CVideo_Decoder()
{
	//init(); 
}

CVideo_Decoder::~CVideo_Decoder()
{
	fini();
}

int CVideo_Decoder::init()
{
    boost::lock_guard<boost::recursive_mutex > lock_(GLOBAL_CONFIG()->_codec_mutex);

    this->codec = avcodec_find_decoder(AV_CODEC_ID_H264);
	if(NULL == this->codec)
    {
        return -1;
    }
	this->avctx_opts= avcodec_alloc_context3(this->codec);
	if(this->avctx_opts == NULL)
	{
		return -1;
	}
	avcodec_get_context_defaults3(this->avctx_opts, this->codec);    	
	if(0 != avcodec_open2(this->avctx_opts, this->codec, NULL))
	{
		return -1;
	}
	this->picture = avcodec_alloc_frame();
	avcodec_get_frame_defaults(this->picture);

	return 0;
}

int CVideo_Decoder::fini()
{
    boost::lock_guard<boost::recursive_mutex > lock_(GLOBAL_CONFIG()->_codec_mutex);

    if (this->picture)
	{
		avcodec_free_frame(&(this->picture));
		this->picture = 0;
	}
	if (this->codec)
	{
		avcodec_close(this->avctx_opts);
		this->codec = 0;
	}
	if(this->avctx_opts)
	{
		av_free(this->avctx_opts);
		this->avctx_opts = 0;
	}
	if (this->_sws_context)
	{
		sws_freeContext(this->_sws_context);
		this->_sws_context = 0;
	}
	return 0;
}

int CVideo_Decoder::encode(unsigned char* src_, int src_len_, int src_width_, int src_height_, int src_format_,
                           unsigned char* dst_, int dst_len_, int *mark_, bool brotate_ /*= true*/)
{
	return 0;
}

int CVideo_Decoder::decode( unsigned char* src_, int src_len_, unsigned char* dst_, int dst_len_, int *mark_, bool brotate_ /*= true*/)
{
	int width(0), height(0);
	return decode(src_, src_len_, dst_, dst_len_, mark_, width, height);
}

//解码器解码成功后，根据解码器返回的图片的宽高动态的进行调整
int CVideo_Decoder::decode( unsigned char* src_, int src_len_, unsigned char* dst_, int dst_len_, int *mark_, int &width, int &height, bool brotate_/* = true*/)
{
	int got_picture = 0;

	AVPacket pkt;
	av_init_packet(&pkt);
	pkt.data = src_;
	pkt.size = src_len_;

	avcodec_get_frame_defaults(this->picture);
	int len = avcodec_decode_video2(this->avctx_opts, this->picture, &got_picture, &pkt);
	av_free_packet(&pkt);

	if (!got_picture) //decode failed.
	{
        BOOST_DEBUG << "video decode failed.";
		return 0;
	}

	if (this->picture->width != this->avctx_opts->width || this->picture->height != this->avctx_opts->height)
	{
		avcodec_flush_buffers(this->avctx_opts);
		return 0;
	}

	if (got_picture)
	{
		if (!this->_sws_context || this->width != this->avctx_opts->width || this->height != this->avctx_opts->height)
		{
			if (this->width != this->avctx_opts->width || this->height != this->avctx_opts->height)
			{
				sws_freeContext(this->_sws_context);
				this->_sws_context = 0;
			}
#if defined WIN32
			this->_sws_context = sws_getContext(this->avctx_opts->width, this->avctx_opts->height, this->avctx_opts->pix_fmt, 
				this->avctx_opts->width, this->avctx_opts->height, AV_PIX_FMT_BGR24, SWS_BILINEAR,0,0,0);
#elif defined ANDROID
			this->_sws_context = sws_getContext(this->avctx_opts->width, this->avctx_opts->height, this->avctx_opts->pix_fmt, 
				this->avctx_opts->width, this->avctx_opts->height, AV_PIX_FMT_BGR24, SWS_BILINEAR,0,0,0);
#endif //WIN32
			this->width = this->avctx_opts->width; 
			this->height = this->avctx_opts->height;
		}
		AVPicture frameRGB;
#ifdef WIN32
		avpicture_fill(&frameRGB, dst_, AV_PIX_FMT_BGR24, this->avctx_opts->width, this->avctx_opts->height);
        if (brotate_)
        {
            this->picture->data[0] += this->picture->linesize[0] * (this->height - 1);
            this->picture->linesize[0] *= -1;                     
            this->picture->data[1] += this->picture->linesize[1] * (this->height / 2 - 1);
            this->picture->linesize[1] *= -1;
            this->picture->data[2] += this->picture->linesize[2] * (this->height / 2 - 1);
            this->picture->linesize[2] *= -1;
        }
#elif defined ANDROID
		avpicture_fill(&frameRGB, dst_, AV_PIX_FMT_BGR24, this->avctx_opts->width, this->avctx_opts->height);

		this->picture->data[0] += this->picture->linesize[0] * (this->height - 1);
		this->picture->linesize[0] *= -1;                     
		this->picture->data[1] += this->picture->linesize[1] * (this->height / 2 - 1);
		this->picture->linesize[1] *= -1;
		this->picture->data[2] += this->picture->linesize[2] * (this->height / 2 - 1);
		this->picture->linesize[2] *= -1;
#endif //win32

		sws_scale(this->_sws_context, this->picture->data, this->picture->linesize, 0, this->avctx_opts->height, frameRGB.data, frameRGB.linesize);		
	}else
	{
		return 0;
	}
	if(mark_)
		*mark_ = this->picture->pict_type;

	//modifyed by cgb at 2015-6-5
	//return len;
	dst_len_ = this->avctx_opts->width * this->avctx_opts->height * 3;
	width = this->avctx_opts->width;
	height = this->avctx_opts->height;

	return dst_len_;
}

int CVideo_Decoder::decode(unsigned char* src_, int src_len_, unsigned char* dst_, int dst_len_, int *mark_,
    int want_width_, int want_height_, int &dst_width_, int &dst_height_, bool brotate_ /*= true*/)
{
    if (this->width != want_width_ || this->height != want_height_)
    {
        this->width = want_width_;
        this->height = want_height_;
    }

    int got_picture = 0;

    AVPacket pkt;
    av_init_packet(&pkt);
    pkt.data = src_;
    pkt.size = src_len_;

    avcodec_get_frame_defaults(this->picture);
    int len = avcodec_decode_video2(this->avctx_opts, this->picture, &got_picture, &pkt);
    av_free_packet(&pkt);

    if (!got_picture) //decode failed.
    {
        BOOST_DEBUG << "video decode failed.";
        return 0;
    }
    //编码源可能会切换分辨率，通过此方法判断
    if (this->picture->width != this->avctx_opts->width || this->picture->height != this->avctx_opts->height)
    {
        avcodec_flush_buffers(this->avctx_opts);
        return 0;
    }

    if (got_picture)
    {
        if (!this->_sws_context || this->width != this->avctx_opts->width || this->height != this->avctx_opts->height)
        {
            if (this->width != this->avctx_opts->width || this->height != this->avctx_opts->height)
            {
                sws_freeContext(this->_sws_context);
                this->_sws_context = 0;
            }
#if defined WIN32
            this->_sws_context = sws_getContext(this->avctx_opts->width, this->avctx_opts->height, this->avctx_opts->pix_fmt,
                this->avctx_opts->width, this->avctx_opts->height, AV_PIX_FMT_BGR24, SWS_BILINEAR, 0, 0, 0);
#elif defined ANDROID
            this->_sws_context = sws_getContext(this->avctx_opts->width, this->avctx_opts->height, this->avctx_opts->pix_fmt,
                this->avctx_opts->width, this->avctx_opts->height, AV_PIX_FMT_BGR24, SWS_BILINEAR, 0, 0, 0);
#endif //WIN32
            this->width = this->avctx_opts->width;
            this->height = this->avctx_opts->height;
        }
        AVPicture frameRGB;
#ifdef WIN32
        avpicture_fill(&frameRGB, dst_, AV_PIX_FMT_BGR24, this->avctx_opts->width, this->avctx_opts->height);
        if (brotate_)
        {
            this->picture->data[0] += this->picture->linesize[0] * (this->height - 1);
            this->picture->linesize[0] *= -1;
            this->picture->data[1] += this->picture->linesize[1] * (this->height / 2 - 1);
            this->picture->linesize[1] *= -1;
            this->picture->data[2] += this->picture->linesize[2] * (this->height / 2 - 1);
            this->picture->linesize[2] *= -1;
        }
#elif defined ANDROID
        avpicture_fill(&frameRGB, dst_, AV_PIX_FMT_BGR24, this->avctx_opts->width, this->avctx_opts->height);

        this->picture->data[0] += this->picture->linesize[0] * (this->height - 1);
        this->picture->linesize[0] *= -1;
        this->picture->data[1] += this->picture->linesize[1] * (this->height / 2 - 1);
        this->picture->linesize[1] *= -1;
        this->picture->data[2] += this->picture->linesize[2] * (this->height / 2 - 1);
        this->picture->linesize[2] *= -1;
#endif //win32

        sws_scale(this->_sws_context, this->picture->data, this->picture->linesize, 0, this->avctx_opts->height, frameRGB.data, frameRGB.linesize);
    } else
    {
        return 0;
    }
    if (mark_)
        *mark_ = this->picture->pict_type;

    //modifyed by cgb at 2015-6-5
    //return len;
    dst_len_ = this->avctx_opts->width * this->avctx_opts->height * 3;
    dst_width_ = this->avctx_opts->width;
    dst_height_ = this->avctx_opts->height;

    return dst_len_;
}

void CVideo_Decoder::flush_codec()
{
    boost::lock_guard<boost::recursive_mutex > lock_(GLOBAL_CONFIG()->_codec_mutex);

    if (this->avctx_opts)
    {
        avcodec_flush_buffers(this->avctx_opts);
    }
}
