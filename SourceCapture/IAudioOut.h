/*!
 * \class IAudioOut
 *
 * \brief 
 * 音频播放抽象接口
 * \author Administrator
 * \date 二月 2017
 */
#pragma once
#include "global_config.h"

class IAudioOut
{
public:
    virtual ~IAudioOut() {}

    //获取声音输出设备个数
    //@param dev_name_：设备名称二维数组,如果不需要可以为NULL
    //@param max_num_：数组最多存储多少个，如果为0，则不存储
    //@return 音频输出设备个数
    virtual int     GetAudioOutDevice(char dev_name_[][256], const int max_num_) = 0;
    //初始化音频播放参数
    virtual int     InitAudioOutDev(int channel_num_ = 2, int sample_rate_ = 44100,
        int sample_bits_ = 16, int nDevID = -1) = 0;
    //反初始化音频播放设备
    virtual void    UninitAudioOutDev() = 0;
    //音频线程（专门做音频解码播放）通过重复调用此接口，来实现解码播放
    virtual int     PlayAudio(void *data_, int size_) = 0;
    //获取缓冲中未播放PCM字节数
    virtual int     GetBufferSize(){ return 0; }
};

