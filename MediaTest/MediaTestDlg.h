// MediaTestDlg.h : 头文件
//

#pragma once

#include "afxwin.h"
#include <list>
#include "dib.h"
#include "afxcmn.h"
#include "LoadLibrary_Interface.h"

//extern "C"
//{
#include "SDL.h"
//};

//SDL2.lib; SDL2main.lib;
#pragma comment(lib, "SDL2.lib")

using namespace std;

class CDShowVideoDevice;
class CRecordInterface;
class CPlayerInterface;

// CMediaTestDlg 对话框
class CMediaTestDlg : public CDialog, public ILiveEvent
{
// 构造
public:
	CMediaTestDlg(CWnd* pParent = NULL);	// 标准构造函数
	~CMediaTestDlg();

// 对话框数据
	enum { IDD = IDD_MEDIATEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
    afx_msg void            OnTimer(UINT_PTR nIDEvent);
    DECLARE_MESSAGE_MAP()


protected:
    CRecordInterface*   _pRecInterface;
    CPlayerInterface*   _pPlayInterface;
    CDib                _dib;

public:
	afx_msg void OnBnClickedOk();
    afx_msg void OnDestroy();
    CStatic _video_show;
    afx_msg void OnBnClickedRecBtn();
    afx_msg void OnBnClickedPlayBtn();

    virtual void on_login_resualt(const int& ret) {};
    virtual void on_logout_resualt(const int& ret) {};
    virtual void on_onmic_resualt(const int& ret) {};
    virtual void on_offmic_resualt(const int& ret) {};
    virtual void on_beginlive_resualt(const int& ret){};
    virtual void on_endlive_resualt(const int& ret){};

    virtual void on_host_onmic(const unsigned int& uid_);
    virtual void on_host_offmic(const unsigned int& uid_);

    virtual void on_video_preview_cb(const int video_show_num, const unsigned char* rgb24_data_,
        const int pic_width_, const int pic_height_);
    virtual void on_video_play_cb(const unsigned char* rgb24_data_, 
        const int pic_width_, const int pic_height_);

    virtual void on_video_dev_lost(const char* dev_name_, const int len_){};

    int          init_sdl();

protected:
    CEdit m_nRoomIdEdit;
    // 音量调节
    CSliderCtrl m_VolumeSlider;
    //list <CPlayerInterface *> _player_list;
public:
    afx_msg void OnBnClickedButtonMoni();
    afx_msg void OnBnClickedBtnCap();
    afx_msg void OnBnClickedBtnBufferNum();
    afx_msg void OnBnClickedBtnPlayerNum();
    afx_msg void OnBnClickedBtnSetMaxPlayer();
    CEdit m_BufferNum;
    CEdit m_CurPlayerNum;
    CEdit m_MaxPlayerNum;
};
