//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 MediaTest.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MEDIATEST_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDC_VIDEO                       1000
#define IDC_REC_BTN                     1001
#define IDC_PLAY_BTN                    1002
#define IDC_PLAYER_VIDEO                1003
#define IDC_VIDEO2                      1004
#define IDC_ROOM_ID                     1005
#define IDC_VOLUME_SLIDER               1006
#define IDC_BUTTON_MONI                 1007
#define IDC_BTN_CAP                     1008
#define IDC_BUFFER_NUM                  1009
#define IDC_BTN_BUFFER_NUM              1010
#define IDC_CUR_PLAYER_NUM              1011
#define IDC_EDIT3                       1012
#define IDC_MAX_PLAYER_NUM              1012
#define IDC_BTN_PLAYER_NUM              1013
#define IDC_BTN_SET_MAX_PLAYER          1014
#define IDC_SEV_IP                      1015
#define IDC_HTTPPORT                    1017
#define IDC_EDIT4                       1018
#define IDC_UDPPORT                     1018

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1019
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
