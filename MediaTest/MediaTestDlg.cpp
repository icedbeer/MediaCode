// MediaTestDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MediaTest.h"
#include "MediaTestDlg.h"
#include <mmdeviceapi.h>
//#include <afxcoll.h>
//#include "dshow_video.h"
#include "player_interface.h"
#include "record_interface.h"

#ifdef _DEBUG
#pragma comment(lib, "CapturePlayD.lib")
//#pragma comment(lib, "libprotobufD.lib")
#else
#pragma comment(lib, "CapturePlay.lib")
//#pragma comment(lib, "libprotobuf.lib")
#endif // _DEBUG


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define EXIT_ON_ERROR(hres)  if (FAILED(hres)) { goto Exit; }
#define SAFE_RELEASE(punk)  \
    if ((punk) != NULL)  \
{ (punk)->Release(); (punk) = NULL; }

#define SAFE_RELEASE(punk)  \
    if ((punk) != NULL)  \
{ (punk)->Release(); (punk) = NULL; }
const CLSID CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);
const IID IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);


class CAboutDlg : public CDialog
{
public:
	CAboutDlg();
 
// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMediaTestDlg 对话框



CMediaTestDlg::CMediaTestDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMediaTestDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    _pRecInterface = NULL;
    _pPlayInterface = NULL;
}

CMediaTestDlg::~CMediaTestDlg()
{
}

void CMediaTestDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_VIDEO, _video_show);
    DDX_Control(pDX, IDC_ROOM_ID, m_nRoomIdEdit);
    DDX_Control(pDX, IDC_VOLUME_SLIDER, m_VolumeSlider);
    DDX_Control(pDX, IDC_BUFFER_NUM, m_BufferNum);
    DDX_Control(pDX, IDC_CUR_PLAYER_NUM, m_CurPlayerNum);
    DDX_Control(pDX, IDC_MAX_PLAYER_NUM, m_MaxPlayerNum);
}

void CMediaTestDlg::OnTimer(UINT_PTR nIDEvent)
{
    OnBnClickedPlayBtn();
    CDialog::OnTimer(nIDEvent);
}

BEGIN_MESSAGE_MAP(CMediaTestDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
    ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CMediaTestDlg::OnBnClickedOk)
    ON_WM_DESTROY()
    ON_WM_TIMER(WM_TIMER, &CMediaTestDlg::OnTimer)
    ON_BN_CLICKED(IDC_REC_BTN, &CMediaTestDlg::OnBnClickedRecBtn)
    ON_BN_CLICKED(IDC_PLAY_BTN, &CMediaTestDlg::OnBnClickedPlayBtn)
    ON_BN_CLICKED(IDC_BUTTON_MONI, &CMediaTestDlg::OnBnClickedButtonMoni)
    ON_BN_CLICKED(IDC_BTN_CAP, &CMediaTestDlg::OnBnClickedBtnCap)
    ON_BN_CLICKED(IDC_BTN_BUFFER_NUM, &CMediaTestDlg::OnBnClickedBtnBufferNum)
    ON_BN_CLICKED(IDC_BTN_PLAYER_NUM, &CMediaTestDlg::OnBnClickedBtnPlayerNum)
    ON_BN_CLICKED(IDC_BTN_SET_MAX_PLAYER, &CMediaTestDlg::OnBnClickedBtnSetMaxPlayer)
END_MESSAGE_MAP()


// CMediaTestDlg 消息处理程序

BOOL CMediaTestDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{ 
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

    m_VolumeSlider.SetRange(0, 100);

    HWND hwn_ = _video_show.GetSafeHwnd();
    CRect rc;
    _video_show.GetWindowRect(&rc);
    _video_show.MoveWindow(rc.left, rc.top, rc.left + 320, rc.top + 240);
    
    //GOOGLE_PROTOBUF_VERIFY_VERSION;

    //SetTimer(100, 5000, 0);
    //初始化Server地址信息
    GetDlgItem(IDC_SEV_IP)->SetWindowText("127.0.0.1");
    GetDlgItem(IDC_HTTPPORT)->SetWindowText("7910");
    GetDlgItem(IDC_UDPPORT)->SetWindowText("6688");
    GetDlgItem(IDC_ROOM_ID)->SetWindowText("666");

    init_sdl();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMediaTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMediaTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint(); 
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMediaTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
 

void CMediaTestDlg::OnBnClickedOk()
{
    //OnOK();
}

void CMediaTestDlg::OnDestroy()
{
    CDialog::OnDestroy();
    // TODO: 在此处添加消息处理程序代码
    if (_pRecInterface)
    {
        delete _pRecInterface;
    }
    if (_pPlayInterface)
    {
        delete _pPlayInterface;
    }
    //list<CPlayerInterface *>::iterator itor = _player_list.begin();
    //while(itor != _player_list.end())
    //{
    //    delete *itor;
    //    itor++;
    //}
    //_player_list.clear();
    //google::protobuf::ShutdownProtobufLibrary();
}

void CMediaTestDlg::OnBnClickedRecBtn()
{
    CString ip_str, room_str, http_str, udp_str;
    GetDlgItem(IDC_SEV_IP)->GetWindowText(ip_str);
    GetDlgItem(IDC_ROOM_ID)->GetWindowTextA(room_str);
    GetDlgItem(IDC_HTTPPORT)->GetWindowTextA(http_str);
    GetDlgItem(IDC_UDPPORT)->GetWindowTextA(udp_str);

    unsigned int room_id = atoi(room_str.GetBuffer());
    if (room_id == 0)
    {
        room_id = 666;
    }
    if (ip_str.IsEmpty())
    {
        ip_str = "127.0.0.1";
    }

    // TODO: 在此添加控件通知处理程序代码
    if (!_pRecInterface)
    {
        _pRecInterface = new CRecordInterface(this);
        _pRecInterface->preview_video(0, 0);
        _pRecInterface->preview_audio(0);
        //_pRecInterface->preview_video(1, 1);
        //_pRecInterface->set_video_show_num(1);
        unsigned long ip_int = inet_addr(ip_str.GetBuffer());
        _pRecInterface->add_sev_ip_info(ip_int, atoi(http_str.GetBuffer()), 0, 0);
        _pRecInterface->set_video_show_num(0);

        srand( (unsigned)time( NULL ) );

        if (_pRecInterface->Login(777777 + rand(), room_id, ip_str.GetBuffer(), atoi(http_str.GetBuffer())) != 0)
         //if(_pRecInterface->Login(777777 + rand(), room_id, "123.207.119.44", strlen("123.207.119.44"), 1234) != 0)
         {
             MessageBox( _T("login server failed."), 0, 0);
             return;
         }
    } else
    {
        //_pRecInterface->Logout(room_id);
        delete _pRecInterface;
        _pRecInterface = NULL;
    }
}

bool g_flag = true;
void CMediaTestDlg::OnBnClickedPlayBtn()
{
    // TODO: 在此添加控件通知处理程序代码
    CString ip_str, room_str, http_str, udp_str;
    GetDlgItem(IDC_SEV_IP)->GetWindowText(ip_str);
    GetDlgItem(IDC_ROOM_ID)->GetWindowTextA(room_str);
    GetDlgItem(IDC_HTTPPORT)->GetWindowTextA(http_str);
    GetDlgItem(IDC_UDPPORT)->GetWindowTextA(udp_str);
    unsigned int room_id = atoi(room_str.GetBuffer());
    if (room_id == 0)
    {
        room_id = 666;
    }
    if (ip_str.IsEmpty())
    {
        ip_str = "127.0.0.1";
    }

    if (!_pPlayInterface)
    {
        _pPlayInterface = new CPlayerInterface(this);
        unsigned long ip_num = inet_addr(ip_str.GetBuffer());
        ip_num = htonl(ip_num);
        _pPlayInterface->add_sev_ip_info(ip_num, atoi(udp_str.GetBuffer()), 0, 0);
    }
    if (g_flag)
    {
        g_flag = false;
        srand((unsigned)time(NULL));
        if (_pPlayInterface->Login(888888 + rand(), room_id, _eUdp) != 0)
        {
            MessageBox(_T("login server failed."), 0, 0);
            return;
        }
    } else
    {
        _pPlayInterface->Logout();
        delete _pPlayInterface;
        _pPlayInterface = NULL;
        g_flag = true;
    }
}

void CMediaTestDlg::on_host_onmic(const unsigned int& uid_)
{
    //CString str;
    //str.Format(_T("%lu onmic."), uid_);
    //MessageBox(str.GetBuffer());
}

void CMediaTestDlg::on_host_offmic(const unsigned int& uid_)
{
    //CString str;
    //str.Format(_T("%lu offmic."), uid_);
    //MessageBox(str.GetBuffer());
}

void CMediaTestDlg::on_video_play_cb(const unsigned char* rgb24_data_, 
                           const int pic_width_, const int pic_height_)
{
    if (_dib.get_width() != pic_width_ || _dib.get_height() != pic_height_)
    {
        _dib.destroy();
        _dib.create(pic_width_, pic_height_, 24);
    }
    _dib.set_dib_bits((void *)rgb24_data_, pic_width_ * pic_height_ * 3);


    CRect rc;
    GetDlgItem(IDC_PLAYER_VIDEO)->GetClientRect(&rc);
    HWND hwnd = GetDlgItem(IDC_PLAYER_VIDEO)->GetSafeHwnd();
    HDC _video_paly_hdc = ::GetDC(hwnd);


    _dib.stretch_blt(_video_paly_hdc, 0, 0, rc.Width(), rc.Height(), 0, 0, pic_width_, pic_height_);
    ::ReleaseDC(hwnd, _video_paly_hdc); 
}

//set '1' to choose a type of file to play
#define LOAD_BGRA    0
#define LOAD_RGB24   0
#define LOAD_BGR24   0
#define LOAD_YUV420P 1

#define HAS_BORDER	 0

//Bit per Pixel
#if LOAD_BGRA
const int bpp = 32;
#elif LOAD_RGB24|LOAD_BGR24
const int bpp = 24;
#elif LOAD_YUV420P
const int bpp = 12;
#endif

const int screen_w = 500, screen_h = 500;
const int pixel_w = 640, pixel_h = 480;

unsigned char buffer[pixel_w*pixel_h*bpp / 8];
//BPP=32
unsigned char buffer_convert[pixel_w*pixel_h * 4];

//Refresh Event
#define REFRESH_EVENT  (SDL_USEREVENT + 1)

int thread_exit = 0;

int refresh_video(void *opaque){
    while (thread_exit == 0) {
        SDL_Event event;
        event.type = REFRESH_EVENT;
        SDL_PushEvent(&event);
        SDL_Delay(40);
    }
    return 0;
}

SDL_Window *screen;
SDL_Renderer* sdlRenderer;
SDL_Texture* sdlTexture;
SDL_Rect sdlRect;

int CMediaTestDlg::init_sdl()
{
    if (SDL_Init(SDL_INIT_VIDEO)) {
        return -1;
    }

    //SDL 2.0 Support for multiple windows
    screen = SDL_CreateWindowFrom(GetDlgItem(IDC_VIDEO)->GetSafeHwnd());
    if (!screen) {
        printf("SDL: could not create window - exiting:%s\n", SDL_GetError());
        return -1;
    }
    sdlRenderer = SDL_CreateRenderer(screen, -1, 0);

    Uint32 pixformat = 0;
#if LOAD_BGRA
    //Note: ARGB8888 in "Little Endian" system stores as B|G|R|A
    pixformat = SDL_PIXELFORMAT_ARGB8888;
#elif LOAD_RGB24
    pixformat = SDL_PIXELFORMAT_RGB888;
#elif LOAD_BGR24
    pixformat = SDL_PIXELFORMAT_BGR888;
#elif LOAD_YUV420P
    //IYUV: Y + U + V  (3 planes)
    //YV12: Y + V + U  (3 planes)
    pixformat = SDL_PIXELFORMAT_IYUV;
#endif

    sdlTexture = SDL_CreateTexture(sdlRenderer, pixformat, SDL_TEXTUREACCESS_STREAMING, pixel_w, pixel_h);

    int border = 0;
    //Make picture smaller, Add a "border"
#if HAS_BORDER
    border = 30;
#endif
    RECT wnd_rc;
    GetDlgItem(IDC_VIDEO)->GetClientRect(&wnd_rc);

    //SDL_Rect sdlRect;
    sdlRect.x = 0 + border;
    sdlRect.y = 0 + border;
    sdlRect.w = /*screen_w - border * 2*/wnd_rc.right;
    sdlRect.h = /*screen_h - border * 2*/wnd_rc.bottom;


    //SDL_Thread *refresh_thread = SDL_CreateThread(refresh_video, NULL, NULL);
//    SDL_Event event;
//    while (1){
//        //Wait
//        SDL_WaitEvent(&event);
//        if (event.type == REFRESH_EVENT){
//            if (fread(buffer, 1, pixel_w*pixel_h*bpp / 8, fp) != pixel_w*pixel_h*bpp / 8){
//                // Loop
//                fseek(fp, 0, SEEK_SET);
//                fread(buffer, 1, pixel_w*pixel_h*bpp / 8, fp);
//            }
//
//#if LOAD_BGRA
//            //We don't need to change Endian
//            //Because input BGRA pixel data(B|G|R|A) is same as ARGB8888 in Little Endian (B|G|R|A)
//            SDL_UpdateTexture(sdlTexture, NULL, buffer, pixel_w * 4);
//#elif LOAD_RGB24|LOAD_BGR24
//            //change 24bit to 32 bit
//            //and in Windows we need to change Endian
//            CONVERT_24to32(buffer, buffer_convert, pixel_w, pixel_h);
//            SDL_UpdateTexture(sdlTexture, NULL, buffer_convert, pixel_w * 4);
//#elif LOAD_YUV420P
//            SDL_UpdateTexture(sdlTexture, NULL, buffer, pixel_w);
//#endif
//
//            SDL_RenderClear(sdlRenderer);
//            SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, &sdlRect);
//            SDL_RenderPresent(sdlRenderer);
//            //Delay 40ms
//            SDL_Delay(40);
//
//        } else if (event.type == SDL_QUIT){
//            break;
//        }
//    }

}

void CMediaTestDlg::on_video_preview_cb(const int video_show_num, const unsigned char* rgb24_data_,
    const int pic_width_, const int pic_height_)
{
#if LOAD_BGRA
    //We don't need to change Endian
    //Because input BGRA pixel data(B|G|R|A) is same as ARGB8888 in Little Endian (B|G|R|A)
    SDL_UpdateTexture(sdlTexture, NULL, buffer, pixel_w * 4);
#elif LOAD_RGB24|LOAD_BGR24
    //change 24bit to 32 bit
    //and in Windows we need to change Endian
    CONVERT_24to32(buffer, buffer_convert, pixel_w, pixel_h);
    SDL_UpdateTexture(sdlTexture, NULL, buffer_convert, pixel_w * 4);
#elif LOAD_YUV420P
    SDL_UpdateTexture(sdlTexture, NULL, rgb24_data_, pixel_w);
#endif

    SDL_RenderClear(sdlRenderer);
    SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, &sdlRect);
    SDL_RenderPresent(sdlRenderer);

    //if (0 == video_show_num)
    //{
    //    if (_dib.get_width() != pic_width_ || _dib.get_height() != pic_height_)
    //    {
    //        _dib.destroy();
    //        _dib.create(pic_width_, pic_height_, 24);
    //    }
    //    _dib.set_dib_bits((void *)rgb24_data_, pic_width_ * pic_height_ * 3);


    //    CRect rc;
    //    GetDlgItem(IDC_VIDEO)->GetClientRect(&rc);
    //    HWND hwnd = GetDlgItem(IDC_VIDEO)->GetSafeHwnd();
    //    HDC _video_paly_hdc = ::GetDC(hwnd);


    //    _dib.stretch_blt(_video_paly_hdc, 0, 0, rc.Width(), rc.Height(), 0, 0, pic_width_, pic_height_);
    //    ::ReleaseDC(hwnd, _video_paly_hdc);
    //}else if (1 == video_show_num)
    //{
    //    if (_dib.get_width() != pic_width_ || _dib.get_height() != pic_height_)
    //    {
    //        _dib.destroy();
    //        _dib.create(pic_width_, pic_height_, 24);
    //    }
    //    _dib.set_dib_bits((void *)rgb24_data_, pic_width_ * pic_height_ * 3);


    //    CRect rc;
    //    GetDlgItem(IDC_VIDEO2)->GetClientRect(&rc);
    //    HWND hwnd = GetDlgItem(IDC_VIDEO2)->GetSafeHwnd();
    //    HDC _video_paly_hdc = ::GetDC(hwnd);


    //    _dib.stretch_blt(_video_paly_hdc, 0, 0, rc.Width(), rc.Height(), 0, 0, pic_width_, pic_height_);
    //    ::ReleaseDC(hwnd, _video_paly_hdc);
    //}
}

void CMediaTestDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    // TODO: Add your message handler code here and/or call default

    if(pScrollBar != NULL )
    {

        // 强制转换成CSliderCtrl
        CSliderCtrl* pSlider = (CSliderCtrl*)pScrollBar;

        // 根据CSliderCtrl ID 来判断是哪一个CSliderCtrl
        if(pSlider->GetDlgCtrlID() == IDC_VOLUME_SLIDER)
        {
            long v = pSlider->GetPos();
            if (_pPlayInterface)
            {
                _pPlayInterface->set_volume(v);
            }
        }
    }
}



void CMediaTestDlg::OnBnClickedButtonMoni()
{
    // TODO: 在此添加控件通知处理程序代码
    //CString str;
    //m_nRoomIdEdit.GetWindowText(str);
    //sscanf(str.GetBuffer(), "%d", &room_id);
    //str.ReleaseBuffer();

    //srand( (unsigned)time( NULL ) );
    //int uid_ = 300000;

    //for (int i(0); i < 150; i++)
    //{
    //    CPlayerInterface* pTmp = new CPlayerInterface(NULL);
    //    if (pTmp)
    //    {
    //        pTmp = new CPlayerInterface(this);
    //        char *ip_str = "20.0.0.146";
    //        unsigned long ip = inet_addr(ip_str);
    //        ip = htonl(ip);
    //        pTmp->add_sev_ip_info(ip, 7910, 0, 0);
    //        if (pTmp->Login(uid_ + i, room_id) != 0)
    //        {
    //            MessageBox( _T("login server failed."), 0, 0);
    //            return;
    //        }
    //    }
    //    _player_list.push_back(pTmp);
    //}
}

void CMediaTestDlg::OnBnClickedBtnCap()
{
    // TODO: 在此添加控件通知处理程序代码
    //if (_pPlayInterface)
    //{
    //    _pPlayInterface->save_bmp("c:/bmp.bmp", 10);
    //}
}

void CMediaTestDlg::OnBnClickedBtnBufferNum()
{
    // TODO: 在此添加控件通知处理程序代码
}

void CMediaTestDlg::OnBnClickedBtnPlayerNum()
{
    // TODO: 在此添加控件通知处理程序代码
}

void CMediaTestDlg::OnBnClickedBtnSetMaxPlayer()
{
    // TODO: 在此添加控件通知处理程序代码
}
