#pragma once

#ifdef __GNUC__
#include <stdint.h>
#include <inttypes.h>
#endif

#if defined(WIN32) || defined(_WIN32)
#pragma warning(disable:4996)
#pragma warning(disable:4005)

#ifndef _STDINT
typedef signed char				int8_t;
#endif
#ifndef _STDINT
typedef short				int16_t;
#endif
#ifndef _STDINT
typedef int					int32_t;
#endif
#ifndef _STDINT
typedef long long			int64_t;
#endif

#ifndef _STDINT
typedef unsigned char		uint8_t;
#endif
#ifndef _STDINT
typedef unsigned short		uint16_t;
#endif
#ifndef _STDINT
typedef unsigned int		uint32_t;
#endif
#ifndef _STDINT
typedef unsigned long long	uint64_t;
#endif

#elif defined(_WIN64)
#ifndef _STDINT
typedef char				int8_t;
#endif
#ifndef _STDINT
typedef short				int16_t;
#endif
#ifndef _STDINT
typedef int					int32_t;
#endif
#ifndef _STDINT
typedef long				int64_t;
#endif

#ifndef _STDINT
typedef unsigned char		uint8_t;
#endif
#ifndef _STDINT
typedef unsigned short		uint16_t;
#endif
#ifndef _STDINT
typedef unsigned int		uint32_t;
#endif
#ifndef _STDINT
typedef unsigned long		uint64_t;
#endif
#endif

#ifndef core_max
#define core_max(a, b)		((a) > (b) ? (a) : (b))
#endif

#ifndef core_min
#define core_min(a, b)		((a) < (b) ? (a) : (b))
#endif

#ifndef core_abs
#define core_abs(a, b)		((a) > (b) ? (a - b) : (b - a))
#endif

#ifndef NULL
#define NULL	0
#endif

#ifndef ROOM_ID
typedef unsigned int ROOM_ID;
#endif //ROOM_ID
#ifndef USER_ID
typedef unsigned int USER_ID;
#endif // USER_ID
#ifndef SEQ_ID
typedef unsigned int SEQ_ID;
#endif // SEQ_ID

