/*!
 * \file IShareMedia.h
 *
 * \author Administrator
 * \date 一月 2017
 *
 * 多种传输协议之间数据共享接口
 */
#pragma once

class IShareMedia
{
public:
    virtual void    add_shared_media_data(unsigned long long& room_id_, unsigned char* data_, const int& len_) = 0;
    virtual void    add_shared_room(unsigned long long room_id_) = 0;
    virtual void    del_shared_room(unsigned long long room_id_) = 0;
    virtual void    reinit_room(unsigned long long room_id_) = 0;
};

