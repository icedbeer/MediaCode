/*************************************************************************************
*filename:	base_packet.h
*
*to do:		定义网络协议基类，如果需要定义一个协议，只需要继承此类，并实现PACK和UNPACK
*Create on: 2012-05
*Author:	zerok
*check list:
*************************************************************************************/
#ifndef __BASE_PACKET_H
#define __BASE_PACKET_H

#include "base_namespace.h"
#include "base_typedef.h"
#include "base_bin_stream.h"

#include <iostream>

typedef uint32_t			USER_ID_TYPE;
typedef uint32_t			ROOM_ID_TYPE;

BASE_NAMESPACE_BEGIN_DECL

class CBasePacket
{
public:
	CBasePacket()
	{
        data_len = 0;
	};

	virtual ~CBasePacket()
	{
	};

	friend BinStream& operator<<(BinStream& strm, CBasePacket& packet)
	{
		packet.Pack(strm);
		return strm;
	};

	friend BinStream& operator>>(BinStream& strm, CBasePacket& packet)
	{	
		packet.UnPack(strm);
		return strm;
	};

	friend std::ostream& operator<<(std::ostream& os, CBasePacket& packet)
	{
		packet.Print(os);
		return os;
	}

	virtual void release_self()
	{
		delete this;
	};
    virtual uint32_t GetPacketSize()
    {
        return 4;
    }

	virtual void Pack(BinStream& strm)  = 0;
	virtual void UnPack(BinStream& strm) = 0;
protected:
    virtual void Print(std::ostream& /*os*/)const {};

public:
    uint8_t			packet_type;
    uint8_t			payload_type;
    ROOM_ID_TYPE    room_id;
    USER_ID_TYPE	user_id;
    uint16_t        data_len;
};

BASE_NAMESPACE_END_DECL
#endif
/************************************************************************************/
