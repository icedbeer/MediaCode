#pragma once

#define     DETECT_LINK_TIME            5  //n S detect all links once
#define     NODE_DEAD_TIME              30  //30S node dead

#define     HTTP_PROTOCAL_CODE_ME       200 //协议包回应码 to self
#define     HTTP_PROTOCAL_CODE_OTHER    201 //协议包回应码 to others
#define     HTTP_MEDIA_CODE_ME          220 //媒体包回应码 to self
#define     HTTP_MEDIA_CODE_OTHER       221 //媒体包回应码 to others
#define     HTTP_HEATBEAT_CODE          230 //心跳GET回应码 has data
#define     HTTP_HEATBEAT_CODE_NULL     231 //心跳GET回应码 no data

#define     HTTP_UNKNOWN_COMMAND        240 //不识别的http请求命令
#define     HTTP_UNKNOWN_PROTOCAL       241 //不识别的协议码
#define     HTTP_ERROR                  250 //处理错误

#define     HTTP_HOST_ONMIC             281 //主播上麦
#define     HTTP_HOST_OFFMIC            282 //主播下麦

//////////////////////////////////////////////////////////////////////////error code
#define     HTTP_ERROR_401              401 //拒绝登陆，可能已经登陆，或者黑名单？
#define     HTTP_ERROR_402              402 //拒绝登出，本来就不在线
#define     HTTP_ERROR_403              403 //已经有人在麦上了，拒绝上麦，或者黑名单？
#define     HTTP_ERROR_404              404 //拒绝下麦，压根不在麦上
#define     HTTP_ERROR_405              405 //已经开始直播，不需要再开始了
#define     HTTP_ERROR_406              406 //拒绝停播，压根不在直播的状态

