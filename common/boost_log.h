/*!
* \file   Boostlog.hpp
* \author cgb
*
* 封装boost log模块，多线程（同步）写入日志；
*/

#pragma once
#include <string>
#include <iostream>
#ifdef _VS2013
#include <stdint.h>
#endif // _VS2013
#include "boost/log/trivial.hpp"
#include "boost/filesystem.hpp"
#include "boost/log/sources/logger.hpp"
#include "boost/log/sources/record_ostream.hpp"
#include "boost/log/sources/global_logger_storage.hpp"
#include "boost/log/utility/setup/file.hpp"
#include "boost/log/utility/setup/console.hpp"
#include "boost/log/utility/setup/common_attributes.hpp"
#include "boost/log/sinks/text_ostream_backend.hpp"
#include "boost/log/attributes/named_scope.hpp"
#include "boost/log/expressions.hpp"
#include "boost/log/support/date_time.hpp"
#include "boost/log/detail/format.hpp"
#include "boost/log/detail/thread_id.hpp"
#include "boost/log/core/core.hpp"

#define _CSTDIO_
#define _CSTRING_
#define _CWCHAR_

using std::string;

namespace boost_log = boost::log;
namespace boost_sources = boost::log::sources;
namespace boost_keywords = boost::log::keywords;
namespace boost_sinks = boost::log::sinks;
namespace boost_expressions = boost::log::expressions;
namespace boost_attributes = boost::log::attributes;
namespace boost_trivial = boost::log::trivial;

/*
* 自定义日志等级
*/
namespace my_log_name_space{
    enum sign_severity_level {
        trace = 0,
        debug,
        info,
        warn,
        error,
        fatal,
        callback
    };
};
using namespace my_log_name_space;

#define BOOST_TRACE\
    BOOST_LOG_SEV((BoostLog::s_slg), (my_log_name_space::trace)) << __FILE__ << "[" << __LINE__ << "]:"
#define BOOST_DEBUG\
    BOOST_LOG_SEV((BoostLog::s_slg), (my_log_name_space::debug)) << __FILE__ << "[" << __LINE__ << "]:"
#define BOOST_INFO\
    BOOST_LOG_SEV((BoostLog::s_slg), (my_log_name_space::info)) << __FILE__ << "[" << __LINE__ << "]:"
#define BOOST_WARNING\
    BOOST_LOG_SEV((BoostLog::s_slg), (my_log_name_space::warn)) << __FILE__ << "[" << __LINE__ << "]:"
#define BOOST_ERROR\
    BOOST_LOG_SEV((BoostLog::s_slg), (my_log_name_space::error)) << __FILE__ << "[" << __LINE__ << "]:"
#define BOOST_FATAL\
    BOOST_LOG_SEV((BoostLog::s_slg), (my_log_name_space::fatal)) << __FILE__ << "[" << __LINE__ << "]:"
#define BOOST_CB\
    BOOST_LOG_SEV((BoostLog::s_slg), (my_log_name_space::callback)) << __FILE__ << "[" << __LINE__ << "]:"

#define RUDP_LOG(level, ...) \
    BoostLog::Log(level, ...)


// The formatting logic for the severity level
template< typename CharT, typename TraitsT >
inline std::basic_ostream< CharT, TraitsT >& operator<< (
	std::basic_ostream< CharT, TraitsT >& strm, sign_severity_level lvl)
{
	static const char* const str[] =
	{
		"trace",
		"debug",
		"info",
		"warn",
		"error",
		"fatal",
		"callback"
	};
	if (static_cast< std::size_t >(lvl) < (sizeof(str) / sizeof(*str)))
		strm << str[lvl];
	else
		strm << static_cast< int >(lvl);
	return strm;
}

/*
自定义日志类，在使用之前必须先调用 init方法；
写日志可以调用如下方法：
LOG_DEBUG<<"test string";
也可以用boost 中的宏  BOOST_LOG_TRIVIAL(info)<<"test msg";
*/
class BoostLog
{
public:
	BoostLog(){};
	~BoostLog(void){ Uninit();}

	/*! 在使用此类之前必须先调用此函数进行初始化工作；
	* Init_Debug 会添加控制台输出和日志文件输出；
	* param@dir : 日志文件存放目录
	* param@fileName : 日志文件名前缀
	* param@level : 日志过滤等级
	* */
	static void Init_Debug(const string & dir, const string & fileName, sign_severity_level level)
	{
        if (_init_flag)
        {
            return;
        }else
        {
            _init_flag = true;
        }
		if (boost::filesystem::exists(dir) == false)
		{
			boost::filesystem::create_directories(dir);
		}
		AddConsoleSink();
		AddFileSink(dir, fileName);
		boost_log::add_common_attributes();
		//设置日志过滤等级
		SetLogLevel(level);
    }

	/*! 在使用此类之前必须先调用此函数进行初始化工作；
	* Init_Release 添加日志文件输出sink；
	* param@dir : 日志文件存放目录
	* param@fileName : 日志文件名前缀
	* param@level : 日志过滤等级
	*/
	static void Init_Release(const string & dir, const string & fileName, sign_severity_level level)
	{
        if (_init_flag)
        {
            return;
        }else
        {
            _init_flag = true;
        }

		if (boost::filesystem::exists(dir) == false)
		{
			boost::filesystem::create_directories(dir);
		}
		AddFileSink(dir, fileName);
		boost_log::add_common_attributes();
		//设置日志过滤等级
		SetLogLevel(level);
    }

	static void Uninit()
	{ 
        if (!_init_flag)
        {
            return;
        }else
        {
            _init_flag = false;
        }
		boost_log::core::get()->flush();
		boost_log::core::get()->remove_all_sinks(); 
    }

	static void SetLogLevel(sign_severity_level level)
	{ 
		boost_log::core::get()->set_filter(boost_expressions::attr< sign_severity_level >("Severity") >= level); 
	}

	static void Log(sign_severity_level level_, const char* fmt, ...)
	{
        char str_buf[1024] = {0};
		memset(str_buf, 0, sizeof(str_buf));
		va_list ap;
		va_start(ap, fmt);
		vsprintf_s(str_buf, fmt, ap);
		va_end(ap);
        switch (level_)
        {
        case trace:
            BOOST_TRACE << str_buf;
            break;
        case debug:
            BOOST_DEBUG << str_buf;
            break;
        case info:
            BOOST_INFO << str_buf;
            break;
        case warn:
            BOOST_WARNING << str_buf;
            break;
        case error:
            BOOST_ERROR << str_buf;
            break;
        case fatal:
            BOOST_FATAL << str_buf;
            break;
        case callback:
            BOOST_CB << str_buf;
            break;
        default:
            break;
        }
	}

protected:
	static void Start(){ boost_log::core::get()->set_logging_enabled(true); }
	static void Stop(){ boost_log::core::get()->set_logging_enabled(false); }

	//打印在控制台	
	static void AddConsoleSink()
	{
        boost_log::add_console_log()->set_formatter(
            boost_expressions::format("[%1%]<%2%>(%3%): %4%")
            % boost_expressions::format_date_time< boost::posix_time::ptime >("TimeStamp", "%H:%M:%S.%f")
            % boost_expressions::attr<sign_severity_level>("Severity")
            % boost_expressions::attr<boost_attributes::current_thread_id::value_type >("ThreadID")
            % boost_expressions::smessage
            );
	}
	//输出至日志文件
	static void AddFileSink(const string & dir, const string & fileName)
	{
		typedef boost_sinks::synchronous_sink< boost_sinks::text_file_backend > sink_t;
		boost::shared_ptr< sink_t > pFileSink = boost_log::add_file_log(
			boost_keywords::open_mode = std::ios::/*app*/trunc,
			boost_keywords::file_name = dir + "/" + fileName + "_%Y-%m-%d(%N).log",
			boost_keywords::rotation_size = 10 * 1024 * 1024,
			boost_keywords::time_based_rotation = boost_sinks::file::rotation_at_time_point(0, 0, 0), //间隔多长时间自动新建文件
			boost_keywords::format =
			(
			boost_expressions::stream
			<< "[" << boost_expressions::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %H:%M:%S.%f") << "]"
			<< "<" << boost_expressions::attr<sign_severity_level>("Severity") << ">"
			//<< "(" << boost_expressions::attr< boost::log::aux::thread::id >("ThreadID") << ")"
			<< boost_expressions::smessage
			)
			);

		pFileSink->locked_backend()->set_file_collector(boost_sinks::file::make_collector(
			boost_keywords::target = dir + "/oldlogs",                //目标文件夹
			boost_keywords::max_size = 500 * 1024 * 1024,        //所有日志加起来的最大大小,
			boost_keywords::min_free_space = 1000 * 1024 * 1024  //最低磁盘空间限制
			));

		pFileSink->locked_backend()->auto_flush(true);//使日志实时更新
		pFileSink->locked_backend()->scan_for_files();
	}

public:
	static boost_sources::severity_logger< sign_severity_level> s_slg;

protected:
    static bool _init_flag;
};
