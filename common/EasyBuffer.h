#pragma once
#include <new>
#include <string.h>
#include "base_typedef.h"

/*
* 固定大小的缓冲区
*/
class CEasyBuffer
{
protected:
	uint8_t *	m_pBuffer;
	int			m_BufferSize;
	int			m_UsedSize;
	uint8_t		m_SelfDelete;
public:
	CEasyBuffer(uint8_t* pBuff,int Size)
	{
		m_pBuffer=pBuff;
		m_BufferSize=Size;
		m_UsedSize=0;
		m_SelfDelete= 0;
	}
	CEasyBuffer( int Size)
	{
        try{
            m_pBuffer=		new uint8_t[Size];
            m_BufferSize=	Size;
            m_SelfDelete=	1;
            m_UsedSize=		0;
        }catch(std::bad_alloc&)
        {
            m_BufferSize=	0;
            m_SelfDelete=	0;
            m_UsedSize  =   0;  
            m_pBuffer   =   NULL;
        }
	}
	~CEasyBuffer(void)
	{
		if( m_SelfDelete )
		{
			delete[] m_pBuffer;
			m_pBuffer=NULL;
		}
		return;
	}
	inline int GetBufferSize() const
	{
		return m_BufferSize;
	}
	inline int &GetUsedSize()
	{
		return m_UsedSize;
	}
	inline bool SetUsedSize(int Size)
	{
		if(Size>=0&&Size<=m_BufferSize)
		{
			m_UsedSize=Size;
			return true;
		}
		return false;
	}
	inline int GetFreeSize() const
	{
		return m_BufferSize-m_UsedSize;
	}
	inline uint8_t* GetBuffer() const
	{
		return m_pBuffer;
	}

	inline uint8_t* GetFreeBuffer() const
	{
		return m_pBuffer+m_UsedSize;
	}

	inline bool PushBack(const void* pData,int Size)
	{
        if (NULL == pData || Size <= 0 || NULL == m_pBuffer)
        {
            return false;
        }
		if(m_UsedSize+Size<=m_BufferSize)
		{		
    		memcpy(m_pBuffer+m_UsedSize,pData,Size);
			m_UsedSize+=Size;
			return true;
		}else
        {
            uint8_t *tmp = new(std::nothrow) uint8_t[m_UsedSize + Size];
            if (NULL == tmp)
            {
                return false;
            }
            memcpy(tmp, m_pBuffer, m_UsedSize);
            memcpy(tmp + m_UsedSize, pData, Size);
            delete []m_pBuffer;
            m_BufferSize = m_UsedSize + Size;
            m_pBuffer = tmp;
            return true;
        }
	}
	
	inline bool PopFront(void* pData, int &Size)
	{
		if(Size<=m_UsedSize || NULL == m_pBuffer)
		{
			if(pData)
				memcpy(pData,m_pBuffer,Size);
			m_UsedSize-=Size;
			memmove(m_pBuffer,m_pBuffer+Size,m_UsedSize);		
			return true;
		}
		return false;
	}

    inline  void reset()
    {
        m_UsedSize = 0;
    }
};

